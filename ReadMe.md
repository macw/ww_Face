### 编辑器记得安装lombok插件

### ww_Face
![https://img.shields.io/badge/license-Apache%202.0-blue.svg?longCache=true&style=flat-square](https://img.shields.io/badge/license-Apache%202.0-blue.svg?longCache=true&style=flat-square)
![https://img.shields.io/badge/springboot-2.2.1-yellow.svg?style=flat-square](https://img.shields.io/badge/springboot-2.2.1-yellow.svg?style=flat-square)
![https://img.shields.io/badge/shiro-1.4.2-orange.svg?longCache=true&style=flat-square](https://img.shields.io/badge/shiro-1.4.2-orange.svg?longCache=true&style=flat-square)
![https://img.shields.io/badge/layui-2.5.5-brightgreen.svg?longCache=true&style=flat-square](https://img.shields.io/badge/layui-2.5.5-brightgreen.svg?longCache=true&style=flat-square)

ww_Face是一款简单的人脸识别在线考勤打卡项目，使用Spring Boot，Shiro和Layui构建。权限框架基于FEBS_Shiro脚手架进行环境搭建




### 演示地址

<a href="http://face.henaumcw.top">演示地址：http://face.henaumcw.top </a>

系统管理员：admin/1234qwer

学生角色测试账号：student001/123456

老师角色测试账号：teacher001/123456

### 系统模块
系统功能模块组成如下所示：


本系统采用百度AI开放平台的人脸识别接口进行开发，

根据人脸检测相似度评分大于80分既认定为同一用户。




```
├─系统管理
│  ├─用户管理
│  ├─角色管理
│  ├─菜单管理
│  └─部门管理
├─系统监控
│  ├─在线用户
│  ├─系统日志
│  ├─登录日志
│  ├─请求追踪
│  ├─系统信息
│  │  ├─JVM信息
│  │  ├─TOMCAT信息
│  │  └─服务器信息
├─任务调度
│  ├─定时任务
│  └─调度日志
├─代码生成
│  ├─生成配置
│  ├─代码生成
└─其他模块
   ├─FEBS组件
   │  ├─表单组件
   │  ├─表单组合
   │  ├─FEBS工具
   │  ├─系统图标
   │  └─其他组件
   ├─APEX图表
   ├─高德地图
   └─导入导出
```
### 系统特点

1. 前后端请求参数校验

2. 支持Excel导入导出

3. 前端页面布局多样化，主题多样化

4. 支持多数据源，代码生成

5. 多Tab页面，适合企业应用

6. 用户权限动态刷新

7. 浏览器兼容性好，页面支持PC，Pad和移动端。

8. 代码简单，结构清晰

### 技术选型

#### 后端
- [Spring Boot 2.2.1](http://spring.io/projects/spring-boot/)
- [Mybatis-Plus](https://mp.baomidou.com/guide/)
- [MySQL 5.7.x](https://dev.mysql.com/downloads/mysql/5.7.html#downloads),[Hikari](https://brettwooldridge.github.io/HikariCP/),[Redis](https://redis.io/)
- [Shiro 1.4.2](http://shiro.apache.org/)

#### 前端
- [Layui 2.5.5](https://www.layui.com/)
- [Nepadmin](https://gitee.com/june000/nep-admin)
- [formSelects 4.x 多选框](https://hnzzmsf.github.io/example/example_v4.html)
- [eleTree 树组件](https://layuiextend.hsianglee.cn/eletree/)
- [formSelect.js树形下拉](https://wujiawei0926.gitee.io/treeselect/docs/doc.html)
- [Apexcharts图表](https://apexcharts.com/)

### 系统截图

#### PC端
![screenshot](screenshot/pc_screenshot_1.jpg)
![screenshot](screenshot/pc_screenshot_2.jpg)
![screenshot](screenshot/pc_screenshot_3.jpg)
![screenshot](screenshot/pc_screenshot_4.jpg)
![screenshot](screenshot/pc_screenshot_5.jpg)
![screenshot](screenshot/pc_screenshot_6.jpg)

#### 手机
![screenshot](screenshot/mobile_screenshot_1.jpg)
![screenshot](screenshot/mobile_screenshot_2.jpg)
#### Pad
![screenshot](screenshot/pad_screenshot_1.jpg)
![screenshot](screenshot/pad_screenshot_2.jpg)
![screenshot](screenshot/pad_screenshot_3.jpg)
### 浏览器兼容
|[<img src="https://raw.github.com/alrra/browser-logos/master/src/archive/internet-explorer_9-11/internet-explorer_9-11_48x48.png" alt="Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>IE| [<img src="https://images.gitee.com/uploads/images/2020/0324/155504_ff6701a3_1668143.png" alt="Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Edge | [<img src="https://images.gitee.com/uploads/images/2020/0324/155508_6fc155ce_1668143.png" alt="Firefox" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Firefox | [<img src="https://images.gitee.com/uploads/images/2020/0324/155507_8d96220f_1668143.png" alt="Chrome" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Chrome | [<img src="https://images.gitee.com/uploads/images/2020/0324/155505_6faaf395_1668143.png" alt="Safari" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Safari |[<img src="https://raw.github.com/alrra/browser-logos/master/src/opera/opera_48x48.png" alt="Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Opera
| --------- | --------- | --------- | --------- | --------- |--------- |
|IE 10+| Edge| last 15 versions| last 15 versions| last 10 versions| last 15 versions
### 参与贡献
欢迎提交PR一起完善项目，

### 反馈交流

有问题请留言。

QQ交流群：795160208

