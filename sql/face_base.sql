/*
 Navicat Premium Data Transfer

 Source Server         : 39.105.167.160
 Source Server Type    : MySQL
 Source Server Version : 50729
 Source Host           : 39.105.167.160:3306
 Source Schema         : face_base

 Target Server Type    : MySQL
 Target Server Version : 50729
 File Encoding         : 65001

 Date: 06/05/2020 10:15:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_attendance
-- ----------------------------
DROP TABLE IF EXISTS `t_attendance`;
CREATE TABLE `t_attendance`  (
  `attendance_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '考勤、出席ID',
  `username` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '出席人用户名',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '出席人ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '出席时间',
  `dept_id` int(11) NULL DEFAULT NULL COMMENT '所属部门ID',
  `dept_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属部门name',
  `status` int(10) NULL DEFAULT 1 COMMENT '1：正常，0缺卡，3请假，4补卡。',
  `type` int(2) NULL DEFAULT NULL COMMENT '1上班，0下班',
  `lat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '维度',
  `lon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '经度',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '详细地址',
  PRIMARY KEY (`attendance_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '考勤出席表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_attendance
-- ----------------------------
INSERT INTO `t_attendance` VALUES (1, 'student001', 15, '2020-04-18 21:19:52', 13, '电子信息工程1301', 1, 1, NULL, NULL, NULL);
INSERT INTO `t_attendance` VALUES (2, 'student001', 15, '2020-04-21 21:23:20', 13, '电子信息工程1301', 1, 1, NULL, NULL, NULL);
INSERT INTO `t_attendance` VALUES (3, 'student001', 15, '2020-04-22 20:59:31', 13, '电子信息工程1301', 1, 1, '34.75661006', '113.64964385', '河南省-郑州市-二七区');
INSERT INTO `t_attendance` VALUES (4, 'student001', 15, '2020-04-22 21:29:16', 13, '电子信息工程1301', 1, 1, '34.75661006', '113.64964385', '河南省-郑州市');
INSERT INTO `t_attendance` VALUES (5, 'student001', 15, '2020-04-28 14:51:42', 13, '电子信息工程1301', 1, 1, '34.75661006', '113.64964385', '河南省-郑州市');
INSERT INTO `t_attendance` VALUES (6, 'student004', 142, '2020-04-28 16:40:15', NULL, NULL, 1, 1, '34.75661006', '113.64964385', '河南省-郑州市');
INSERT INTO `t_attendance` VALUES (7, 'student004', 142, '2020-04-28 16:40:33', NULL, NULL, 1, 1, '34.75661006', '113.64964385', '河南省-郑州市');
INSERT INTO `t_attendance` VALUES (8, 'student004', 142, '2020-04-28 16:41:32', NULL, NULL, 1, 1, '34.75661006', '113.64964385', '河南省-郑州市');
INSERT INTO `t_attendance` VALUES (9, 'student004', 142, '2020-04-29 09:34:27', NULL, NULL, 1, 0, '34.75661006', '113.64964385', '河南省-郑州市');
INSERT INTO `t_attendance` VALUES (10, 'student006', 146, '2020-05-05 00:05:07', 26, '注册用户', 1, 1, '34.75661006', '113.64964385', '河南省-郑州市');
INSERT INTO `t_attendance` VALUES (11, 'student006', 146, '2020-05-05 00:05:11', 26, '注册用户', 1, 1, '34.75661006', '113.64964385', '河南省-郑州市');
INSERT INTO `t_attendance` VALUES (12, 'student006', 146, '2020-05-05 00:07:03', 26, '注册用户', 1, 1, '34.75661006', '113.64964385', '河南省-郑州市');
INSERT INTO `t_attendance` VALUES (13, 'student006', 146, '2020-05-05 00:08:44', 26, '注册用户', 1, 1, '34.75661006', '113.64964385', '河南省-郑州市');
INSERT INTO `t_attendance` VALUES (14, 'student006', 146, '2020-05-05 00:08:56', 26, '注册用户', 1, 1, '34.75661006', '113.64964385', '河南省-郑州市');
INSERT INTO `t_attendance` VALUES (15, 'student007', 147, '2020-05-05 09:58:16', 26, '注册用户', 1, 1, '34.75661006', '113.64964385', '河南省-郑州市');
INSERT INTO `t_attendance` VALUES (16, 'student007', 147, '2020-05-05 09:58:28', 26, '注册用户', 1, 1, '34.75661006', '113.64964385', '河南省-郑州市');
INSERT INTO `t_attendance` VALUES (17, 'student007', 147, '2020-05-05 10:11:32', 26, '注册用户', 1, 1, '34.75661006', '113.64964385', '河南省-郑州市');
INSERT INTO `t_attendance` VALUES (18, 'student006', 146, '2020-05-05 15:06:39', 26, '注册用户', 1, 1, '34.75661006', '113.64964385', '河南省-郑州市');
INSERT INTO `t_attendance` VALUES (19, 'student006', 146, '2020-05-05 15:06:58', 26, '注册用户', 1, 1, '34.75661006', '113.64964385', '河南省-郑州市');

-- ----------------------------
-- Table structure for t_course
-- ----------------------------
DROP TABLE IF EXISTS `t_course`;
CREATE TABLE `t_course`  (
  `course_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '课程id',
  `course_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '课程名称',
  `course_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '课程内容',
  `dept_name_list` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '班级列表',
  `state` tinyint(3) UNSIGNED NULL DEFAULT 1 COMMENT '删除状态[0:停用, 1:未开始 20:执行中 30:已完成]',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '操作人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`course_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '课程表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_course
-- ----------------------------
INSERT INTO `t_course` VALUES (22, '数字电子实验课', '请输入&nbsp; &nbsp;数字电子在线实验室采用远程实验技术，通过网络访问远程端真实的数字电路实验板、电平开关、电平指示器及电子仪器仪表，完成基本的数字电子实验。该实验室可作为电子技术类课程实验教学，也可用于教师在课堂上通过投影为学生演示，还可作为开放实验室供学生使用。&nbsp; &nbsp; &nbsp; &nbsp; 数字电路可以分为组合逻辑电路和时序逻辑电路两大类，实验项目涵盖了主要的电路类型。通过实验室可培养学生的基本实验技能，加深对数字电子技术基本原理的理解，了解数字电路逻辑的组成及设计方法。', '外国语学院,农学院', 1, '', '2020-04-01 14:31:58', NULL);
INSERT INTO `t_course` VALUES (23, '电工技术实验课', '&nbsp;电工技术实验室，是面向电路等课程开设相关实验。电工技术在线实验室，通过网络技术结合智能仪表，实现远程实验功能。采用浏览器，可通过互联网访问远程端真实的仪器仪表及实验电路对象，完成基本的电路及电工技术相关实验。&nbsp;&nbsp;', '1522班,软件工程', 1, '', '2020-04-01 14:32:51', '2020-04-07 09:58:47');
INSERT INTO `t_course` VALUES (24, '传感器实验课', '请输入 课程内容 或描述&nbsp; &nbsp; &nbsp; 传感器实验是针对传感器原理、测试技术等课程的专业基础类实验。传感器在线实验室采用远程实验技术，通过网络访问控制远程端传感器及相关仪表，实时测量传感器变化带来的电气参数改变。实现与新临实验室相同的实验效果。', '经济学院,外国语学院', 1, '', '2020-04-01 14:33:20', NULL);
INSERT INTO `t_course` VALUES (29, '传感器实验课222', '请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述', '电子信息工程1301,计算机科学,1522班,软件工程,经济学院,外国语学院', 1, '', '2020-04-07 10:01:01', NULL);

-- ----------------------------
-- Table structure for t_course_dept
-- ----------------------------
DROP TABLE IF EXISTS `t_course_dept`;
CREATE TABLE `t_course_dept`  (
  `course_dept_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '课程班级表ID',
  `course_id` int(11) NULL DEFAULT NULL COMMENT '课程ID',
  `dept_id` int(11) NULL DEFAULT NULL COMMENT '班级表ID',
  PRIMARY KEY (`course_dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '课程班级表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_course_dept
-- ----------------------------
INSERT INTO `t_course_dept` VALUES (42, 29, 13);
INSERT INTO `t_course_dept` VALUES (43, 29, 18);
INSERT INTO `t_course_dept` VALUES (44, 29, 20);
INSERT INTO `t_course_dept` VALUES (45, 29, 19);
INSERT INTO `t_course_dept` VALUES (46, 29, 15);
INSERT INTO `t_course_dept` VALUES (47, 29, 16);

-- ----------------------------
-- Table structure for t_course_student
-- ----------------------------
DROP TABLE IF EXISTS `t_course_student`;
CREATE TABLE `t_course_student`  (
  `course_student_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '课程选择学生id',
  `course_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '课程id',
  `course_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '课程名称',
  `student_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '学生id',
  `student_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '学生名称',
  `example_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '已做实验报告内容',
  `example_flag` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '实验报告学生已做[0:未做, 1:已做]',
  `example_checked` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '实验报告是否批改[0:未批改, 1:已批改]',
  `example_score` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '实验报告评分',
  `example_comment` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '实验报告评语',
  `experiment_report` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '学生实验实验报告',
  `experiment_flag` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '实验报告学生已做[0:未做, 1:已做]',
  `experiment_checked` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '实验报告是否批改[0:未批改, 1:已批改]',
  `experiment_score` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '实验报告评分',
  `experiment_comment` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '实验报告评语',
  `begin_time` datetime(0) NOT NULL COMMENT '开始答题时间',
  `end_time` datetime(0) NOT NULL COMMENT '结束答题时间',
  `example_state` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '作业状态[0:停用, 1:未开始 20:执行中 30:已完成]',
  `experiment_state` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '实验报告状态[0:停用, 1:未开始 20:执行中 30:已完成]',
  `operator` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '操作人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`course_student_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '课程学生表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_course_student
-- ----------------------------

-- ----------------------------
-- Table structure for t_dept
-- ----------------------------
DROP TABLE IF EXISTS `t_dept`;
CREATE TABLE `t_dept`  (
  `DEPT_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门ID',
  `PARENT_ID` bigint(20) NOT NULL COMMENT '上级部门ID',
  `DEPT_NAME` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门名称',
  `ORDER_NUM` bigint(20) NULL DEFAULT NULL COMMENT '排序',
  `CREATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `MODIFY_TIME` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`DEPT_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_dept
-- ----------------------------
INSERT INTO `t_dept` VALUES (11, 0, '机电学院', 1, '2020-03-20 09:41:50', NULL);
INSERT INTO `t_dept` VALUES (12, 11, '2013', 2, '2020-03-20 09:43:32', '2020-03-24 14:33:11');
INSERT INTO `t_dept` VALUES (13, 12, '电子信息工程1301', 3, '2020-03-20 09:43:55', '2020-03-24 14:27:48');
INSERT INTO `t_dept` VALUES (14, 0, '计算机学院', 2, '2020-03-20 15:12:15', NULL);
INSERT INTO `t_dept` VALUES (15, 0, '经济学院', 3, '2020-03-20 15:12:23', NULL);
INSERT INTO `t_dept` VALUES (16, 0, '外国语学院', 4, '2020-03-20 15:12:30', NULL);
INSERT INTO `t_dept` VALUES (17, 0, '农学院', 5, '2020-03-20 15:13:02', NULL);
INSERT INTO `t_dept` VALUES (18, 14, '计算机科学', 1, '2020-03-20 15:13:46', NULL);
INSERT INTO `t_dept` VALUES (19, 14, '软件工程', 2, '2020-03-20 15:14:01', NULL);
INSERT INTO `t_dept` VALUES (20, 18, '1522班', 2, '2020-03-20 15:27:05', NULL);
INSERT INTO `t_dept` VALUES (21, 0, '郑州大学', 10, '2020-03-23 17:49:07', NULL);
INSERT INTO `t_dept` VALUES (22, 21, '计算机学院', 20, '2020-03-23 17:49:26', NULL);
INSERT INTO `t_dept` VALUES (23, 22, '计算机科学1501', 20, '2020-03-23 18:25:08', NULL);
INSERT INTO `t_dept` VALUES (24, 23, '模电', 20, '2020-03-23 18:25:28', NULL);
INSERT INTO `t_dept` VALUES (25, 22, '计算科学1602', 30, '2020-03-23 18:25:56', NULL);
INSERT INTO `t_dept` VALUES (26, 0, '注册用户', NULL, '2020-04-28 16:29:58', NULL);

-- ----------------------------
-- Table structure for t_eximport
-- ----------------------------
DROP TABLE IF EXISTS `t_eximport`;
CREATE TABLE `t_eximport`  (
  `FIELD1` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字段1',
  `FIELD2` int(11) NOT NULL COMMENT '字段2',
  `FIELD3` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字段3',
  `CREATE_TIME` datetime(0) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Excel导入导出测试' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_eximport
-- ----------------------------
INSERT INTO `t_eximport` VALUES ('字段1', 1, 'mrbird0@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 2, 'mrbird1@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 3, 'mrbird2@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 4, 'mrbird3@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 5, 'mrbird4@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 6, 'mrbird5@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 7, 'mrbird6@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 8, 'mrbird7@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 9, 'mrbird8@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 10, 'mrbird9@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 11, 'mrbird10@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 12, 'mrbird11@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 13, 'mrbird12@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 14, 'mrbird13@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 15, 'mrbird14@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 16, 'mrbird15@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 17, 'mrbird16@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 18, 'mrbird17@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 19, 'mrbird18@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 20, 'mrbird19@gmail.com', '2019-06-13 03:14:06');

-- ----------------------------
-- Table structure for t_experiment
-- ----------------------------
DROP TABLE IF EXISTS `t_experiment`;
CREATE TABLE `t_experiment`  (
  `experiment_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '实验id',
  `experiment_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '实验编号',
  `experiment_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '实验名称',
  `student_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '实验者类别',
  `study_time` tinyint(3) UNSIGNED NULL DEFAULT 0 COMMENT '计划学时',
  `experiment_book` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '实验教程',
  `check_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '考核方式',
  `experiment_count` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '实验设备及数量',
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '实验描述',
  `require_content` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '目的与要求',
  `file_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '实验附件名称',
  `file_url` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '实验附件url',
  `file_size` tinyint(3) UNSIGNED NULL DEFAULT 0 COMMENT '实验附件大小',
  `state` tinyint(3) UNSIGNED NULL DEFAULT 1 COMMENT '删除状态[0:停用, 1:启用 2：正常]',
  `operator` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '操作人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`experiment_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '实验表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_experiment
-- ----------------------------
INSERT INTO `t_experiment` VALUES (1, '', 'aaa', '', 0, '', '', '', NULL, '', '', '', 0, 1, '', NULL, NULL);
INSERT INTO `t_experiment` VALUES (2, '', 'bbb', '', 0, '', '', '', NULL, '', '', '', 0, 0, '', NULL, NULL);
INSERT INTO `t_experiment` VALUES (3, '', '模拟电子实验', '', 0, '', '统考', '', '请输入 实验内容 和描述&nbsp; 模拟电子在线实验室采用远程实验技术，通过网络访问远程端真实的电子仪器仪表及实验电路板，完成基本的模拟电子实验。该实验室可作为电子技术类课程实验教学，也可用于教师在课堂上通过投影为学生演示，还可作为开放实验室供学生使用。&nbsp; &nbsp; &nbsp; &nbsp; 实验室旨在培养学生的基本实验技能，加深对模拟电子技术基本原理的理解，了解电子线路的设计分析方法。熟悉并掌握常用仪器的使用方法，学会正确测量电子器件和系统参数的方法。', '', '', '', 0, 1, '', '2020-03-25 17:26:00', NULL);
INSERT INTO `t_experiment` VALUES (4, '', '数字电子实验', '', 0, '', '统考', '', '&nbsp; &nbsp; &nbsp; &nbsp; 数字电子在线实验室采用远程实验技术，通过网络访问远程端真实的数字电路实验板、电平开关、电平指示器及电子仪器仪表，完成基本的数字电子实验。该实验室可作为电子技术类课程实验教学，也可用于教师在课堂上通过投影为学生演示，还可作为开放实验室供学生使用。&nbsp; &nbsp; &nbsp; &nbsp; 数字电路可以分为组合逻辑电路和时序逻辑电路两大类，实验项目涵盖了主要的电路类型。通过实验室可培养学生的基本实验技能，加深对数字电子技术基本原理的理解，了解数字电路逻辑的组成及设计方法。', '', '', '', 0, 1, '', '2020-03-25 17:30:56', NULL);

-- ----------------------------
-- Table structure for t_experiment_project
-- ----------------------------
DROP TABLE IF EXISTS `t_experiment_project`;
CREATE TABLE `t_experiment_project`  (
  `project_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '实验项目id',
  `project_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '实验项目编号',
  `project_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '实验项目名称',
  `course_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '所属实验ID',
  `course_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属实验名称',
  `project_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '实验项目内容',
  `project_sort` int(11) NULL DEFAULT NULL COMMENT '排序字段，小的靠前',
  `report_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '报告URL',
  `report_size` varbinary(50) NULL DEFAULT NULL COMMENT '文件大小',
  `state` tinyint(3) UNSIGNED NULL DEFAULT 1 COMMENT '删除状态[0:停用, 1:启用 2：正常]',
  `operator` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`project_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '实验项目表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_experiment_project
-- ----------------------------
INSERT INTO `t_experiment_project` VALUES (1, NULL, '实验一 单管放大器放大电路', 22, '传感器实验课222', '实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路', NULL, NULL, NULL, 1, NULL, NULL, '2020-04-07 10:01:19');
INSERT INTO `t_experiment_project` VALUES (5, NULL, '实验三 负反馈放大器', 21, '传感器实验课222', '请输入 项目描述实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器', NULL, NULL, NULL, 1, NULL, '2020-03-26 11:29:20', '2020-04-07 10:01:25');
INSERT INTO `t_experiment_project` VALUES (6, NULL, '实验四 运算放大器信号运算电路', 24, '电工技术实验课', '实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路', NULL, NULL, NULL, 1, NULL, '2020-03-26 11:32:06', '2020-04-07 09:59:16');
INSERT INTO `t_experiment_project` VALUES (7, NULL, '实验二  单管共射放大电路的测试', 23, '电工技术实验课', '实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路', NULL, NULL, NULL, 1, NULL, '2020-03-31 14:48:25', '2020-04-07 09:58:01');
INSERT INTO `t_experiment_project` VALUES (24, NULL, '范德萨发的', 29, '数字电子实验', '实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路', NULL, 'project/8/80959d7eb6f64dd2959bd9b233370c96单管共射放大电路的测试(1).docx', 0x3236372E32324B42, 1, NULL, '2020-04-03 17:36:38', '2020-04-07 09:58:15');
INSERT INTO `t_experiment_project` VALUES (28, NULL, 'hgfhgfhgfj', 21, '模拟电子实验', '&nbsp; 请输入 实验描述&nbsp; 请输入 实验描述&nbsp; 请输入 实验描述&nbsp; 请输入 实验描述&nbsp; 请输入 实验描述&nbsp; 请输入 实验描述&nbsp; 请输入 实验描述&nbsp; 请输入 实验描述', NULL, NULL, NULL, 1, NULL, '2020-04-03 18:14:43', '2020-04-07 09:58:24');
INSERT INTO `t_experiment_project` VALUES (29, NULL, 'sss', NULL, '', '请输入 实验描述', NULL, 'http://macwoss.http://oss-cn-beijing.aliyuncs.com/http://henaumcw.top//project/2020/04/07/989cf650-81b7-4bb1-aefd-6d46b6ef49dd/设计思路(1).docx', 0x31352E38364B42, 1, NULL, '2020-04-07 17:02:23', NULL);
INSERT INTO `t_experiment_project` VALUES (30, NULL, 'ssssddddd', NULL, '', '请输入 实验描述', NULL, 'project/2020/04/07/db326a98878e4ecfb23d3c73c0913e73/测试数据.docx', 0x31382E37394B42, 1, NULL, '2020-04-07 17:07:47', NULL);
INSERT INTO `t_experiment_project` VALUES (31, NULL, 'dsffdfd', 21, '模拟电子实验课', '请输入 实验描述sdsadfsddc', NULL, 'http://henaumcw.top/project/2020/04/07/5214d374e5ef4a728c8af1da35f93fe0/智慧电子实验室.docx', 0x3434382E39364B42, 1, NULL, '2020-04-07 17:56:09', NULL);
INSERT INTO `t_experiment_project` VALUES (32, NULL, 'dsdsdsd', NULL, '', '请输入 实验描述', NULL, 'http://file.huistone.com/project/2020/04/09/160c7126f986432ab3d0bcefcaaeb73a/用Java语言向串口读写数据的方法.docx', 0x31372E32354B42, 1, NULL, '2020-04-09 16:18:15', NULL);

-- ----------------------------
-- Table structure for t_experiment_report
-- ----------------------------
DROP TABLE IF EXISTS `t_experiment_report`;
CREATE TABLE `t_experiment_report`  (
  `report_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '报告id',
  `user_id` int(10) NULL DEFAULT NULL COMMENT '学生用户ID',
  `user_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '学生姓名',
  `user_number` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '学生证号',
  `dept_id` int(10) NULL DEFAULT NULL COMMENT '班级ID',
  `dept_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '班级名称',
  `experiment_id` int(10) NULL DEFAULT NULL COMMENT '实验ID',
  `experiment_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '实验名称',
  `teacher_id` int(10) NULL DEFAULT NULL COMMENT '课程老师ID',
  `teacher_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '课程老师名称',
  `looked` tinyint(3) UNSIGNED NULL DEFAULT 0 COMMENT '是否查看[0:未查看, 1:已查看]',
  `state` tinyint(3) UNSIGNED NULL DEFAULT 1 COMMENT '删除状态[0:停用, 1:启用]',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '操作人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`report_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '实验报告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_experiment_report
-- ----------------------------

-- ----------------------------
-- Table structure for t_experiment_type
-- ----------------------------
DROP TABLE IF EXISTS `t_experiment_type`;
CREATE TABLE `t_experiment_type`  (
  `type_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '实验id',
  `type_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '实验类型名称',
  `state` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '删除状态[0:停用, 1:启用]',
  `operator` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '操作人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`type_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '实验类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_experiment_type
-- ----------------------------

-- ----------------------------
-- Table structure for t_generator_config
-- ----------------------------
DROP TABLE IF EXISTS `t_generator_config`;
CREATE TABLE `t_generator_config`  (
  `id` int(11) NOT NULL COMMENT '主键',
  `author` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '作者',
  `base_package` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '基础包名',
  `entity_package` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'entity文件存放路径',
  `mapper_package` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'mapper文件存放路径',
  `mapper_xml_package` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'mapper xml文件存放路径',
  `service_package` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'servcie文件存放路径',
  `service_impl_package` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'serviceImpl文件存放路径',
  `controller_package` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'controller文件存放路径',
  `is_trim` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否去除前缀 1是 0否',
  `trim_value` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '前缀内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_generator_config
-- ----------------------------
INSERT INTO `t_generator_config` VALUES (1, 'Macw', 'cc.mrbird.febs.system', 'entity', 'mapper', 'mapper', 'service', 'service.impl', 'controller', '1', 't_');

-- ----------------------------
-- Table structure for t_job
-- ----------------------------
DROP TABLE IF EXISTS `t_job`;
CREATE TABLE `t_job`  (
  `JOB_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务id',
  `BEAN_NAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'spring bean名称',
  `METHOD_NAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '方法名',
  `PARAMS` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数',
  `CRON_EXPRESSION` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'cron表达式',
  `STATUS` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务状态  0：正常  1：暂停',
  `REMARK` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `CREATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`JOB_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_job
-- ----------------------------
INSERT INTO `t_job` VALUES (1, 'testTask', 'test', 'mrbird', '0/1 * * * * ?', '1', '有参任务调度测试~~', '2018-02-24 16:26:14');
INSERT INTO `t_job` VALUES (2, 'testTask', 'test1', NULL, '0/10 * * * * ?', '1', '无参任务调度测试', '2018-02-24 17:06:23');
INSERT INTO `t_job` VALUES (3, 'testTask', 'test', 'hello world', '0/1 * * * * ?', '1', '有参任务调度测试,每隔一秒触发', '2018-02-26 09:28:26');
INSERT INTO `t_job` VALUES (11, 'testTask', 'test2', NULL, '0/5 * * * * ?', '1', '测试异常', '2018-02-26 11:15:30');

-- ----------------------------
-- Table structure for t_job_log
-- ----------------------------
DROP TABLE IF EXISTS `t_job_log`;
CREATE TABLE `t_job_log`  (
  `LOG_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志id',
  `JOB_ID` bigint(20) NOT NULL COMMENT '任务id',
  `BEAN_NAME` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'spring bean名称',
  `METHOD_NAME` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '方法名',
  `PARAMS` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数',
  `STATUS` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务状态    0：成功    1：失败',
  `ERROR` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '失败信息',
  `TIMES` decimal(11, 0) NULL DEFAULT NULL COMMENT '耗时(单位：毫秒)',
  `CREATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`LOG_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4509 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_job_log
-- ----------------------------
INSERT INTO `t_job_log` VALUES (2562, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 161, '2020-03-19 15:30:56');
INSERT INTO `t_job_log` VALUES (2563, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-03-19 15:31:01');
INSERT INTO `t_job_log` VALUES (2564, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:31:05');
INSERT INTO `t_job_log` VALUES (2565, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-03-19 15:31:10');
INSERT INTO `t_job_log` VALUES (2566, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:31:15');
INSERT INTO `t_job_log` VALUES (2567, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 2, '2020-03-19 15:31:20');
INSERT INTO `t_job_log` VALUES (2568, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 5, '2020-03-19 15:31:25');
INSERT INTO `t_job_log` VALUES (2569, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-03-19 15:31:30');
INSERT INTO `t_job_log` VALUES (2570, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:31:35');
INSERT INTO `t_job_log` VALUES (2571, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:31:40');
INSERT INTO `t_job_log` VALUES (2572, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:31:47');
INSERT INTO `t_job_log` VALUES (2573, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-03-19 15:31:57');
INSERT INTO `t_job_log` VALUES (2574, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-03-19 15:32:00');
INSERT INTO `t_job_log` VALUES (2575, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:32:01');
INSERT INTO `t_job_log` VALUES (2576, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:32:05');
INSERT INTO `t_job_log` VALUES (2577, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 2, '2020-03-19 15:32:10');
INSERT INTO `t_job_log` VALUES (2578, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:32:15');
INSERT INTO `t_job_log` VALUES (2579, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:32:20');
INSERT INTO `t_job_log` VALUES (2580, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:32:27');
INSERT INTO `t_job_log` VALUES (2581, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:32:30');
INSERT INTO `t_job_log` VALUES (2582, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:32:35');
INSERT INTO `t_job_log` VALUES (2583, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 4, '2020-03-19 15:32:40');
INSERT INTO `t_job_log` VALUES (2584, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:32:45');
INSERT INTO `t_job_log` VALUES (2585, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:32:51');
INSERT INTO `t_job_log` VALUES (2586, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:32:55');
INSERT INTO `t_job_log` VALUES (2587, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:33:00');
INSERT INTO `t_job_log` VALUES (2588, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:33:06');
INSERT INTO `t_job_log` VALUES (2589, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:33:10');
INSERT INTO `t_job_log` VALUES (2590, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 7, '2020-03-19 15:33:15');
INSERT INTO `t_job_log` VALUES (2591, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:33:21');
INSERT INTO `t_job_log` VALUES (2592, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:33:25');
INSERT INTO `t_job_log` VALUES (2593, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 2, '2020-03-19 15:33:30');
INSERT INTO `t_job_log` VALUES (2594, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-19 15:33:35');
INSERT INTO `t_job_log` VALUES (2595, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-03-19 15:33:41');
INSERT INTO `t_job_log` VALUES (2596, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 9, '2020-03-19 15:33:45');
INSERT INTO `t_job_log` VALUES (2597, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 7, '2020-03-19 15:33:50');
INSERT INTO `t_job_log` VALUES (2598, 3, 'testTask', 'test', 'hello world', '0', NULL, 13, '2020-03-23 09:41:53');
INSERT INTO `t_job_log` VALUES (2599, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2020-03-23 09:41:54');
INSERT INTO `t_job_log` VALUES (2600, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:41:54');
INSERT INTO `t_job_log` VALUES (2601, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:41:55');
INSERT INTO `t_job_log` VALUES (2602, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:41:56');
INSERT INTO `t_job_log` VALUES (2603, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:41:57');
INSERT INTO `t_job_log` VALUES (2604, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:41:58');
INSERT INTO `t_job_log` VALUES (2605, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:41:59');
INSERT INTO `t_job_log` VALUES (2606, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:42:00');
INSERT INTO `t_job_log` VALUES (2607, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:01');
INSERT INTO `t_job_log` VALUES (2608, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:42:02');
INSERT INTO `t_job_log` VALUES (2609, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:42:03');
INSERT INTO `t_job_log` VALUES (2610, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:42:04');
INSERT INTO `t_job_log` VALUES (2611, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:05');
INSERT INTO `t_job_log` VALUES (2612, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:06');
INSERT INTO `t_job_log` VALUES (2613, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:42:07');
INSERT INTO `t_job_log` VALUES (2614, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:08');
INSERT INTO `t_job_log` VALUES (2615, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:09');
INSERT INTO `t_job_log` VALUES (2616, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:42:10');
INSERT INTO `t_job_log` VALUES (2617, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:11');
INSERT INTO `t_job_log` VALUES (2618, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2020-03-23 09:42:12');
INSERT INTO `t_job_log` VALUES (2619, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:42:13');
INSERT INTO `t_job_log` VALUES (2620, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:42:14');
INSERT INTO `t_job_log` VALUES (2621, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2020-03-23 09:42:15');
INSERT INTO `t_job_log` VALUES (2622, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:16');
INSERT INTO `t_job_log` VALUES (2623, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:17');
INSERT INTO `t_job_log` VALUES (2624, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:18');
INSERT INTO `t_job_log` VALUES (2625, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:19');
INSERT INTO `t_job_log` VALUES (2626, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:42:20');
INSERT INTO `t_job_log` VALUES (2627, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:21');
INSERT INTO `t_job_log` VALUES (2628, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:22');
INSERT INTO `t_job_log` VALUES (2629, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:23');
INSERT INTO `t_job_log` VALUES (2630, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:42:24');
INSERT INTO `t_job_log` VALUES (2631, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2020-03-23 09:42:25');
INSERT INTO `t_job_log` VALUES (2632, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:42:26');
INSERT INTO `t_job_log` VALUES (2633, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:27');
INSERT INTO `t_job_log` VALUES (2634, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:42:28');
INSERT INTO `t_job_log` VALUES (2635, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:29');
INSERT INTO `t_job_log` VALUES (2636, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:31');
INSERT INTO `t_job_log` VALUES (2637, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:31');
INSERT INTO `t_job_log` VALUES (2638, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:32');
INSERT INTO `t_job_log` VALUES (2639, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:42:33');
INSERT INTO `t_job_log` VALUES (2640, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:34');
INSERT INTO `t_job_log` VALUES (2641, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:42:35');
INSERT INTO `t_job_log` VALUES (2642, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:36');
INSERT INTO `t_job_log` VALUES (2643, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:37');
INSERT INTO `t_job_log` VALUES (2644, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:38');
INSERT INTO `t_job_log` VALUES (2645, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:42:39');
INSERT INTO `t_job_log` VALUES (2646, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:42:40');
INSERT INTO `t_job_log` VALUES (2647, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:41');
INSERT INTO `t_job_log` VALUES (2648, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:42');
INSERT INTO `t_job_log` VALUES (2649, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:43');
INSERT INTO `t_job_log` VALUES (2650, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:42:44');
INSERT INTO `t_job_log` VALUES (2651, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:45');
INSERT INTO `t_job_log` VALUES (2652, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:42:46');
INSERT INTO `t_job_log` VALUES (2653, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:42:47');
INSERT INTO `t_job_log` VALUES (2654, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:48');
INSERT INTO `t_job_log` VALUES (2655, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:49');
INSERT INTO `t_job_log` VALUES (2656, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:51');
INSERT INTO `t_job_log` VALUES (2657, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:42:51');
INSERT INTO `t_job_log` VALUES (2658, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:42:52');
INSERT INTO `t_job_log` VALUES (2659, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:53');
INSERT INTO `t_job_log` VALUES (2660, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:54');
INSERT INTO `t_job_log` VALUES (2661, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:42:55');
INSERT INTO `t_job_log` VALUES (2662, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:42:56');
INSERT INTO `t_job_log` VALUES (2663, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:42:57');
INSERT INTO `t_job_log` VALUES (2664, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:42:58');
INSERT INTO `t_job_log` VALUES (2665, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:42:59');
INSERT INTO `t_job_log` VALUES (2666, 3, 'testTask', 'test', 'hello world', '0', NULL, 7, '2020-03-23 09:43:00');
INSERT INTO `t_job_log` VALUES (2667, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:01');
INSERT INTO `t_job_log` VALUES (2668, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:02');
INSERT INTO `t_job_log` VALUES (2669, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:04');
INSERT INTO `t_job_log` VALUES (2670, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:43:04');
INSERT INTO `t_job_log` VALUES (2671, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:05');
INSERT INTO `t_job_log` VALUES (2672, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:06');
INSERT INTO `t_job_log` VALUES (2673, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:07');
INSERT INTO `t_job_log` VALUES (2674, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:08');
INSERT INTO `t_job_log` VALUES (2675, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:09');
INSERT INTO `t_job_log` VALUES (2676, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:10');
INSERT INTO `t_job_log` VALUES (2677, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:11');
INSERT INTO `t_job_log` VALUES (2678, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:43:12');
INSERT INTO `t_job_log` VALUES (2679, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:43:13');
INSERT INTO `t_job_log` VALUES (2680, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:43:14');
INSERT INTO `t_job_log` VALUES (2681, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:15');
INSERT INTO `t_job_log` VALUES (2682, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:16');
INSERT INTO `t_job_log` VALUES (2683, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:43:17');
INSERT INTO `t_job_log` VALUES (2684, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:43:18');
INSERT INTO `t_job_log` VALUES (2685, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:43:19');
INSERT INTO `t_job_log` VALUES (2686, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:43:20');
INSERT INTO `t_job_log` VALUES (2687, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:43:21');
INSERT INTO `t_job_log` VALUES (2688, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:22');
INSERT INTO `t_job_log` VALUES (2689, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:43:23');
INSERT INTO `t_job_log` VALUES (2690, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:24');
INSERT INTO `t_job_log` VALUES (2691, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:43:25');
INSERT INTO `t_job_log` VALUES (2692, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:26');
INSERT INTO `t_job_log` VALUES (2693, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:43:27');
INSERT INTO `t_job_log` VALUES (2694, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:43:28');
INSERT INTO `t_job_log` VALUES (2695, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:29');
INSERT INTO `t_job_log` VALUES (2696, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:30');
INSERT INTO `t_job_log` VALUES (2697, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:43:31');
INSERT INTO `t_job_log` VALUES (2698, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:32');
INSERT INTO `t_job_log` VALUES (2699, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:43:33');
INSERT INTO `t_job_log` VALUES (2700, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:34');
INSERT INTO `t_job_log` VALUES (2701, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:35');
INSERT INTO `t_job_log` VALUES (2702, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:36');
INSERT INTO `t_job_log` VALUES (2703, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:37');
INSERT INTO `t_job_log` VALUES (2704, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:43:38');
INSERT INTO `t_job_log` VALUES (2705, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:43:39');
INSERT INTO `t_job_log` VALUES (2706, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:40');
INSERT INTO `t_job_log` VALUES (2707, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:43:41');
INSERT INTO `t_job_log` VALUES (2708, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:42');
INSERT INTO `t_job_log` VALUES (2709, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:44');
INSERT INTO `t_job_log` VALUES (2710, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:43:44');
INSERT INTO `t_job_log` VALUES (2711, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:43:45');
INSERT INTO `t_job_log` VALUES (2712, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:43:46');
INSERT INTO `t_job_log` VALUES (2713, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:47');
INSERT INTO `t_job_log` VALUES (2714, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:49');
INSERT INTO `t_job_log` VALUES (2715, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:50');
INSERT INTO `t_job_log` VALUES (2716, 3, 'testTask', 'test', 'hello world', '0', NULL, 6, '2020-03-23 09:43:50');
INSERT INTO `t_job_log` VALUES (2717, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:43:51');
INSERT INTO `t_job_log` VALUES (2718, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:52');
INSERT INTO `t_job_log` VALUES (2719, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:53');
INSERT INTO `t_job_log` VALUES (2720, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:54');
INSERT INTO `t_job_log` VALUES (2721, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:43:55');
INSERT INTO `t_job_log` VALUES (2722, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:43:56');
INSERT INTO `t_job_log` VALUES (2723, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:57');
INSERT INTO `t_job_log` VALUES (2724, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:43:59');
INSERT INTO `t_job_log` VALUES (2725, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:43:59');
INSERT INTO `t_job_log` VALUES (2726, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:44:00');
INSERT INTO `t_job_log` VALUES (2727, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:01');
INSERT INTO `t_job_log` VALUES (2728, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:02');
INSERT INTO `t_job_log` VALUES (2729, 3, 'testTask', 'test', 'hello world', '0', NULL, 13, '2020-03-23 09:44:03');
INSERT INTO `t_job_log` VALUES (2730, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:04');
INSERT INTO `t_job_log` VALUES (2731, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2020-03-23 09:44:05');
INSERT INTO `t_job_log` VALUES (2732, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:44:06');
INSERT INTO `t_job_log` VALUES (2733, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:44:07');
INSERT INTO `t_job_log` VALUES (2734, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:08');
INSERT INTO `t_job_log` VALUES (2735, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:09');
INSERT INTO `t_job_log` VALUES (2736, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:44:10');
INSERT INTO `t_job_log` VALUES (2737, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:11');
INSERT INTO `t_job_log` VALUES (2738, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:12');
INSERT INTO `t_job_log` VALUES (2739, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:13');
INSERT INTO `t_job_log` VALUES (2740, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:44:14');
INSERT INTO `t_job_log` VALUES (2741, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:44:15');
INSERT INTO `t_job_log` VALUES (2742, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:16');
INSERT INTO `t_job_log` VALUES (2743, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:44:17');
INSERT INTO `t_job_log` VALUES (2744, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:18');
INSERT INTO `t_job_log` VALUES (2745, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:19');
INSERT INTO `t_job_log` VALUES (2746, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:44:20');
INSERT INTO `t_job_log` VALUES (2747, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:21');
INSERT INTO `t_job_log` VALUES (2748, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:22');
INSERT INTO `t_job_log` VALUES (2749, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:23');
INSERT INTO `t_job_log` VALUES (2750, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:24');
INSERT INTO `t_job_log` VALUES (2751, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:26');
INSERT INTO `t_job_log` VALUES (2752, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:27');
INSERT INTO `t_job_log` VALUES (2753, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:27');
INSERT INTO `t_job_log` VALUES (2754, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:29');
INSERT INTO `t_job_log` VALUES (2755, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:29');
INSERT INTO `t_job_log` VALUES (2756, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:30');
INSERT INTO `t_job_log` VALUES (2757, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:31');
INSERT INTO `t_job_log` VALUES (2758, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:44:32');
INSERT INTO `t_job_log` VALUES (2759, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:33');
INSERT INTO `t_job_log` VALUES (2760, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:34');
INSERT INTO `t_job_log` VALUES (2761, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:35');
INSERT INTO `t_job_log` VALUES (2762, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:44:36');
INSERT INTO `t_job_log` VALUES (2763, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:37');
INSERT INTO `t_job_log` VALUES (2764, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:44:38');
INSERT INTO `t_job_log` VALUES (2765, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:44:39');
INSERT INTO `t_job_log` VALUES (2766, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:40');
INSERT INTO `t_job_log` VALUES (2767, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:41');
INSERT INTO `t_job_log` VALUES (2768, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:42');
INSERT INTO `t_job_log` VALUES (2769, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:43');
INSERT INTO `t_job_log` VALUES (2770, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:44:44');
INSERT INTO `t_job_log` VALUES (2771, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:45');
INSERT INTO `t_job_log` VALUES (2772, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:44:46');
INSERT INTO `t_job_log` VALUES (2773, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:47');
INSERT INTO `t_job_log` VALUES (2774, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:44:48');
INSERT INTO `t_job_log` VALUES (2775, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:49');
INSERT INTO `t_job_log` VALUES (2776, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:44:50');
INSERT INTO `t_job_log` VALUES (2777, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:44:51');
INSERT INTO `t_job_log` VALUES (2778, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:44:52');
INSERT INTO `t_job_log` VALUES (2779, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:53');
INSERT INTO `t_job_log` VALUES (2780, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:54');
INSERT INTO `t_job_log` VALUES (2781, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:44:55');
INSERT INTO `t_job_log` VALUES (2782, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:44:56');
INSERT INTO `t_job_log` VALUES (2783, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:57');
INSERT INTO `t_job_log` VALUES (2784, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:44:59');
INSERT INTO `t_job_log` VALUES (2785, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:44:59');
INSERT INTO `t_job_log` VALUES (2786, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:00');
INSERT INTO `t_job_log` VALUES (2787, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:01');
INSERT INTO `t_job_log` VALUES (2788, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:02');
INSERT INTO `t_job_log` VALUES (2789, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:03');
INSERT INTO `t_job_log` VALUES (2790, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:04');
INSERT INTO `t_job_log` VALUES (2791, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:05');
INSERT INTO `t_job_log` VALUES (2792, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:06');
INSERT INTO `t_job_log` VALUES (2793, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:07');
INSERT INTO `t_job_log` VALUES (2794, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:08');
INSERT INTO `t_job_log` VALUES (2795, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:09');
INSERT INTO `t_job_log` VALUES (2796, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:10');
INSERT INTO `t_job_log` VALUES (2797, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:11');
INSERT INTO `t_job_log` VALUES (2798, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:12');
INSERT INTO `t_job_log` VALUES (2799, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:13');
INSERT INTO `t_job_log` VALUES (2800, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:14');
INSERT INTO `t_job_log` VALUES (2801, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2020-03-23 09:45:15');
INSERT INTO `t_job_log` VALUES (2802, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:16');
INSERT INTO `t_job_log` VALUES (2803, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:17');
INSERT INTO `t_job_log` VALUES (2804, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:18');
INSERT INTO `t_job_log` VALUES (2805, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:19');
INSERT INTO `t_job_log` VALUES (2806, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:20');
INSERT INTO `t_job_log` VALUES (2807, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:21');
INSERT INTO `t_job_log` VALUES (2808, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:45:22');
INSERT INTO `t_job_log` VALUES (2809, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:23');
INSERT INTO `t_job_log` VALUES (2810, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:24');
INSERT INTO `t_job_log` VALUES (2811, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:25');
INSERT INTO `t_job_log` VALUES (2812, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:26');
INSERT INTO `t_job_log` VALUES (2813, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:28');
INSERT INTO `t_job_log` VALUES (2814, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:28');
INSERT INTO `t_job_log` VALUES (2815, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:29');
INSERT INTO `t_job_log` VALUES (2816, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:30');
INSERT INTO `t_job_log` VALUES (2817, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2020-03-23 09:45:31');
INSERT INTO `t_job_log` VALUES (2818, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:32');
INSERT INTO `t_job_log` VALUES (2819, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:33');
INSERT INTO `t_job_log` VALUES (2820, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:34');
INSERT INTO `t_job_log` VALUES (2821, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:35');
INSERT INTO `t_job_log` VALUES (2822, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:36');
INSERT INTO `t_job_log` VALUES (2823, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:37');
INSERT INTO `t_job_log` VALUES (2824, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:38');
INSERT INTO `t_job_log` VALUES (2825, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:39');
INSERT INTO `t_job_log` VALUES (2826, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:40');
INSERT INTO `t_job_log` VALUES (2827, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:41');
INSERT INTO `t_job_log` VALUES (2828, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:42');
INSERT INTO `t_job_log` VALUES (2829, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:43');
INSERT INTO `t_job_log` VALUES (2830, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:44');
INSERT INTO `t_job_log` VALUES (2831, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:45:45');
INSERT INTO `t_job_log` VALUES (2832, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:45:46');
INSERT INTO `t_job_log` VALUES (2833, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:45:47');
INSERT INTO `t_job_log` VALUES (2834, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:48');
INSERT INTO `t_job_log` VALUES (2835, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:49');
INSERT INTO `t_job_log` VALUES (2836, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:50');
INSERT INTO `t_job_log` VALUES (2837, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:51');
INSERT INTO `t_job_log` VALUES (2838, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:45:52');
INSERT INTO `t_job_log` VALUES (2839, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:53');
INSERT INTO `t_job_log` VALUES (2840, 3, 'testTask', 'test', 'hello world', '0', NULL, 9, '2020-03-23 09:45:54');
INSERT INTO `t_job_log` VALUES (2841, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:55');
INSERT INTO `t_job_log` VALUES (2842, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:56');
INSERT INTO `t_job_log` VALUES (2843, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:57');
INSERT INTO `t_job_log` VALUES (2844, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:45:58');
INSERT INTO `t_job_log` VALUES (2845, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:45:59');
INSERT INTO `t_job_log` VALUES (2846, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:00');
INSERT INTO `t_job_log` VALUES (2847, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:01');
INSERT INTO `t_job_log` VALUES (2848, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:02');
INSERT INTO `t_job_log` VALUES (2849, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:03');
INSERT INTO `t_job_log` VALUES (2850, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:04');
INSERT INTO `t_job_log` VALUES (2851, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:05');
INSERT INTO `t_job_log` VALUES (2852, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:46:06');
INSERT INTO `t_job_log` VALUES (2853, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:07');
INSERT INTO `t_job_log` VALUES (2854, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:46:08');
INSERT INTO `t_job_log` VALUES (2855, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:46:09');
INSERT INTO `t_job_log` VALUES (2856, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:10');
INSERT INTO `t_job_log` VALUES (2857, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:11');
INSERT INTO `t_job_log` VALUES (2858, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:12');
INSERT INTO `t_job_log` VALUES (2859, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:13');
INSERT INTO `t_job_log` VALUES (2860, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:14');
INSERT INTO `t_job_log` VALUES (2861, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:46:15');
INSERT INTO `t_job_log` VALUES (2862, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:46:16');
INSERT INTO `t_job_log` VALUES (2863, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:46:17');
INSERT INTO `t_job_log` VALUES (2864, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:46:18');
INSERT INTO `t_job_log` VALUES (2865, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:19');
INSERT INTO `t_job_log` VALUES (2866, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:20');
INSERT INTO `t_job_log` VALUES (2867, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:21');
INSERT INTO `t_job_log` VALUES (2868, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:22');
INSERT INTO `t_job_log` VALUES (2869, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:46:23');
INSERT INTO `t_job_log` VALUES (2870, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:24');
INSERT INTO `t_job_log` VALUES (2871, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:25');
INSERT INTO `t_job_log` VALUES (2872, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:26');
INSERT INTO `t_job_log` VALUES (2873, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:27');
INSERT INTO `t_job_log` VALUES (2874, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:28');
INSERT INTO `t_job_log` VALUES (2875, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:29');
INSERT INTO `t_job_log` VALUES (2876, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:30');
INSERT INTO `t_job_log` VALUES (2877, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:46:31');
INSERT INTO `t_job_log` VALUES (2878, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:32');
INSERT INTO `t_job_log` VALUES (2879, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:46:33');
INSERT INTO `t_job_log` VALUES (2880, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:46:34');
INSERT INTO `t_job_log` VALUES (2881, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:35');
INSERT INTO `t_job_log` VALUES (2882, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:36');
INSERT INTO `t_job_log` VALUES (2883, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:37');
INSERT INTO `t_job_log` VALUES (2884, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:46:38');
INSERT INTO `t_job_log` VALUES (2885, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:46:39');
INSERT INTO `t_job_log` VALUES (2886, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:40');
INSERT INTO `t_job_log` VALUES (2887, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:46:41');
INSERT INTO `t_job_log` VALUES (2888, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:42');
INSERT INTO `t_job_log` VALUES (2889, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:43');
INSERT INTO `t_job_log` VALUES (2890, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:46:44');
INSERT INTO `t_job_log` VALUES (2891, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:46:45');
INSERT INTO `t_job_log` VALUES (2892, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:46:46');
INSERT INTO `t_job_log` VALUES (2893, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:47');
INSERT INTO `t_job_log` VALUES (2894, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:46:48');
INSERT INTO `t_job_log` VALUES (2895, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:49');
INSERT INTO `t_job_log` VALUES (2896, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:50');
INSERT INTO `t_job_log` VALUES (2897, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:46:51');
INSERT INTO `t_job_log` VALUES (2898, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:46:52');
INSERT INTO `t_job_log` VALUES (2899, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:46:53');
INSERT INTO `t_job_log` VALUES (2900, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:46:54');
INSERT INTO `t_job_log` VALUES (2901, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:46:55');
INSERT INTO `t_job_log` VALUES (2902, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:56');
INSERT INTO `t_job_log` VALUES (2903, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:46:57');
INSERT INTO `t_job_log` VALUES (2904, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:58');
INSERT INTO `t_job_log` VALUES (2905, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:46:59');
INSERT INTO `t_job_log` VALUES (2906, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:00');
INSERT INTO `t_job_log` VALUES (2907, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:47:01');
INSERT INTO `t_job_log` VALUES (2908, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:47:02');
INSERT INTO `t_job_log` VALUES (2909, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:47:03');
INSERT INTO `t_job_log` VALUES (2910, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:04');
INSERT INTO `t_job_log` VALUES (2911, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:47:05');
INSERT INTO `t_job_log` VALUES (2912, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:47:06');
INSERT INTO `t_job_log` VALUES (2913, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:07');
INSERT INTO `t_job_log` VALUES (2914, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:47:08');
INSERT INTO `t_job_log` VALUES (2915, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:47:09');
INSERT INTO `t_job_log` VALUES (2916, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:10');
INSERT INTO `t_job_log` VALUES (2917, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:11');
INSERT INTO `t_job_log` VALUES (2918, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:12');
INSERT INTO `t_job_log` VALUES (2919, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:47:13');
INSERT INTO `t_job_log` VALUES (2920, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:47:14');
INSERT INTO `t_job_log` VALUES (2921, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:15');
INSERT INTO `t_job_log` VALUES (2922, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:16');
INSERT INTO `t_job_log` VALUES (2923, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:17');
INSERT INTO `t_job_log` VALUES (2924, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:18');
INSERT INTO `t_job_log` VALUES (2925, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:19');
INSERT INTO `t_job_log` VALUES (2926, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:47:20');
INSERT INTO `t_job_log` VALUES (2927, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:47:21');
INSERT INTO `t_job_log` VALUES (2928, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:47:22');
INSERT INTO `t_job_log` VALUES (2929, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:47:23');
INSERT INTO `t_job_log` VALUES (2930, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:47:24');
INSERT INTO `t_job_log` VALUES (2931, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:25');
INSERT INTO `t_job_log` VALUES (2932, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:47:26');
INSERT INTO `t_job_log` VALUES (2933, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:27');
INSERT INTO `t_job_log` VALUES (2934, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:47:28');
INSERT INTO `t_job_log` VALUES (2935, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:29');
INSERT INTO `t_job_log` VALUES (2936, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:47:30');
INSERT INTO `t_job_log` VALUES (2937, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:31');
INSERT INTO `t_job_log` VALUES (2938, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:47:32');
INSERT INTO `t_job_log` VALUES (2939, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:33');
INSERT INTO `t_job_log` VALUES (2940, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:34');
INSERT INTO `t_job_log` VALUES (2941, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:47:35');
INSERT INTO `t_job_log` VALUES (2942, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:36');
INSERT INTO `t_job_log` VALUES (2943, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:47:37');
INSERT INTO `t_job_log` VALUES (2944, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:38');
INSERT INTO `t_job_log` VALUES (2945, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:39');
INSERT INTO `t_job_log` VALUES (2946, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:40');
INSERT INTO `t_job_log` VALUES (2947, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:47:41');
INSERT INTO `t_job_log` VALUES (2948, 3, 'testTask', 'test', 'hello world', '0', NULL, 18, '2020-03-23 09:47:42');
INSERT INTO `t_job_log` VALUES (2949, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:43');
INSERT INTO `t_job_log` VALUES (2950, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:44');
INSERT INTO `t_job_log` VALUES (2951, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:47:45');
INSERT INTO `t_job_log` VALUES (2952, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:46');
INSERT INTO `t_job_log` VALUES (2953, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:47');
INSERT INTO `t_job_log` VALUES (2954, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:48');
INSERT INTO `t_job_log` VALUES (2955, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:49');
INSERT INTO `t_job_log` VALUES (2956, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:47:50');
INSERT INTO `t_job_log` VALUES (2957, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:51');
INSERT INTO `t_job_log` VALUES (2958, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:52');
INSERT INTO `t_job_log` VALUES (2959, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:47:53');
INSERT INTO `t_job_log` VALUES (2960, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:47:54');
INSERT INTO `t_job_log` VALUES (2961, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:55');
INSERT INTO `t_job_log` VALUES (2962, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:47:56');
INSERT INTO `t_job_log` VALUES (2963, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:57');
INSERT INTO `t_job_log` VALUES (2964, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:47:58');
INSERT INTO `t_job_log` VALUES (2965, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:47:59');
INSERT INTO `t_job_log` VALUES (2966, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:48:00');
INSERT INTO `t_job_log` VALUES (2967, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:01');
INSERT INTO `t_job_log` VALUES (2968, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:02');
INSERT INTO `t_job_log` VALUES (2969, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:03');
INSERT INTO `t_job_log` VALUES (2970, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:04');
INSERT INTO `t_job_log` VALUES (2971, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:48:05');
INSERT INTO `t_job_log` VALUES (2972, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:06');
INSERT INTO `t_job_log` VALUES (2973, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:07');
INSERT INTO `t_job_log` VALUES (2974, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:48:08');
INSERT INTO `t_job_log` VALUES (2975, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:09');
INSERT INTO `t_job_log` VALUES (2976, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:48:10');
INSERT INTO `t_job_log` VALUES (2977, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:48:11');
INSERT INTO `t_job_log` VALUES (2978, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:12');
INSERT INTO `t_job_log` VALUES (2979, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:48:13');
INSERT INTO `t_job_log` VALUES (2980, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:14');
INSERT INTO `t_job_log` VALUES (2981, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:15');
INSERT INTO `t_job_log` VALUES (2982, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:16');
INSERT INTO `t_job_log` VALUES (2983, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:17');
INSERT INTO `t_job_log` VALUES (2984, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:18');
INSERT INTO `t_job_log` VALUES (2985, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:48:19');
INSERT INTO `t_job_log` VALUES (2986, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:20');
INSERT INTO `t_job_log` VALUES (2987, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:21');
INSERT INTO `t_job_log` VALUES (2988, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:22');
INSERT INTO `t_job_log` VALUES (2989, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:23');
INSERT INTO `t_job_log` VALUES (2990, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:24');
INSERT INTO `t_job_log` VALUES (2991, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:48:25');
INSERT INTO `t_job_log` VALUES (2992, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:48:26');
INSERT INTO `t_job_log` VALUES (2993, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:48:27');
INSERT INTO `t_job_log` VALUES (2994, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:28');
INSERT INTO `t_job_log` VALUES (2995, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:29');
INSERT INTO `t_job_log` VALUES (2996, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:48:30');
INSERT INTO `t_job_log` VALUES (2997, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:48:31');
INSERT INTO `t_job_log` VALUES (2998, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:48:32');
INSERT INTO `t_job_log` VALUES (2999, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:48:33');
INSERT INTO `t_job_log` VALUES (3000, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:48:34');
INSERT INTO `t_job_log` VALUES (3001, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:35');
INSERT INTO `t_job_log` VALUES (3002, 3, 'testTask', 'test', 'hello world', '0', NULL, 30, '2020-03-23 09:48:36');
INSERT INTO `t_job_log` VALUES (3003, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:48:37');
INSERT INTO `t_job_log` VALUES (3004, 3, 'testTask', 'test', 'hello world', '0', NULL, 44, '2020-03-23 09:48:38');
INSERT INTO `t_job_log` VALUES (3005, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:39');
INSERT INTO `t_job_log` VALUES (3006, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:40');
INSERT INTO `t_job_log` VALUES (3007, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:41');
INSERT INTO `t_job_log` VALUES (3008, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:48:42');
INSERT INTO `t_job_log` VALUES (3009, 3, 'testTask', 'test', 'hello world', '0', NULL, 5, '2020-03-23 09:48:43');
INSERT INTO `t_job_log` VALUES (3010, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:44');
INSERT INTO `t_job_log` VALUES (3011, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:48:45');
INSERT INTO `t_job_log` VALUES (3012, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:48:46');
INSERT INTO `t_job_log` VALUES (3013, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:47');
INSERT INTO `t_job_log` VALUES (3014, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:48');
INSERT INTO `t_job_log` VALUES (3015, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:48:49');
INSERT INTO `t_job_log` VALUES (3016, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:48:50');
INSERT INTO `t_job_log` VALUES (3017, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:48:51');
INSERT INTO `t_job_log` VALUES (3018, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:52');
INSERT INTO `t_job_log` VALUES (3019, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:48:53');
INSERT INTO `t_job_log` VALUES (3020, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:54');
INSERT INTO `t_job_log` VALUES (3021, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:55');
INSERT INTO `t_job_log` VALUES (3022, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:56');
INSERT INTO `t_job_log` VALUES (3023, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:57');
INSERT INTO `t_job_log` VALUES (3024, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:58');
INSERT INTO `t_job_log` VALUES (3025, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:48:59');
INSERT INTO `t_job_log` VALUES (3026, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:00');
INSERT INTO `t_job_log` VALUES (3027, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:01');
INSERT INTO `t_job_log` VALUES (3028, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:02');
INSERT INTO `t_job_log` VALUES (3029, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:03');
INSERT INTO `t_job_log` VALUES (3030, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:04');
INSERT INTO `t_job_log` VALUES (3031, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:49:05');
INSERT INTO `t_job_log` VALUES (3032, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:49:06');
INSERT INTO `t_job_log` VALUES (3033, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:07');
INSERT INTO `t_job_log` VALUES (3034, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:49:08');
INSERT INTO `t_job_log` VALUES (3035, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:49:09');
INSERT INTO `t_job_log` VALUES (3036, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:11');
INSERT INTO `t_job_log` VALUES (3037, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:12');
INSERT INTO `t_job_log` VALUES (3038, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:16');
INSERT INTO `t_job_log` VALUES (3039, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:16');
INSERT INTO `t_job_log` VALUES (3040, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:16');
INSERT INTO `t_job_log` VALUES (3041, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:19');
INSERT INTO `t_job_log` VALUES (3042, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:22');
INSERT INTO `t_job_log` VALUES (3043, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:24');
INSERT INTO `t_job_log` VALUES (3044, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:25');
INSERT INTO `t_job_log` VALUES (3045, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:27');
INSERT INTO `t_job_log` VALUES (3046, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:49:28');
INSERT INTO `t_job_log` VALUES (3047, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:28');
INSERT INTO `t_job_log` VALUES (3048, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:28');
INSERT INTO `t_job_log` VALUES (3049, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:49:29');
INSERT INTO `t_job_log` VALUES (3050, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:49:29');
INSERT INTO `t_job_log` VALUES (3051, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:29');
INSERT INTO `t_job_log` VALUES (3052, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:30');
INSERT INTO `t_job_log` VALUES (3053, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:30');
INSERT INTO `t_job_log` VALUES (3054, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:49:30');
INSERT INTO `t_job_log` VALUES (3055, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:31');
INSERT INTO `t_job_log` VALUES (3056, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:31');
INSERT INTO `t_job_log` VALUES (3057, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:49:31');
INSERT INTO `t_job_log` VALUES (3058, 3, 'testTask', 'test', 'hello world', '0', NULL, 6, '2020-03-23 09:49:32');
INSERT INTO `t_job_log` VALUES (3059, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:33');
INSERT INTO `t_job_log` VALUES (3060, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:34');
INSERT INTO `t_job_log` VALUES (3061, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:35');
INSERT INTO `t_job_log` VALUES (3062, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:49:36');
INSERT INTO `t_job_log` VALUES (3063, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:38');
INSERT INTO `t_job_log` VALUES (3064, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:38');
INSERT INTO `t_job_log` VALUES (3065, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:39');
INSERT INTO `t_job_log` VALUES (3066, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:49:40');
INSERT INTO `t_job_log` VALUES (3067, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:49:41');
INSERT INTO `t_job_log` VALUES (3068, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:49:42');
INSERT INTO `t_job_log` VALUES (3069, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:43');
INSERT INTO `t_job_log` VALUES (3070, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:49:44');
INSERT INTO `t_job_log` VALUES (3071, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:45');
INSERT INTO `t_job_log` VALUES (3072, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:46');
INSERT INTO `t_job_log` VALUES (3073, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:47');
INSERT INTO `t_job_log` VALUES (3074, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:48');
INSERT INTO `t_job_log` VALUES (3075, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:49:49');
INSERT INTO `t_job_log` VALUES (3076, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:49:50');
INSERT INTO `t_job_log` VALUES (3077, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:49:51');
INSERT INTO `t_job_log` VALUES (3078, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:49:52');
INSERT INTO `t_job_log` VALUES (3079, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:49:53');
INSERT INTO `t_job_log` VALUES (3080, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:54');
INSERT INTO `t_job_log` VALUES (3081, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:55');
INSERT INTO `t_job_log` VALUES (3082, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:56');
INSERT INTO `t_job_log` VALUES (3083, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:57');
INSERT INTO `t_job_log` VALUES (3084, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:58');
INSERT INTO `t_job_log` VALUES (3085, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:49:59');
INSERT INTO `t_job_log` VALUES (3086, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:00');
INSERT INTO `t_job_log` VALUES (3087, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:01');
INSERT INTO `t_job_log` VALUES (3088, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:02');
INSERT INTO `t_job_log` VALUES (3089, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:03');
INSERT INTO `t_job_log` VALUES (3090, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:04');
INSERT INTO `t_job_log` VALUES (3091, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:50:05');
INSERT INTO `t_job_log` VALUES (3092, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:50:06');
INSERT INTO `t_job_log` VALUES (3093, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:50:07');
INSERT INTO `t_job_log` VALUES (3094, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:50:08');
INSERT INTO `t_job_log` VALUES (3095, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:09');
INSERT INTO `t_job_log` VALUES (3096, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:10');
INSERT INTO `t_job_log` VALUES (3097, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:11');
INSERT INTO `t_job_log` VALUES (3098, 3, 'testTask', 'test', 'hello world', '0', NULL, 6, '2020-03-23 09:50:12');
INSERT INTO `t_job_log` VALUES (3099, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:13');
INSERT INTO `t_job_log` VALUES (3100, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:50:14');
INSERT INTO `t_job_log` VALUES (3101, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:50:15');
INSERT INTO `t_job_log` VALUES (3102, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:16');
INSERT INTO `t_job_log` VALUES (3103, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:17');
INSERT INTO `t_job_log` VALUES (3104, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:18');
INSERT INTO `t_job_log` VALUES (3105, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:19');
INSERT INTO `t_job_log` VALUES (3106, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:50:20');
INSERT INTO `t_job_log` VALUES (3107, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:50:21');
INSERT INTO `t_job_log` VALUES (3108, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:22');
INSERT INTO `t_job_log` VALUES (3109, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:50:23');
INSERT INTO `t_job_log` VALUES (3110, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:24');
INSERT INTO `t_job_log` VALUES (3111, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:50:25');
INSERT INTO `t_job_log` VALUES (3112, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:26');
INSERT INTO `t_job_log` VALUES (3113, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:27');
INSERT INTO `t_job_log` VALUES (3114, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:50:28');
INSERT INTO `t_job_log` VALUES (3115, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:50:29');
INSERT INTO `t_job_log` VALUES (3116, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:50:30');
INSERT INTO `t_job_log` VALUES (3117, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:31');
INSERT INTO `t_job_log` VALUES (3118, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:50:32');
INSERT INTO `t_job_log` VALUES (3119, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:50:33');
INSERT INTO `t_job_log` VALUES (3120, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:34');
INSERT INTO `t_job_log` VALUES (3121, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:50:35');
INSERT INTO `t_job_log` VALUES (3122, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:36');
INSERT INTO `t_job_log` VALUES (3123, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:37');
INSERT INTO `t_job_log` VALUES (3124, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:38');
INSERT INTO `t_job_log` VALUES (3125, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:39');
INSERT INTO `t_job_log` VALUES (3126, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:40');
INSERT INTO `t_job_log` VALUES (3127, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:41');
INSERT INTO `t_job_log` VALUES (3128, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:50:42');
INSERT INTO `t_job_log` VALUES (3129, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:44');
INSERT INTO `t_job_log` VALUES (3130, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:44');
INSERT INTO `t_job_log` VALUES (3131, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:45');
INSERT INTO `t_job_log` VALUES (3132, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:50:46');
INSERT INTO `t_job_log` VALUES (3133, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:47');
INSERT INTO `t_job_log` VALUES (3134, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:48');
INSERT INTO `t_job_log` VALUES (3135, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:50:49');
INSERT INTO `t_job_log` VALUES (3136, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:50');
INSERT INTO `t_job_log` VALUES (3137, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:50:51');
INSERT INTO `t_job_log` VALUES (3138, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:50:52');
INSERT INTO `t_job_log` VALUES (3139, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:50:53');
INSERT INTO `t_job_log` VALUES (3140, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:50:54');
INSERT INTO `t_job_log` VALUES (3141, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:50:55');
INSERT INTO `t_job_log` VALUES (3142, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:50:56');
INSERT INTO `t_job_log` VALUES (3143, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:57');
INSERT INTO `t_job_log` VALUES (3144, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:50:58');
INSERT INTO `t_job_log` VALUES (3145, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:50:59');
INSERT INTO `t_job_log` VALUES (3146, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:51:00');
INSERT INTO `t_job_log` VALUES (3147, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:51:01');
INSERT INTO `t_job_log` VALUES (3148, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:02');
INSERT INTO `t_job_log` VALUES (3149, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:03');
INSERT INTO `t_job_log` VALUES (3150, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:04');
INSERT INTO `t_job_log` VALUES (3151, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:51:05');
INSERT INTO `t_job_log` VALUES (3152, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:51:06');
INSERT INTO `t_job_log` VALUES (3153, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:07');
INSERT INTO `t_job_log` VALUES (3154, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:08');
INSERT INTO `t_job_log` VALUES (3155, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:09');
INSERT INTO `t_job_log` VALUES (3156, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:10');
INSERT INTO `t_job_log` VALUES (3157, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:51:11');
INSERT INTO `t_job_log` VALUES (3158, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:12');
INSERT INTO `t_job_log` VALUES (3159, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:13');
INSERT INTO `t_job_log` VALUES (3160, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:14');
INSERT INTO `t_job_log` VALUES (3161, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:51:15');
INSERT INTO `t_job_log` VALUES (3162, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:16');
INSERT INTO `t_job_log` VALUES (3163, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:17');
INSERT INTO `t_job_log` VALUES (3164, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:18');
INSERT INTO `t_job_log` VALUES (3165, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:19');
INSERT INTO `t_job_log` VALUES (3166, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:20');
INSERT INTO `t_job_log` VALUES (3167, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:51:21');
INSERT INTO `t_job_log` VALUES (3168, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:22');
INSERT INTO `t_job_log` VALUES (3169, 3, 'testTask', 'test', 'hello world', '0', NULL, 9, '2020-03-23 09:51:23');
INSERT INTO `t_job_log` VALUES (3170, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:24');
INSERT INTO `t_job_log` VALUES (3171, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2020-03-23 09:51:25');
INSERT INTO `t_job_log` VALUES (3172, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:51:26');
INSERT INTO `t_job_log` VALUES (3173, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:27');
INSERT INTO `t_job_log` VALUES (3174, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:51:28');
INSERT INTO `t_job_log` VALUES (3175, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:29');
INSERT INTO `t_job_log` VALUES (3176, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:30');
INSERT INTO `t_job_log` VALUES (3177, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:31');
INSERT INTO `t_job_log` VALUES (3178, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:32');
INSERT INTO `t_job_log` VALUES (3179, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:33');
INSERT INTO `t_job_log` VALUES (3180, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:34');
INSERT INTO `t_job_log` VALUES (3181, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:35');
INSERT INTO `t_job_log` VALUES (3182, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:51:36');
INSERT INTO `t_job_log` VALUES (3183, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:37');
INSERT INTO `t_job_log` VALUES (3184, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:51:38');
INSERT INTO `t_job_log` VALUES (3185, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:39');
INSERT INTO `t_job_log` VALUES (3186, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:51:40');
INSERT INTO `t_job_log` VALUES (3187, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:51:42');
INSERT INTO `t_job_log` VALUES (3188, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:51:42');
INSERT INTO `t_job_log` VALUES (3189, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:51:43');
INSERT INTO `t_job_log` VALUES (3190, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:44');
INSERT INTO `t_job_log` VALUES (3191, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:51:45');
INSERT INTO `t_job_log` VALUES (3192, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:46');
INSERT INTO `t_job_log` VALUES (3193, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:51:47');
INSERT INTO `t_job_log` VALUES (3194, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:48');
INSERT INTO `t_job_log` VALUES (3195, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:51:49');
INSERT INTO `t_job_log` VALUES (3196, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:51:50');
INSERT INTO `t_job_log` VALUES (3197, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:51:51');
INSERT INTO `t_job_log` VALUES (3198, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:52');
INSERT INTO `t_job_log` VALUES (3199, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:53');
INSERT INTO `t_job_log` VALUES (3200, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:54');
INSERT INTO `t_job_log` VALUES (3201, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:51:55');
INSERT INTO `t_job_log` VALUES (3202, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:56');
INSERT INTO `t_job_log` VALUES (3203, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:57');
INSERT INTO `t_job_log` VALUES (3204, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:58');
INSERT INTO `t_job_log` VALUES (3205, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:51:59');
INSERT INTO `t_job_log` VALUES (3206, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:52:00');
INSERT INTO `t_job_log` VALUES (3207, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:01');
INSERT INTO `t_job_log` VALUES (3208, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:02');
INSERT INTO `t_job_log` VALUES (3209, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:03');
INSERT INTO `t_job_log` VALUES (3210, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:04');
INSERT INTO `t_job_log` VALUES (3211, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:06');
INSERT INTO `t_job_log` VALUES (3212, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:06');
INSERT INTO `t_job_log` VALUES (3213, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:52:07');
INSERT INTO `t_job_log` VALUES (3214, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:52:08');
INSERT INTO `t_job_log` VALUES (3215, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:52:09');
INSERT INTO `t_job_log` VALUES (3216, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:10');
INSERT INTO `t_job_log` VALUES (3217, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:11');
INSERT INTO `t_job_log` VALUES (3218, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:52:12');
INSERT INTO `t_job_log` VALUES (3219, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:52:13');
INSERT INTO `t_job_log` VALUES (3220, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:14');
INSERT INTO `t_job_log` VALUES (3221, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:52:15');
INSERT INTO `t_job_log` VALUES (3222, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:52:16');
INSERT INTO `t_job_log` VALUES (3223, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:17');
INSERT INTO `t_job_log` VALUES (3224, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:52:18');
INSERT INTO `t_job_log` VALUES (3225, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:19');
INSERT INTO `t_job_log` VALUES (3226, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:52:20');
INSERT INTO `t_job_log` VALUES (3227, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:21');
INSERT INTO `t_job_log` VALUES (3228, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:22');
INSERT INTO `t_job_log` VALUES (3229, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:23');
INSERT INTO `t_job_log` VALUES (3230, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:24');
INSERT INTO `t_job_log` VALUES (3231, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:25');
INSERT INTO `t_job_log` VALUES (3232, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:52:26');
INSERT INTO `t_job_log` VALUES (3233, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:27');
INSERT INTO `t_job_log` VALUES (3234, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:28');
INSERT INTO `t_job_log` VALUES (3235, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:29');
INSERT INTO `t_job_log` VALUES (3236, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:52:30');
INSERT INTO `t_job_log` VALUES (3237, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:31');
INSERT INTO `t_job_log` VALUES (3238, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:32');
INSERT INTO `t_job_log` VALUES (3239, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:52:33');
INSERT INTO `t_job_log` VALUES (3240, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:52:34');
INSERT INTO `t_job_log` VALUES (3241, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:35');
INSERT INTO `t_job_log` VALUES (3242, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:36');
INSERT INTO `t_job_log` VALUES (3243, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:37');
INSERT INTO `t_job_log` VALUES (3244, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:38');
INSERT INTO `t_job_log` VALUES (3245, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:39');
INSERT INTO `t_job_log` VALUES (3246, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:40');
INSERT INTO `t_job_log` VALUES (3247, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:41');
INSERT INTO `t_job_log` VALUES (3248, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:42');
INSERT INTO `t_job_log` VALUES (3249, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:52:43');
INSERT INTO `t_job_log` VALUES (3250, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:44');
INSERT INTO `t_job_log` VALUES (3251, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:45');
INSERT INTO `t_job_log` VALUES (3252, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:46');
INSERT INTO `t_job_log` VALUES (3253, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:47');
INSERT INTO `t_job_log` VALUES (3254, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:48');
INSERT INTO `t_job_log` VALUES (3255, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:52:49');
INSERT INTO `t_job_log` VALUES (3256, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:50');
INSERT INTO `t_job_log` VALUES (3257, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:52:51');
INSERT INTO `t_job_log` VALUES (3258, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:52:52');
INSERT INTO `t_job_log` VALUES (3259, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:53');
INSERT INTO `t_job_log` VALUES (3260, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:54');
INSERT INTO `t_job_log` VALUES (3261, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:55');
INSERT INTO `t_job_log` VALUES (3262, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:56');
INSERT INTO `t_job_log` VALUES (3263, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:52:57');
INSERT INTO `t_job_log` VALUES (3264, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:58');
INSERT INTO `t_job_log` VALUES (3265, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:52:59');
INSERT INTO `t_job_log` VALUES (3266, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:00');
INSERT INTO `t_job_log` VALUES (3267, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2020-03-23 09:53:01');
INSERT INTO `t_job_log` VALUES (3268, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:02');
INSERT INTO `t_job_log` VALUES (3269, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:03');
INSERT INTO `t_job_log` VALUES (3270, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:04');
INSERT INTO `t_job_log` VALUES (3271, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:05');
INSERT INTO `t_job_log` VALUES (3272, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:06');
INSERT INTO `t_job_log` VALUES (3273, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:07');
INSERT INTO `t_job_log` VALUES (3274, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:08');
INSERT INTO `t_job_log` VALUES (3275, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:09');
INSERT INTO `t_job_log` VALUES (3276, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:10');
INSERT INTO `t_job_log` VALUES (3277, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:11');
INSERT INTO `t_job_log` VALUES (3278, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:12');
INSERT INTO `t_job_log` VALUES (3279, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:13');
INSERT INTO `t_job_log` VALUES (3280, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:14');
INSERT INTO `t_job_log` VALUES (3281, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:15');
INSERT INTO `t_job_log` VALUES (3282, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:16');
INSERT INTO `t_job_log` VALUES (3283, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:17');
INSERT INTO `t_job_log` VALUES (3284, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:18');
INSERT INTO `t_job_log` VALUES (3285, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:19');
INSERT INTO `t_job_log` VALUES (3286, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:21');
INSERT INTO `t_job_log` VALUES (3287, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:21');
INSERT INTO `t_job_log` VALUES (3288, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:22');
INSERT INTO `t_job_log` VALUES (3289, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:23');
INSERT INTO `t_job_log` VALUES (3290, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:24');
INSERT INTO `t_job_log` VALUES (3291, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:25');
INSERT INTO `t_job_log` VALUES (3292, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:26');
INSERT INTO `t_job_log` VALUES (3293, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:27');
INSERT INTO `t_job_log` VALUES (3294, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:28');
INSERT INTO `t_job_log` VALUES (3295, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:29');
INSERT INTO `t_job_log` VALUES (3296, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:30');
INSERT INTO `t_job_log` VALUES (3297, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:31');
INSERT INTO `t_job_log` VALUES (3298, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:32');
INSERT INTO `t_job_log` VALUES (3299, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:33');
INSERT INTO `t_job_log` VALUES (3300, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:34');
INSERT INTO `t_job_log` VALUES (3301, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:35');
INSERT INTO `t_job_log` VALUES (3302, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:36');
INSERT INTO `t_job_log` VALUES (3303, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:37');
INSERT INTO `t_job_log` VALUES (3304, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:38');
INSERT INTO `t_job_log` VALUES (3305, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:39');
INSERT INTO `t_job_log` VALUES (3306, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:40');
INSERT INTO `t_job_log` VALUES (3307, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:41');
INSERT INTO `t_job_log` VALUES (3308, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:42');
INSERT INTO `t_job_log` VALUES (3309, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:43');
INSERT INTO `t_job_log` VALUES (3310, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:44');
INSERT INTO `t_job_log` VALUES (3311, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:45');
INSERT INTO `t_job_log` VALUES (3312, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:46');
INSERT INTO `t_job_log` VALUES (3313, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:47');
INSERT INTO `t_job_log` VALUES (3314, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:48');
INSERT INTO `t_job_log` VALUES (3315, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:49');
INSERT INTO `t_job_log` VALUES (3316, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:50');
INSERT INTO `t_job_log` VALUES (3317, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:51');
INSERT INTO `t_job_log` VALUES (3318, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:52');
INSERT INTO `t_job_log` VALUES (3319, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:53');
INSERT INTO `t_job_log` VALUES (3320, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:53:54');
INSERT INTO `t_job_log` VALUES (3321, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:55');
INSERT INTO `t_job_log` VALUES (3322, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:53:56');
INSERT INTO `t_job_log` VALUES (3323, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:57');
INSERT INTO `t_job_log` VALUES (3324, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:53:58');
INSERT INTO `t_job_log` VALUES (3325, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:53:59');
INSERT INTO `t_job_log` VALUES (3326, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:01');
INSERT INTO `t_job_log` VALUES (3327, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:01');
INSERT INTO `t_job_log` VALUES (3328, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:02');
INSERT INTO `t_job_log` VALUES (3329, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:54:03');
INSERT INTO `t_job_log` VALUES (3330, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:04');
INSERT INTO `t_job_log` VALUES (3331, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:54:05');
INSERT INTO `t_job_log` VALUES (3332, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:06');
INSERT INTO `t_job_log` VALUES (3333, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:07');
INSERT INTO `t_job_log` VALUES (3334, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:08');
INSERT INTO `t_job_log` VALUES (3335, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:09');
INSERT INTO `t_job_log` VALUES (3336, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:10');
INSERT INTO `t_job_log` VALUES (3337, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:11');
INSERT INTO `t_job_log` VALUES (3338, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:12');
INSERT INTO `t_job_log` VALUES (3339, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:54:13');
INSERT INTO `t_job_log` VALUES (3340, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:14');
INSERT INTO `t_job_log` VALUES (3341, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:15');
INSERT INTO `t_job_log` VALUES (3342, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:16');
INSERT INTO `t_job_log` VALUES (3343, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:17');
INSERT INTO `t_job_log` VALUES (3344, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:18');
INSERT INTO `t_job_log` VALUES (3345, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:19');
INSERT INTO `t_job_log` VALUES (3346, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:20');
INSERT INTO `t_job_log` VALUES (3347, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:21');
INSERT INTO `t_job_log` VALUES (3348, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:22');
INSERT INTO `t_job_log` VALUES (3349, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:23');
INSERT INTO `t_job_log` VALUES (3350, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:24');
INSERT INTO `t_job_log` VALUES (3351, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:25');
INSERT INTO `t_job_log` VALUES (3352, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:26');
INSERT INTO `t_job_log` VALUES (3353, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:28');
INSERT INTO `t_job_log` VALUES (3354, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:28');
INSERT INTO `t_job_log` VALUES (3355, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:29');
INSERT INTO `t_job_log` VALUES (3356, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:31');
INSERT INTO `t_job_log` VALUES (3357, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:32');
INSERT INTO `t_job_log` VALUES (3358, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:38');
INSERT INTO `t_job_log` VALUES (3359, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:38');
INSERT INTO `t_job_log` VALUES (3360, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:42');
INSERT INTO `t_job_log` VALUES (3361, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:42');
INSERT INTO `t_job_log` VALUES (3362, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:43');
INSERT INTO `t_job_log` VALUES (3363, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:54:43');
INSERT INTO `t_job_log` VALUES (3364, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:44');
INSERT INTO `t_job_log` VALUES (3365, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:44');
INSERT INTO `t_job_log` VALUES (3366, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:45');
INSERT INTO `t_job_log` VALUES (3367, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:45');
INSERT INTO `t_job_log` VALUES (3368, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:45');
INSERT INTO `t_job_log` VALUES (3369, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:51');
INSERT INTO `t_job_log` VALUES (3370, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:52');
INSERT INTO `t_job_log` VALUES (3371, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:53');
INSERT INTO `t_job_log` VALUES (3372, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:53');
INSERT INTO `t_job_log` VALUES (3373, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:53');
INSERT INTO `t_job_log` VALUES (3374, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:54');
INSERT INTO `t_job_log` VALUES (3375, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:54');
INSERT INTO `t_job_log` VALUES (3376, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:55');
INSERT INTO `t_job_log` VALUES (3377, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:55');
INSERT INTO `t_job_log` VALUES (3378, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:56');
INSERT INTO `t_job_log` VALUES (3379, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:56');
INSERT INTO `t_job_log` VALUES (3380, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:54:57');
INSERT INTO `t_job_log` VALUES (3381, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:57');
INSERT INTO `t_job_log` VALUES (3382, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:57');
INSERT INTO `t_job_log` VALUES (3383, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:57');
INSERT INTO `t_job_log` VALUES (3384, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:58');
INSERT INTO `t_job_log` VALUES (3385, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:54:59');
INSERT INTO `t_job_log` VALUES (3386, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:00');
INSERT INTO `t_job_log` VALUES (3387, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:01');
INSERT INTO `t_job_log` VALUES (3388, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:55:02');
INSERT INTO `t_job_log` VALUES (3389, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:03');
INSERT INTO `t_job_log` VALUES (3390, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:55:04');
INSERT INTO `t_job_log` VALUES (3391, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:05');
INSERT INTO `t_job_log` VALUES (3392, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:06');
INSERT INTO `t_job_log` VALUES (3393, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:07');
INSERT INTO `t_job_log` VALUES (3394, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:55:08');
INSERT INTO `t_job_log` VALUES (3395, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:55:09');
INSERT INTO `t_job_log` VALUES (3396, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:55:10');
INSERT INTO `t_job_log` VALUES (3397, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:55:11');
INSERT INTO `t_job_log` VALUES (3398, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:12');
INSERT INTO `t_job_log` VALUES (3399, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:13');
INSERT INTO `t_job_log` VALUES (3400, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:14');
INSERT INTO `t_job_log` VALUES (3401, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:15');
INSERT INTO `t_job_log` VALUES (3402, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:55:16');
INSERT INTO `t_job_log` VALUES (3403, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:17');
INSERT INTO `t_job_log` VALUES (3404, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:18');
INSERT INTO `t_job_log` VALUES (3405, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:19');
INSERT INTO `t_job_log` VALUES (3406, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:55:20');
INSERT INTO `t_job_log` VALUES (3407, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:21');
INSERT INTO `t_job_log` VALUES (3408, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:55:22');
INSERT INTO `t_job_log` VALUES (3409, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:23');
INSERT INTO `t_job_log` VALUES (3410, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:24');
INSERT INTO `t_job_log` VALUES (3411, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:55:25');
INSERT INTO `t_job_log` VALUES (3412, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:55:26');
INSERT INTO `t_job_log` VALUES (3413, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:27');
INSERT INTO `t_job_log` VALUES (3414, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:55:28');
INSERT INTO `t_job_log` VALUES (3415, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:29');
INSERT INTO `t_job_log` VALUES (3416, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:55:30');
INSERT INTO `t_job_log` VALUES (3417, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:31');
INSERT INTO `t_job_log` VALUES (3418, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:55:32');
INSERT INTO `t_job_log` VALUES (3419, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:33');
INSERT INTO `t_job_log` VALUES (3420, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:55:34');
INSERT INTO `t_job_log` VALUES (3421, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:35');
INSERT INTO `t_job_log` VALUES (3422, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:55:36');
INSERT INTO `t_job_log` VALUES (3423, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:55:37');
INSERT INTO `t_job_log` VALUES (3424, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:38');
INSERT INTO `t_job_log` VALUES (3425, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2020-03-23 09:55:39');
INSERT INTO `t_job_log` VALUES (3426, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:55:40');
INSERT INTO `t_job_log` VALUES (3427, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-03-23 09:55:41');
INSERT INTO `t_job_log` VALUES (3428, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-03-23 09:55:42');
INSERT INTO `t_job_log` VALUES (3429, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 6, '2020-03-23 10:22:45');
INSERT INTO `t_job_log` VALUES (3430, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 4, '2020-03-23 10:22:50');
INSERT INTO `t_job_log` VALUES (3431, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 2, '2020-03-23 10:22:55');
INSERT INTO `t_job_log` VALUES (3432, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 7, '2020-03-23 10:23:00');
INSERT INTO `t_job_log` VALUES (3433, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 4, '2020-03-23 10:23:05');
INSERT INTO `t_job_log` VALUES (3434, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 2, '2020-03-23 10:23:10');
INSERT INTO `t_job_log` VALUES (3435, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-23 10:23:15');
INSERT INTO `t_job_log` VALUES (3436, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-23 10:23:21');
INSERT INTO `t_job_log` VALUES (3437, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-03-23 10:23:25');
INSERT INTO `t_job_log` VALUES (3438, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-23 10:23:30');
INSERT INTO `t_job_log` VALUES (3439, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 2, '2020-03-23 10:23:35');
INSERT INTO `t_job_log` VALUES (3440, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 5, '2020-03-23 10:23:40');
INSERT INTO `t_job_log` VALUES (3441, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-03-23 10:23:45');
INSERT INTO `t_job_log` VALUES (3442, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 10, '2020-03-23 10:23:50');
INSERT INTO `t_job_log` VALUES (3443, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 6, '2020-03-23 10:23:55');
INSERT INTO `t_job_log` VALUES (3444, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-23 10:24:00');
INSERT INTO `t_job_log` VALUES (3445, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-03-23 10:24:06');
INSERT INTO `t_job_log` VALUES (3446, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 2, '2020-03-23 10:24:10');
INSERT INTO `t_job_log` VALUES (3447, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 2, '2020-03-23 10:24:15');
INSERT INTO `t_job_log` VALUES (3448, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-03-23 10:24:21');
INSERT INTO `t_job_log` VALUES (3449, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 4, '2020-03-23 10:24:25');
INSERT INTO `t_job_log` VALUES (3450, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 4, '2020-03-23 10:24:30');
INSERT INTO `t_job_log` VALUES (3451, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-23 10:24:35');
INSERT INTO `t_job_log` VALUES (3452, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 3, '2020-03-23 10:24:40');
INSERT INTO `t_job_log` VALUES (3453, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-23 10:24:45');
INSERT INTO `t_job_log` VALUES (3454, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 2, '2020-03-23 10:24:50');
INSERT INTO `t_job_log` VALUES (3455, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-23 10:24:55');
INSERT INTO `t_job_log` VALUES (3456, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 5, '2020-03-23 10:25:00');
INSERT INTO `t_job_log` VALUES (3457, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 2, '2020-03-23 10:25:05');
INSERT INTO `t_job_log` VALUES (3458, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-23 10:25:10');
INSERT INTO `t_job_log` VALUES (3459, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 2, '2020-03-23 10:25:15');
INSERT INTO `t_job_log` VALUES (3460, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-23 10:25:20');
INSERT INTO `t_job_log` VALUES (3461, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 12, '2020-03-23 10:25:25');
INSERT INTO `t_job_log` VALUES (3462, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-23 10:25:30');
INSERT INTO `t_job_log` VALUES (3463, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-23 10:25:35');
INSERT INTO `t_job_log` VALUES (3464, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-03-28 16:59:37');
INSERT INTO `t_job_log` VALUES (3465, 2, 'testTask', 'test1', NULL, '0', NULL, 19, '2020-04-01 10:35:00');
INSERT INTO `t_job_log` VALUES (3466, 2, 'testTask', 'test1', NULL, '0', NULL, 20, '2020-04-01 10:35:10');
INSERT INTO `t_job_log` VALUES (3467, 2, 'testTask', 'test1', NULL, '0', NULL, 2, '2020-04-01 10:35:20');
INSERT INTO `t_job_log` VALUES (3468, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2020-04-01 10:35:30');
INSERT INTO `t_job_log` VALUES (3469, 2, 'testTask', 'test1', NULL, '0', NULL, 2, '2020-04-01 10:35:40');
INSERT INTO `t_job_log` VALUES (3470, 2, 'testTask', 'test1', NULL, '0', NULL, 2, '2020-04-01 10:35:50');
INSERT INTO `t_job_log` VALUES (3471, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2020-04-01 10:36:00');
INSERT INTO `t_job_log` VALUES (3472, 2, 'testTask', 'test1', NULL, '0', NULL, 2, '2020-04-01 10:36:10');
INSERT INTO `t_job_log` VALUES (3473, 2, 'testTask', 'test1', NULL, '0', NULL, 2, '2020-04-01 10:36:20');
INSERT INTO `t_job_log` VALUES (3474, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2020-04-01 10:36:30');
INSERT INTO `t_job_log` VALUES (3475, 2, 'testTask', 'test1', NULL, '0', NULL, 2, '2020-04-01 10:36:40');
INSERT INTO `t_job_log` VALUES (3476, 2, 'testTask', 'test1', NULL, '0', NULL, 16, '2020-04-01 10:36:50');
INSERT INTO `t_job_log` VALUES (3477, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2020-04-01 10:37:00');
INSERT INTO `t_job_log` VALUES (3478, 2, 'testTask', 'test1', NULL, '0', NULL, 2, '2020-04-01 10:37:10');
INSERT INTO `t_job_log` VALUES (3479, 2, 'testTask', 'test1', NULL, '0', NULL, 4, '2020-04-01 10:37:20');
INSERT INTO `t_job_log` VALUES (3480, 1, 'testTask', 'test', 'mrbird', '0', NULL, 216, '2020-04-02 11:45:17');
INSERT INTO `t_job_log` VALUES (3481, 1, 'testTask', 'test', 'mrbird', '0', NULL, 65, '2020-04-02 11:45:18');
INSERT INTO `t_job_log` VALUES (3482, 1, 'testTask', 'test', 'mrbird', '0', NULL, 229, '2020-04-02 11:45:17');
INSERT INTO `t_job_log` VALUES (3483, 1, 'testTask', 'test', 'mrbird', '0', NULL, 216, '2020-04-02 11:45:17');
INSERT INTO `t_job_log` VALUES (3484, 1, 'testTask', 'test', 'mrbird', '0', NULL, 195, '2020-04-02 11:45:17');
INSERT INTO `t_job_log` VALUES (3485, 1, 'testTask', 'test', 'mrbird', '0', NULL, 49, '2020-04-02 11:45:17');
INSERT INTO `t_job_log` VALUES (3486, 1, 'testTask', 'test', 'mrbird', '0', NULL, 143, '2020-04-02 11:45:19');
INSERT INTO `t_job_log` VALUES (3487, 1, 'testTask', 'test', 'mrbird', '0', NULL, 271, '2020-04-02 11:45:20');
INSERT INTO `t_job_log` VALUES (3488, 1, 'testTask', 'test', 'mrbird', '0', NULL, 34, '2020-04-02 11:45:21');
INSERT INTO `t_job_log` VALUES (3489, 1, 'testTask', 'test', 'mrbird', '0', NULL, 6, '2020-04-02 11:45:22');
INSERT INTO `t_job_log` VALUES (3490, 1, 'testTask', 'test', 'mrbird', '0', NULL, 6, '2020-04-02 11:45:23');
INSERT INTO `t_job_log` VALUES (3491, 1, 'testTask', 'test', 'mrbird', '0', NULL, 14, '2020-04-02 11:45:24');
INSERT INTO `t_job_log` VALUES (3492, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:45:25');
INSERT INTO `t_job_log` VALUES (3493, 1, 'testTask', 'test', 'mrbird', '0', NULL, 31, '2020-04-02 11:45:26');
INSERT INTO `t_job_log` VALUES (3494, 1, 'testTask', 'test', 'mrbird', '0', NULL, 34, '2020-04-02 11:45:27');
INSERT INTO `t_job_log` VALUES (3495, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:45:28');
INSERT INTO `t_job_log` VALUES (3496, 1, 'testTask', 'test', 'mrbird', '0', NULL, 11, '2020-04-02 11:45:29');
INSERT INTO `t_job_log` VALUES (3497, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-02 11:45:30');
INSERT INTO `t_job_log` VALUES (3498, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:45:31');
INSERT INTO `t_job_log` VALUES (3499, 1, 'testTask', 'test', 'mrbird', '0', NULL, 24, '2020-04-02 11:45:32');
INSERT INTO `t_job_log` VALUES (3500, 1, 'testTask', 'test', 'mrbird', '0', NULL, 26, '2020-04-02 11:45:33');
INSERT INTO `t_job_log` VALUES (3501, 1, 'testTask', 'test', 'mrbird', '0', NULL, 14, '2020-04-02 11:45:34');
INSERT INTO `t_job_log` VALUES (3502, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:45:35');
INSERT INTO `t_job_log` VALUES (3503, 1, 'testTask', 'test', 'mrbird', '0', NULL, 10, '2020-04-02 11:45:36');
INSERT INTO `t_job_log` VALUES (3504, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-02 11:45:37');
INSERT INTO `t_job_log` VALUES (3505, 1, 'testTask', 'test', 'mrbird', '0', NULL, 24, '2020-04-02 11:45:38');
INSERT INTO `t_job_log` VALUES (3506, 1, 'testTask', 'test', 'mrbird', '0', NULL, 52, '2020-04-02 11:45:39');
INSERT INTO `t_job_log` VALUES (3507, 1, 'testTask', 'test', 'mrbird', '0', NULL, 14, '2020-04-02 11:45:40');
INSERT INTO `t_job_log` VALUES (3508, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:45:41');
INSERT INTO `t_job_log` VALUES (3509, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:45:42');
INSERT INTO `t_job_log` VALUES (3510, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:45:43');
INSERT INTO `t_job_log` VALUES (3511, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:45:44');
INSERT INTO `t_job_log` VALUES (3512, 1, 'testTask', 'test', 'mrbird', '0', NULL, 52, '2020-04-02 11:45:45');
INSERT INTO `t_job_log` VALUES (3513, 1, 'testTask', 'test', 'mrbird', '0', NULL, 28, '2020-04-02 11:45:46');
INSERT INTO `t_job_log` VALUES (3514, 1, 'testTask', 'test', 'mrbird', '0', NULL, 13, '2020-04-02 11:45:47');
INSERT INTO `t_job_log` VALUES (3515, 1, 'testTask', 'test', 'mrbird', '0', NULL, 13, '2020-04-02 11:45:48');
INSERT INTO `t_job_log` VALUES (3516, 1, 'testTask', 'test', 'mrbird', '0', NULL, 13, '2020-04-02 11:45:49');
INSERT INTO `t_job_log` VALUES (3517, 1, 'testTask', 'test', 'mrbird', '0', NULL, 11, '2020-04-02 11:45:50');
INSERT INTO `t_job_log` VALUES (3518, 1, 'testTask', 'test', 'mrbird', '0', NULL, 27, '2020-04-02 11:45:51');
INSERT INTO `t_job_log` VALUES (3519, 1, 'testTask', 'test', 'mrbird', '0', NULL, 31, '2020-04-02 11:45:52');
INSERT INTO `t_job_log` VALUES (3520, 1, 'testTask', 'test', 'mrbird', '0', NULL, 26, '2020-04-02 11:45:53');
INSERT INTO `t_job_log` VALUES (3521, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:45:54');
INSERT INTO `t_job_log` VALUES (3522, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:45:55');
INSERT INTO `t_job_log` VALUES (3523, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:45:56');
INSERT INTO `t_job_log` VALUES (3524, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:45:57');
INSERT INTO `t_job_log` VALUES (3525, 1, 'testTask', 'test', 'mrbird', '0', NULL, 5, '2020-04-02 11:45:59');
INSERT INTO `t_job_log` VALUES (3526, 1, 'testTask', 'test', 'mrbird', '0', NULL, 15, '2020-04-02 11:46:00');
INSERT INTO `t_job_log` VALUES (3527, 1, 'testTask', 'test', 'mrbird', '0', NULL, 132, '2020-04-02 11:46:00');
INSERT INTO `t_job_log` VALUES (3528, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:46:01');
INSERT INTO `t_job_log` VALUES (3529, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:46:02');
INSERT INTO `t_job_log` VALUES (3530, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:46:03');
INSERT INTO `t_job_log` VALUES (3531, 1, 'testTask', 'test', 'mrbird', '0', NULL, 49, '2020-04-02 11:46:04');
INSERT INTO `t_job_log` VALUES (3532, 1, 'testTask', 'test', 'mrbird', '0', NULL, 38, '2020-04-02 11:46:05');
INSERT INTO `t_job_log` VALUES (3533, 1, 'testTask', 'test', 'mrbird', '0', NULL, 60, '2020-04-02 11:46:06');
INSERT INTO `t_job_log` VALUES (3534, 1, 'testTask', 'test', 'mrbird', '0', NULL, 86, '2020-04-02 11:46:07');
INSERT INTO `t_job_log` VALUES (3535, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:46:08');
INSERT INTO `t_job_log` VALUES (3536, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:46:09');
INSERT INTO `t_job_log` VALUES (3537, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-02 11:46:10');
INSERT INTO `t_job_log` VALUES (3538, 1, 'testTask', 'test', 'mrbird', '0', NULL, 34, '2020-04-02 11:46:11');
INSERT INTO `t_job_log` VALUES (3539, 1, 'testTask', 'test', 'mrbird', '0', NULL, 24, '2020-04-02 11:46:12');
INSERT INTO `t_job_log` VALUES (3540, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:46:13');
INSERT INTO `t_job_log` VALUES (3541, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:46:14');
INSERT INTO `t_job_log` VALUES (3542, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:46:15');
INSERT INTO `t_job_log` VALUES (3543, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:46:16');
INSERT INTO `t_job_log` VALUES (3544, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:46:17');
INSERT INTO `t_job_log` VALUES (3545, 1, 'testTask', 'test', 'mrbird', '0', NULL, 27, '2020-04-02 11:46:18');
INSERT INTO `t_job_log` VALUES (3546, 1, 'testTask', 'test', 'mrbird', '0', NULL, 11, '2020-04-02 11:46:19');
INSERT INTO `t_job_log` VALUES (3547, 1, 'testTask', 'test', 'mrbird', '0', NULL, 45, '2020-04-02 11:46:20');
INSERT INTO `t_job_log` VALUES (3548, 1, 'testTask', 'test', 'mrbird', '0', NULL, 8, '2020-04-02 11:46:21');
INSERT INTO `t_job_log` VALUES (3549, 1, 'testTask', 'test', 'mrbird', '0', NULL, 10, '2020-04-02 11:46:22');
INSERT INTO `t_job_log` VALUES (3550, 1, 'testTask', 'test', 'mrbird', '0', NULL, 41, '2020-04-02 11:46:23');
INSERT INTO `t_job_log` VALUES (3551, 1, 'testTask', 'test', 'mrbird', '0', NULL, 26, '2020-04-02 11:46:24');
INSERT INTO `t_job_log` VALUES (3552, 1, 'testTask', 'test', 'mrbird', '0', NULL, 37, '2020-04-02 11:46:25');
INSERT INTO `t_job_log` VALUES (3553, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:46:26');
INSERT INTO `t_job_log` VALUES (3554, 1, 'testTask', 'test', 'mrbird', '0', NULL, 63, '2020-04-02 11:46:27');
INSERT INTO `t_job_log` VALUES (3555, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-02 11:46:28');
INSERT INTO `t_job_log` VALUES (3556, 1, 'testTask', 'test', 'mrbird', '0', NULL, 37, '2020-04-02 11:46:29');
INSERT INTO `t_job_log` VALUES (3557, 1, 'testTask', 'test', 'mrbird', '0', NULL, 52, '2020-04-02 11:46:30');
INSERT INTO `t_job_log` VALUES (3558, 1, 'testTask', 'test', 'mrbird', '0', NULL, 12, '2020-04-02 11:46:31');
INSERT INTO `t_job_log` VALUES (3559, 1, 'testTask', 'test', 'mrbird', '0', NULL, 13, '2020-04-02 11:46:32');
INSERT INTO `t_job_log` VALUES (3560, 1, 'testTask', 'test', 'mrbird', '0', NULL, 8, '2020-04-02 11:46:33');
INSERT INTO `t_job_log` VALUES (3561, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:46:34');
INSERT INTO `t_job_log` VALUES (3562, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:46:35');
INSERT INTO `t_job_log` VALUES (3563, 1, 'testTask', 'test', 'mrbird', '0', NULL, 39, '2020-04-02 11:46:36');
INSERT INTO `t_job_log` VALUES (3564, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:46:37');
INSERT INTO `t_job_log` VALUES (3565, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:46:38');
INSERT INTO `t_job_log` VALUES (3566, 1, 'testTask', 'test', 'mrbird', '0', NULL, 26, '2020-04-02 11:46:39');
INSERT INTO `t_job_log` VALUES (3567, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:46:40');
INSERT INTO `t_job_log` VALUES (3568, 1, 'testTask', 'test', 'mrbird', '0', NULL, 24, '2020-04-02 11:46:41');
INSERT INTO `t_job_log` VALUES (3569, 1, 'testTask', 'test', 'mrbird', '0', NULL, 58, '2020-04-02 11:46:42');
INSERT INTO `t_job_log` VALUES (3570, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:46:43');
INSERT INTO `t_job_log` VALUES (3571, 1, 'testTask', 'test', 'mrbird', '0', NULL, 17, '2020-04-02 11:46:44');
INSERT INTO `t_job_log` VALUES (3572, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:46:45');
INSERT INTO `t_job_log` VALUES (3573, 1, 'testTask', 'test', 'mrbird', '0', NULL, 13, '2020-04-02 11:46:46');
INSERT INTO `t_job_log` VALUES (3574, 1, 'testTask', 'test', 'mrbird', '0', NULL, 20, '2020-04-02 11:46:47');
INSERT INTO `t_job_log` VALUES (3575, 1, 'testTask', 'test', 'mrbird', '0', NULL, 26, '2020-04-02 11:46:48');
INSERT INTO `t_job_log` VALUES (3576, 1, 'testTask', 'test', 'mrbird', '0', NULL, 45, '2020-04-02 11:46:49');
INSERT INTO `t_job_log` VALUES (3577, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-02 11:46:50');
INSERT INTO `t_job_log` VALUES (3578, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:46:51');
INSERT INTO `t_job_log` VALUES (3579, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:46:52');
INSERT INTO `t_job_log` VALUES (3580, 1, 'testTask', 'test', 'mrbird', '0', NULL, 15, '2020-04-02 11:46:53');
INSERT INTO `t_job_log` VALUES (3581, 1, 'testTask', 'test', 'mrbird', '0', NULL, 12, '2020-04-02 11:46:54');
INSERT INTO `t_job_log` VALUES (3582, 1, 'testTask', 'test', 'mrbird', '0', NULL, 6, '2020-04-02 11:46:55');
INSERT INTO `t_job_log` VALUES (3583, 1, 'testTask', 'test', 'mrbird', '0', NULL, 8, '2020-04-02 11:46:56');
INSERT INTO `t_job_log` VALUES (3584, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:46:57');
INSERT INTO `t_job_log` VALUES (3585, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-02 11:46:58');
INSERT INTO `t_job_log` VALUES (3586, 1, 'testTask', 'test', 'mrbird', '0', NULL, 13, '2020-04-02 11:46:59');
INSERT INTO `t_job_log` VALUES (3587, 1, 'testTask', 'test', 'mrbird', '0', NULL, 15, '2020-04-02 11:47:00');
INSERT INTO `t_job_log` VALUES (3588, 1, 'testTask', 'test', 'mrbird', '0', NULL, 36, '2020-04-02 11:47:01');
INSERT INTO `t_job_log` VALUES (3589, 1, 'testTask', 'test', 'mrbird', '0', NULL, 13, '2020-04-02 11:47:02');
INSERT INTO `t_job_log` VALUES (3590, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:03');
INSERT INTO `t_job_log` VALUES (3591, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:04');
INSERT INTO `t_job_log` VALUES (3592, 1, 'testTask', 'test', 'mrbird', '0', NULL, 14, '2020-04-02 11:47:05');
INSERT INTO `t_job_log` VALUES (3593, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:47:06');
INSERT INTO `t_job_log` VALUES (3594, 1, 'testTask', 'test', 'mrbird', '0', NULL, 5, '2020-04-02 11:47:07');
INSERT INTO `t_job_log` VALUES (3595, 1, 'testTask', 'test', 'mrbird', '0', NULL, 26, '2020-04-02 11:47:08');
INSERT INTO `t_job_log` VALUES (3596, 1, 'testTask', 'test', 'mrbird', '0', NULL, 29, '2020-04-02 11:47:09');
INSERT INTO `t_job_log` VALUES (3597, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:47:10');
INSERT INTO `t_job_log` VALUES (3598, 1, 'testTask', 'test', 'mrbird', '0', NULL, 27, '2020-04-02 11:47:11');
INSERT INTO `t_job_log` VALUES (3599, 1, 'testTask', 'test', 'mrbird', '0', NULL, 17, '2020-04-02 11:47:12');
INSERT INTO `t_job_log` VALUES (3600, 1, 'testTask', 'test', 'mrbird', '0', NULL, 11, '2020-04-02 11:47:13');
INSERT INTO `t_job_log` VALUES (3601, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:47:14');
INSERT INTO `t_job_log` VALUES (3602, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:47:15');
INSERT INTO `t_job_log` VALUES (3603, 1, 'testTask', 'test', 'mrbird', '0', NULL, 10, '2020-04-02 11:47:16');
INSERT INTO `t_job_log` VALUES (3604, 1, 'testTask', 'test', 'mrbird', '0', NULL, 17, '2020-04-02 11:47:17');
INSERT INTO `t_job_log` VALUES (3605, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:18');
INSERT INTO `t_job_log` VALUES (3606, 1, 'testTask', 'test', 'mrbird', '0', NULL, 33, '2020-04-02 11:47:19');
INSERT INTO `t_job_log` VALUES (3607, 1, 'testTask', 'test', 'mrbird', '0', NULL, 11, '2020-04-02 11:47:20');
INSERT INTO `t_job_log` VALUES (3608, 1, 'testTask', 'test', 'mrbird', '0', NULL, 12, '2020-04-02 11:47:21');
INSERT INTO `t_job_log` VALUES (3609, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:47:22');
INSERT INTO `t_job_log` VALUES (3610, 1, 'testTask', 'test', 'mrbird', '0', NULL, 54, '2020-04-02 11:47:23');
INSERT INTO `t_job_log` VALUES (3611, 1, 'testTask', 'test', 'mrbird', '0', NULL, 25, '2020-04-02 11:47:24');
INSERT INTO `t_job_log` VALUES (3612, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:25');
INSERT INTO `t_job_log` VALUES (3613, 1, 'testTask', 'test', 'mrbird', '0', NULL, 22, '2020-04-02 11:47:26');
INSERT INTO `t_job_log` VALUES (3614, 1, 'testTask', 'test', 'mrbird', '0', NULL, 28, '2020-04-02 11:47:27');
INSERT INTO `t_job_log` VALUES (3615, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:47:28');
INSERT INTO `t_job_log` VALUES (3616, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:29');
INSERT INTO `t_job_log` VALUES (3617, 1, 'testTask', 'test', 'mrbird', '0', NULL, 65, '2020-04-02 11:47:30');
INSERT INTO `t_job_log` VALUES (3618, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:47:31');
INSERT INTO `t_job_log` VALUES (3619, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:32');
INSERT INTO `t_job_log` VALUES (3620, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:33');
INSERT INTO `t_job_log` VALUES (3621, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:34');
INSERT INTO `t_job_log` VALUES (3622, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:35');
INSERT INTO `t_job_log` VALUES (3623, 1, 'testTask', 'test', 'mrbird', '0', NULL, 23, '2020-04-02 11:47:36');
INSERT INTO `t_job_log` VALUES (3624, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:37');
INSERT INTO `t_job_log` VALUES (3625, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:38');
INSERT INTO `t_job_log` VALUES (3626, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:39');
INSERT INTO `t_job_log` VALUES (3627, 1, 'testTask', 'test', 'mrbird', '0', NULL, 67, '2020-04-02 11:47:40');
INSERT INTO `t_job_log` VALUES (3628, 1, 'testTask', 'test', 'mrbird', '0', NULL, 12, '2020-04-02 11:47:41');
INSERT INTO `t_job_log` VALUES (3629, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:47:42');
INSERT INTO `t_job_log` VALUES (3630, 1, 'testTask', 'test', 'mrbird', '0', NULL, 46, '2020-04-02 11:47:43');
INSERT INTO `t_job_log` VALUES (3631, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:44');
INSERT INTO `t_job_log` VALUES (3632, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:45');
INSERT INTO `t_job_log` VALUES (3633, 1, 'testTask', 'test', 'mrbird', '0', NULL, 196, '2020-04-02 11:47:46');
INSERT INTO `t_job_log` VALUES (3634, 1, 'testTask', 'test', 'mrbird', '0', NULL, 20, '2020-04-02 11:47:47');
INSERT INTO `t_job_log` VALUES (3635, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:48');
INSERT INTO `t_job_log` VALUES (3636, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:49');
INSERT INTO `t_job_log` VALUES (3637, 1, 'testTask', 'test', 'mrbird', '0', NULL, 59, '2020-04-02 11:47:50');
INSERT INTO `t_job_log` VALUES (3638, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:51');
INSERT INTO `t_job_log` VALUES (3639, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:52');
INSERT INTO `t_job_log` VALUES (3640, 1, 'testTask', 'test', 'mrbird', '0', NULL, 21, '2020-04-02 11:47:53');
INSERT INTO `t_job_log` VALUES (3641, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:47:54');
INSERT INTO `t_job_log` VALUES (3642, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:47:55');
INSERT INTO `t_job_log` VALUES (3643, 1, 'testTask', 'test', 'mrbird', '0', NULL, 11, '2020-04-02 11:47:56');
INSERT INTO `t_job_log` VALUES (3644, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:57');
INSERT INTO `t_job_log` VALUES (3645, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:47:58');
INSERT INTO `t_job_log` VALUES (3646, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-02 11:47:59');
INSERT INTO `t_job_log` VALUES (3647, 1, 'testTask', 'test', 'mrbird', '0', NULL, 36, '2020-04-02 11:48:00');
INSERT INTO `t_job_log` VALUES (3648, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:48:01');
INSERT INTO `t_job_log` VALUES (3649, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:02');
INSERT INTO `t_job_log` VALUES (3650, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:03');
INSERT INTO `t_job_log` VALUES (3651, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:04');
INSERT INTO `t_job_log` VALUES (3652, 1, 'testTask', 'test', 'mrbird', '0', NULL, 10, '2020-04-02 11:48:05');
INSERT INTO `t_job_log` VALUES (3653, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-02 11:48:06');
INSERT INTO `t_job_log` VALUES (3654, 1, 'testTask', 'test', 'mrbird', '0', NULL, 28, '2020-04-02 11:48:07');
INSERT INTO `t_job_log` VALUES (3655, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:48:08');
INSERT INTO `t_job_log` VALUES (3656, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:48:09');
INSERT INTO `t_job_log` VALUES (3657, 1, 'testTask', 'test', 'mrbird', '0', NULL, 100, '2020-04-02 11:48:10');
INSERT INTO `t_job_log` VALUES (3658, 1, 'testTask', 'test', 'mrbird', '0', NULL, 24, '2020-04-02 11:48:11');
INSERT INTO `t_job_log` VALUES (3659, 1, 'testTask', 'test', 'mrbird', '0', NULL, 6, '2020-04-02 11:48:12');
INSERT INTO `t_job_log` VALUES (3660, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:13');
INSERT INTO `t_job_log` VALUES (3661, 1, 'testTask', 'test', 'mrbird', '0', NULL, 25, '2020-04-02 11:48:14');
INSERT INTO `t_job_log` VALUES (3662, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:15');
INSERT INTO `t_job_log` VALUES (3663, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:16');
INSERT INTO `t_job_log` VALUES (3664, 1, 'testTask', 'test', 'mrbird', '0', NULL, 68, '2020-04-02 11:48:17');
INSERT INTO `t_job_log` VALUES (3665, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:18');
INSERT INTO `t_job_log` VALUES (3666, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:19');
INSERT INTO `t_job_log` VALUES (3667, 1, 'testTask', 'test', 'mrbird', '0', NULL, 84, '2020-04-02 11:48:20');
INSERT INTO `t_job_log` VALUES (3668, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:21');
INSERT INTO `t_job_log` VALUES (3669, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:22');
INSERT INTO `t_job_log` VALUES (3670, 1, 'testTask', 'test', 'mrbird', '0', NULL, 28, '2020-04-02 11:48:23');
INSERT INTO `t_job_log` VALUES (3671, 1, 'testTask', 'test', 'mrbird', '0', NULL, 11, '2020-04-02 11:48:24');
INSERT INTO `t_job_log` VALUES (3672, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-02 11:48:25');
INSERT INTO `t_job_log` VALUES (3673, 1, 'testTask', 'test', 'mrbird', '0', NULL, 60, '2020-04-02 11:48:26');
INSERT INTO `t_job_log` VALUES (3674, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:48:27');
INSERT INTO `t_job_log` VALUES (3675, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:28');
INSERT INTO `t_job_log` VALUES (3676, 1, 'testTask', 'test', 'mrbird', '0', NULL, 42, '2020-04-02 11:48:29');
INSERT INTO `t_job_log` VALUES (3677, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:48:30');
INSERT INTO `t_job_log` VALUES (3678, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:48:31');
INSERT INTO `t_job_log` VALUES (3679, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:32');
INSERT INTO `t_job_log` VALUES (3680, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:48:33');
INSERT INTO `t_job_log` VALUES (3681, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:34');
INSERT INTO `t_job_log` VALUES (3682, 1, 'testTask', 'test', 'mrbird', '0', NULL, 21, '2020-04-02 11:48:35');
INSERT INTO `t_job_log` VALUES (3683, 1, 'testTask', 'test', 'mrbird', '0', NULL, 10, '2020-04-02 11:48:36');
INSERT INTO `t_job_log` VALUES (3684, 1, 'testTask', 'test', 'mrbird', '0', NULL, 5, '2020-04-02 11:48:37');
INSERT INTO `t_job_log` VALUES (3685, 1, 'testTask', 'test', 'mrbird', '0', NULL, 24, '2020-04-02 11:48:38');
INSERT INTO `t_job_log` VALUES (3686, 1, 'testTask', 'test', 'mrbird', '0', NULL, 5, '2020-04-02 11:48:39');
INSERT INTO `t_job_log` VALUES (3687, 1, 'testTask', 'test', 'mrbird', '0', NULL, 46, '2020-04-02 11:48:40');
INSERT INTO `t_job_log` VALUES (3688, 1, 'testTask', 'test', 'mrbird', '0', NULL, 41, '2020-04-02 11:48:41');
INSERT INTO `t_job_log` VALUES (3689, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:42');
INSERT INTO `t_job_log` VALUES (3690, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-02 11:48:43');
INSERT INTO `t_job_log` VALUES (3691, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:44');
INSERT INTO `t_job_log` VALUES (3692, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:45');
INSERT INTO `t_job_log` VALUES (3693, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:48:46');
INSERT INTO `t_job_log` VALUES (3694, 1, 'testTask', 'test', 'mrbird', '0', NULL, 47, '2020-04-02 11:48:47');
INSERT INTO `t_job_log` VALUES (3695, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:48');
INSERT INTO `t_job_log` VALUES (3696, 1, 'testTask', 'test', 'mrbird', '0', NULL, 24, '2020-04-02 11:48:49');
INSERT INTO `t_job_log` VALUES (3697, 1, 'testTask', 'test', 'mrbird', '0', NULL, 111, '2020-04-02 11:48:50');
INSERT INTO `t_job_log` VALUES (3698, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:48:51');
INSERT INTO `t_job_log` VALUES (3699, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:52');
INSERT INTO `t_job_log` VALUES (3700, 1, 'testTask', 'test', 'mrbird', '0', NULL, 42, '2020-04-02 11:48:53');
INSERT INTO `t_job_log` VALUES (3701, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:54');
INSERT INTO `t_job_log` VALUES (3702, 1, 'testTask', 'test', 'mrbird', '0', NULL, 10, '2020-04-02 11:48:55');
INSERT INTO `t_job_log` VALUES (3703, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:48:56');
INSERT INTO `t_job_log` VALUES (3704, 1, 'testTask', 'test', 'mrbird', '0', NULL, 19, '2020-04-02 11:48:57');
INSERT INTO `t_job_log` VALUES (3705, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:48:58');
INSERT INTO `t_job_log` VALUES (3706, 1, 'testTask', 'test', 'mrbird', '0', NULL, 12, '2020-04-02 11:48:59');
INSERT INTO `t_job_log` VALUES (3707, 1, 'testTask', 'test', 'mrbird', '0', NULL, 60, '2020-04-02 11:49:00');
INSERT INTO `t_job_log` VALUES (3708, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:01');
INSERT INTO `t_job_log` VALUES (3709, 1, 'testTask', 'test', 'mrbird', '0', NULL, 28, '2020-04-02 11:49:02');
INSERT INTO `t_job_log` VALUES (3710, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:03');
INSERT INTO `t_job_log` VALUES (3711, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:04');
INSERT INTO `t_job_log` VALUES (3712, 1, 'testTask', 'test', 'mrbird', '0', NULL, 25, '2020-04-02 11:49:05');
INSERT INTO `t_job_log` VALUES (3713, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:49:06');
INSERT INTO `t_job_log` VALUES (3714, 1, 'testTask', 'test', 'mrbird', '0', NULL, 156, '2020-04-02 11:49:07');
INSERT INTO `t_job_log` VALUES (3715, 1, 'testTask', 'test', 'mrbird', '0', NULL, 7, '2020-04-02 11:49:08');
INSERT INTO `t_job_log` VALUES (3716, 1, 'testTask', 'test', 'mrbird', '0', NULL, 10, '2020-04-02 11:49:09');
INSERT INTO `t_job_log` VALUES (3717, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:10');
INSERT INTO `t_job_log` VALUES (3718, 1, 'testTask', 'test', 'mrbird', '0', NULL, 22, '2020-04-02 11:49:11');
INSERT INTO `t_job_log` VALUES (3719, 1, 'testTask', 'test', 'mrbird', '0', NULL, 46, '2020-04-02 11:49:12');
INSERT INTO `t_job_log` VALUES (3720, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:13');
INSERT INTO `t_job_log` VALUES (3721, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-02 11:49:14');
INSERT INTO `t_job_log` VALUES (3722, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:15');
INSERT INTO `t_job_log` VALUES (3723, 1, 'testTask', 'test', 'mrbird', '0', NULL, 6, '2020-04-02 11:49:16');
INSERT INTO `t_job_log` VALUES (3724, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:17');
INSERT INTO `t_job_log` VALUES (3725, 1, 'testTask', 'test', 'mrbird', '0', NULL, 32, '2020-04-02 11:49:18');
INSERT INTO `t_job_log` VALUES (3726, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:49:19');
INSERT INTO `t_job_log` VALUES (3727, 1, 'testTask', 'test', 'mrbird', '0', NULL, 8, '2020-04-02 11:49:20');
INSERT INTO `t_job_log` VALUES (3728, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:21');
INSERT INTO `t_job_log` VALUES (3729, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:22');
INSERT INTO `t_job_log` VALUES (3730, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:23');
INSERT INTO `t_job_log` VALUES (3731, 1, 'testTask', 'test', 'mrbird', '0', NULL, 21, '2020-04-02 11:49:24');
INSERT INTO `t_job_log` VALUES (3732, 1, 'testTask', 'test', 'mrbird', '0', NULL, 26, '2020-04-02 11:49:25');
INSERT INTO `t_job_log` VALUES (3733, 1, 'testTask', 'test', 'mrbird', '0', NULL, 6, '2020-04-02 11:49:26');
INSERT INTO `t_job_log` VALUES (3734, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:49:27');
INSERT INTO `t_job_log` VALUES (3735, 1, 'testTask', 'test', 'mrbird', '0', NULL, 5, '2020-04-02 11:49:28');
INSERT INTO `t_job_log` VALUES (3736, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:29');
INSERT INTO `t_job_log` VALUES (3737, 1, 'testTask', 'test', 'mrbird', '0', NULL, 63, '2020-04-02 11:49:30');
INSERT INTO `t_job_log` VALUES (3738, 1, 'testTask', 'test', 'mrbird', '0', NULL, 121, '2020-04-02 11:49:31');
INSERT INTO `t_job_log` VALUES (3739, 1, 'testTask', 'test', 'mrbird', '0', NULL, 33, '2020-04-02 11:49:32');
INSERT INTO `t_job_log` VALUES (3740, 1, 'testTask', 'test', 'mrbird', '0', NULL, 64, '2020-04-02 11:49:33');
INSERT INTO `t_job_log` VALUES (3741, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:34');
INSERT INTO `t_job_log` VALUES (3742, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:49:35');
INSERT INTO `t_job_log` VALUES (3743, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:36');
INSERT INTO `t_job_log` VALUES (3744, 1, 'testTask', 'test', 'mrbird', '0', NULL, 22, '2020-04-02 11:49:37');
INSERT INTO `t_job_log` VALUES (3745, 1, 'testTask', 'test', 'mrbird', '0', NULL, 17, '2020-04-02 11:49:38');
INSERT INTO `t_job_log` VALUES (3746, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-02 11:49:39');
INSERT INTO `t_job_log` VALUES (3747, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:40');
INSERT INTO `t_job_log` VALUES (3748, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-02 11:49:41');
INSERT INTO `t_job_log` VALUES (3749, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-02 11:49:42');
INSERT INTO `t_job_log` VALUES (3750, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:43');
INSERT INTO `t_job_log` VALUES (3751, 1, 'testTask', 'test', 'mrbird', '0', NULL, 22, '2020-04-02 11:49:44');
INSERT INTO `t_job_log` VALUES (3752, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:45');
INSERT INTO `t_job_log` VALUES (3753, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:49:46');
INSERT INTO `t_job_log` VALUES (3754, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-02 11:49:47');
INSERT INTO `t_job_log` VALUES (3755, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:49:48');
INSERT INTO `t_job_log` VALUES (3756, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-02 11:49:49');
INSERT INTO `t_job_log` VALUES (3757, 1, 'testTask', 'test', 'mrbird', '0', NULL, 37, '2020-04-02 11:49:50');
INSERT INTO `t_job_log` VALUES (3758, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:51');
INSERT INTO `t_job_log` VALUES (3759, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-02 11:49:52');
INSERT INTO `t_job_log` VALUES (3760, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:53');
INSERT INTO `t_job_log` VALUES (3761, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:49:54');
INSERT INTO `t_job_log` VALUES (3762, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:55');
INSERT INTO `t_job_log` VALUES (3763, 1, 'testTask', 'test', 'mrbird', '0', NULL, 18, '2020-04-02 11:49:56');
INSERT INTO `t_job_log` VALUES (3764, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-02 11:49:57');
INSERT INTO `t_job_log` VALUES (3765, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:58');
INSERT INTO `t_job_log` VALUES (3766, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:49:59');
INSERT INTO `t_job_log` VALUES (3767, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:50:00');
INSERT INTO `t_job_log` VALUES (3768, 1, 'testTask', 'test', 'mrbird', '0', NULL, 6, '2020-04-02 11:50:01');
INSERT INTO `t_job_log` VALUES (3769, 1, 'testTask', 'test', 'mrbird', '0', NULL, 46, '2020-04-02 11:50:02');
INSERT INTO `t_job_log` VALUES (3770, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:50:03');
INSERT INTO `t_job_log` VALUES (3771, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:50:04');
INSERT INTO `t_job_log` VALUES (3772, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:50:05');
INSERT INTO `t_job_log` VALUES (3773, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:50:06');
INSERT INTO `t_job_log` VALUES (3774, 1, 'testTask', 'test', 'mrbird', '0', NULL, 39, '2020-04-02 11:50:07');
INSERT INTO `t_job_log` VALUES (3775, 1, 'testTask', 'test', 'mrbird', '0', NULL, 41, '2020-04-02 11:50:08');
INSERT INTO `t_job_log` VALUES (3776, 1, 'testTask', 'test', 'mrbird', '0', NULL, 6, '2020-04-02 11:50:09');
INSERT INTO `t_job_log` VALUES (3777, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:50:10');
INSERT INTO `t_job_log` VALUES (3778, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-02 11:50:11');
INSERT INTO `t_job_log` VALUES (3779, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:50:12');
INSERT INTO `t_job_log` VALUES (3780, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:50:13');
INSERT INTO `t_job_log` VALUES (3781, 1, 'testTask', 'test', 'mrbird', '0', NULL, 16, '2020-04-02 11:50:14');
INSERT INTO `t_job_log` VALUES (3782, 1, 'testTask', 'test', 'mrbird', '0', NULL, 53, '2020-04-02 11:50:15');
INSERT INTO `t_job_log` VALUES (3783, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:50:16');
INSERT INTO `t_job_log` VALUES (3784, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:50:17');
INSERT INTO `t_job_log` VALUES (3785, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:50:18');
INSERT INTO `t_job_log` VALUES (3786, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:50:19');
INSERT INTO `t_job_log` VALUES (3787, 1, 'testTask', 'test', 'mrbird', '0', NULL, 45, '2020-04-02 11:50:20');
INSERT INTO `t_job_log` VALUES (3788, 1, 'testTask', 'test', 'mrbird', '0', NULL, 27, '2020-04-02 11:50:21');
INSERT INTO `t_job_log` VALUES (3789, 1, 'testTask', 'test', 'mrbird', '0', NULL, 33, '2020-04-02 11:50:22');
INSERT INTO `t_job_log` VALUES (3790, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:50:23');
INSERT INTO `t_job_log` VALUES (3791, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:50:24');
INSERT INTO `t_job_log` VALUES (3792, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:50:25');
INSERT INTO `t_job_log` VALUES (3793, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-02 11:50:26');
INSERT INTO `t_job_log` VALUES (3794, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:50:27');
INSERT INTO `t_job_log` VALUES (3795, 1, 'testTask', 'test', 'mrbird', '0', NULL, 12, '2020-04-02 11:50:28');
INSERT INTO `t_job_log` VALUES (3796, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:50:29');
INSERT INTO `t_job_log` VALUES (3797, 1, 'testTask', 'test', 'mrbird', '0', NULL, 9, '2020-04-02 11:50:30');
INSERT INTO `t_job_log` VALUES (3798, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:50:31');
INSERT INTO `t_job_log` VALUES (3799, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:50:32');
INSERT INTO `t_job_log` VALUES (3800, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:50:33');
INSERT INTO `t_job_log` VALUES (3801, 1, 'testTask', 'test', 'mrbird', '0', NULL, 44, '2020-04-02 11:50:34');
INSERT INTO `t_job_log` VALUES (3802, 1, 'testTask', 'test', 'mrbird', '0', NULL, 21, '2020-04-02 11:50:35');
INSERT INTO `t_job_log` VALUES (3803, 1, 'testTask', 'test', 'mrbird', '0', NULL, 21, '2020-04-02 11:50:36');
INSERT INTO `t_job_log` VALUES (3804, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:50:37');
INSERT INTO `t_job_log` VALUES (3805, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:50:38');
INSERT INTO `t_job_log` VALUES (3806, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-02 11:50:39');
INSERT INTO `t_job_log` VALUES (3807, 1, 'testTask', 'test', 'mrbird', '0', NULL, 24, '2020-04-02 11:50:40');
INSERT INTO `t_job_log` VALUES (3808, 1, 'testTask', 'test', 'mrbird', '0', NULL, 282, '2020-04-02 11:50:41');
INSERT INTO `t_job_log` VALUES (3809, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:50:42');
INSERT INTO `t_job_log` VALUES (3810, 1, 'testTask', 'test', 'mrbird', '0', NULL, 49, '2020-04-02 11:50:43');
INSERT INTO `t_job_log` VALUES (3811, 1, 'testTask', 'test', 'mrbird', '0', NULL, 45, '2020-04-02 11:50:44');
INSERT INTO `t_job_log` VALUES (3812, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:50:45');
INSERT INTO `t_job_log` VALUES (3813, 1, 'testTask', 'test', 'mrbird', '0', NULL, 14, '2020-04-02 11:50:46');
INSERT INTO `t_job_log` VALUES (3814, 1, 'testTask', 'test', 'mrbird', '0', NULL, 28, '2020-04-02 11:50:47');
INSERT INTO `t_job_log` VALUES (3815, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:50:48');
INSERT INTO `t_job_log` VALUES (3816, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:50:49');
INSERT INTO `t_job_log` VALUES (3817, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:50:50');
INSERT INTO `t_job_log` VALUES (3818, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:50:51');
INSERT INTO `t_job_log` VALUES (3819, 1, 'testTask', 'test', 'mrbird', '0', NULL, 12, '2020-04-02 11:50:52');
INSERT INTO `t_job_log` VALUES (3820, 1, 'testTask', 'test', 'mrbird', '0', NULL, 12, '2020-04-02 11:50:53');
INSERT INTO `t_job_log` VALUES (3821, 1, 'testTask', 'test', 'mrbird', '0', NULL, 60, '2020-04-02 11:50:54');
INSERT INTO `t_job_log` VALUES (3822, 1, 'testTask', 'test', 'mrbird', '0', NULL, 71, '2020-04-02 11:50:55');
INSERT INTO `t_job_log` VALUES (3823, 1, 'testTask', 'test', 'mrbird', '0', NULL, 129, '2020-04-02 11:50:56');
INSERT INTO `t_job_log` VALUES (3824, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:50:57');
INSERT INTO `t_job_log` VALUES (3825, 1, 'testTask', 'test', 'mrbird', '0', NULL, 8, '2020-04-02 11:50:58');
INSERT INTO `t_job_log` VALUES (3826, 1, 'testTask', 'test', 'mrbird', '0', NULL, 20, '2020-04-02 11:50:59');
INSERT INTO `t_job_log` VALUES (3827, 1, 'testTask', 'test', 'mrbird', '0', NULL, 50, '2020-04-02 11:51:00');
INSERT INTO `t_job_log` VALUES (3828, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:01');
INSERT INTO `t_job_log` VALUES (3829, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:51:02');
INSERT INTO `t_job_log` VALUES (3830, 1, 'testTask', 'test', 'mrbird', '0', NULL, 12, '2020-04-02 11:51:03');
INSERT INTO `t_job_log` VALUES (3831, 1, 'testTask', 'test', 'mrbird', '0', NULL, 9, '2020-04-02 11:51:04');
INSERT INTO `t_job_log` VALUES (3832, 1, 'testTask', 'test', 'mrbird', '0', NULL, 24, '2020-04-02 11:51:05');
INSERT INTO `t_job_log` VALUES (3833, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:06');
INSERT INTO `t_job_log` VALUES (3834, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:51:07');
INSERT INTO `t_job_log` VALUES (3835, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:08');
INSERT INTO `t_job_log` VALUES (3836, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:09');
INSERT INTO `t_job_log` VALUES (3837, 1, 'testTask', 'test', 'mrbird', '0', NULL, 102, '2020-04-02 11:51:10');
INSERT INTO `t_job_log` VALUES (3838, 1, 'testTask', 'test', 'mrbird', '0', NULL, 26, '2020-04-02 11:51:11');
INSERT INTO `t_job_log` VALUES (3839, 1, 'testTask', 'test', 'mrbird', '0', NULL, 21, '2020-04-02 11:51:12');
INSERT INTO `t_job_log` VALUES (3840, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:13');
INSERT INTO `t_job_log` VALUES (3841, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-02 11:51:14');
INSERT INTO `t_job_log` VALUES (3842, 1, 'testTask', 'test', 'mrbird', '0', NULL, 27, '2020-04-02 11:51:15');
INSERT INTO `t_job_log` VALUES (3843, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:51:16');
INSERT INTO `t_job_log` VALUES (3844, 1, 'testTask', 'test', 'mrbird', '0', NULL, 71, '2020-04-02 11:51:17');
INSERT INTO `t_job_log` VALUES (3845, 1, 'testTask', 'test', 'mrbird', '0', NULL, 35, '2020-04-02 11:51:18');
INSERT INTO `t_job_log` VALUES (3846, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:19');
INSERT INTO `t_job_log` VALUES (3847, 1, 'testTask', 'test', 'mrbird', '0', NULL, 157, '2020-04-02 11:51:20');
INSERT INTO `t_job_log` VALUES (3848, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:21');
INSERT INTO `t_job_log` VALUES (3849, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:22');
INSERT INTO `t_job_log` VALUES (3850, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:23');
INSERT INTO `t_job_log` VALUES (3851, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-02 11:51:24');
INSERT INTO `t_job_log` VALUES (3852, 1, 'testTask', 'test', 'mrbird', '0', NULL, 20, '2020-04-02 11:51:25');
INSERT INTO `t_job_log` VALUES (3853, 1, 'testTask', 'test', 'mrbird', '0', NULL, 9, '2020-04-02 11:51:26');
INSERT INTO `t_job_log` VALUES (3854, 1, 'testTask', 'test', 'mrbird', '0', NULL, 6, '2020-04-02 11:51:27');
INSERT INTO `t_job_log` VALUES (3855, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:28');
INSERT INTO `t_job_log` VALUES (3856, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:29');
INSERT INTO `t_job_log` VALUES (3857, 1, 'testTask', 'test', 'mrbird', '0', NULL, 185, '2020-04-02 11:51:30');
INSERT INTO `t_job_log` VALUES (3858, 1, 'testTask', 'test', 'mrbird', '0', NULL, 72, '2020-04-02 11:51:31');
INSERT INTO `t_job_log` VALUES (3859, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:32');
INSERT INTO `t_job_log` VALUES (3860, 1, 'testTask', 'test', 'mrbird', '0', NULL, 22, '2020-04-02 11:51:33');
INSERT INTO `t_job_log` VALUES (3861, 1, 'testTask', 'test', 'mrbird', '0', NULL, 15, '2020-04-02 11:51:34');
INSERT INTO `t_job_log` VALUES (3862, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:51:35');
INSERT INTO `t_job_log` VALUES (3863, 1, 'testTask', 'test', 'mrbird', '0', NULL, 49, '2020-04-02 11:51:36');
INSERT INTO `t_job_log` VALUES (3864, 1, 'testTask', 'test', 'mrbird', '0', NULL, 17, '2020-04-02 11:51:37');
INSERT INTO `t_job_log` VALUES (3865, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:38');
INSERT INTO `t_job_log` VALUES (3866, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-02 11:51:39');
INSERT INTO `t_job_log` VALUES (3867, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:51:40');
INSERT INTO `t_job_log` VALUES (3868, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:41');
INSERT INTO `t_job_log` VALUES (3869, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-02 11:51:42');
INSERT INTO `t_job_log` VALUES (3870, 1, 'testTask', 'test', 'mrbird', '0', NULL, 29, '2020-04-02 11:51:43');
INSERT INTO `t_job_log` VALUES (3871, 1, 'testTask', 'test', 'mrbird', '0', NULL, 8, '2020-04-02 11:51:44');
INSERT INTO `t_job_log` VALUES (3872, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:51:45');
INSERT INTO `t_job_log` VALUES (3873, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:46');
INSERT INTO `t_job_log` VALUES (3874, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:51:47');
INSERT INTO `t_job_log` VALUES (3875, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:48');
INSERT INTO `t_job_log` VALUES (3876, 1, 'testTask', 'test', 'mrbird', '0', NULL, 16, '2020-04-02 11:51:49');
INSERT INTO `t_job_log` VALUES (3877, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:50');
INSERT INTO `t_job_log` VALUES (3878, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:51');
INSERT INTO `t_job_log` VALUES (3879, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:52');
INSERT INTO `t_job_log` VALUES (3880, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:53');
INSERT INTO `t_job_log` VALUES (3881, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:54');
INSERT INTO `t_job_log` VALUES (3882, 1, 'testTask', 'test', 'mrbird', '0', NULL, 42, '2020-04-02 11:51:55');
INSERT INTO `t_job_log` VALUES (3883, 1, 'testTask', 'test', 'mrbird', '0', NULL, 42, '2020-04-02 11:51:56');
INSERT INTO `t_job_log` VALUES (3884, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:51:57');
INSERT INTO `t_job_log` VALUES (3885, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:51:58');
INSERT INTO `t_job_log` VALUES (3886, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:51:59');
INSERT INTO `t_job_log` VALUES (3887, 1, 'testTask', 'test', 'mrbird', '0', NULL, 30, '2020-04-02 11:52:00');
INSERT INTO `t_job_log` VALUES (3888, 1, 'testTask', 'test', 'mrbird', '0', NULL, 58, '2020-04-02 11:52:01');
INSERT INTO `t_job_log` VALUES (3889, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:52:02');
INSERT INTO `t_job_log` VALUES (3890, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:52:03');
INSERT INTO `t_job_log` VALUES (3891, 1, 'testTask', 'test', 'mrbird', '0', NULL, 5, '2020-04-02 11:52:04');
INSERT INTO `t_job_log` VALUES (3892, 1, 'testTask', 'test', 'mrbird', '0', NULL, 18, '2020-04-02 11:52:05');
INSERT INTO `t_job_log` VALUES (3893, 1, 'testTask', 'test', 'mrbird', '0', NULL, 25, '2020-04-02 11:52:06');
INSERT INTO `t_job_log` VALUES (3894, 1, 'testTask', 'test', 'mrbird', '0', NULL, 43, '2020-04-02 11:52:07');
INSERT INTO `t_job_log` VALUES (3895, 1, 'testTask', 'test', 'mrbird', '0', NULL, 44, '2020-04-02 11:52:08');
INSERT INTO `t_job_log` VALUES (3896, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:09');
INSERT INTO `t_job_log` VALUES (3897, 1, 'testTask', 'test', 'mrbird', '0', NULL, 265, '2020-04-02 11:52:10');
INSERT INTO `t_job_log` VALUES (3898, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:11');
INSERT INTO `t_job_log` VALUES (3899, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-02 11:52:12');
INSERT INTO `t_job_log` VALUES (3900, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:13');
INSERT INTO `t_job_log` VALUES (3901, 1, 'testTask', 'test', 'mrbird', '0', NULL, 30, '2020-04-02 11:52:14');
INSERT INTO `t_job_log` VALUES (3902, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:15');
INSERT INTO `t_job_log` VALUES (3903, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:16');
INSERT INTO `t_job_log` VALUES (3904, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:17');
INSERT INTO `t_job_log` VALUES (3905, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:52:18');
INSERT INTO `t_job_log` VALUES (3906, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:19');
INSERT INTO `t_job_log` VALUES (3907, 1, 'testTask', 'test', 'mrbird', '0', NULL, 83, '2020-04-02 11:52:20');
INSERT INTO `t_job_log` VALUES (3908, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:21');
INSERT INTO `t_job_log` VALUES (3909, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-02 11:52:22');
INSERT INTO `t_job_log` VALUES (3910, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:52:23');
INSERT INTO `t_job_log` VALUES (3911, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:52:24');
INSERT INTO `t_job_log` VALUES (3912, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:52:25');
INSERT INTO `t_job_log` VALUES (3913, 1, 'testTask', 'test', 'mrbird', '0', NULL, 17, '2020-04-02 11:52:26');
INSERT INTO `t_job_log` VALUES (3914, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-02 11:52:27');
INSERT INTO `t_job_log` VALUES (3915, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:52:28');
INSERT INTO `t_job_log` VALUES (3916, 1, 'testTask', 'test', 'mrbird', '0', NULL, 6, '2020-04-02 11:52:29');
INSERT INTO `t_job_log` VALUES (3917, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-02 11:52:30');
INSERT INTO `t_job_log` VALUES (3918, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:31');
INSERT INTO `t_job_log` VALUES (3919, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:52:32');
INSERT INTO `t_job_log` VALUES (3920, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:33');
INSERT INTO `t_job_log` VALUES (3921, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:34');
INSERT INTO `t_job_log` VALUES (3922, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:52:35');
INSERT INTO `t_job_log` VALUES (3923, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-02 11:52:36');
INSERT INTO `t_job_log` VALUES (3924, 1, 'testTask', 'test', 'mrbird', '0', NULL, 19, '2020-04-02 11:52:37');
INSERT INTO `t_job_log` VALUES (3925, 1, 'testTask', 'test', 'mrbird', '0', NULL, 5, '2020-04-02 11:52:38');
INSERT INTO `t_job_log` VALUES (3926, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:39');
INSERT INTO `t_job_log` VALUES (3927, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:52:40');
INSERT INTO `t_job_log` VALUES (3928, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:41');
INSERT INTO `t_job_log` VALUES (3929, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-02 11:52:42');
INSERT INTO `t_job_log` VALUES (3930, 1, 'testTask', 'test', 'mrbird', '0', NULL, 172, '2020-04-02 11:52:43');
INSERT INTO `t_job_log` VALUES (3931, 1, 'testTask', 'test', 'mrbird', '0', NULL, 14, '2020-04-02 11:52:44');
INSERT INTO `t_job_log` VALUES (3932, 1, 'testTask', 'test', 'mrbird', '0', NULL, 11, '2020-04-02 11:52:45');
INSERT INTO `t_job_log` VALUES (3933, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:46');
INSERT INTO `t_job_log` VALUES (3934, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:47');
INSERT INTO `t_job_log` VALUES (3935, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:48');
INSERT INTO `t_job_log` VALUES (3936, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-02 11:52:49');
INSERT INTO `t_job_log` VALUES (3937, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:50');
INSERT INTO `t_job_log` VALUES (3938, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:51');
INSERT INTO `t_job_log` VALUES (3939, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-02 11:52:52');
INSERT INTO `t_job_log` VALUES (3940, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:53');
INSERT INTO `t_job_log` VALUES (3941, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-02 11:52:54');
INSERT INTO `t_job_log` VALUES (3942, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-02 11:52:55');
INSERT INTO `t_job_log` VALUES (3943, 1, 'testTask', 'test', 'mrbird', '0', NULL, 74, '2020-04-03 15:20:39');
INSERT INTO `t_job_log` VALUES (3944, 1, 'testTask', 'test', 'mrbird', '0', NULL, 5, '2020-04-03 15:20:40');
INSERT INTO `t_job_log` VALUES (3945, 1, 'testTask', 'test', 'mrbird', '0', NULL, 5, '2020-04-03 15:20:41');
INSERT INTO `t_job_log` VALUES (3946, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-03 15:20:42');
INSERT INTO `t_job_log` VALUES (3947, 1, 'testTask', 'test', 'mrbird', '0', NULL, 11, '2020-04-03 15:20:43');
INSERT INTO `t_job_log` VALUES (3948, 1, 'testTask', 'test', 'mrbird', '0', NULL, 18, '2020-04-03 15:20:44');
INSERT INTO `t_job_log` VALUES (3949, 1, 'testTask', 'test', 'mrbird', '0', NULL, 55, '2020-04-03 15:20:45');
INSERT INTO `t_job_log` VALUES (3950, 1, 'testTask', 'test', 'mrbird', '0', NULL, 28, '2020-04-03 15:20:46');
INSERT INTO `t_job_log` VALUES (3951, 1, 'testTask', 'test', 'mrbird', '0', NULL, 7, '2020-04-03 15:20:47');
INSERT INTO `t_job_log` VALUES (3952, 1, 'testTask', 'test', 'mrbird', '0', NULL, 8, '2020-04-03 15:20:48');
INSERT INTO `t_job_log` VALUES (3953, 1, 'testTask', 'test', 'mrbird', '0', NULL, 26, '2020-04-03 15:20:49');
INSERT INTO `t_job_log` VALUES (3954, 1, 'testTask', 'test', 'mrbird', '0', NULL, 8, '2020-04-03 15:20:50');
INSERT INTO `t_job_log` VALUES (3955, 1, 'testTask', 'test', 'mrbird', '0', NULL, 9, '2020-04-03 15:20:51');
INSERT INTO `t_job_log` VALUES (3956, 1, 'testTask', 'test', 'mrbird', '0', NULL, 35, '2020-04-03 15:20:52');
INSERT INTO `t_job_log` VALUES (3957, 1, 'testTask', 'test', 'mrbird', '0', NULL, 7, '2020-04-03 15:20:53');
INSERT INTO `t_job_log` VALUES (3958, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-03 15:20:54');
INSERT INTO `t_job_log` VALUES (3959, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-03 15:20:55');
INSERT INTO `t_job_log` VALUES (3960, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-03 15:20:56');
INSERT INTO `t_job_log` VALUES (3961, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-03 15:20:57');
INSERT INTO `t_job_log` VALUES (3962, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-03 15:20:58');
INSERT INTO `t_job_log` VALUES (3963, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-03 15:20:59');
INSERT INTO `t_job_log` VALUES (3964, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-03 15:21:00');
INSERT INTO `t_job_log` VALUES (3965, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-03 15:21:01');
INSERT INTO `t_job_log` VALUES (3966, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-03 15:21:02');
INSERT INTO `t_job_log` VALUES (3967, 1, 'testTask', 'test', 'mrbird', '0', NULL, 30, '2020-04-03 15:21:03');
INSERT INTO `t_job_log` VALUES (3968, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-03 15:21:04');
INSERT INTO `t_job_log` VALUES (3969, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-03 15:21:05');
INSERT INTO `t_job_log` VALUES (3970, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-03 15:21:06');
INSERT INTO `t_job_log` VALUES (3971, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-03 15:21:07');
INSERT INTO `t_job_log` VALUES (3972, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-03 15:21:08');
INSERT INTO `t_job_log` VALUES (3973, 1, 'testTask', 'test', 'mrbird', '0', NULL, 44, '2020-04-03 15:21:09');
INSERT INTO `t_job_log` VALUES (3974, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-03 15:21:10');
INSERT INTO `t_job_log` VALUES (3975, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-03 15:21:11');
INSERT INTO `t_job_log` VALUES (3976, 1, 'testTask', 'test', 'mrbird', '0', NULL, 14, '2020-04-03 15:21:12');
INSERT INTO `t_job_log` VALUES (3977, 1, 'testTask', 'test', 'mrbird', '0', NULL, 19, '2020-04-03 15:21:13');
INSERT INTO `t_job_log` VALUES (3978, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-03 15:21:14');
INSERT INTO `t_job_log` VALUES (3979, 1, 'testTask', 'test', 'mrbird', '0', NULL, 22, '2020-04-03 15:21:15');
INSERT INTO `t_job_log` VALUES (3980, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-03 15:21:16');
INSERT INTO `t_job_log` VALUES (3981, 1, 'testTask', 'test', 'mrbird', '0', NULL, 27, '2020-04-03 15:21:17');
INSERT INTO `t_job_log` VALUES (3982, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-03 15:21:18');
INSERT INTO `t_job_log` VALUES (3983, 1, 'testTask', 'test', 'mrbird', '0', NULL, 197, '2020-04-03 15:21:19');
INSERT INTO `t_job_log` VALUES (3984, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-03 15:21:20');
INSERT INTO `t_job_log` VALUES (3985, 1, 'testTask', 'test', 'mrbird', '0', NULL, 91, '2020-04-03 15:21:21');
INSERT INTO `t_job_log` VALUES (3986, 1, 'testTask', 'test', 'mrbird', '0', NULL, 102, '2020-04-03 15:21:22');
INSERT INTO `t_job_log` VALUES (3987, 1, 'testTask', 'test', 'mrbird', '0', NULL, 79, '2020-04-03 15:21:23');
INSERT INTO `t_job_log` VALUES (3988, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-03 15:21:24');
INSERT INTO `t_job_log` VALUES (3989, 1, 'testTask', 'test', 'mrbird', '0', NULL, 120, '2020-04-03 15:21:25');
INSERT INTO `t_job_log` VALUES (3990, 1, 'testTask', 'test', 'mrbird', '0', NULL, 22, '2020-04-03 15:21:26');
INSERT INTO `t_job_log` VALUES (3991, 1, 'testTask', 'test', 'mrbird', '0', NULL, 62, '2020-04-03 15:21:27');
INSERT INTO `t_job_log` VALUES (3992, 1, 'testTask', 'test', 'mrbird', '0', NULL, 105, '2020-04-03 15:21:28');
INSERT INTO `t_job_log` VALUES (3993, 1, 'testTask', 'test', 'mrbird', '0', NULL, 102, '2020-04-03 15:21:29');
INSERT INTO `t_job_log` VALUES (3994, 1, 'testTask', 'test', 'mrbird', '0', NULL, 211, '2020-04-03 15:21:30');
INSERT INTO `t_job_log` VALUES (3995, 1, 'testTask', 'test', 'mrbird', '0', NULL, 67, '2020-04-03 15:21:31');
INSERT INTO `t_job_log` VALUES (3996, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-03 15:21:32');
INSERT INTO `t_job_log` VALUES (3997, 1, 'testTask', 'test', 'mrbird', '0', NULL, 31, '2020-04-03 15:21:33');
INSERT INTO `t_job_log` VALUES (3998, 1, 'testTask', 'test', 'mrbird', '0', NULL, 6, '2020-04-03 15:21:34');
INSERT INTO `t_job_log` VALUES (3999, 1, 'testTask', 'test', 'mrbird', '0', NULL, 36, '2020-04-03 15:21:35');
INSERT INTO `t_job_log` VALUES (4000, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-03 15:21:36');
INSERT INTO `t_job_log` VALUES (4001, 1, 'testTask', 'test', 'mrbird', '0', NULL, 45, '2020-04-03 15:21:37');
INSERT INTO `t_job_log` VALUES (4002, 1, 'testTask', 'test', 'mrbird', '0', NULL, 79, '2020-04-03 15:21:38');
INSERT INTO `t_job_log` VALUES (4003, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-03 15:21:39');
INSERT INTO `t_job_log` VALUES (4004, 1, 'testTask', 'test', 'mrbird', '0', NULL, 11, '2020-04-03 15:21:40');
INSERT INTO `t_job_log` VALUES (4005, 1, 'testTask', 'test', 'mrbird', '0', NULL, 24, '2020-04-03 15:21:41');
INSERT INTO `t_job_log` VALUES (4006, 1, 'testTask', 'test', 'mrbird', '0', NULL, 19, '2020-04-03 15:21:42');
INSERT INTO `t_job_log` VALUES (4007, 1, 'testTask', 'test', 'mrbird', '0', NULL, 47, '2020-04-03 15:21:43');
INSERT INTO `t_job_log` VALUES (4008, 1, 'testTask', 'test', 'mrbird', '0', NULL, 37, '2020-04-03 15:21:44');
INSERT INTO `t_job_log` VALUES (4009, 1, 'testTask', 'test', 'mrbird', '0', NULL, 100, '2020-04-03 15:21:45');
INSERT INTO `t_job_log` VALUES (4010, 1, 'testTask', 'test', 'mrbird', '0', NULL, 13, '2020-04-03 15:21:46');
INSERT INTO `t_job_log` VALUES (4011, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-03 15:21:47');
INSERT INTO `t_job_log` VALUES (4012, 1, 'testTask', 'test', 'mrbird', '0', NULL, 154, '2020-04-03 15:21:48');
INSERT INTO `t_job_log` VALUES (4013, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-03 15:21:49');
INSERT INTO `t_job_log` VALUES (4014, 1, 'testTask', 'test', 'mrbird', '0', NULL, 11, '2020-04-03 15:21:50');
INSERT INTO `t_job_log` VALUES (4015, 1, 'testTask', 'test', 'mrbird', '0', NULL, 48, '2020-04-03 15:21:51');
INSERT INTO `t_job_log` VALUES (4016, 1, 'testTask', 'test', 'mrbird', '0', NULL, 212, '2020-04-03 15:21:52');
INSERT INTO `t_job_log` VALUES (4017, 1, 'testTask', 'test', 'mrbird', '0', NULL, 66, '2020-04-03 15:21:53');
INSERT INTO `t_job_log` VALUES (4018, 1, 'testTask', 'test', 'mrbird', '0', NULL, 304, '2020-04-03 15:21:54');
INSERT INTO `t_job_log` VALUES (4019, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-03 15:21:55');
INSERT INTO `t_job_log` VALUES (4020, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-03 15:21:56');
INSERT INTO `t_job_log` VALUES (4021, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-03 15:21:57');
INSERT INTO `t_job_log` VALUES (4022, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-03 15:21:58');
INSERT INTO `t_job_log` VALUES (4023, 1, 'testTask', 'test', 'mrbird', '0', NULL, 21, '2020-04-03 15:21:59');
INSERT INTO `t_job_log` VALUES (4024, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-03 15:22:00');
INSERT INTO `t_job_log` VALUES (4025, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-03 15:22:01');
INSERT INTO `t_job_log` VALUES (4026, 1, 'testTask', 'test', 'mrbird', '0', NULL, 8, '2020-04-03 15:22:02');
INSERT INTO `t_job_log` VALUES (4027, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-03 15:22:03');
INSERT INTO `t_job_log` VALUES (4028, 1, 'testTask', 'test', 'mrbird', '0', NULL, 32, '2020-04-03 15:22:04');
INSERT INTO `t_job_log` VALUES (4029, 1, 'testTask', 'test', 'mrbird', '0', NULL, 59, '2020-04-03 15:22:05');
INSERT INTO `t_job_log` VALUES (4030, 1, 'testTask', 'test', 'mrbird', '0', NULL, 49, '2020-04-03 15:22:07');
INSERT INTO `t_job_log` VALUES (4031, 1, 'testTask', 'test', 'mrbird', '0', NULL, 5, '2020-04-03 15:22:08');
INSERT INTO `t_job_log` VALUES (4032, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-03 15:22:08');
INSERT INTO `t_job_log` VALUES (4033, 1, 'testTask', 'test', 'mrbird', '0', NULL, 16, '2020-04-03 15:22:09');
INSERT INTO `t_job_log` VALUES (4034, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-04-03 16:01:05');
INSERT INTO `t_job_log` VALUES (4035, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:01:10');
INSERT INTO `t_job_log` VALUES (4036, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:01:15');
INSERT INTO `t_job_log` VALUES (4037, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:01:20');
INSERT INTO `t_job_log` VALUES (4038, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:01:25');
INSERT INTO `t_job_log` VALUES (4039, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:01:30');
INSERT INTO `t_job_log` VALUES (4040, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:01:35');
INSERT INTO `t_job_log` VALUES (4041, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:01:40');
INSERT INTO `t_job_log` VALUES (4042, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:01:45');
INSERT INTO `t_job_log` VALUES (4043, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:01:50');
INSERT INTO `t_job_log` VALUES (4044, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:01:55');
INSERT INTO `t_job_log` VALUES (4045, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:02:00');
INSERT INTO `t_job_log` VALUES (4046, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-04-03 16:02:05');
INSERT INTO `t_job_log` VALUES (4047, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-04-03 16:02:10');
INSERT INTO `t_job_log` VALUES (4048, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:02:15');
INSERT INTO `t_job_log` VALUES (4049, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:02:20');
INSERT INTO `t_job_log` VALUES (4050, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:02:25');
INSERT INTO `t_job_log` VALUES (4051, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-04-03 16:02:30');
INSERT INTO `t_job_log` VALUES (4052, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:02:35');
INSERT INTO `t_job_log` VALUES (4053, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 9, '2020-04-03 16:02:40');
INSERT INTO `t_job_log` VALUES (4054, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:02:45');
INSERT INTO `t_job_log` VALUES (4055, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:02:50');
INSERT INTO `t_job_log` VALUES (4056, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:02:55');
INSERT INTO `t_job_log` VALUES (4057, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-04-03 16:03:00');
INSERT INTO `t_job_log` VALUES (4058, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:03:05');
INSERT INTO `t_job_log` VALUES (4059, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-04-03 16:03:10');
INSERT INTO `t_job_log` VALUES (4060, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-04-03 16:03:15');
INSERT INTO `t_job_log` VALUES (4061, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:03:20');
INSERT INTO `t_job_log` VALUES (4062, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-04-03 16:03:25');
INSERT INTO `t_job_log` VALUES (4063, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:03:30');
INSERT INTO `t_job_log` VALUES (4064, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:03:35');
INSERT INTO `t_job_log` VALUES (4065, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 0, '2020-04-03 16:03:40');
INSERT INTO `t_job_log` VALUES (4066, 11, 'testTask', 'test2', NULL, '1', 'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', 1, '2020-04-03 16:03:45');
INSERT INTO `t_job_log` VALUES (4067, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-07 11:42:22');
INSERT INTO `t_job_log` VALUES (4068, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:23');
INSERT INTO `t_job_log` VALUES (4069, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:24');
INSERT INTO `t_job_log` VALUES (4070, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:42:25');
INSERT INTO `t_job_log` VALUES (4071, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:26');
INSERT INTO `t_job_log` VALUES (4072, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-07 11:42:27');
INSERT INTO `t_job_log` VALUES (4073, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-07 11:42:28');
INSERT INTO `t_job_log` VALUES (4074, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:42:29');
INSERT INTO `t_job_log` VALUES (4075, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:30');
INSERT INTO `t_job_log` VALUES (4076, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:31');
INSERT INTO `t_job_log` VALUES (4077, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:32');
INSERT INTO `t_job_log` VALUES (4078, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:33');
INSERT INTO `t_job_log` VALUES (4079, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:34');
INSERT INTO `t_job_log` VALUES (4080, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:42:35');
INSERT INTO `t_job_log` VALUES (4081, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-07 11:42:36');
INSERT INTO `t_job_log` VALUES (4082, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:37');
INSERT INTO `t_job_log` VALUES (4083, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:38');
INSERT INTO `t_job_log` VALUES (4084, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:42:39');
INSERT INTO `t_job_log` VALUES (4085, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:40');
INSERT INTO `t_job_log` VALUES (4086, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:41');
INSERT INTO `t_job_log` VALUES (4087, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:42');
INSERT INTO `t_job_log` VALUES (4088, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:42:43');
INSERT INTO `t_job_log` VALUES (4089, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:42:44');
INSERT INTO `t_job_log` VALUES (4090, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:42:45');
INSERT INTO `t_job_log` VALUES (4091, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:46');
INSERT INTO `t_job_log` VALUES (4092, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:47');
INSERT INTO `t_job_log` VALUES (4093, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:48');
INSERT INTO `t_job_log` VALUES (4094, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:49');
INSERT INTO `t_job_log` VALUES (4095, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:42:50');
INSERT INTO `t_job_log` VALUES (4096, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:51');
INSERT INTO `t_job_log` VALUES (4097, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:42:52');
INSERT INTO `t_job_log` VALUES (4098, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:53');
INSERT INTO `t_job_log` VALUES (4099, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:54');
INSERT INTO `t_job_log` VALUES (4100, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:42:55');
INSERT INTO `t_job_log` VALUES (4101, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:42:56');
INSERT INTO `t_job_log` VALUES (4102, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:42:57');
INSERT INTO `t_job_log` VALUES (4103, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:42:58');
INSERT INTO `t_job_log` VALUES (4104, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:42:59');
INSERT INTO `t_job_log` VALUES (4105, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:43:00');
INSERT INTO `t_job_log` VALUES (4106, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:43:01');
INSERT INTO `t_job_log` VALUES (4107, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:43:02');
INSERT INTO `t_job_log` VALUES (4108, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:03');
INSERT INTO `t_job_log` VALUES (4109, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:43:04');
INSERT INTO `t_job_log` VALUES (4110, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:05');
INSERT INTO `t_job_log` VALUES (4111, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:06');
INSERT INTO `t_job_log` VALUES (4112, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:43:07');
INSERT INTO `t_job_log` VALUES (4113, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:43:08');
INSERT INTO `t_job_log` VALUES (4114, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:43:09');
INSERT INTO `t_job_log` VALUES (4115, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:10');
INSERT INTO `t_job_log` VALUES (4116, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:11');
INSERT INTO `t_job_log` VALUES (4117, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:12');
INSERT INTO `t_job_log` VALUES (4118, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-07 11:43:13');
INSERT INTO `t_job_log` VALUES (4119, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-07 11:43:14');
INSERT INTO `t_job_log` VALUES (4120, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:15');
INSERT INTO `t_job_log` VALUES (4121, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:16');
INSERT INTO `t_job_log` VALUES (4122, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:43:17');
INSERT INTO `t_job_log` VALUES (4123, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:18');
INSERT INTO `t_job_log` VALUES (4124, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:19');
INSERT INTO `t_job_log` VALUES (4125, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:43:20');
INSERT INTO `t_job_log` VALUES (4126, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:21');
INSERT INTO `t_job_log` VALUES (4127, 1, 'testTask', 'test', 'mrbird', '0', NULL, 85, '2020-04-07 11:43:22');
INSERT INTO `t_job_log` VALUES (4128, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:23');
INSERT INTO `t_job_log` VALUES (4129, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:24');
INSERT INTO `t_job_log` VALUES (4130, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:25');
INSERT INTO `t_job_log` VALUES (4131, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:43:26');
INSERT INTO `t_job_log` VALUES (4132, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:27');
INSERT INTO `t_job_log` VALUES (4133, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-07 11:43:28');
INSERT INTO `t_job_log` VALUES (4134, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:29');
INSERT INTO `t_job_log` VALUES (4135, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:43:30');
INSERT INTO `t_job_log` VALUES (4136, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-07 11:43:31');
INSERT INTO `t_job_log` VALUES (4137, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:32');
INSERT INTO `t_job_log` VALUES (4138, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:33');
INSERT INTO `t_job_log` VALUES (4139, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:34');
INSERT INTO `t_job_log` VALUES (4140, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:35');
INSERT INTO `t_job_log` VALUES (4141, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:36');
INSERT INTO `t_job_log` VALUES (4142, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:43:37');
INSERT INTO `t_job_log` VALUES (4143, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:38');
INSERT INTO `t_job_log` VALUES (4144, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:43:39');
INSERT INTO `t_job_log` VALUES (4145, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:43:40');
INSERT INTO `t_job_log` VALUES (4146, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:43:41');
INSERT INTO `t_job_log` VALUES (4147, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:43:42');
INSERT INTO `t_job_log` VALUES (4148, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:43');
INSERT INTO `t_job_log` VALUES (4149, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:44');
INSERT INTO `t_job_log` VALUES (4150, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:45');
INSERT INTO `t_job_log` VALUES (4151, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:43:46');
INSERT INTO `t_job_log` VALUES (4152, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-07 11:43:47');
INSERT INTO `t_job_log` VALUES (4153, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:48');
INSERT INTO `t_job_log` VALUES (4154, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:43:49');
INSERT INTO `t_job_log` VALUES (4155, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:43:50');
INSERT INTO `t_job_log` VALUES (4156, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:43:51');
INSERT INTO `t_job_log` VALUES (4157, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:52');
INSERT INTO `t_job_log` VALUES (4158, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:53');
INSERT INTO `t_job_log` VALUES (4159, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:54');
INSERT INTO `t_job_log` VALUES (4160, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:43:55');
INSERT INTO `t_job_log` VALUES (4161, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:56');
INSERT INTO `t_job_log` VALUES (4162, 1, 'testTask', 'test', 'mrbird', '0', NULL, 4, '2020-04-07 11:43:57');
INSERT INTO `t_job_log` VALUES (4163, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:58');
INSERT INTO `t_job_log` VALUES (4164, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:43:59');
INSERT INTO `t_job_log` VALUES (4165, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:00');
INSERT INTO `t_job_log` VALUES (4166, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:44:01');
INSERT INTO `t_job_log` VALUES (4167, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:02');
INSERT INTO `t_job_log` VALUES (4168, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:44:03');
INSERT INTO `t_job_log` VALUES (4169, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:44:04');
INSERT INTO `t_job_log` VALUES (4170, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:05');
INSERT INTO `t_job_log` VALUES (4171, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:44:06');
INSERT INTO `t_job_log` VALUES (4172, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:07');
INSERT INTO `t_job_log` VALUES (4173, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:08');
INSERT INTO `t_job_log` VALUES (4174, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:09');
INSERT INTO `t_job_log` VALUES (4175, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:44:10');
INSERT INTO `t_job_log` VALUES (4176, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:44:11');
INSERT INTO `t_job_log` VALUES (4177, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:44:12');
INSERT INTO `t_job_log` VALUES (4178, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:44:13');
INSERT INTO `t_job_log` VALUES (4179, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:44:14');
INSERT INTO `t_job_log` VALUES (4180, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:44:15');
INSERT INTO `t_job_log` VALUES (4181, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:16');
INSERT INTO `t_job_log` VALUES (4182, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:17');
INSERT INTO `t_job_log` VALUES (4183, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:18');
INSERT INTO `t_job_log` VALUES (4184, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:44:19');
INSERT INTO `t_job_log` VALUES (4185, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:20');
INSERT INTO `t_job_log` VALUES (4186, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:44:21');
INSERT INTO `t_job_log` VALUES (4187, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:44:22');
INSERT INTO `t_job_log` VALUES (4188, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:44:23');
INSERT INTO `t_job_log` VALUES (4189, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-07 11:44:24');
INSERT INTO `t_job_log` VALUES (4190, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:44:25');
INSERT INTO `t_job_log` VALUES (4191, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:44:26');
INSERT INTO `t_job_log` VALUES (4192, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:27');
INSERT INTO `t_job_log` VALUES (4193, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:44:28');
INSERT INTO `t_job_log` VALUES (4194, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:29');
INSERT INTO `t_job_log` VALUES (4195, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:44:30');
INSERT INTO `t_job_log` VALUES (4196, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:31');
INSERT INTO `t_job_log` VALUES (4197, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:44:32');
INSERT INTO `t_job_log` VALUES (4198, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:33');
INSERT INTO `t_job_log` VALUES (4199, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:34');
INSERT INTO `t_job_log` VALUES (4200, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:44:35');
INSERT INTO `t_job_log` VALUES (4201, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:44:36');
INSERT INTO `t_job_log` VALUES (4202, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:37');
INSERT INTO `t_job_log` VALUES (4203, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:44:38');
INSERT INTO `t_job_log` VALUES (4204, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:39');
INSERT INTO `t_job_log` VALUES (4205, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:40');
INSERT INTO `t_job_log` VALUES (4206, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:41');
INSERT INTO `t_job_log` VALUES (4207, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:42');
INSERT INTO `t_job_log` VALUES (4208, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:43');
INSERT INTO `t_job_log` VALUES (4209, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:44:44');
INSERT INTO `t_job_log` VALUES (4210, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:44:45');
INSERT INTO `t_job_log` VALUES (4211, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:46');
INSERT INTO `t_job_log` VALUES (4212, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:47');
INSERT INTO `t_job_log` VALUES (4213, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:48');
INSERT INTO `t_job_log` VALUES (4214, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:44:49');
INSERT INTO `t_job_log` VALUES (4215, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:44:50');
INSERT INTO `t_job_log` VALUES (4216, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:51');
INSERT INTO `t_job_log` VALUES (4217, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-07 11:44:52');
INSERT INTO `t_job_log` VALUES (4218, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:53');
INSERT INTO `t_job_log` VALUES (4219, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:54');
INSERT INTO `t_job_log` VALUES (4220, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:55');
INSERT INTO `t_job_log` VALUES (4221, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:56');
INSERT INTO `t_job_log` VALUES (4222, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:57');
INSERT INTO `t_job_log` VALUES (4223, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:58');
INSERT INTO `t_job_log` VALUES (4224, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:44:59');
INSERT INTO `t_job_log` VALUES (4225, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:00');
INSERT INTO `t_job_log` VALUES (4226, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:45:01');
INSERT INTO `t_job_log` VALUES (4227, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:02');
INSERT INTO `t_job_log` VALUES (4228, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:45:03');
INSERT INTO `t_job_log` VALUES (4229, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:45:04');
INSERT INTO `t_job_log` VALUES (4230, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:05');
INSERT INTO `t_job_log` VALUES (4231, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:45:06');
INSERT INTO `t_job_log` VALUES (4232, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:07');
INSERT INTO `t_job_log` VALUES (4233, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:45:08');
INSERT INTO `t_job_log` VALUES (4234, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:09');
INSERT INTO `t_job_log` VALUES (4235, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:10');
INSERT INTO `t_job_log` VALUES (4236, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:45:11');
INSERT INTO `t_job_log` VALUES (4237, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:12');
INSERT INTO `t_job_log` VALUES (4238, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:13');
INSERT INTO `t_job_log` VALUES (4239, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:45:14');
INSERT INTO `t_job_log` VALUES (4240, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:15');
INSERT INTO `t_job_log` VALUES (4241, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:16');
INSERT INTO `t_job_log` VALUES (4242, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:17');
INSERT INTO `t_job_log` VALUES (4243, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:18');
INSERT INTO `t_job_log` VALUES (4244, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:19');
INSERT INTO `t_job_log` VALUES (4245, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:20');
INSERT INTO `t_job_log` VALUES (4246, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:45:21');
INSERT INTO `t_job_log` VALUES (4247, 1, 'testTask', 'test', 'mrbird', '0', NULL, 7, '2020-04-07 11:45:22');
INSERT INTO `t_job_log` VALUES (4248, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:45:23');
INSERT INTO `t_job_log` VALUES (4249, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:24');
INSERT INTO `t_job_log` VALUES (4250, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:45:25');
INSERT INTO `t_job_log` VALUES (4251, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:26');
INSERT INTO `t_job_log` VALUES (4252, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:27');
INSERT INTO `t_job_log` VALUES (4253, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:28');
INSERT INTO `t_job_log` VALUES (4254, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:29');
INSERT INTO `t_job_log` VALUES (4255, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:30');
INSERT INTO `t_job_log` VALUES (4256, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:31');
INSERT INTO `t_job_log` VALUES (4257, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:45:32');
INSERT INTO `t_job_log` VALUES (4258, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:33');
INSERT INTO `t_job_log` VALUES (4259, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:34');
INSERT INTO `t_job_log` VALUES (4260, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:35');
INSERT INTO `t_job_log` VALUES (4261, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:36');
INSERT INTO `t_job_log` VALUES (4262, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:45:37');
INSERT INTO `t_job_log` VALUES (4263, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:38');
INSERT INTO `t_job_log` VALUES (4264, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:39');
INSERT INTO `t_job_log` VALUES (4265, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:45:40');
INSERT INTO `t_job_log` VALUES (4266, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:41');
INSERT INTO `t_job_log` VALUES (4267, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:45:42');
INSERT INTO `t_job_log` VALUES (4268, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:43');
INSERT INTO `t_job_log` VALUES (4269, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:45:44');
INSERT INTO `t_job_log` VALUES (4270, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:45');
INSERT INTO `t_job_log` VALUES (4271, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:46');
INSERT INTO `t_job_log` VALUES (4272, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:47');
INSERT INTO `t_job_log` VALUES (4273, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:45:48');
INSERT INTO `t_job_log` VALUES (4274, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:49');
INSERT INTO `t_job_log` VALUES (4275, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:50');
INSERT INTO `t_job_log` VALUES (4276, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:51');
INSERT INTO `t_job_log` VALUES (4277, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:45:52');
INSERT INTO `t_job_log` VALUES (4278, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:45:53');
INSERT INTO `t_job_log` VALUES (4279, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:54');
INSERT INTO `t_job_log` VALUES (4280, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:55');
INSERT INTO `t_job_log` VALUES (4281, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:56');
INSERT INTO `t_job_log` VALUES (4282, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:57');
INSERT INTO `t_job_log` VALUES (4283, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-07 11:45:58');
INSERT INTO `t_job_log` VALUES (4284, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:45:59');
INSERT INTO `t_job_log` VALUES (4285, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:00');
INSERT INTO `t_job_log` VALUES (4286, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:01');
INSERT INTO `t_job_log` VALUES (4287, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:02');
INSERT INTO `t_job_log` VALUES (4288, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:03');
INSERT INTO `t_job_log` VALUES (4289, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:04');
INSERT INTO `t_job_log` VALUES (4290, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:05');
INSERT INTO `t_job_log` VALUES (4291, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:06');
INSERT INTO `t_job_log` VALUES (4292, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:07');
INSERT INTO `t_job_log` VALUES (4293, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:46:08');
INSERT INTO `t_job_log` VALUES (4294, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:46:09');
INSERT INTO `t_job_log` VALUES (4295, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:46:10');
INSERT INTO `t_job_log` VALUES (4296, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:11');
INSERT INTO `t_job_log` VALUES (4297, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:12');
INSERT INTO `t_job_log` VALUES (4298, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:46:13');
INSERT INTO `t_job_log` VALUES (4299, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:14');
INSERT INTO `t_job_log` VALUES (4300, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:46:15');
INSERT INTO `t_job_log` VALUES (4301, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:16');
INSERT INTO `t_job_log` VALUES (4302, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:46:17');
INSERT INTO `t_job_log` VALUES (4303, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:18');
INSERT INTO `t_job_log` VALUES (4304, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:46:19');
INSERT INTO `t_job_log` VALUES (4305, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:20');
INSERT INTO `t_job_log` VALUES (4306, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:21');
INSERT INTO `t_job_log` VALUES (4307, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:46:22');
INSERT INTO `t_job_log` VALUES (4308, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:46:23');
INSERT INTO `t_job_log` VALUES (4309, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:24');
INSERT INTO `t_job_log` VALUES (4310, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:46:25');
INSERT INTO `t_job_log` VALUES (4311, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:26');
INSERT INTO `t_job_log` VALUES (4312, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:27');
INSERT INTO `t_job_log` VALUES (4313, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:28');
INSERT INTO `t_job_log` VALUES (4314, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:29');
INSERT INTO `t_job_log` VALUES (4315, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:46:30');
INSERT INTO `t_job_log` VALUES (4316, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:31');
INSERT INTO `t_job_log` VALUES (4317, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:32');
INSERT INTO `t_job_log` VALUES (4318, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:33');
INSERT INTO `t_job_log` VALUES (4319, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:46:34');
INSERT INTO `t_job_log` VALUES (4320, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:35');
INSERT INTO `t_job_log` VALUES (4321, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:36');
INSERT INTO `t_job_log` VALUES (4322, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:37');
INSERT INTO `t_job_log` VALUES (4323, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:38');
INSERT INTO `t_job_log` VALUES (4324, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:46:39');
INSERT INTO `t_job_log` VALUES (4325, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:46:40');
INSERT INTO `t_job_log` VALUES (4326, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:41');
INSERT INTO `t_job_log` VALUES (4327, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:42');
INSERT INTO `t_job_log` VALUES (4328, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:46:43');
INSERT INTO `t_job_log` VALUES (4329, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:44');
INSERT INTO `t_job_log` VALUES (4330, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:46:45');
INSERT INTO `t_job_log` VALUES (4331, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:46:46');
INSERT INTO `t_job_log` VALUES (4332, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:47');
INSERT INTO `t_job_log` VALUES (4333, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:46:48');
INSERT INTO `t_job_log` VALUES (4334, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:49');
INSERT INTO `t_job_log` VALUES (4335, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:50');
INSERT INTO `t_job_log` VALUES (4336, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:51');
INSERT INTO `t_job_log` VALUES (4337, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:52');
INSERT INTO `t_job_log` VALUES (4338, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:53');
INSERT INTO `t_job_log` VALUES (4339, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:54');
INSERT INTO `t_job_log` VALUES (4340, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:46:55');
INSERT INTO `t_job_log` VALUES (4341, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:46:56');
INSERT INTO `t_job_log` VALUES (4342, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:57');
INSERT INTO `t_job_log` VALUES (4343, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:46:58');
INSERT INTO `t_job_log` VALUES (4344, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:46:59');
INSERT INTO `t_job_log` VALUES (4345, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:00');
INSERT INTO `t_job_log` VALUES (4346, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:01');
INSERT INTO `t_job_log` VALUES (4347, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:02');
INSERT INTO `t_job_log` VALUES (4348, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-07 11:47:03');
INSERT INTO `t_job_log` VALUES (4349, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:04');
INSERT INTO `t_job_log` VALUES (4350, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:05');
INSERT INTO `t_job_log` VALUES (4351, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:06');
INSERT INTO `t_job_log` VALUES (4352, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:07');
INSERT INTO `t_job_log` VALUES (4353, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:08');
INSERT INTO `t_job_log` VALUES (4354, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:47:09');
INSERT INTO `t_job_log` VALUES (4355, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:10');
INSERT INTO `t_job_log` VALUES (4356, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:11');
INSERT INTO `t_job_log` VALUES (4357, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:47:12');
INSERT INTO `t_job_log` VALUES (4358, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:13');
INSERT INTO `t_job_log` VALUES (4359, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:47:14');
INSERT INTO `t_job_log` VALUES (4360, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:15');
INSERT INTO `t_job_log` VALUES (4361, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:16');
INSERT INTO `t_job_log` VALUES (4362, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:17');
INSERT INTO `t_job_log` VALUES (4363, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:18');
INSERT INTO `t_job_log` VALUES (4364, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:19');
INSERT INTO `t_job_log` VALUES (4365, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:20');
INSERT INTO `t_job_log` VALUES (4366, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:21');
INSERT INTO `t_job_log` VALUES (4367, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:47:22');
INSERT INTO `t_job_log` VALUES (4368, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:23');
INSERT INTO `t_job_log` VALUES (4369, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:24');
INSERT INTO `t_job_log` VALUES (4370, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:25');
INSERT INTO `t_job_log` VALUES (4371, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:26');
INSERT INTO `t_job_log` VALUES (4372, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:47:27');
INSERT INTO `t_job_log` VALUES (4373, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:28');
INSERT INTO `t_job_log` VALUES (4374, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:29');
INSERT INTO `t_job_log` VALUES (4375, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:30');
INSERT INTO `t_job_log` VALUES (4376, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:31');
INSERT INTO `t_job_log` VALUES (4377, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:32');
INSERT INTO `t_job_log` VALUES (4378, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:33');
INSERT INTO `t_job_log` VALUES (4379, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:34');
INSERT INTO `t_job_log` VALUES (4380, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:35');
INSERT INTO `t_job_log` VALUES (4381, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:36');
INSERT INTO `t_job_log` VALUES (4382, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:37');
INSERT INTO `t_job_log` VALUES (4383, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:47:38');
INSERT INTO `t_job_log` VALUES (4384, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:39');
INSERT INTO `t_job_log` VALUES (4385, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:40');
INSERT INTO `t_job_log` VALUES (4386, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:41');
INSERT INTO `t_job_log` VALUES (4387, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:42');
INSERT INTO `t_job_log` VALUES (4388, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:43');
INSERT INTO `t_job_log` VALUES (4389, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:44');
INSERT INTO `t_job_log` VALUES (4390, 1, 'testTask', 'test', 'mrbird', '0', NULL, 5, '2020-04-07 11:47:45');
INSERT INTO `t_job_log` VALUES (4391, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:46');
INSERT INTO `t_job_log` VALUES (4392, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:47');
INSERT INTO `t_job_log` VALUES (4393, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:52');
INSERT INTO `t_job_log` VALUES (4394, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:52');
INSERT INTO `t_job_log` VALUES (4395, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:52');
INSERT INTO `t_job_log` VALUES (4396, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-07 11:47:52');
INSERT INTO `t_job_log` VALUES (4397, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:52');
INSERT INTO `t_job_log` VALUES (4398, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-07 11:47:53');
INSERT INTO `t_job_log` VALUES (4399, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:54');
INSERT INTO `t_job_log` VALUES (4400, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-07 11:47:55');
INSERT INTO `t_job_log` VALUES (4401, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:56');
INSERT INTO `t_job_log` VALUES (4402, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:47:57');
INSERT INTO `t_job_log` VALUES (4403, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-07 11:47:58');
INSERT INTO `t_job_log` VALUES (4404, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:47:59');
INSERT INTO `t_job_log` VALUES (4405, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:48:00');
INSERT INTO `t_job_log` VALUES (4406, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-07 11:48:01');
INSERT INTO `t_job_log` VALUES (4407, 1, 'testTask', 'test', 'mrbird', '0', NULL, 2, '2020-04-09 14:02:37');
INSERT INTO `t_job_log` VALUES (4408, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:02:38');
INSERT INTO `t_job_log` VALUES (4409, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:02:39');
INSERT INTO `t_job_log` VALUES (4410, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:02:40');
INSERT INTO `t_job_log` VALUES (4411, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:02:41');
INSERT INTO `t_job_log` VALUES (4412, 1, 'testTask', 'test', 'mrbird', '0', NULL, 3, '2020-04-09 14:02:42');
INSERT INTO `t_job_log` VALUES (4413, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-09 14:02:43');
INSERT INTO `t_job_log` VALUES (4414, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-09 14:02:44');
INSERT INTO `t_job_log` VALUES (4415, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:02:45');
INSERT INTO `t_job_log` VALUES (4416, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:02:46');
INSERT INTO `t_job_log` VALUES (4417, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:02:47');
INSERT INTO `t_job_log` VALUES (4418, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-09 14:02:48');
INSERT INTO `t_job_log` VALUES (4419, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:02:49');
INSERT INTO `t_job_log` VALUES (4420, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:02:50');
INSERT INTO `t_job_log` VALUES (4421, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:02:51');
INSERT INTO `t_job_log` VALUES (4422, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:02:52');
INSERT INTO `t_job_log` VALUES (4423, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:02:53');
INSERT INTO `t_job_log` VALUES (4424, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:02:54');
INSERT INTO `t_job_log` VALUES (4425, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:02:55');
INSERT INTO `t_job_log` VALUES (4426, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-09 14:02:56');
INSERT INTO `t_job_log` VALUES (4427, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:02:57');
INSERT INTO `t_job_log` VALUES (4428, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-09 14:02:58');
INSERT INTO `t_job_log` VALUES (4429, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-09 14:02:59');
INSERT INTO `t_job_log` VALUES (4430, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-09 14:03:00');
INSERT INTO `t_job_log` VALUES (4431, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-09 14:03:01');
INSERT INTO `t_job_log` VALUES (4432, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:03:02');
INSERT INTO `t_job_log` VALUES (4433, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:03:03');
INSERT INTO `t_job_log` VALUES (4434, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:03:04');
INSERT INTO `t_job_log` VALUES (4435, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-09 14:03:05');
INSERT INTO `t_job_log` VALUES (4436, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-09 14:03:06');
INSERT INTO `t_job_log` VALUES (4437, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:03:07');
INSERT INTO `t_job_log` VALUES (4438, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:03:08');
INSERT INTO `t_job_log` VALUES (4439, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:03:09');
INSERT INTO `t_job_log` VALUES (4440, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:03:10');
INSERT INTO `t_job_log` VALUES (4441, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-09 14:03:11');
INSERT INTO `t_job_log` VALUES (4442, 1, 'testTask', 'test', 'mrbird', '0', NULL, 1, '2020-04-09 14:03:12');
INSERT INTO `t_job_log` VALUES (4443, 1, 'testTask', 'test', 'mrbird', '0', NULL, 0, '2020-04-09 14:03:13');
INSERT INTO `t_job_log` VALUES (4444, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2020-04-10 11:12:52');
INSERT INTO `t_job_log` VALUES (4445, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:12:53');
INSERT INTO `t_job_log` VALUES (4446, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:12:54');
INSERT INTO `t_job_log` VALUES (4447, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2020-04-10 11:12:55');
INSERT INTO `t_job_log` VALUES (4448, 3, 'testTask', 'test', 'hello world', '0', NULL, 5, '2020-04-10 11:12:56');
INSERT INTO `t_job_log` VALUES (4449, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2020-04-10 11:12:57');
INSERT INTO `t_job_log` VALUES (4450, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:12:59');
INSERT INTO `t_job_log` VALUES (4451, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2020-04-10 11:12:59');
INSERT INTO `t_job_log` VALUES (4452, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:00');
INSERT INTO `t_job_log` VALUES (4453, 3, 'testTask', 'test', 'hello world', '0', NULL, 21, '2020-04-10 11:13:01');
INSERT INTO `t_job_log` VALUES (4454, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2020-04-10 11:13:02');
INSERT INTO `t_job_log` VALUES (4455, 3, 'testTask', 'test', 'hello world', '0', NULL, 10, '2020-04-10 11:13:03');
INSERT INTO `t_job_log` VALUES (4456, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:04');
INSERT INTO `t_job_log` VALUES (4457, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-04-10 11:13:05');
INSERT INTO `t_job_log` VALUES (4458, 3, 'testTask', 'test', 'hello world', '0', NULL, 7, '2020-04-10 11:13:06');
INSERT INTO `t_job_log` VALUES (4459, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-04-10 11:13:08');
INSERT INTO `t_job_log` VALUES (4460, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2020-04-10 11:13:09');
INSERT INTO `t_job_log` VALUES (4461, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:09');
INSERT INTO `t_job_log` VALUES (4462, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2020-04-10 11:13:10');
INSERT INTO `t_job_log` VALUES (4463, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2020-04-10 11:13:11');
INSERT INTO `t_job_log` VALUES (4464, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:12');
INSERT INTO `t_job_log` VALUES (4465, 3, 'testTask', 'test', 'hello world', '0', NULL, 6, '2020-04-10 11:13:13');
INSERT INTO `t_job_log` VALUES (4466, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:15');
INSERT INTO `t_job_log` VALUES (4467, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:15');
INSERT INTO `t_job_log` VALUES (4468, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:16');
INSERT INTO `t_job_log` VALUES (4469, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-04-10 11:13:17');
INSERT INTO `t_job_log` VALUES (4470, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2020-04-10 11:13:18');
INSERT INTO `t_job_log` VALUES (4471, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:19');
INSERT INTO `t_job_log` VALUES (4472, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:20');
INSERT INTO `t_job_log` VALUES (4473, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:21');
INSERT INTO `t_job_log` VALUES (4474, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:22');
INSERT INTO `t_job_log` VALUES (4475, 3, 'testTask', 'test', 'hello world', '0', NULL, 5, '2020-04-10 11:13:23');
INSERT INTO `t_job_log` VALUES (4476, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-04-10 11:13:24');
INSERT INTO `t_job_log` VALUES (4477, 3, 'testTask', 'test', 'hello world', '0', NULL, 29, '2020-04-10 11:13:25');
INSERT INTO `t_job_log` VALUES (4478, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-04-10 11:13:26');
INSERT INTO `t_job_log` VALUES (4479, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:27');
INSERT INTO `t_job_log` VALUES (4480, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-04-10 11:13:28');
INSERT INTO `t_job_log` VALUES (4481, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-04-10 11:13:29');
INSERT INTO `t_job_log` VALUES (4482, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2020-04-10 11:13:30');
INSERT INTO `t_job_log` VALUES (4483, 3, 'testTask', 'test', 'hello world', '0', NULL, 74, '2020-04-10 11:13:31');
INSERT INTO `t_job_log` VALUES (4484, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:32');
INSERT INTO `t_job_log` VALUES (4485, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:33');
INSERT INTO `t_job_log` VALUES (4486, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2020-04-10 11:13:34');
INSERT INTO `t_job_log` VALUES (4487, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:35');
INSERT INTO `t_job_log` VALUES (4488, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:36');
INSERT INTO `t_job_log` VALUES (4489, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-04-10 11:13:37');
INSERT INTO `t_job_log` VALUES (4490, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-04-10 11:13:38');
INSERT INTO `t_job_log` VALUES (4491, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2020-04-10 11:13:39');
INSERT INTO `t_job_log` VALUES (4492, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:40');
INSERT INTO `t_job_log` VALUES (4493, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:41');
INSERT INTO `t_job_log` VALUES (4494, 3, 'testTask', 'test', 'hello world', '0', NULL, 11, '2020-04-10 11:13:42');
INSERT INTO `t_job_log` VALUES (4495, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-04-10 11:13:43');
INSERT INTO `t_job_log` VALUES (4496, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:44');
INSERT INTO `t_job_log` VALUES (4497, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:45');
INSERT INTO `t_job_log` VALUES (4498, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:46');
INSERT INTO `t_job_log` VALUES (4499, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-04-10 11:13:47');
INSERT INTO `t_job_log` VALUES (4500, 3, 'testTask', 'test', 'hello world', '0', NULL, 8, '2020-04-10 11:13:48');
INSERT INTO `t_job_log` VALUES (4501, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:49');
INSERT INTO `t_job_log` VALUES (4502, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2020-04-10 11:13:50');
INSERT INTO `t_job_log` VALUES (4503, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2020-04-10 11:13:51');
INSERT INTO `t_job_log` VALUES (4504, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:52');
INSERT INTO `t_job_log` VALUES (4505, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:53');
INSERT INTO `t_job_log` VALUES (4506, 3, 'testTask', 'test', 'hello world', '0', NULL, 22, '2020-04-10 11:13:54');
INSERT INTO `t_job_log` VALUES (4507, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2020-04-10 11:13:55');
INSERT INTO `t_job_log` VALUES (4508, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2020-04-10 11:13:56');

-- ----------------------------
-- Table structure for t_log
-- ----------------------------
DROP TABLE IF EXISTS `t_log`;
CREATE TABLE `t_log`  (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志ID',
  `USERNAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作用户',
  `OPERATION` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '操作内容',
  `TIME` decimal(11, 0) NULL DEFAULT NULL COMMENT '耗时',
  `METHOD` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '操作方法',
  `PARAMS` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '方法参数',
  `IP` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作者IP',
  `CREATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `location` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作地点',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1391 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_log
-- ----------------------------
INSERT INTO `t_log` VALUES (1011, 'MrBird', '修改角色', 254, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Thu Mar 19 15:14:16 CST 2020, menuIds=1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164,178,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.138', '2020-03-19 15:14:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1012, 'MrBird', '修改角色', 282, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Thu Mar 19 15:14:34 CST 2020, menuIds=1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164,178,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.138', '2020-03-19 15:14:35', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1013, 'MrBird', '修改角色', 408, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Thu Mar 19 15:16:55 CST 2020, menuIds=1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164,178,179,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.138', '2020-03-19 15:16:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1014, 'MrBird', '新增角色', 142, 'cc.mrbird.febs.system.controller.RoleController.addRole()', ' role: \"Role(roleId=81, roleName=老师, remark=学校老师, createTime=Fri Mar 20 08:58:03 CST 2020, modifyTime=null, menuIds=1,178,2)\"', '192.168.2.138', '2020-03-20 08:58:03', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1015, 'MrBird', '新增角色', 145, 'cc.mrbird.febs.system.controller.RoleController.addRole()', ' role: \"Role(roleId=82, roleName=学生, remark=学校学生, createTime=Fri Mar 20 08:59:28 CST 2020, modifyTime=null, menuIds=1,178)\"', '192.168.2.138', '2020-03-20 08:59:29', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1016, 'MrBird', '新增用户', 144, 'cc.mrbird.febs.system.controller.UserController.addUser()', ' user: \"User(userId=8, username=admin, password=470263a5622b6153be9e681545233f55, deptId=1, email=105@qq.com, mobile=13011112222, status=1, createTime=Fri Mar 20 09:23:15 CST 2020, modifyTime=null, lastLoginTime=null, sex=0, avatar=default.jpg, theme=black, isTab=1, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1, roleName=null)\"', '192.168.2.138', '2020-03-20 09:23:16', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1017, 'MrBird', '新增部门', 121, 'cc.mrbird.febs.system.controller.DeptController.addDept()', ' dept: \"Dept(deptId=11, parentId=0, deptName=机电学院, orderNum=1, createTime=Fri Mar 20 09:41:50 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-20 09:41:50', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1018, 'MrBird', '新增部门', 193, 'cc.mrbird.febs.system.controller.DeptController.addDept()', ' dept: \"Dept(deptId=12, parentId=11, deptName=电子信息工程, orderNum=2, createTime=Fri Mar 20 09:43:32 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-20 09:43:32', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1019, 'MrBird', '新增部门', 774, 'cc.mrbird.febs.system.controller.DeptController.addDept()', ' dept: \"Dept(deptId=13, parentId=12, deptName=1班, orderNum=3, createTime=Fri Mar 20 09:43:55 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-20 09:43:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1020, 'MrBird', '修改用户', 221, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=11, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Fri Mar 20 09:51:36 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-20 09:51:37', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1021, 'admin', '修改角色', 390, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=82, roleName=学生, remark=学校学生, createTime=null, modifyTime=Fri Mar 20 15:26:27 CST 2020, menuIds=181,182,178,179,180)\"', '192.168.2.138', '2020-03-20 15:26:28', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1022, 'admin', '新增部门', 118, 'cc.mrbird.febs.system.controller.DeptController.addDept()', ' dept: \"Dept(deptId=20, parentId=18, deptName=1522班, orderNum=2, createTime=Fri Mar 20 15:27:05 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-20 15:27:05', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1023, 'admin', '修改用户', 304, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 14:54:02 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 14:54:03', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1024, 'admin', '修改用户', 154, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 14:54:15 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 14:54:16', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1025, 'admin', '修改用户', 186, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:01:47 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:01:48', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1026, 'admin', '修改用户', 260, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:01:55 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:01:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1027, 'admin', '修改用户', 156, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:02:17 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:02:18', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1028, 'admin', '修改用户', 198, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:03:11 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:03:11', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1029, 'admin', '修改用户', 225, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:03:56 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:03:57', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1030, 'admin', '修改用户', 234, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:07:18 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:07:19', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1031, 'admin', '修改用户', 231, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:09:29 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:09:30', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1032, 'admin', '修改用户', 161, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:09:38 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:09:38', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1033, 'admin', '修改用户', 245, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:11:25 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:11:26', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1034, 'admin', '修改用户', 235, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=15, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:17:16 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:17:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1035, 'admin', '修改用户', 195, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:28:38 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81,82, roleName=null)\"', '192.168.2.138', '2020-03-23 15:28:39', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1036, 'admin', '修改用户', 195, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:32:41 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:32:42', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1037, 'admin', '修改用户', 172, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:32:52 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:32:53', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1038, 'admin', '修改用户', 246, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:37:50 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:37:51', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1039, 'admin', '修改用户', 230, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:38:17 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:38:18', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1040, 'admin', '修改用户', 174, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:38:55 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:38:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1041, 'admin', '修改用户', 172, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:39:15 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:39:16', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1042, 'admin', '修改用户', 244, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:39:49 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:39:50', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1043, 'admin', '修改用户', 188, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:40:09 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:40:10', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1044, 'admin', '修改用户', 193, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:40:26 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:40:27', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1045, 'admin', '修改用户', 627, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:43:44 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:43:45', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1046, 'admin', '修改用户', 222, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 15:55:21 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\"', '192.168.2.138', '2020-03-23 15:55:22', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1047, 'admin', '修改用户', 358, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 16:01:04 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\" dept: \"[Ljava.lang.Integer;@1765c87\"', '192.168.2.138', '2020-03-23 16:01:05', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1048, 'admin', '修改用户', 51679, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 16:03:04 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\" dept: \"[Ljava.lang.Integer;@2fd8b03\"', '192.168.2.138', '2020-03-23 16:03:05', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1049, 'admin', '修改GeneratorConfig', 120, 'cc.mrbird.febs.generator.controller.GeneratorConfigController.updateGeneratorConfig()', ' generatorConfig: GeneratorConfig(id=1, author=Macw, basePackage=cc.mrbird.febs.system, entityPackage=entity, mapperPackage=mapper, mapperXmlPackage=mapper, servicePackage=service, serviceImplPackage=service.impl, controllerPackage=controller, isTrim=1, trimValue=t_, javaPath=/src/main/java/, resourcesPath=src/main/resources, date=2020-03-23 16:04:27, tableName=null, tableComment=null, className=null)', '192.168.2.138', '2020-03-23 16:04:27', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1050, 'admin', '修改用户', 21245, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, deptId=null, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Mon Mar 23 16:14:53 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\" dept: \"[Ljava.lang.Integer;@49c1f4b1\"', '192.168.2.138', '2020-03-23 16:14:54', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1051, 'admin', '修改用户', 232, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=10, username=null, password=null, deptId=null, email=135@qq.com, mobile=13055556666, status=1, createTime=null, modifyTime=Mon Mar 23 17:36:52 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=老师测试, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=81, roleName=null)\" dept: \"[Ljava.lang.Integer;@7e7a6977\"', '192.168.2.138', '2020-03-23 17:36:52', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1052, 'admin', '修改用户', 395, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=9, username=null, password=null, deptId=null, email=1111@qq.com, mobile=13566699330, status=1, createTime=null, modifyTime=Mon Mar 23 17:44:19 CST 2020, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=学生1号, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=82, roleName=null)\" dept: \"[Ljava.lang.Integer;@39eeb04a\"', '192.168.2.138', '2020-03-23 17:44:19', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1053, 'admin', '修改用户', 293, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=10, username=null, password=null, deptId=null, email=135@qq.com, mobile=13055556666, status=1, createTime=null, modifyTime=Mon Mar 23 17:47:08 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=老师测试, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=81, roleName=null)\" dept: \"[Ljava.lang.Integer;@101356c\"', '192.168.2.138', '2020-03-23 17:47:08', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1054, 'admin', '修改用户', 211, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=9, username=null, password=null, deptId=null, email=1111@qq.com, mobile=13566699330, status=1, createTime=null, modifyTime=Mon Mar 23 17:47:30 CST 2020, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=学生1号, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=82, roleName=null)\" dept: \"[Ljava.lang.Integer;@e621f31\"', '192.168.2.138', '2020-03-23 17:47:31', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1055, 'admin', '修改用户', 248, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=9, username=null, password=null, deptId=null, email=1111@qq.com, mobile=13566699330, status=1, createTime=null, modifyTime=Mon Mar 23 17:47:39 CST 2020, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=学生1号, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=82, roleName=null)\" dept: \"[Ljava.lang.Integer;@4d9560d\"', '192.168.2.138', '2020-03-23 17:47:40', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1056, 'admin', '新增部门', 178, 'cc.mrbird.febs.system.controller.DeptController.addDept()', ' dept: \"Dept(deptId=21, parentId=0, deptName=郑州大学, orderNum=10, createTime=Mon Mar 23 17:49:07 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-23 17:49:08', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1057, 'admin', '新增部门', 123, 'cc.mrbird.febs.system.controller.DeptController.addDept()', ' dept: \"Dept(deptId=22, parentId=21, deptName=计算机学院, orderNum=20, createTime=Mon Mar 23 17:49:26 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-23 17:49:26', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1058, 'admin', '修改用户', 543, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=9, username=null, password=null, deptId=null, email=1111@qq.com, mobile=13566699330, status=1, createTime=null, modifyTime=Mon Mar 23 17:57:57 CST 2020, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=学生1号, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=82, roleName=null)\" dept: \"[Ljava.lang.Integer;@65d92863\"', '192.168.2.138', '2020-03-23 17:57:57', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1059, 'admin', '修改用户', 280, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=10, username=null, password=null, deptId=null, email=135@qq.com, mobile=13055556666, status=1, createTime=null, modifyTime=Mon Mar 23 17:58:05 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=老师测试, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=81, roleName=null)\" dept: \"[Ljava.lang.Integer;@2f2f6bf8\"', '192.168.2.138', '2020-03-23 17:58:05', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1060, 'admin', '新增部门', 193, 'cc.mrbird.febs.system.controller.DeptController.addDept()', ' dept: \"Dept(deptId=23, parentId=22, deptName=计算机科学1501, orderNum=20, createTime=Mon Mar 23 18:25:08 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-23 18:25:09', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1061, 'admin', '新增部门', 165, 'cc.mrbird.febs.system.controller.DeptController.addDept()', ' dept: \"Dept(deptId=24, parentId=23, deptName=模电, orderNum=20, createTime=Mon Mar 23 18:25:28 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-23 18:25:28', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1062, 'admin', '新增部门', 191, 'cc.mrbird.febs.system.controller.DeptController.addDept()', ' dept: \"Dept(deptId=25, parentId=22, deptName=计算科学1602, orderNum=30, createTime=Mon Mar 23 18:25:56 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-23 18:25:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1063, 'admin', '修改用户', 345, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=9, username=null, password=null, deptId=null, email=1111@qq.com, mobile=13566699330, status=1, createTime=null, modifyTime=Mon Mar 23 18:26:34 CST 2020, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=学生1号, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=82, roleName=null)\" dept: \"[Ljava.lang.Integer;@18ec91b2\"', '192.168.2.138', '2020-03-23 18:26:34', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1064, 'admin', '新增菜单/按钮', 105, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=184, parentId=0, menuName=教学管理, url=, perms=, icon=layui-icon-qrcode, type=0, orderNum=null, createTime=Tue Mar 24 09:50:57 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-24 09:50:57', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1065, 'admin', '新增菜单/按钮', 153, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=185, parentId=184, menuName=老师管理, url=, perms=, icon=layui-icon-deleteteam, type=0, orderNum=null, createTime=Tue Mar 24 09:52:18 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-24 09:52:18', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1066, 'admin', '新增菜单/按钮', 106, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=186, parentId=184, menuName=学生管理, url=, perms=, icon=layui-icon-deleteuser, type=0, orderNum=null, createTime=Tue Mar 24 09:52:45 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-24 09:52:45', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1067, 'admin', '新增菜单/按钮', 176, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=187, parentId=184, menuName=班级管理, url=, perms=, icon=, type=0, orderNum=null, createTime=Tue Mar 24 10:24:30 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-24 10:24:31', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1068, 'admin', '删除菜单/按钮', 193, 'cc.mrbird.febs.system.controller.MenuController.deleteMenus()', ' menuIds: \"187\"', '192.168.2.138', '2020-03-24 10:27:52', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1069, 'admin', '修改菜单/按钮', 169, 'cc.mrbird.febs.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=6, parentId=184, menuName=班级管理, url=/system/dept, perms=dept:view, icon=, type=0, orderNum=4, createTime=null, modifyTime=Tue Mar 24 10:28:06 CST 2020)\"', '192.168.2.138', '2020-03-24 10:28:07', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1070, 'admin', '修改角色', 491, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Tue Mar 24 10:29:39 CST 2020, menuIds=181,182,183,184,185,186,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,178,179,180,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.138', '2020-03-24 10:29:40', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1071, 'admin', '修改菜单/按钮', 151, 'cc.mrbird.febs.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=181, parentId=0, menuName=实验管理, url=, perms=, icon=layui-icon-project, type=0, orderNum=null, createTime=null, modifyTime=Tue Mar 24 10:34:09 CST 2020)\"', '192.168.2.138', '2020-03-24 10:34:10', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1072, 'admin', '修改菜单/按钮', 200, 'cc.mrbird.febs.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=182, parentId=181, menuName=项目管理, url=, perms=, icon=layui-icon-unorderedlist, type=0, orderNum=null, createTime=null, modifyTime=Tue Mar 24 10:35:46 CST 2020)\"', '192.168.2.138', '2020-03-24 10:35:47', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1073, 'admin', '修改菜单/按钮', 169, 'cc.mrbird.febs.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=183, parentId=181, menuName=报告管理, url=, perms=, icon=layui-icon-barchart, type=0, orderNum=null, createTime=null, modifyTime=Tue Mar 24 10:36:04 CST 2020)\"', '192.168.2.138', '2020-03-24 10:36:04', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1074, 'admin', '新增菜单/按钮', 91, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=188, parentId=181, menuName=消息管理, url=, perms=, icon=, type=0, orderNum=null, createTime=Tue Mar 24 10:36:49 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-24 10:36:49', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1075, 'admin', '修改菜单/按钮', 159, 'cc.mrbird.febs.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=6, parentId=184, menuName=班级管理, url=/system/dept, perms=dept:view, icon=layui-icon-solution, type=0, orderNum=4, createTime=null, modifyTime=Tue Mar 24 10:38:42 CST 2020)\"', '192.168.2.138', '2020-03-24 10:38:42', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1076, 'admin', '删除菜单/按钮', 186, 'cc.mrbird.febs.system.controller.MenuController.deleteMenus()', ' menuIds: \"185,186\"', '192.168.2.138', '2020-03-24 10:39:22', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1077, 'admin', '删除菜单/按钮', 159, 'cc.mrbird.febs.system.controller.MenuController.deleteMenus()', ' menuIds: \"178\"', '192.168.2.138', '2020-03-24 10:40:38', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1078, 'admin', '新增菜单/按钮', 113, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=189, parentId=184, menuName=课程管理, url=, perms=, icon=, type=0, orderNum=null, createTime=Tue Mar 24 10:43:51 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-24 10:43:51', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1079, 'admin', '修改角色', 251, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Tue Mar 24 10:44:01 CST 2020, menuIds=181,182,183,184,189,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.138', '2020-03-24 10:44:02', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1080, 'admin', '修改菜单/按钮', 132, 'cc.mrbird.febs.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=189, parentId=184, menuName=课程管理, url=, perms=, icon=layui-icon-detail, type=0, orderNum=null, createTime=null, modifyTime=Tue Mar 24 10:44:44 CST 2020)\"', '192.168.2.138', '2020-03-24 10:44:45', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1081, 'admin', '修改部门', 203, 'cc.mrbird.febs.system.controller.DeptController.updateDept()', ' dept: \"Dept(deptId=12, parentId=11, deptName=2013级, orderNum=2, createTime=null, modifyTime=Tue Mar 24 14:27:16 CST 2020)\"', '192.168.2.138', '2020-03-24 14:27:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1082, 'admin', '修改部门', 121, 'cc.mrbird.febs.system.controller.DeptController.updateDept()', ' dept: \"Dept(deptId=13, parentId=12, deptName=电子信息工程, orderNum=3, createTime=null, modifyTime=Tue Mar 24 14:27:33 CST 2020)\"', '192.168.2.138', '2020-03-24 14:27:34', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1083, 'admin', '修改部门', 126, 'cc.mrbird.febs.system.controller.DeptController.updateDept()', ' dept: \"Dept(deptId=13, parentId=12, deptName=电子信息工程1301, orderNum=3, createTime=null, modifyTime=Tue Mar 24 14:27:48 CST 2020)\"', '192.168.2.138', '2020-03-24 14:27:48', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1084, 'admin', '修改部门', 111, 'cc.mrbird.febs.system.controller.DeptController.updateDept()', ' dept: \"Dept(deptId=12, parentId=11, deptName=2013, orderNum=2, createTime=null, modifyTime=Tue Mar 24 14:33:11 CST 2020)\"', '192.168.2.138', '2020-03-24 14:33:11', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1085, 'admin', '新增菜单/按钮', 88, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=190, parentId=189, menuName=添加, url=null, perms=course:add, icon=null, type=1, orderNum=null, createTime=Wed Mar 25 10:49:05 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-25 10:49:06', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1086, 'admin', '修改角色', 312, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Wed Mar 25 11:26:44 CST 2020, menuIds=181,182,183,184,189,190,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.138', '2020-03-25 11:26:45', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1087, 'admin', '删除菜单/按钮', 167, 'cc.mrbird.febs.system.controller.MenuController.deleteMenus()', ' menuIds: \"188\"', '192.168.2.138', '2020-03-25 15:43:07', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1088, 'admin', '修改菜单/按钮', 126, 'cc.mrbird.febs.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=182, parentId=181, menuName=在线实验, url=/system/experiment, perms=experiment:view, icon=layui-icon-unorderedlist, type=0, orderNum=null, createTime=null, modifyTime=Wed Mar 25 15:43:26 CST 2020)\"', '192.168.2.138', '2020-03-25 15:43:27', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1089, 'admin', '新增菜单/按钮', 87, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=191, parentId=181, menuName=实验项目, url=, perms=, icon=layui-icon-detail, type=0, orderNum=null, createTime=Wed Mar 25 15:43:55 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-25 15:43:55', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1090, 'admin', '修改菜单/按钮', 123, 'cc.mrbird.febs.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=182, parentId=181, menuName=在线实验, url=/system/experiment, perms=experiment:view, icon=layui-icon-unorderedlist, type=0, orderNum=1, createTime=null, modifyTime=Wed Mar 25 15:44:05 CST 2020)\"', '192.168.2.138', '2020-03-25 15:44:05', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1091, 'admin', '修改菜单/按钮', 134, 'cc.mrbird.febs.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=191, parentId=181, menuName=实验项目, url=, perms=, icon=layui-icon-detail, type=0, orderNum=2, createTime=null, modifyTime=Wed Mar 25 15:44:17 CST 2020)\"', '192.168.2.138', '2020-03-25 15:44:18', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1092, 'admin', '修改菜单/按钮', 114, 'cc.mrbird.febs.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=183, parentId=181, menuName=报告管理, url=, perms=, icon=layui-icon-barchart, type=0, orderNum=3, createTime=null, modifyTime=Wed Mar 25 15:44:29 CST 2020)\"', '192.168.2.138', '2020-03-25 15:44:29', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1093, 'admin', '修改菜单/按钮', 115, 'cc.mrbird.febs.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=183, parentId=181, menuName=实验报告, url=, perms=, icon=layui-icon-barchart, type=0, orderNum=3, createTime=null, modifyTime=Wed Mar 25 15:44:44 CST 2020)\"', '192.168.2.138', '2020-03-25 15:44:45', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1094, 'admin', '修改角色', 282, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Wed Mar 25 15:45:40 CST 2020, menuIds=181,182,191,183,184,189,190,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.138', '2020-03-25 15:45:41', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1095, 'admin', '新增菜单/按钮', 86, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=192, parentId=182, menuName=添加实验, url=null, perms=experiment:add, icon=null, type=1, orderNum=null, createTime=Wed Mar 25 16:10:53 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-25 16:10:54', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1096, 'admin', '修改角色', 235, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Wed Mar 25 16:42:32 CST 2020, menuIds=181,182,192,191,183,184,189,190,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.138', '2020-03-25 16:42:32', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1097, 'admin', '新增实验', 94, 'cc.mrbird.febs.system.controller.ExperimentController.addExperiment()', ' experiment: \"Experiment(experimentId=3, experimentCode=null, experimentName=模拟电子实验, studyTime=null, experimentBook=null, checkType=统考, experimentCount=null, remark=请输入 实验内容 和描述&nbsp; 模拟电子在线实验室采用远程实验技术，通过网络访问远程端真实的电子仪器仪表及实验电路板，完成基本的模拟电子实验。该实验室可作为电子技术类课程实验教学，也可用于教师在课堂上通过投影为学生演示，还可作为开放实验室供学生使用。&nbsp; &nbsp; &nbsp; &nbsp; 实验室旨在培养学生的基本实验技能，加深对模拟电子技术基本原理的理解，了解电子线路的设计分析方法。熟悉并掌握常用仪器的使用方法，学会正确测量电子器件和系统参数的方法。, requireContent=null, fileName=null, fileUrl=null, fileSize=null, state=1, operator=null, createTime=2020-03-25T17:25:59.861, updateTime=null)\"', '192.168.2.138', '2020-03-25 17:26:00', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1098, 'admin', '新增实验', 87, 'cc.mrbird.febs.system.controller.ExperimentController.addExperiment()', ' experiment: \"Experiment(experimentId=4, experimentCode=null, experimentName=数字电子实验, studyTime=null, experimentBook=null, checkType=统考, experimentCount=null, remark=&nbsp; &nbsp; &nbsp; &nbsp; 数字电子在线实验室采用远程实验技术，通过网络访问远程端真实的数字电路实验板、电平开关、电平指示器及电子仪器仪表，完成基本的数字电子实验。该实验室可作为电子技术类课程实验教学，也可用于教师在课堂上通过投影为学生演示，还可作为开放实验室供学生使用。&nbsp; &nbsp; &nbsp; &nbsp; 数字电路可以分为组合逻辑电路和时序逻辑电路两大类，实验项目涵盖了主要的电路类型。通过实验室可培养学生的基本实验技能，加深对数字电子技术基本原理的理解，了解数字电路逻辑的组成及设计方法。, requireContent=null, fileName=null, fileUrl=null, fileSize=null, state=1, operator=null, createTime=2020-03-25T17:30:56.102, updateTime=null)\"', '192.168.2.138', '2020-03-25 17:30:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1099, 'admin', '新增菜单/按钮', 69, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=193, parentId=191, menuName=添加实验, url=null, perms=experimentProject:add, icon=null, type=1, orderNum=null, createTime=Thu Mar 26 10:32:05 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-26 10:32:06', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1100, 'admin', '修改角色', 242, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Thu Mar 26 10:32:16 CST 2020, menuIds=181,182,192,191,193,183,184,189,190,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.138', '2020-03-26 10:32:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1101, 'admin', '修改菜单/按钮', 144, 'cc.mrbird.febs.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=193, parentId=191, menuName=添加项目, url=null, perms=experimentProject:add, icon=null, type=1, orderNum=null, createTime=null, modifyTime=Thu Mar 26 10:32:30 CST 2020)\"', '192.168.2.138', '2020-03-26 10:32:30', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1102, 'admin', '新增项目', 94, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=2, projectCode=null, projectName=null, experimentId=3, projectContent=请输入 项目描述, projectSort=null, state=1, operator=null, createTime=2020-03-26T10:54:16.480, updateTime=null)\"', '192.168.2.138', '2020-03-26 10:54:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1103, 'admin', '新增项目', 72, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=3, projectCode=null, projectName=null, experimentId=3, projectContent=请输入 项目描述实验二 单级放大器的幅频特性和输入输出电阻的测量实验二 单级放大器的幅频特性和输入输出电阻的测量实验二 单级放大器的幅频特性和输入输出电阻的测量实验二 单级放大器的幅频特性和输入输出电阻的测量实验二 单级放大器的幅频特性和输入输出电阻的测量实验二 单级放大器的幅频特性和输入输出电阻的测量实验二 单级放大器的幅频特性和输入输出电阻的测量实验二 单级放大器的幅频特性和输入输出电阻的测量实验二 单级放大器的幅频特性和输入输出电阻的测量, projectSort=null, state=1, operator=null, createTime=2020-03-26T11:20:15.804, updateTime=null)\"', '192.168.2.138', '2020-03-26 11:20:16', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1104, 'admin', '新增项目', 78, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=4, projectCode=null, projectName=null, experimentId=3, projectContent=请输入 项目描述实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器, projectSort=null, state=1, operator=null, createTime=2020-03-26T11:27:30.855, updateTime=null)\"', '192.168.2.138', '2020-03-26 11:27:31', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1105, 'admin', '新增项目', 68, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=5, projectCode=null, projectName=实验三 负反馈放大器, experimentId=4, projectContent=请输入 项目描述实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器, projectSort=null, state=1, operator=null, createTime=2020-03-26T11:29:20.103, updateTime=null)\"', '192.168.2.138', '2020-03-26 11:29:20', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1106, 'admin', '新增项目', 66, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=6, projectCode=null, projectName=实验四 运算放大器信号运算电路, experimentId=4, experimentName=数字电子实验, projectContent=实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路, projectSort=null, state=1, operator=null, createTime=2020-03-26T11:32:06.012, updateTime=null)\"', '192.168.2.138', '2020-03-26 11:32:06', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1107, 'admin', '新增课程', 86, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=3, courseName=电子实验, classId=13, className=, state=1, teacherId=8, teacherName=admin, operator=null, createTime=2020-03-26T15:45:28.165, updateTime=null)\"', '192.168.2.138', '2020-03-26 15:45:28', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1108, 'admin', '新增课程', 90, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=4, courseName=数字电子, classId=13, className=电子信息工程1301, state=1, teacherId=10, teacherName=teacher001, operator=null, createTime=2020-03-26T15:57:04.098, updateTime=null)\"', '192.168.2.138', '2020-03-26 15:57:04', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1109, 'admin', '修改角色', 288, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Thu Mar 26 16:12:56 CST 2020, menuIds=181,182,192,191,193,183,184,189,190,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,8,23,10,24,170,136,171,172,127,128,129,131,175,102,103,104,105,106,107,108,173,109,110,174,138,165,139,166,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.138', '2020-03-26 16:12:57', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1110, 'admin', '新增菜单/按钮', 68, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=194, parentId=1, menuName=老师管理, url=, perms=, icon=, type=0, orderNum=null, createTime=Thu Mar 26 16:15:13 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-26 16:15:14', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1111, 'admin', '修改菜单/按钮', 127, 'cc.mrbird.febs.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=3, parentId=1, menuName=学生管理, url=/system/user, perms=user:view, icon=, type=0, orderNum=1, createTime=null, modifyTime=Thu Mar 26 16:16:27 CST 2020)\"', '192.168.2.138', '2020-03-26 16:16:28', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1112, 'admin', '修改角色', 189, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Thu Mar 26 16:20:16 CST 2020, menuIds=181,182,192,191,193,183,184,189,190,6,20,21,22,164,1,194,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,8,23,10,24,170,136,171,172,127,128,129,131,175,102,103,104,105,106,107,108,173,109,110,174,138,165,139,166,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.138', '2020-03-26 16:20:16', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1113, 'admin', '删除菜单/按钮', 118, 'cc.mrbird.febs.system.controller.MenuController.deleteMenus()', ' menuIds: \"194\"', '192.168.2.138', '2020-03-26 16:38:48', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1114, 'admin', '修改菜单/按钮', 111, 'cc.mrbird.febs.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=3, parentId=1, menuName=用户管理, url=/system/user, perms=user:view, icon=, type=0, orderNum=1, createTime=null, modifyTime=Thu Mar 26 16:38:59 CST 2020)\"', '192.168.2.138', '2020-03-26 16:38:59', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1115, 'admin', '修改角色', 232, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Thu Mar 26 17:32:47 CST 2020, menuIds=181,192,191,193,183,184,189,190,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,8,23,10,24,170,136,171,172,127,128,129,131,175,102,103,104,105,106,107,108,173,109,110,174,138,165,139,166,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.138', '2020-03-26 17:32:47', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1116, 'admin', '修改角色', 156, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Thu Mar 26 17:32:57 CST 2020, menuIds=181,191,193,183,184,189,190,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,8,23,10,24,170,136,171,172,127,128,129,131,175,102,103,104,105,106,107,108,173,109,110,174,138,165,139,166,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.138', '2020-03-26 17:32:57', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1117, 'admin', '新增课程', 254, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=6, courseName=电工技术, courseContent=null, deptNameList=[Ljava.lang.String;@17d070dd, state=1, createUser=null, createTime=2020-03-27T08:49:31.674, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@44202a37\" deptNameLists: \"[Ljava.lang.String;@17d070dd\"', '192.168.2.138', '2020-03-27 08:49:32', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1118, 'admin', '新增课程', 137501, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=7, courseName=sss, courseContent=null, deptNameList=[Ljava.lang.String;@3e774d5e, state=1, createUser=null, createTime=2020-03-27T08:52:22.037, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@49ef905c\" deptNameLists: \"[Ljava.lang.String;@3e774d5e\"', '192.168.2.138', '2020-03-27 08:54:40', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1119, 'admin', '新增课程', 47392, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=9, courseName=sss, courseContent=null, deptNameList=经济学院,外国语学院,农学院, state=1, createUser=null, createTime=2020-03-27T08:59:56.389, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@4b589d13\" deptNameLists: \"[Ljava.lang.String;@3e6bff53\"', '192.168.2.138', '2020-03-27 09:00:44', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1120, 'admin', '新增课程', 64333, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=10, courseName=wwww, courseContent=null, deptNameList=外国语学院,农学院, state=1, createUser=null, createTime=2020-03-27T09:02:47.096, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@69555286\" deptNameLists: \"[Ljava.lang.String;@227f74d3\"', '192.168.2.138', '2020-03-27 09:03:51', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1121, 'admin', '新增课程', 5610, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=11, courseName=ddd, courseContent=null, deptNameList=经济学院,外国语学院, state=1, createUser=null, createTime=2020-03-27T09:13:10.259, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@514dcdb6\" deptNameLists: \"[Ljava.lang.String;@6be50db4\"', '192.168.2.138', '2020-03-27 09:13:16', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1122, 'admin', '新增课程', 130, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=12, courseName=传感器技术, courseContent=&nbsp; 传感器实验是针对传感器原理、测试技术等课程的专业基础类实验。传感器在线实验室采用远程实验技术，通过网络访问控制远程端传感器及相关仪表，实时测量传感器变化带来的电气参数改变。实现与新临实验室相同的实验效果。进入实验&nbsp;&nbsp; 传感器实验是针对传感器原理、测试技术等课程的专业基础类实验。传感器在线实验室采用远程实验技术，通过网络访问控制远程端传感器及相关仪表，实时测量传感器变化带来的电气参数改变。实现与新临实验室相同的实验效果。进入实验, deptNameList=经济学院,外国语学院,农学院, state=1, createUser=null, createTime=2020-03-27T09:28:23.132, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@18c0d2d2\" deptNameLists: \"[Ljava.lang.String;@1cff5512\"', '192.168.2.138', '2020-03-27 09:28:23', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1123, 'admin', '新增课程', 173, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=13, courseName=ddddd, courseContent=请输入 课程内容 或描述gfdghvbjh, deptNameList=电子信息工程1301,1522班, state=1, createUser=null, createTime=2020-03-27T09:43:11.705, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@751ad4f8\" deptNameLists: \"[Ljava.lang.String;@4efe4aeb\"', '192.168.2.138', '2020-03-27 09:43:12', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1124, 'admin', '新增菜单/按钮', 69, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=195, parentId=0, menuName=学生管理, url=, perms=, icon=layui-icon-team, type=0, orderNum=null, createTime=Fri Mar 27 10:31:24 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-27 10:31:25', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1125, 'admin', '新增菜单/按钮', 76, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=196, parentId=195, menuName=学生信息, url=, perms=, icon=, type=0, orderNum=null, createTime=Fri Mar 27 10:32:30 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-27 10:32:31', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1126, 'admin', '新增菜单/按钮', 85, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=197, parentId=195, menuName=学生成绩, url=, perms=, icon=, type=0, orderNum=null, createTime=Fri Mar 27 10:32:40 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-27 10:32:40', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1127, 'admin', '新增菜单/按钮', 79, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=198, parentId=0, menuName=班级管理, url=, perms=, icon=layui-icon-layout, type=0, orderNum=null, createTime=Fri Mar 27 11:17:51 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-27 11:17:52', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1128, 'admin', '修改角色', 232, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=81, roleName=老师, remark=学校老师, createTime=null, modifyTime=Fri Mar 27 14:19:24 CST 2020, menuIds=183,195,196,197,198,199,200,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163)\"', '192.168.2.138', '2020-03-27 14:19:25', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1129, 'teacher001', '删除菜单/按钮', 166, 'cc.mrbird.febs.system.controller.MenuController.deleteMenus()', ' menuIds: \"196,197\"', '192.168.2.138', '2020-03-27 14:21:48', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1130, 'admin', '新增用户', 86, 'cc.mrbird.febs.system.controller.UserController.addUser()', ' user: \"User(userId=12, username=teacher002, password=8dc7a55f5a4fc7a1b90c6615b85e941a, deptId=null, email=135@163.com, mobile=13500001111, status=1, createTime=Fri Mar 27 15:58:10 CST 2020, modifyTime=null, lastLoginTime=null, sex=1, avatar=default.jpg, theme=black, isTab=1, description=我是一名老师, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=81, roleName=null)\"', '192.168.2.138', '2020-03-27 15:58:11', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1131, 'admin', '新增用户', 182, 'cc.mrbird.febs.system.controller.UserController.addUser()', ' user: \"User(userId=13, username=teacher003, password=4058039a61d21a269e114f3621b89a30, idNumber=11513153153, deptId=null, courseId=null, courseName=电工技术, email=163@163.cc, mobile=13522223333, status=1, createTime=Fri Mar 27 16:26:32 CST 2020, modifyTime=null, lastLoginTime=null, sex=1, avatar=default.jpg, theme=black, isTab=1, description=ssss, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=81, roleName=null)\" dept: \"[Ljava.lang.Integer;@3cd6ad80\"', '192.168.2.138', '2020-03-27 16:26:32', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1132, 'admin', '修改用户', 165, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=12, username=null, password=null, idNumber=null, deptId=null, courseId=null, courseName=null, email=135@163.com, mobile=13500001111, status=1, createTime=null, modifyTime=Fri Mar 27 16:38:14 CST 2020, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=我是一名老师, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=81, roleName=null)\" dept: \"[Ljava.lang.Integer;@5b8c4a41\"', '192.168.2.138', '2020-03-27 16:38:15', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1133, 'admin', '删除用户', 90, 'cc.mrbird.febs.system.controller.UserController.deleteUsers()', ' userIds: \"11\"', '192.168.2.138', '2020-03-27 16:38:25', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1134, 'admin', '修改用户', 263, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=13, username=null, password=null, idNumber=null, deptId=null, courseId=null, courseName=电工技术, email=163@163.cc, mobile=13522223333, status=1, createTime=null, modifyTime=Fri Mar 27 17:09:00 CST 2020, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=ssss, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=81, roleName=null)\" dept: \"[Ljava.lang.Integer;@1ddbfc52\"', '192.168.2.138', '2020-03-27 17:09:00', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1135, 'admin', '修改用户', 202, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=13, username=null, password=null, idNumber=null, deptId=null, courseId=null, courseName=电工技术, email=163@163.cc, mobile=13522223333, status=1, createTime=null, modifyTime=Fri Mar 27 17:09:13 CST 2020, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=ssss, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=81, roleName=null)\" dept: \"[Ljava.lang.Integer;@28f28f06\"', '192.168.2.138', '2020-03-27 17:09:14', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1136, 'admin', '修改用户', 201, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=13, username=null, password=null, idNumber=null, deptId=null, courseId=null, courseName=电子实验, email=163@163.cc, mobile=13522223333, status=1, createTime=null, modifyTime=Fri Mar 27 17:09:29 CST 2020, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=ssss, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=81, roleName=null)\" dept: \"[Ljava.lang.Integer;@269af953\"', '192.168.2.138', '2020-03-27 17:09:30', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1137, 'admin', '修改用户', 263, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=13, username=null, password=null, idNumber=null, deptId=null, courseId=null, courseName=电子实验, email=163@163.cc, mobile=13522223333, status=1, createTime=null, modifyTime=Fri Mar 27 17:11:49 CST 2020, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=ssss, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=81, roleName=null)\" dept: \"[Ljava.lang.Integer;@490b26f6\"', '192.168.2.138', '2020-03-27 17:11:50', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1138, 'admin', '修改用户', 265, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=13, username=null, password=null, idNumber=null, deptId=null, courseId=null, courseName=电工技术, email=163@163.cc, mobile=13522223333, status=1, createTime=null, modifyTime=Fri Mar 27 17:12:56 CST 2020, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=ssss, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=81, roleName=null)\" dept: \"[Ljava.lang.Integer;@41724299\"', '192.168.2.138', '2020-03-27 17:12:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1139, 'admin', '新增菜单/按钮', 69, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=201, parentId=0, menuName=学生管理, url=, perms=, icon=layui-icon-user, type=0, orderNum=null, createTime=Sat Mar 28 10:07:41 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-03-28 10:07:41', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1140, 'admin', '修改角色', 335, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Sat Mar 28 10:07:56 CST 2020, menuIds=181,191,193,183,184,189,190,6,20,21,22,164,201,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,8,23,10,24,170,136,171,172,127,128,129,131,175,102,103,104,105,106,107,108,173,109,110,174,138,165,139,166,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.138', '2020-03-28 10:07:57', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1141, 'admin', '修改角色', 306, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=81, roleName=老师, remark=学校老师, createTime=null, modifyTime=Sat Mar 28 15:46:38 CST 2020, menuIds=183,198,199,200,201,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163)\"', '192.168.2.138', '2020-03-28 15:46:39', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1142, 'admin', '修改角色', 239, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=81, roleName=老师, remark=学校老师, createTime=null, modifyTime=Sat Mar 28 16:01:35 CST 2020, menuIds=183,198,199,200,201)\"', '192.168.2.138', '2020-03-28 16:01:36', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1143, 'admin', '修改用户', 258, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, idNumber=null, deptId=null, courseId=null, courseName=, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Sat Mar 28 16:01:48 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=1, roleName=null)\" dept: \"[Ljava.lang.Integer;@3a235411\"', '192.168.2.138', '2020-03-28 16:01:49', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1144, 'admin', '修改角色', 196, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Sat Mar 28 16:02:32 CST 2020, menuIds=181,191,193,183,184,189,190,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,8,23,10,24,170,136,171,172,127,128,129,131,175,102,103,104,105,106,107,108,173,109,110,174,138,165,139,166,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.138', '2020-03-28 16:02:32', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1145, 'admin', '修改角色', 227, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Sat Mar 28 16:38:36 CST 2020, menuIds=181,191,193,183,184,189,190,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.138', '2020-03-28 16:38:37', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1146, 'admin', '新增菜单/按钮', 82, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=202, parentId=189, menuName=更新课程, url=null, perms=course:update, icon=null, type=1, orderNum=null, createTime=Mon Mar 30 15:46:39 CST 2020, modifyTime=null)\"', '192.168.2.169', '2020-03-30 15:46:40', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1147, 'admin', '新增菜单/按钮', 72, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=203, parentId=189, menuName=查询课程, url=null, perms=sourse:select, icon=null, type=1, orderNum=null, createTime=Mon Mar 30 15:50:00 CST 2020, modifyTime=null)\"', '192.168.2.169', '2020-03-30 15:50:01', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1148, 'admin', '新增菜单/按钮', 88, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=204, parentId=189, menuName=删除课程, url=null, perms=course:delete, icon=null, type=1, orderNum=null, createTime=Mon Mar 30 15:51:13 CST 2020, modifyTime=null)\"', '192.168.2.169', '2020-03-30 15:51:14', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1149, 'admin', '删除用户', 130, 'cc.mrbird.febs.system.controller.UserController.deleteUsers()', ' userIds: \"9\"', '192.168.2.169', '2020-03-30 17:48:53', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1150, 'admin', '删除用户', 100, 'cc.mrbird.febs.system.controller.UserController.deleteUsers()', ' userIds: \"10\"', '192.168.2.169', '2020-03-31 08:58:07', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1151, 'admin', '修改角色', 419, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Tue Mar 31 09:22:00 CST 2020, menuIds=181,191,193,183,184,189,190,204,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.169', '2020-03-31 09:22:01', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1152, 'admin', '删除课程', 70, 'cc.mrbird.febs.system.controller.CourseController.deleteCourses()', ' courseIds: \"3\"', '192.168.2.169', '2020-03-31 09:23:03', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1153, 'admin', '删除课程', 75, 'cc.mrbird.febs.system.controller.CourseController.deleteCourses()', ' courseIds: \"4\"', '192.168.2.169', '2020-03-31 09:29:53', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1154, 'admin', '删除课程', 76, 'cc.mrbird.febs.system.controller.CourseController.deleteCourses()', ' courseIds: \"11\"', '192.168.2.169', '2020-03-31 09:30:23', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1155, 'admin', '删除课程', 73, 'cc.mrbird.febs.system.controller.CourseController.deleteCourses()', ' courseIds: \"12\"', '192.168.2.169', '2020-03-31 09:31:06', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1156, 'admin', '删除课程', 89, 'cc.mrbird.febs.system.controller.CourseController.deleteCourses()', ' courseIds: \"13\"', '192.168.2.169', '2020-03-31 09:31:46', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1157, 'admin', '新增课程', 169, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=14, courseName=test, courseContent=请输入 课程内容 或描述test&nbsp; test, deptNameList=机电学院,计算机学院, state=1, createUser=null, createTime=2020-03-31T09:32:23.579, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@6dbaa1e5\" deptNameLists: \"[Ljava.lang.String;@526e4a1a\"', '192.168.2.169', '2020-03-31 09:32:24', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1158, 'admin', '新增课程', 103, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=15, courseName=AAAA, courseContent=请输入 课程内容 或描述AAAAAAAAAAA, deptNameList=计算机学院, state=1, createUser=null, createTime=2020-03-31T09:32:44.153, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@676ba32f\" deptNameLists: \"[Ljava.lang.String;@b2f1a42\"', '192.168.2.169', '2020-03-31 09:32:44', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1159, 'admin', '删除课程', 82, 'cc.mrbird.febs.system.controller.CourseController.deleteCourses()', ' courseIds: \"14\"', '192.168.2.169', '2020-03-31 09:33:18', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1160, 'admin', '删除课程', 70, 'cc.mrbird.febs.system.controller.CourseController.deleteCourses()', ' courseIds: \"15\"', '192.168.2.169', '2020-03-31 09:33:30', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1161, 'admin', '新增课程', 147, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=16, courseName=AAAA, courseContent=请输入 课程内容 或描述AAAA, deptNameList=机电学院, state=1, createUser=null, createTime=2020-03-31T09:34:41.355, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@53de7015\" deptNameLists: \"[Ljava.lang.String;@2b06638e\"', '192.168.2.169', '2020-03-31 09:34:42', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1162, 'admin', '新增课程', 113, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=17, courseName=啊啊啊, courseContent=请输入 课程内容 或描述啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊, deptNameList=计算机学院, state=1, createUser=null, createTime=2020-03-31T09:35:13.852, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@4d687b78\" deptNameLists: \"[Ljava.lang.String;@49a636d1\"', '192.168.2.169', '2020-03-31 09:35:14', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1163, 'admin', '新增课程', 156, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=18, courseName=哇哇哇哇, courseContent=请输入 课程内容 或描述哇哇哇哇哇, deptNameList=计算机学院,外国语学院, state=1, createUser=null, createTime=2020-03-31T09:35:28.375, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@7783f33\" deptNameLists: \"[Ljava.lang.String;@3fb7d90\"', '192.168.2.169', '2020-03-31 09:35:29', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1164, 'admin', '删除课程', 15326, 'cc.mrbird.febs.system.controller.CourseController.deleteCourses()', ' courseIds: \"17\"', '192.168.2.169', '2020-03-31 11:58:44', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1165, 'admin', '新增项目', 168, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=7, projectCode=null, projectName=实验二  单管共射放大电路的测试, experimentId=null, experimentName=null, projectContent=<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">一、实验目的1、通过实验认识实际电路，加深对放大电路的理解，进一步建立信号放大的概念。<p style=\\\"margin-left:21.0pt;mso-para-margin-left:2.0gd;\\ntab-stops:0cm\\\">2<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">、掌握静态工作点的调整与测试方法。观察静态工作点对放大电路输出波形的影响。3、掌握测试电压放大倍数的方法。4、分析静态工作点对放大电路性能的影响。<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">二、实验仪器与器材1、<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">模拟电路实验板&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2、稳压电源&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3、示波器&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4、函数信号发生器&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5、万用表&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">三、实验原理实验电路图如图2-1。该图是分压式偏置单管共射放大电路，图中B1—B1ˊ和C1—C<span style=\\\"font-family:\\n宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">ˊ<span style=\\\"font-family:\\n宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">间断开是为了测电流用的，不测电流时应短接。1、静态工作点的测量与调试测量静态工作点，应断开交流信号源，并在放大电路的u<span style=\\\"font-size:7.5pt;\\nmso-bidi-font-size:12.0pt\\\">i<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">输入端短路的状态下测量，用万用表的直流电压挡及直流电流挡分别测量I<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt\\\">B、IC、UCE及各极对地电位，也可测量URC，计算出<span style=\\\"position:relative;\\ntop:15.0pt;mso-text-raise:-15.0pt\\\">\\n \\n \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n \\n \\n \\n\\n \\n\\n \\n \\n<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">。该图电路中，当UCC、RC、RE等参数确定以后，工作点主要靠调节偏置电路的电阻RP来实现。如果静态工作点调得过高或过低，当输入端加入正弦信号u<span style=\\\"font-size:7.5pt;\\nmso-bidi-font-size:12.0pt\\\">i<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">时，若幅度较大，则输出信号u<span style=\\\"font-size:\\n7.5pt;mso-bidi-font-size:12.0pt\\\">o<span style=\\\"font-family:宋体;\\nmso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">将会产生饱和或截止失真。只有当静态工作点调得适中时，可以使三极管工作在最大动态范围。</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p><p style=\\\"text-align:center;text-indent:21.0pt;\\nmso-char-indent-count:2.0\\\">\\n \\n\\n \\n \\n<span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\"></span></p><p style=\\\"text-align:center;text-indent:18.05pt;\\nmso-char-indent-count:2.0\\\"><span style=\\\"font-size:9.0pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-bidi-font-family:宋体\\\">图2-1&nbsp; 单管共射放大电路2、放大电路动态参数测试</span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l2 level1 lfo1\\\">①　测电压放大倍数Au将图中F—G短接。在放大电路的输入端加交流信号u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">i<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">时，在输出端输出一个放大了的交流信号u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">o<span style=\\\"font-size:7.5pt;\\nmso-bidi-font-size:12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">。则电压放大倍数Au的计算公式为：</span></span></span></span></p><p style=\\\"margin-left:21.0pt;mso-para-margin-left:\\n2.0gd;text-align:center\\\"><span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\"><span style=\\\"position:relative;top:15.0pt;mso-text-raise:\\n-15.0pt\\\">\\n \\n\\n \\n \\n由上面公式可知：测出u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">o<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">与u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">i<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">的值即可算出A<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">u<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">的值。应当注意，测量u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">o<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">与u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">i<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">时，必须保证放大器的输出电压为不失真波形，这可以用示波器监视。</span></span></span></span></span></span></span></span></span></span></span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l2 level1 lfo1\\\">②　输入电阻r<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">i<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">和输出电阻r<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">o<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">的测量图2-2是放大电路输入与输出电路的等效电路图，根据图中的电压电流关系可以看出，只要测量出相应的电压值，便可求出输入电阻r<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">i<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">和输出电阻r<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">o<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">。&nbsp;</span></span></span></span></span></span></span></span></p><p style=\\\"text-align:center;text-indent:18.0pt;\\nmso-char-indent-count:2.0\\\"><span style=\\\"font-size:9.0pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">放大电路</span></p><p style=\\\"text-align:center;text-indent:21.0pt;\\nmso-char-indent-count:2.0\\\">\\n \\n\\n \\n \\n<span style=\\\"font-size:9.0pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;;background:yellow\\\"></span></p><p style=\\\"text-align:center;text-indent:18.0pt;\\nmso-char-indent-count:2.0\\\"><span style=\\\"font-size:9.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">图2-2&nbsp;&nbsp; 输入电阻与输出电阻的测量输入电阻r<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">i的测量：由图2-2看出：<span style=\\\"position:relative;\\ntop:30.0pt;mso-text-raise:-30.0pt\\\">\\n \\n\\n \\n \\n式中的R<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">S是已知的，因此，只要用电子毫伏表分别量出u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">i<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">与u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">s<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">即可求得r<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">i<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">。&nbsp;输出电阻r<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">o的测量：图2-2中，<span style=\\\"font-size:14.0pt;\\nmso-bidi-font-size:12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">o<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">是负载开路时的输出电压，u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">L<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">是接入负载R<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">L<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">后的输出电压。则：\\n \\n\\n \\n \\n&nbsp;&nbsp;&nbsp;&nbsp; 所以&nbsp; <span style=\\\"position:relative;\\ntop:15.0pt;mso-text-raise:-15.0pt\\\">\\n \\n\\n \\n \\n因此只要测量出u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">o<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">、u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">L<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">，即可求得r<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">o<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">。&nbsp;<span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">四、实验内容与步骤</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l3 level1 lfo2;tab-stops:36.75pt\\\">1、确认实验电路及各测试点的位置，测量电流放大系数β值对照模拟实验系统与图2-1，把稳压电源的输出调至12V，将放大电路的U<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">CC端和地端分别接+12V电源的正极和负极。VT<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">1<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">接在电路上，分别将电流表串入基极和集电极支路，连接读出Ib、Ic，计算β值，记入表2-1中。静态工作点的调试与测量</span></span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l8 level1 lfo3\\\">①　调整并测试给定的静态工作点<span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">为防止干扰，应先将<span style=\\\"font-size:12.0pt;\\nfont-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">i<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">短路。将万用表的直流5mA挡串接在集电极的C—C<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">1<span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">ˊ中，B1—B1ˊ短接，调节R<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">P1使I<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">C=1mA，分别测出此时的I<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">B、U<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">B、U<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">C U<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">E<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">、U<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">RC<span style=\\\"font-family:\\n宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">值，并将测试结果记入表2-1中。I<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">C1<span style=\\\"font-family:\\n宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">也可通过测R<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">C1<span style=\\\"font-family:\\n宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">两端电压再换算。</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p><p style=\\\"margin-left:21.0pt;mso-para-margin-left:\\n2.0gd;text-align:center\\\"><span style=\\\"position:relative;\\ntop:5.0pt;mso-text-raise:-5.0pt\\\">\\n \\n\\n \\n \\n<span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\"><span style=\\\"position:relative;top:15.0pt;mso-text-raise:\\n-15.0pt\\\">\\n \\n\\n \\n \\n</span></span></span></p><p style=\\\"margin-left:0cm;text-align:left;\\ntext-indent:21.0pt;mso-char-indent-count:2.0;mso-list:l8 level1 lfo3\\\">②　调整并测试最大动态范围的静态工作点。调节函数信号发生器使其产生1KHz<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">的正弦波信号，幅度调在最低位，将此信号作为us<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">信号接至放大电路的输入端，经电阻<span style=\\\"mso-bidi-font-size:\\n10.5pt\\\">Rs<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;\\nmso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">送到VT1<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">的基极，电路的净输入信号为<span style=\\\"mso-bidi-font-size:\\n10.5pt\\\">ui<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;\\nmso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">，这样可防止us<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">太高时损坏三极管<span style=\\\"mso-bidi-font-size:\\n10.5pt\\\">VT1<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;\\nmso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">。用示波器监视输出电压uo<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">波形。调节函数信号发生器使<span style=\\\"mso-bidi-font-size:\\n10.5pt\\\">us<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;\\nmso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">逐渐加大，如uo<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">出现半边失真则可调节<span style=\\\"mso-bidi-font-size:\\n10.5pt\\\">RP1<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;\\nmso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">，使失真消失；再继续加大us<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">，经过反复加大<span style=\\\"mso-bidi-font-size:\\n10.5pt\\\">us<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;\\nmso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">及调节RP1<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">，uo波形会出现上下峰均稍有相同程度的失真，这叫双向失真。此时电路所处的工作点为最大动态范围的静态工作点，即工作点Q<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">刚好在交流负载线的中间，也称最佳工作点。波形调好后将输入端的信号us<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">撤除，保持RP1不变，<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">将ui短路。<span style=\\\"mso-bidi-font-size:10.5pt;\\nfont-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">用万用表直流档分别测<span style=\\\"mso-bidi-font-size:\\n10.5pt\\\">IB1<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;\\nmso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">、IC1<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">、UB1、UC1<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">、UE1、URC<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">，记入表2-1中。</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l3 level1 lfo2;tab-stops:36.75pt\\\">2、<span style=\\\"font-family:\\n宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">测量放大电路的电压放大倍数A<span style=\\\"font-size:\\n7.5pt;mso-bidi-font-size:12.0pt\\\">u</span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l1 level1 lfo4\\\">①　将电路中<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">B<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">1<span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">—B<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">1<span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">ˊ连接，C—Cˊ连接，将F点与G点连接。</span></span></span></span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l1 level1 lfo4\\\">②　将函数信号发生器输出调为正弦波，频率调为1KHZ，幅度调至最小。并将函数信号发生器的输出作为放大器的输入信号us加至放大电路输入端，用示波器监视<span style=\\\"font-size:\\n14.0pt;mso-bidi-font-size:12.0pt\\\">u<span style=\\\"font-size:\\n7.5pt;mso-bidi-font-size:12.0pt\\\">o<span style=\\\"font-family:宋体;\\nmso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">波形不失真，调节信号发生器的幅度旋钮，适当增加us幅度，若输出波形失真可适当减小<span style=\\\"font-size:\\n12.0pt\\\">u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt\\\">s幅值。</span></span></span></span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l1 level1 lfo4\\\">③　按表2-2<span style=\\\"font-family:\\n宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">所列测试条件用电子毫伏表测试相应的u<span style=\\\"font-size:\\n7.5pt;mso-bidi-font-size:12.0pt\\\">i 和uo的有效值Ui和Uo，并用示波器观察uo波形与RL的关系，将以上结果填入表2-2中。</span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l3 level1 lfo2;tab-stops:36.75pt\\\">3、<span style=\\\"font-family:\\n宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">测量输入和输出电阻①　输入交流1KHZ的正弦信号，并使输出波形不失真，用示波器分别测出u<span style=\\\"font-size:7.5pt;\\nmso-bidi-font-size:12.0pt\\\">s<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">和u<span style=\\\"font-size:\\n7.5pt;mso-bidi-font-size:12.0pt\\\">i<span style=\\\"mso-bidi-font-size:10.5pt;\\nfont-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">幅值有效值<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">记入表2-3中。②　先测输出开路时的输出电压uo值，再接入RL的负载电阻，测出输出电压uL，记入表2-3中。③　根据以测量值分别计算出<span style=\\\"font-size:12.0pt;\\nfont-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">r<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">i<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">和\\nr<span style=\\\"font-size:9.0pt;mso-bidi-font-size:12.0pt;\\nfont-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">o写入表2-3中。4、观察静态工作点对放大电路的输出电压波形的影响。</span></span></span></span></span></span></span></span></span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l0 level1 lfo6\\\">①　断开RL，输入1KHZ的交流信号，调节RP1，直到观察到uo负半周波形有被削顶的失真，将波形画入表2-4中。撤掉信号us，测量此时的UCE，并将结果记入表2-4中。</p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l0 level1 lfo6\\\">②　仍加入1KHZ交流信号，RL仍断开，反向调节RP1，直到观察到uo正半周波形有被削顶的失真，将波形画入表2-4中。撤掉输入信号us，测量此时的UCE并记入表2-4中。</p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l0 level1 lfo6\\\">③　加入us观察到不失真的输出电压波形。再加大us的幅值并调整RP1，直到波形正负半周都有削顶，观察波形失真情况并记入表2-4中，这种失真称为大信号失真或双向失真，撤掉us，测出此时的UCE，记入表2-4中。</p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l0 level1 lfo6\\\">④　双向失真时，将电路中的F点与G<span style=\\\"font-family:\\n宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">点连线断开，把F点与H点短接，再观察波形有何变化（应当使uo幅度下降并减轻失真）。<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">五、实验注意事项</span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l5 level1 lfo7;tab-stops:36.75pt\\\">1、<span style=\\\"font-family:\\n宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">要爱护实验设备，不得损坏各种零配件。不要用力拉扯连接线，不要随意插拔元件。</span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l5 level1 lfo7;tab-stops:36.75pt\\\">2、实验前应先将稳压电源空载调至所需电压值后，关掉电源再接至电路，实验时再打开电源。改变电路结构前也应将电源断开。应保证电源和信号源不能出现短路。</p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l5 level1 lfo7;tab-stops:36.75pt\\\">3、实验过程中保持实验电路与各仪器仪表“共地”。<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">六、<span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">预习要求</span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l6 level1 lfo8;tab-stops:36.75pt\\\">1、复习单管放大电路的分析方法。</p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l6 level1 lfo8;tab-stops:36.75pt\\\">2、<span style=\\\"font-family:\\n宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">熟悉实验原理及表2-1、2-2、2-3中的测试要求。</span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l6 level1 lfo8;tab-stops:36.75pt\\\">3、<span style=\\\"font-family:\\n宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">估算表2-1、2-2中各数值的范围。<span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">七、实验报告内容</span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l7 level1 lfo9;tab-stops:36.75pt\\\">1、认真列表整理结果，将测量值与理论值相比较，分析误差原因。</p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l7 level1 lfo9;tab-stops:36.75pt\\\">2、分析静态工作点对放大器的影响。</p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l7 level1 lfo9;tab-stops:36.75pt\\\">3、总结分析实验过程中出现的问题。&nbsp;表2-1&nbsp; 放大电路的静态工作点（Uces=1.2V）\\n\\n</p>\\n \\n  \\n  测试条件\\n  \\n  \\n  测试值 Ic=1mA\\n  \\n  \\n  计算值\\n  \\n \\n \\n  \\n  IB\\n  \\n  \\n  IC\\n  \\n  \\n  UB\\n  \\n  \\n  UE\\n  \\n  \\n  UC\\n  \\n  \\n  RP1<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;mso-fareast-font-family:\\n  宋体;mso-fareast-theme-font:minor-fareast\\\">\\n  \\n  </span>\\n  UBE\\n  \\n  \\n  UCE\\n  \\n  \\n  β\\n  \\n \\n \\n  \\n  理论值\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n \\n \\n  \\n  IC=2mA\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n \\n \\n  \\n  最大动态范围\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n \\n\\n\\n&nbsp;&nbsp;表2-2&nbsp; 放大电路的放大倍数（f=1kHz<span style=\\\"font-size:\\n9.0pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">）\\n\\n\\n \\n  \\n  测试条件\\n  \\n  \\n  测试值\\n  \\n  \\n  计算值\\n  \\n \\n \\n  \\n  Ui(mV)\\n  \\n  \\n  Uo(mV)\\n  \\n  \\n  uo<span style=\\\"mso-bidi-font-size:\\n  10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n  &quot;Times New Roman&quot;\\\">波形\\n  \\n  </span>\\n  Au= Uo/ Ui\\n  \\n \\n \\n  \\n  RL=∞\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  \\n   \\n   \\n  \\n   \\n   \\n  \\n  \\n  \\n  &nbsp;\\n  \\n \\n \\n  \\n  RL=1KΩ\\n  \\n  \\n  &nbsp;\\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  \\n   \\n   \\n  \\n   \\n   \\n  \\n  \\n  \\n  &nbsp;\\n  \\n \\n\\n\\n&nbsp;&nbsp;表2-3&nbsp; 静态工作点对输出电压波形的影响\\n\\n\\n \\n  \\n  测试条件&nbsp; R<span style=\\\"font-size:\\n  7.5pt;mso-bidi-font-size:12.0pt\\\">L=∞\\n  \\n  </span>\\n  波形失真类&nbsp;&nbsp;&nbsp; <span style=\\\"font-family:宋体;\\n  mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">型\\n  \\n \\n \\n  </span>\\n  输出电压波形\\n  \\n  \\n  UCE\\n  \\n \\n \\n  \\n  正半周削顶 \\n  &nbsp;\\n  \\n  \\n  \\n   \\n   \\n  \\n   \\n   \\n  \\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n \\n \\n  \\n  负半周削顶 \\n  &nbsp;\\n  \\n  \\n  \\n   \\n   \\n  \\n   \\n   \\n  \\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n \\n \\n  \\n  <span style=\\\"font-family:\\n  宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">正负半周削顶\\n  \\n  </span>\\n  \\n   \\n   \\n  \\n   \\n   \\n  \\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n \\n\\n\\n<span style=\\\"font-size:12.0pt;\\nfont-family:宋体;mso-bidi-font-family:宋体\\\">&nbsp;<span style=\\\"font-size:12.0pt;\\nfont-family:宋体;mso-bidi-font-family:宋体\\\">&nbsp;表2-4&nbsp; 输入与输出电阻的测量\\n\\n\\n \\n  \\n  u<span style=\\\"font-size:7.5pt;\\n  mso-bidi-font-size:12.0pt\\\">s\\n  \\n  </span>\\n  u<span style=\\\"font-size:7.5pt;\\n  mso-bidi-font-size:12.0pt\\\">i\\n  \\n  </span>\\n  r<span style=\\\"font-size:7.5pt;\\n  mso-bidi-font-size:12.0pt\\\">i<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n  &quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">（KΩ）\\n  \\n  </span></span>\\n  u<span style=\\\"font-size:7.5pt;\\n  mso-bidi-font-size:12.0pt\\\">o\\n  \\n  </span>\\n  u<span style=\\\"font-size:5.5pt;\\n  mso-bidi-font-size:12.0pt\\\">L\\n  \\n  </span>\\n  r<span style=\\\"font-size:7.5pt;\\n  mso-bidi-font-size:12.0pt\\\">o<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n  &quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">（KΩ）\\n  \\n \\n \\n  </span></span>\\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  测量值\\n  \\n  \\n  理论值\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  测量值\\n  \\n  \\n  理论值\\n  \\n \\n \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n \\n\\n\\n<span style=\\\"font-size:12.0pt;\\nfont-family:宋体;mso-bidi-font-family:宋体\\\">&nbsp;<span style=\\\"font-size:12.0pt;\\nfont-family:宋体;mso-bidi-font-family:宋体\\\">&nbsp;<span style=\\\"font-size:12.0pt;\\nfont-family:宋体;mso-bidi-font-family:宋体\\\">&nbsp;<span style=\\\"font-size:12.0pt;font-family:\\n宋体;mso-bidi-font-family:宋体\\\">八、实验思考题<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">1<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">、能否用直流电压表直接测量晶体管的<span style=\\\"position:relative;\\ntop:5.0pt;mso-text-raise:-5.0pt\\\">\\n \\n\\n \\n \\n？ 为什么实验中要采用测<span style=\\\"position:\\nrelative;top:5.0pt;mso-text-raise:-5.0pt\\\">\\n \\n\\n \\n \\n、<span style=\\\"position:relative;\\ntop:5.0pt;mso-text-raise:-5.0pt\\\">\\n \\n\\n \\n \\n，再间接算出<span style=\\\"position:relative;\\ntop:5.0pt;mso-text-raise:-5.0pt\\\">\\n \\n\\n \\n \\n的方法？<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">2<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">、当调节偏置电阻<span style=\\\"position:relative;top:5.0pt;\\nmso-text-raise:-5.0pt\\\">\\n \\n\\n \\n \\n，使放大器输出波形出现饱和或截止失真时，晶体管的管压降\\n \\n\\n \\n \\n怎样变化？<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">3<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">、改变静态工作点对放大器的输入电阻<span style=\\\"position:relative;\\ntop:6.0pt;mso-text-raise:-6.0pt\\\">\\n \\n\\n \\n \\n有否影响？改变外接电阻<span style=\\\"position:\\nrelative;top:5.0pt;mso-text-raise:-5.0pt\\\">\\n \\n\\n \\n \\n对输出电阻<span style=\\\"position:relative;\\ntop:6.0pt;mso-text-raise:-6.0pt\\\">\\n \\n\\n \\n \\n有否影响？<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">4<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">、在测试<span style=\\\"position:relative;top:6.0pt;mso-text-raise:\\n-6.0pt\\\">\\n \\n\\n \\n \\n，<span style=\\\"position:relative;\\ntop:6.0pt;mso-text-raise:-6.0pt\\\">\\n \\n\\n \\n \\n和<span style=\\\"position:relative;\\ntop:6.0pt;mso-text-raise:-6.0pt\\\">\\n \\n\\n \\n \\n时怎样选择输入信号的大小和频率？为什么信号频率一般选1KHz，而不选100KHz或更高？ <span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">5<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">、测试中，如果将函数信号发生器、交流毫伏表、示波器中任一仪器的二个测试端子接线换位（即各仪器的接地端不再连在一起），将会出现什么问题？<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">6<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">、调试电路的静态工作点时，电阻Rb1为什么需要用一只固定电阻与可调电阻相串联？7<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">、负载大小对放大倍数有何影响？<span style=\\\"mso-bidi-font-size:\\n10.5pt\\\">8<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">、如果把F点悬空（电容Ce1<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">接入电路)，对放大倍数有何影响？\\n\\n\\n\\n\\nv\\\\:* {behavior:url(#default#VML);}\\no\\\\:* {behavior:url(#default#VML);}\\nw\\\\:* {behavior:url(#default#VML);}\\n.shape {behavior:url(#default#VML);}\\n\\n\\n\\n\\n\\n \\n  Normal\\n  0\\n  \\n  \\n  \\n  7.8 磅\\n  0\\n  2\\n  \\n  false\\n  false\\n  false\\n  \\n  EN-US\\n  ZH-CN\\n  X-NONE\\n  \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n  \\n  \\n  \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n  \\n\\n \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n \\n\\n\\n\\n\\n /* Style Definitions */\\n table.MsoNormalTable\\n\\t{mso-style-name:普通表格;\\n\\tmso-tstyle-rowband-size:0;\\n\\tmso-tstyle-colband-size:0;\\n\\tmso-style-noshow:yes;\\n\\tmso-style-priority:99;\\n\\tmso-style-parent:\\\"\\\";\\n\\tmso-padding-alt:0cm 5.4pt 0cm 5.4pt;\\n\\tmso-para-margin:0cm;\\n\\tmso-para-margin-bottom:.0001pt;\\n\\tmso-pagination:widow-orphan;\\n\\tfont-size:10.0pt;\\n\\tfont-family:\\\"Times New Roman\\\",serif;}\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n&nbsp;</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span>, projectSort=null, state=1, operator=null, createTime=2020-03-31T14:48:25.472, updateTime=null)\"', '192.168.2.138', '2020-03-31 14:48:26', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1166, 'admin', '修改角色', 421, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Tue Mar 31 15:27:01 CST 2020, menuIds=181,191,193,183,184,189,190,202,203,204,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.169', '2020-03-31 15:27:02', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1167, 'admin', '修改用户', 246, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=12, username=null, password=null, idNumber=null, deptId=null, courseId=null, courseName=, email=135@163.com, mobile=13500002222, status=1, createTime=null, modifyTime=Tue Mar 31 16:23:02 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=我是一名老师, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=81, roleName=null)\" dept: \"[Ljava.lang.Integer;@2179eea0\"', '192.168.2.138', '2020-03-31 16:23:03', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1168, 'admin', '修改角色', 290, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Tue Mar 31 18:09:04 CST 2020, menuIds=181,191,193,183,184,189,190,202,203,204,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.111', '2020-03-31 18:09:05', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1169, 'admin', '修改角色', 319, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Tue Mar 31 18:15:51 CST 2020, menuIds=181,191,193,183,184,189,190,202,203,204,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.111', '2020-03-31 18:15:52', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1170, 'admin', '删除课程', 99, 'cc.mrbird.febs.system.controller.CourseController.deleteCourses()', ' courseIds: \"16\"', '192.168.2.169', '2020-03-31 18:24:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1171, 'admin', '删除课程', 93, 'cc.mrbird.febs.system.controller.CourseController.deleteCourses()', ' courseIds: \"18\"', '192.168.2.169', '2020-03-31 18:27:28', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1172, 'admin', '新增课程', 221, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=19, courseName=啊啊啊啊, courseContent=请输入 课程内容 或描述啊啊啊啊, deptNameList=机电学院, state=1, createUser=null, createTime=2020-03-31T18:29:09.728, createTimes=null, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@64129320\" deptNameLists: \"[Ljava.lang.String;@602d1b7a\"', '192.168.2.169', '2020-03-31 18:29:10', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1173, 'admin', '删除课程', 69, 'cc.mrbird.febs.system.controller.CourseController.deleteCourses()', ' courseIds: \"19\"', '192.168.2.169', '2020-03-31 18:29:31', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1174, 'admin', '新增课程', 134, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=20, courseName=wwww, courseContent=请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述, deptNameList=经济学院, state=1, createUser=null, createTime=2020-03-31T18:31:26.424, createTimes=null, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@7fb424c2\" deptNameLists: \"[Ljava.lang.String;@7b8bbfec\"', '192.168.2.169', '2020-03-31 18:31:27', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1175, 'admin', '修改课程', 57, 'cc.mrbird.febs.system.controller.CourseController.deleteCourses()', ' course: \"Course(courseId=1, courseName=null, courseContent=null, deptNameList=null, state=null, createUser=null, createTime=null, createTimes=null, updateTime=2020-04-01T09:21:39.255)\"', '192.168.2.169', '2020-04-01 09:21:39', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1176, 'admin', '修改课程', 75, 'cc.mrbird.febs.system.controller.CourseController.deleteCourses()', ' course: \"Course(courseId=20, courseName=wwww, courseContent=啊啊啊, deptNameList=经济学院, state=1, createUser=null, createTime=null, createTimes=null, updateTime=2020-04-01T09:25:16.309)\"', '192.168.2.169', '2020-04-01 09:25:16', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1177, 'admin', '修改课程', 75, 'cc.mrbird.febs.system.controller.CourseController.deleteCourses()', ' course: \"Course(courseId=20, courseName=wwww, courseContent=水水水水, deptNameList=经济学院, state=1, createUser=null, createTime=null, createTimes=null, updateTime=2020-04-01T09:25:28.425)\"', '192.168.2.169', '2020-04-01 09:25:29', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1178, 'admin', '删除用户', 97, 'cc.mrbird.febs.system.controller.UserController.deleteUsers()', ' userIds: \"13\"', '192.168.2.169', '2020-04-01 09:28:25', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1179, 'admin', '新增项目', 87, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=8, projectCode=null, projectName=test, experimentId=null, experimentName=null, projectContent=请输入 项目描述哇哇哇哇哇哇哇哇哇哇哇哇哇哇哇哇哇哇哇, projectSort=null, state=1, operator=null, createTime=2020-04-01T09:41:27.760, updateTime=null)\"', '192.168.2.169', '2020-04-01 09:41:28', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1180, 'admin', '新增课程', 237, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=21, courseName=模拟电子实验, courseContent=模拟电子在线实验室采用远程实验技术，通过网络访问远程端真实的电子仪器仪表及实验电路板，完成基本的模拟电子实验。该实验室可作为电子技术类课程实验教学，也可用于教师在课堂上通过投影为学生演示，还可作为开放实验室供学生使用。&nbsp; &nbsp; &nbsp; &nbsp; 实验室旨在培养学生的基本实验技能，加深对模拟电子技术基本原理的理解，了解电子线路的设计分析方法。熟悉并掌握常用仪器的使用方法，学会正确测量电子器件和系统参数的方法。, deptNameList=电子信息工程1301,软件工程, state=1, createUser=null, createTime=2020-04-01T14:31:07.873, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@6b17801a\" deptNameLists: \"[Ljava.lang.String;@243366b4\"', '192.168.2.138', '2020-04-01 14:31:08', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1181, 'admin', '新增课程', 148, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=22, courseName=数字电子实验, courseContent=请输入&nbsp; &nbsp;数字电子在线实验室采用远程实验技术，通过网络访问远程端真实的数字电路实验板、电平开关、电平指示器及电子仪器仪表，完成基本的数字电子实验。该实验室可作为电子技术类课程实验教学，也可用于教师在课堂上通过投影为学生演示，还可作为开放实验室供学生使用。&nbsp; &nbsp; &nbsp; &nbsp; 数字电路可以分为组合逻辑电路和时序逻辑电路两大类，实验项目涵盖了主要的电路类型。通过实验室可培养学生的基本实验技能，加深对数字电子技术基本原理的理解，了解数字电路逻辑的组成及设计方法。, deptNameList=外国语学院,农学院, state=1, createUser=null, createTime=2020-04-01T14:31:58.114, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@7b6aa91e\" deptNameLists: \"[Ljava.lang.String;@7f5532ab\"', '192.168.2.138', '2020-04-01 14:31:58', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1182, 'admin', '新增课程', 167, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=23, courseName=电工技术实验, courseContent=&nbsp;电工技术实验室，是面向电路等课程开设相关实验。电工技术在线实验室，通过网络技术结合智能仪表，实现远程实验功能。采用浏览器，可通过互联网访问远程端真实的仪器仪表及实验电路对象，完成基本的电路及电工技术相关实验。&nbsp;&nbsp;, deptNameList=1522班,软件工程, state=1, createUser=null, createTime=2020-04-01T14:32:50.662, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@3b9bcc85\" deptNameLists: \"[Ljava.lang.String;@3fa44f2e\"', '192.168.2.138', '2020-04-01 14:32:51', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1183, 'admin', '新增课程', 181, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=24, courseName=传感器实验, courseContent=请输入 课程内容 或描述&nbsp; &nbsp; &nbsp; 传感器实验是针对传感器原理、测试技术等课程的专业基础类实验。传感器在线实验室采用远程实验技术，通过网络访问控制远程端传感器及相关仪表，实时测量传感器变化带来的电气参数改变。实现与新临实验室相同的实验效果。, deptNameList=经济学院,外国语学院, state=1, createUser=null, createTime=2020-04-01T14:33:19.697, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@3bc18d81\" deptNameLists: \"[Ljava.lang.String;@3ffe0e21\"', '192.168.2.138', '2020-04-01 14:33:20', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1184, 'admin', '新增用户', 265, 'cc.mrbird.febs.system.controller.UserController.addUser()', ' user: \"User(userId=14, username=teacher001, password=31bccac7c4f4ad7c19d6b60e8bca9653, idNumber=201311112232, deptId=null, courseId=21, courseName=模拟电子实验, email=105@qq.com, mobile=13512341234, status=1, createTime=Wed Apr 01 14:37:37 CST 2020, modifyTime=null, lastLoginTime=null, sex=1, avatar=default.jpg, theme=black, isTab=1, description=我是一名老师, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=81, roleName=null)\" dept: \"[Ljava.lang.Integer;@49ec2198\"', '192.168.2.138', '2020-04-01 14:37:38', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1185, 'admin', '新增用户', 113, 'cc.mrbird.febs.system.controller.UserController.addUser()', ' user: \"User(userId=15, username=student001, password=2e50094ee60d4e9d1b5d3fff7853ef00, idNumber=2121221323, deptId=null, courseId=null, courseName=, email=120@163.com, mobile=13526325632, status=1, createTime=Wed Apr 01 14:42:31 CST 2020, modifyTime=null, lastLoginTime=null, sex=0, avatar=default.jpg, theme=black, isTab=1, description=我是一名学生, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=82, roleName=null)\" dept: \"[Ljava.lang.Integer;@47fc9da1\"', '192.168.2.138', '2020-04-01 14:42:32', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1186, 'admin', '修改角色', 344, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Wed Apr 01 14:48:23 CST 2020, menuIds=181,182,191,193,183,184,189,190,202,203,204,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.169', '2020-04-01 14:48:23', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1187, 'admin', '修改角色', 330, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Wed Apr 01 14:56:17 CST 2020, menuIds=181,191,193,183,184,189,190,202,203,204,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.169', '2020-04-01 14:56:18', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1188, 'admin', '新增菜单/按钮', 77, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=205, parentId=191, menuName=修改项目, url=null, perms=project:update, icon=null, type=1, orderNum=null, createTime=Wed Apr 01 14:59:04 CST 2020, modifyTime=null)\"', '192.168.2.169', '2020-04-01 14:59:04', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1189, 'admin', '修改角色', 379, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Wed Apr 01 14:59:38 CST 2020, menuIds=181,191,193,205,183,184,189,190,202,203,204,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.169', '2020-04-01 14:59:39', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1190, 'admin', '修改项目', 50, 'cc.mrbird.febs.system.controller.ExperimentProjectController.updateExperiment()', ' experimentProject: \"ExperimentProject(projectId=null, projectCode=null, projectName=test01, experimentId=null, experimentName=null, projectContent=null, projectSort=null, state=1, operator=null, createTime=2020-04-01T15:29:31.203, updateTime=null)\"', '192.168.2.169', '2020-04-01 15:29:31', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1191, 'admin', '修改项目', 42, 'cc.mrbird.febs.system.controller.ExperimentProjectController.updateExperiment()', ' experimentProject: \"ExperimentProject(projectId=null, projectCode=null, projectName=test01, experimentId=null, experimentName=null, projectContent=null, projectSort=null, state=1, operator=null, createTime=2020-04-01T15:29:49.334, updateTime=null)\"', '192.168.2.169', '2020-04-01 15:29:49', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1192, 'admin', '修改项目', 41, 'cc.mrbird.febs.system.controller.ExperimentProjectController.updateExperiment()', ' experimentProject: \"ExperimentProject(projectId=null, projectCode=null, projectName=test01, experimentId=null, experimentName=null, projectContent=null, projectSort=null, state=1, operator=null, createTime=2020-04-01T15:30:16.702, updateTime=null)\"', '192.168.2.169', '2020-04-01 15:30:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1193, 'admin', '修改项目', 59, 'cc.mrbird.febs.system.controller.ExperimentProjectController.updateExperiment()', ' experimentProject: \"ExperimentProject(projectId=null, projectCode=null, projectName=test01, experimentId=8, experimentName=null, projectContent=null, projectSort=null, state=1, operator=null, createTime=null, updateTime=2020-04-01T16:16:36.440)\"', '192.168.2.169', '2020-04-01 16:16:37', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1194, 'admin', '修改项目', 41, 'cc.mrbird.febs.system.controller.ExperimentProjectController.updateExperiment()', ' experimentProject: \"ExperimentProject(projectId=null, projectCode=null, projectName=test01, experimentId=8, experimentName=null, projectContent=null, projectSort=null, state=1, operator=null, createTime=null, updateTime=2020-04-01T16:16:57.634)\"', '192.168.2.169', '2020-04-01 16:16:58', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1195, 'admin', '新增项目', 400, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=9, projectCode=null, projectName=ssss, experimentId=null, experimentName=null, projectContent=请输入 实验描述ssss, projectSort=null, state=1, operator=null, createTime=2020-04-01T16:38:46.743, updateTime=null)\"', '192.168.2.169', '2020-04-01 16:38:47', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1196, 'admin', '新增项目', 131, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=10, projectCode=null, projectName=wwwedsdsadad, experimentId=null, experimentName=null, projectContent=请输入 实验描述dsadsdsd, projectSort=null, state=1, operator=null, createTime=2020-04-01T16:39:20.457, updateTime=null)\"', '192.168.2.169', '2020-04-01 16:39:21', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1197, 'admin', '新增项目', 84, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=11, projectCode=null, projectName=fdsfsfsdf, courseId=null, courseName=电工技术实验, projectContent=请输入 实验描述dsfdsdsf, projectSort=null, state=1, operator=null, createTime=2020-04-01T16:43:10.135, updateTime=null)\"', '192.168.2.169', '2020-04-01 16:43:10', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1198, 'admin', '新增项目', 93, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=12, projectCode=null, projectName=dsafdsfd, courseId=22, courseName=数字电子实验, projectContent=请输入 实验描述fdsfdsfd, projectSort=null, state=1, operator=null, createTime=2020-04-01T16:46:03.694, updateTime=null)\"', '192.168.2.169', '2020-04-01 16:46:04', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1199, 'admin', '新增项目', 89, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=13, projectCode=null, projectName=fdsfsfsdf, courseId=23, courseName=电工技术实验, projectContent=请输入 实验描述dsfdsdsf, projectSort=null, state=1, operator=null, createTime=2020-04-01T16:59:27.513, updateTime=null)\"', '192.168.2.169', '2020-04-01 16:59:28', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1200, 'admin', '新增项目', 81, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=14, projectCode=null, projectName=啊啊啊啊啊啊, courseId=22, courseName=数字电子实验, projectContent=啊啊啊啊啊啊啊啊啊啊啊啊啊啊, projectSort=null, state=1, operator=null, createTime=2020-04-01T16:59:57.584, updateTime=null)\"', '192.168.2.169', '2020-04-01 16:59:58', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1201, 'admin', '修改项目', 67, 'cc.mrbird.febs.system.controller.ExperimentProjectController.updateExperiment()', ' experimentProject: \"ExperimentProject(projectId=14, projectCode=null, projectName=哇哇哇哇哇哇哇哇哇哇哇, courseId=null, courseName=模拟电子实验, projectContent=哇哇哇哇哇哇哇哇哇哇哇, projectSort=null, state=1, operator=null, createTime=null, updateTime=2020-04-01T17:08:29.101)\"', '192.168.2.169', '2020-04-01 17:08:29', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1202, 'admin', '新增菜单/按钮', 98, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=210, parentId=0, menuName=删除项目, url=null, perms=experimentProject:delect, icon=null, type=1, orderNum=null, createTime=Wed Apr 01 17:27:59 CST 2020, modifyTime=null)\"', '192.168.2.169', '2020-04-01 17:27:59', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1203, 'admin', '新增菜单/按钮', 71, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=211, parentId=191, menuName=删除项目, url=null, perms=experimentProject:delete, icon=null, type=1, orderNum=null, createTime=Wed Apr 01 17:30:13 CST 2020, modifyTime=null)\"', '192.168.2.169', '2020-04-01 17:30:13', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1204, 'admin', '修改角色', 332, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Wed Apr 01 17:31:33 CST 2020, menuIds=181,191,193,205,211,183,184,189,190,202,203,204,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.169', '2020-04-01 17:31:34', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1205, 'admin', '删除实验项目', 70, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"13\"', '192.168.2.169', '2020-04-01 17:31:44', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1206, 'admin', '删除实验项目', 85, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"14\"', '192.168.2.169', '2020-04-01 17:31:58', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1207, 'admin', '删除实验项目', 107, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"[Ljava.lang.String;@64abd9dc\"', '192.168.2.169', '2020-04-01 17:35:10', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1208, 'admin', '删除实验项目', 97, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"8\"', '192.168.2.169', '2020-04-01 17:47:29', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1209, 'admin', '新增项目', 90, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=15, projectCode=null, projectName=test, courseId=20, courseName=wwww, projectContent=请输入 实验描述aaaaa, projectSort=null, state=1, operator=null, createTime=2020-04-01T17:47:58.343, updateTime=null)\"', '192.168.2.169', '2020-04-01 17:47:58', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1210, 'admin', '删除实验项目', 61, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"15\"', '192.168.2.169', '2020-04-01 18:31:49', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1211, 'admin', '删除实验项目', 73, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"12\"', '192.168.2.169', '2020-04-01 18:31:55', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1212, 'admin', '新增项目', 76, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=16, projectCode=null, projectName=啊啊啊, courseId=20, courseName=wwww, projectContent=请输入 实验描述啊啊啊, projectSort=null, state=1, operator=null, createTime=2020-04-01T18:32:14.798, updateTime=null)\"', '192.168.2.169', '2020-04-01 18:32:15', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1213, 'admin', '删除实验项目', 58, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"16\"', '192.168.2.169', '2020-04-01 18:33:37', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1214, 'admin', '修改项目', 186, 'cc.mrbird.febs.system.controller.ExperimentProjectController.updateExperiment()', ' experimentProject: \"ExperimentProject(projectId=1, projectCode=null, projectName=实验一 单管放大器放大电路, courseId=null, courseName=, projectContent=实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路, projectSort=null, state=1, operator=null, createTime=null, updateTime=2020-04-02T08:34:51.339)\"', '192.168.2.169', '2020-04-02 08:34:52', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1215, 'admin', '修改项目', 118, 'cc.mrbird.febs.system.controller.ExperimentProjectController.updateExperiment()', ' experimentProject: \"ExperimentProject(projectId=6, projectCode=null, projectName=实验四 运算放大器信号运算电路, courseId=null, courseName=wwww, projectContent=实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路, projectSort=null, state=1, operator=null, createTime=null, updateTime=2020-04-02T08:35:11.422)\"', '192.168.2.169', '2020-04-02 08:35:12', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1216, 'admin', '修改角色', 251, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=81, roleName=老师, remark=学校老师, createTime=null, modifyTime=Thu Apr 02 09:26:15 CST 2020, menuIds=183,199,206,207,200,208,209,201)\"', '192.168.2.169', '2020-04-02 09:26:16', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1217, 'admin', '修改角色', 310, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=81, roleName=老师, remark=学校老师, createTime=null, modifyTime=Thu Apr 02 09:26:36 CST 2020, menuIds=183,199,206,207,200,208,209,201)\"', '192.168.2.169', '2020-04-02 09:26:37', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1218, 'admin', '修改角色', 245, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=81, roleName=老师, remark=学校老师, createTime=null, modifyTime=Thu Apr 02 09:35:33 CST 2020, menuIds=183,199,206,207,200,208,209,201)\"', '192.168.2.169', '2020-04-02 09:35:34', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1219, 'admin', '修改角色', 235, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=81, roleName=老师, remark=学校老师, createTime=null, modifyTime=Thu Apr 02 14:23:24 CST 2020, menuIds=193,205,211,183,199,206,207,200,208,209,201)\"', '192.168.2.169', '2020-04-02 14:23:25', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1220, 'admin', '新增菜单/按钮', 73, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=212, parentId=191, menuName=excel导出, url=null, perms=teacherProject:excel, icon=null, type=1, orderNum=null, createTime=Thu Apr 02 14:25:21 CST 2020, modifyTime=null)\"', '192.168.2.169', '2020-04-02 14:25:21', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1221, 'admin', '删除菜单/按钮', 136, 'cc.mrbird.febs.system.controller.MenuController.deleteMenus()', ' menuIds: \"212\"', '192.168.2.169', '2020-04-02 14:25:37', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1222, 'admin', '修改角色', 268, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=81, roleName=老师, remark=学校老师, createTime=null, modifyTime=Thu Apr 02 14:41:41 CST 2020, menuIds=181,191,193,205,211,183,199,206,207,200,208,209,201)\"', '192.168.2.169', '2020-04-02 14:41:42', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1223, 'admin', '修改用户', 309, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, idNumber=null, deptId=null, courseId=null, courseName=, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Thu Apr 02 16:04:14 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=1,81, roleName=null)\" dept: \"[Ljava.lang.Integer;@429c7420\"', '192.168.2.138', '2020-04-02 16:04:15', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1224, 'admin', '修改用户', 315, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=8, username=null, password=null, idNumber=null, deptId=null, courseId=null, courseName=, email=105@qq.com, mobile=13011112222, status=1, createTime=null, modifyTime=Thu Apr 02 17:04:04 CST 2020, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=系统管理员, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=1, roleName=null)\" dept: \"[Ljava.lang.Integer;@7c83675a\"', '192.168.2.138', '2020-04-02 17:04:04', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1225, 'admin', '新增菜单/按钮', 77, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=213, parentId=0, menuName=我的课程, url=, perms=, icon=layui-icon-eye, type=0, orderNum=null, createTime=Thu Apr 02 17:43:19 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-04-02 17:43:19', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1226, 'admin', '新增菜单/按钮', 74, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=214, parentId=213, menuName=我的项目, url=, perms=, icon=, type=0, orderNum=null, createTime=Thu Apr 02 17:43:42 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-04-02 17:43:43', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1227, 'admin', '修改角色', 163, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=82, roleName=学生, remark=学校学生, createTime=null, modifyTime=Thu Apr 02 17:43:53 CST 2020, menuIds=213,214)\"', '192.168.2.138', '2020-04-02 17:43:54', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1228, 'admin', '新增菜单/按钮', 69, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=215, parentId=206, menuName=新增实验项目, url=null, perms=teacherProject:add, icon=null, type=1, orderNum=null, createTime=Fri Apr 03 09:32:33 CST 2020, modifyTime=null)\"', '192.168.2.169', '2020-04-03 09:32:34', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1229, 'admin', '修改角色', 266, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=81, roleName=老师, remark=学校老师, createTime=null, modifyTime=Fri Apr 03 09:33:37 CST 2020, menuIds=181,191,193,205,211,183,199,206,215,207,200,208,209,201)\"', '192.168.2.169', '2020-04-03 09:33:38', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1230, 'teacher001', '新增实验项目', 31, 'cc.mrbird.febs.system.controller.TeacherProjectController.teacherProjectAdd()', ' deptId: \"13\" projectId: \"6,7\"', '192.168.2.169', '2020-04-03 09:40:39', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1231, 'teacher001', '新增实验项目', 36, 'cc.mrbird.febs.system.controller.TeacherProjectController.teacherProjectAdd()', ' deptId: \"13\" projectId: \"6,7\"', '192.168.2.169', '2020-04-03 09:43:32', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1232, 'teacher001', '新增实验项目', 35, 'cc.mrbird.febs.system.controller.TeacherProjectController.teacherProjectAdd()', ' deptId: \"13\" projectId: \"6,7\"', '192.168.2.169', '2020-04-03 09:47:11', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1233, 'teacher001', '新增实验项目', 136, 'cc.mrbird.febs.system.controller.TeacherProjectController.teacherProjectAdd()', ' deptId: \"13\" projectId: \"6,7\"', '192.168.2.169', '2020-04-03 09:53:45', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1234, 'teacher001', '新增实验项目', 255, 'cc.mrbird.febs.system.controller.TeacherProjectController.teacherProjectAdd()', ' deptId: \"20\" projectId: \"6,7,6,7\"', '192.168.2.169', '2020-04-03 09:54:43', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1235, 'teacher001', '新增实验项目', 129, 'cc.mrbird.febs.system.controller.TeacherProjectController.teacherProjectAdd()', ' deptId: \"13\" projectId: \"6,7\"', '192.168.2.169', '2020-04-03 11:40:50', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1236, 'teacher001', '新增实验项目', 154, 'cc.mrbird.febs.system.controller.TeacherProjectController.teacherProjectAdd()', ' deptId: \"20\" projectId: \"6,6\"', '192.168.2.169', '2020-04-03 11:42:34', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1237, 'admin', '新增菜单/按钮', 94, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=216, parentId=206, menuName=修改实验项目, url=null, perms=teacherProject:update, icon=null, type=1, orderNum=null, createTime=Fri Apr 03 11:48:17 CST 2020, modifyTime=null)\"', '192.168.2.169', '2020-04-03 11:48:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1238, 'admin', '修改角色', 294, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=81, roleName=老师, remark=学校老师, createTime=null, modifyTime=Fri Apr 03 11:48:26 CST 2020, menuIds=181,191,193,205,211,183,199,206,215,216,207,200,208,209,201)\"', '192.168.2.169', '2020-04-03 11:48:27', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1239, 'admin', '新增菜单/按钮', 72, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=217, parentId=206, menuName=删除实验项目, url=null, perms=teacherProject:delete, icon=null, type=1, orderNum=null, createTime=Fri Apr 03 11:49:16 CST 2020, modifyTime=null)\"', '192.168.2.169', '2020-04-03 11:49:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1240, 'teacher001', '新增实验项目', 273, 'cc.mrbird.febs.system.controller.TeacherProjectController.teacherProjectAdd()', ' deptId: \"13\" projectId: \"6,6,7\"', '192.168.2.169', '2020-04-03 12:02:26', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1241, 'teacher001', '新增实验项目', 223, 'cc.mrbird.febs.system.controller.TeacherProjectController.teacherProjectAdd()', ' deptId: \"13\" projectId: \"6\"', '192.168.2.169', '2020-04-03 12:02:42', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1242, 'teacher001', '删除实验项目', 95, 'cc.mrbird.febs.system.controller.TeacherProjectController.deleteTeacherProject()', ' teacherProjectId: \"1\"', '192.168.2.169', '2020-04-03 15:33:06', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1243, 'teacher001', '删除实验项目', 89, 'cc.mrbird.febs.system.controller.TeacherProjectController.deleteTeacherProject()', ' teacherProjectId: \"11\"', '192.168.2.169', '2020-04-03 15:33:22', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1244, 'admin', '新增项目', 87, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=8, projectCode=null, projectName=ssssssssss, courseId=21, courseName=模拟电子实验, projectContent=请输入 实验描述ds, projectSort=null, state=1, reportUrl=null, operator=null, createTime=2020-04-03T15:35:50.505, updateTime=null)\" file: null', '192.168.2.138', '2020-04-03 15:35:51', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1245, 'teacher001', '删除实验项目', 86, 'cc.mrbird.febs.system.controller.TeacherProjectController.deleteTeacherProject()', ' teacherProjectId: \"13,15\"', '192.168.2.169', '2020-04-03 15:40:04', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1246, 'admin', '新增项目', 409289, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=9, projectCode=null, projectName=sss, courseId=21, courseName=模拟电子实验, projectContent=请输入 实验描述, projectSort=null, state=1, reportUrl=null, operator=null, createTime=2020-04-03T15:45:20.718, updateTime=null)\" file: null', '192.168.2.138', '2020-04-03 15:45:21', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1247, 'admin', '新增项目', 153405, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=11, projectCode=null, projectName=ssss, courseId=null, courseName=, projectContent=请输入 实验描述, projectSort=null, state=1, reportUrl=null, operator=null, createTime=2020-04-03T15:55:47.911, updateTime=null)\" file: null', '192.168.2.138', '2020-04-03 15:56:10', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1248, 'admin', '新增项目', 77122, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', NULL, '192.168.2.138', '2020-04-03 16:00:50', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1249, 'admin', '新增项目', 290349, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=15, projectCode=null, projectName=dsdsdsd, courseId=null, courseName=, projectContent=null, projectSort=null, state=1, reportUrl=null, operator=null, createTime=2020-04-03T16:09:05.346, updateTime=null)\" file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@1c15835a\"', '192.168.2.138', '2020-04-03 16:09:05', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1250, 'admin', '新增项目', 43943, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=18, projectCode=null, projectName=dsd, courseId=null, courseName=, projectContent=null, projectSort=null, state=1, reportUrl=null, operator=null, createTime=2020-04-03T16:22:06.128, updateTime=null)\" file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@42549281\"', '192.168.2.138', '2020-04-03 16:22:06', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1251, 'admin', '新增项目', 648060, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=20, projectCode=null, projectName=ddsddsadd, courseId=22, courseName=数字电子实验, projectContent=null, projectSort=null, state=1, reportUrl=null, operator=null, createTime=2020-04-03T16:50:52.091, updateTime=null)\" file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@1affb3c2\"', '192.168.2.138', '2020-04-03 16:50:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1252, 'admin', '新增项目', 207, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=22, projectCode=null, projectName=惹我热无热无若, courseId=22, courseName=数字电子实验, projectContent=null, projectSort=null, state=1, reportUrl=project/8/deee5132fd7b49bdba0414836060bb76单管共射放大电路的测试(1).docx, reportSize=267.22KB, operator=null, createTime=2020-04-03T17:32:02.700, updateTime=null)\" file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@4864fe37\"', '192.168.2.138', '2020-04-03 17:32:03', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1253, 'admin', '新增项目', 143, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=23, projectCode=null, projectName=佛挡杀佛各点, courseId=21, courseName=模拟电子实验, projectContent=null, projectSort=null, state=1, reportUrl=project/8/e831a9f31cd14c878b2152c99f356773测试数据.docx, reportSize=18.79KB, operator=null, createTime=2020-04-03T17:34:31.605, updateTime=null)\" file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@4cb45ea\"', '192.168.2.138', '2020-04-03 17:34:32', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1254, 'admin', '新增项目', 185, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=24, projectCode=null, projectName=范德萨发的, courseId=22, courseName=数字电子实验, projectContent=null, projectSort=null, state=1, reportUrl=project/8/80959d7eb6f64dd2959bd9b233370c96单管共射放大电路的测试(1).docx, reportSize=267.22KB, operator=null, createTime=2020-04-03T17:36:37.668, updateTime=null)\" file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@555d8a11\"', '192.168.2.138', '2020-04-03 17:36:38', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1255, 'admin', '新增项目', 58, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=25, projectCode=null, projectName=撒大声地, courseId=21, courseName=模拟电子实验, projectContent=请输入 实验描述, projectSort=null, state=1, reportUrl=null, reportSize=null, operator=null, createTime=2020-04-03T17:37:36.044, updateTime=null)\" file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@2261f594\"', '192.168.2.138', '2020-04-03 17:37:36', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1256, 'admin', '新增项目', 141, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=26, projectCode=null, projectName=aasasa, courseId=null, courseName=, projectContent=请输入 实验描述, projectSort=null, state=1, reportUrl=macw.serverUrlproject/8/cae6ee10f0f64985b15a985585e44814用Java语言向串口读写数据的方法.docx, reportSize=17.25KB, operator=null, createTime=2020-04-03T17:55:01.555, updateTime=null)\" file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@733e9e5f\"', '192.168.2.138', '2020-04-03 17:55:02', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1257, 'admin', '新增项目', 10027, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=27, projectCode=null, projectName=rwewer, courseId=null, courseName=, projectContent=请输入 实验描述rwerwee, projectSort=null, state=1, reportUrl=http://henaumcw.top/project/8/de792f6d78524472aceea31570b66c33任务.docx, reportSize=13.75KB, operator=null, createTime=2020-04-03T17:57:55.595, updateTime=null)\" file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@586a4e87\"', '192.168.2.138', '2020-04-03 17:57:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1258, 'admin', '删除实验项目', 78, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"10\"', '192.168.2.138', '2020-04-03 18:06:01', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1259, 'admin', '删除实验项目', 62, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"11\"', '192.168.2.138', '2020-04-03 18:06:06', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1260, 'admin', '删除实验项目', 71, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"12\"', '192.168.2.138', '2020-04-03 18:06:09', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1261, 'admin', '删除实验项目', 60, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"13\"', '192.168.2.138', '2020-04-03 18:06:12', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1262, 'admin', '删除实验项目', 106, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"14\"', '192.168.2.138', '2020-04-03 18:06:41', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1263, 'admin', '删除实验项目', 55, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"15\"', '192.168.2.138', '2020-04-03 18:06:46', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1264, 'admin', '删除实验项目', 74, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"16\"', '192.168.2.138', '2020-04-03 18:06:52', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1265, 'admin', '删除实验项目', 70, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"8\"', '192.168.2.138', '2020-04-03 18:06:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1266, 'admin', '删除实验项目', 60, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"9\"', '192.168.2.138', '2020-04-03 18:06:59', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1267, 'admin', '删除实验项目', 73, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"18\"', '192.168.2.138', '2020-04-03 18:07:01', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1268, 'admin', '删除实验项目', 129, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"17\"', '192.168.2.138', '2020-04-03 18:07:04', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1269, 'admin', '删除实验项目', 62, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"20\"', '192.168.2.138', '2020-04-03 18:07:13', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1270, 'admin', '新增项目', 81, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=28, projectCode=null, projectName=hgfhgfhgfj, courseId=21, courseName=模拟电子实验, projectContent=请输入 实验描述, projectSort=null, state=1, reportUrl=null, reportSize=null, operator=null, createTime=2020-04-03T18:14:43.423, updateTime=null)\" file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@65b0f474\"', '192.168.2.138', '2020-04-03 18:14:44', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1271, 'teacher001', '删除实验项目', 101, 'cc.mrbird.febs.system.controller.TeacherProjectController.deleteTeacherProject()', ' teacherProjectId: \"19\"', '192.168.2.169', '2020-04-06 08:42:50', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1272, 'teacher001', '删除实验项目', 69, 'cc.mrbird.febs.system.controller.TeacherProjectController.deleteTeacherProject()', ' teacherProjectId: \"12,14\"', '192.168.2.169', '2020-04-06 08:42:58', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1273, 'teacher001', '新增实验项目', 84, 'cc.mrbird.febs.system.controller.TeacherProjectController.teacherProjectAdd()', ' deptId: \"13\" projectId: \"6\"', '192.168.2.169', '2020-04-06 08:43:09', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1274, 'admin', '新增课程', 193, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=25, courseName=test, courseContent=请输入 课程内容 或描述testtesttesttesttesttesttesttesttesttesttesttest, deptNameList=经济学院, state=1, createUser=null, createTime=2020-04-06T08:45:03.749, createTimes=null, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@101ca85b\" deptNameLists: \"[Ljava.lang.String;@14232e80\"', '192.168.2.169', '2020-04-06 08:45:04', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1275, 'admin', '新增课程', 215, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=26, courseName=test2, courseContent=请输入 课程内容 或描述test testtesttesttesttesttesttesttest&nbsp;, deptNameList=农学院, state=1, createUser=null, createTime=2020-04-06T08:45:21.120, createTimes=null, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@7115acf1\" deptNameLists: \"[Ljava.lang.String;@752a260b\"', '192.168.2.169', '2020-04-06 08:45:21', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1276, 'teacher001', '删除课程', 63, 'cc.mrbird.febs.system.controller.CourseController.deleteCourses()', ' courseIds: \"25,26\"', '192.168.2.169', '2020-04-06 08:45:30', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1277, 'teacher001', '删除实验项目', 80, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"19\"', '192.168.2.169', '2020-04-06 08:51:04', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1278, 'admin', '删除实验项目', 106, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"23\"', '192.168.2.169', '2020-04-06 09:10:39', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1279, 'admin', '删除实验项目', 81, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"21,22\"', '192.168.2.169', '2020-04-06 09:33:23', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1280, 'admin', '删除实验项目', 81, 'cc.mrbird.febs.system.controller.ExperimentProjectController.deleteCourses()', ' projectIds: \"25\"', '192.168.2.169', '2020-04-06 09:34:33', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1281, 'admin', '新增课程', 169, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=27, courseName=test3, courseContent=请输入 课程内容 或描述test, deptNameList=农学院, state=1, createUser=null, createTime=2020-04-06T09:34:55.591, createTimes=null, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@789fe84\" deptNameLists: \"[Ljava.lang.String;@3b66aad\"', '192.168.2.169', '2020-04-06 09:34:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1282, 'admin', '删除课程', 84, 'cc.mrbird.febs.system.controller.CourseController.deleteCourses()', ' courseIds: \"20,27\"', '192.168.2.169', '2020-04-06 09:35:02', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1283, 'admin', '新增课程', 102, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=28, courseName=test, courseContent=请输入 课程内容 或描述test, deptNameList=外国语学院, state=1, createUser=null, createTime=2020-04-06T09:35:16.446, createTimes=null, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@3b15508d\" deptNameLists: \"[Ljava.lang.String;@3f2ac337\"', '192.168.2.169', '2020-04-06 09:35:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1284, 'admin', '删除课程', 58, 'cc.mrbird.febs.system.controller.CourseController.deleteCourses()', ' courseIds: \"28\"', '192.168.2.169', '2020-04-06 09:35:21', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1285, 'admin', '修改角色', 274, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=81, roleName=老师, remark=学校老师, createTime=null, modifyTime=Mon Apr 06 09:38:35 CST 2020, menuIds=181,191,193,205,211,183,199,206,215,216,217,207,200,208,201)\"', '192.168.2.169', '2020-04-06 09:38:35', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1286, 'admin', '新增用户', 174, 'cc.mrbird.febs.system.controller.UserController.addUser()', ' user: \"User(userId=17, username=gzca, password=63caeb98447f5d554789321adfed7eba, idNumber=, deptId=null, courseId=null, courseName=, email=872231868@qq.com, mobile=17637389874, status=1, createTime=Mon Apr 06 10:13:40 CST 2020, modifyTime=null, lastLoginTime=null, sex=0, avatar=default.jpg, theme=black, isTab=1, description=, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=82, roleName=null)\" dept: \"[Ljava.lang.Integer;@73e905d3\"', '192.168.2.169', '2020-04-06 10:13:41', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1287, 'admin', '删除用户', 77, 'cc.mrbird.febs.system.controller.UserController.deleteUsers()', ' userIds: \"16\"', '192.168.2.169', '2020-04-06 10:14:02', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1288, 'admin', '修改项目', 104, 'cc.mrbird.febs.system.controller.ExperimentProjectController.updateExperiment()', ' experimentProject: \"ExperimentProject(projectId=1, projectCode=null, projectName=实验一 单管放大器放大电路, courseId=null, courseName=数字电子实验课, projectContent=实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路, projectSort=null, state=1, reportUrl=null, reportSize=null, operator=null, createTime=null, updateTime=2020-04-07T09:57:40.977)\"', '192.168.2.169', '2020-04-07 09:57:41', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1289, 'admin', '修改项目', 73, 'cc.mrbird.febs.system.controller.ExperimentProjectController.updateExperiment()', ' experimentProject: \"ExperimentProject(projectId=5, projectCode=null, projectName=实验三 负反馈放大器, courseId=null, courseName=模拟电子实验课, projectContent=请输入 项目描述实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器, projectSort=null, state=1, reportUrl=null, reportSize=null, operator=null, createTime=null, updateTime=2020-04-07T09:57:48.216)\"', '192.168.2.169', '2020-04-07 09:57:48', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1290, 'admin', '修改项目', 103, 'cc.mrbird.febs.system.controller.ExperimentProjectController.updateExperiment()', ' experimentProject: \"ExperimentProject(projectId=7, projectCode=null, projectName=实验二  单管共射放大电路的测试, courseId=null, courseName=电工技术实验课, projectContent=<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">一、实验目的1、通过实验认识实际电路，加深对放大电路的理解，进一步建立信号放大的概念。<p style=\\\"margin-left:21.0pt;mso-para-margin-left:2.0gd;\\ntab-stops:0cm\\\">2<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">、掌握静态工作点的调整与测试方法。观察静态工作点对放大电路输出波形的影响。3、掌握测试电压放大倍数的方法。4、分析静态工作点对放大电路性能的影响。<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">二、实验仪器与器材1、<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">模拟电路实验板&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2、稳压电源&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3、示波器&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4、函数信号发生器&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5、万用表&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">三、实验原理实验电路图如图2-1。该图是分压式偏置单管共射放大电路，图中B1—B1ˊ和C1—C<span style=\\\"font-family:\\n宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">ˊ<span style=\\\"font-family:\\n宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">间断开是为了测电流用的，不测电流时应短接。1、静态工作点的测量与调试测量静态工作点，应断开交流信号源，并在放大电路的u<span style=\\\"font-size:7.5pt;\\nmso-bidi-font-size:12.0pt\\\">i<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">输入端短路的状态下测量，用万用表的直流电压挡及直流电流挡分别测量I<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt\\\">B、IC、UCE及各极对地电位，也可测量URC，计算出<span style=\\\"position:relative;\\ntop:15.0pt;mso-text-raise:-15.0pt\\\">\\n \\n \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n \\n \\n \\n\\n \\n\\n \\n \\n<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">。该图电路中，当UCC、RC、RE等参数确定以后，工作点主要靠调节偏置电路的电阻RP来实现。如果静态工作点调得过高或过低，当输入端加入正弦信号u<span style=\\\"font-size:7.5pt;\\nmso-bidi-font-size:12.0pt\\\">i<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">时，若幅度较大，则输出信号u<span style=\\\"font-size:\\n7.5pt;mso-bidi-font-size:12.0pt\\\">o<span style=\\\"font-family:宋体;\\nmso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">将会产生饱和或截止失真。只有当静态工作点调得适中时，可以使三极管工作在最大动态范围。</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p><p style=\\\"text-align:center;text-indent:21.0pt;\\nmso-char-indent-count:2.0\\\">\\n \\n\\n \\n \\n<span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\"></span></p><p style=\\\"text-align:center;text-indent:18.05pt;\\nmso-char-indent-count:2.0\\\"><span style=\\\"font-size:9.0pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-bidi-font-family:宋体\\\">图2-1&nbsp; 单管共射放大电路2、放大电路动态参数测试</span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l2 level1 lfo1\\\">①　测电压放大倍数Au将图中F—G短接。在放大电路的输入端加交流信号u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">i<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">时，在输出端输出一个放大了的交流信号u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">o<span style=\\\"font-size:7.5pt;\\nmso-bidi-font-size:12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">。则电压放大倍数Au的计算公式为：</span></span></span></span></p><p style=\\\"margin-left:21.0pt;mso-para-margin-left:\\n2.0gd;text-align:center\\\"><span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\"><span style=\\\"position:relative;top:15.0pt;mso-text-raise:\\n-15.0pt\\\">\\n \\n\\n \\n \\n由上面公式可知：测出u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">o<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">与u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">i<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">的值即可算出A<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">u<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">的值。应当注意，测量u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">o<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">与u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">i<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">时，必须保证放大器的输出电压为不失真波形，这可以用示波器监视。</span></span></span></span></span></span></span></span></span></span></span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l2 level1 lfo1\\\">②　输入电阻r<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">i<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">和输出电阻r<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">o<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">的测量图2-2是放大电路输入与输出电路的等效电路图，根据图中的电压电流关系可以看出，只要测量出相应的电压值，便可求出输入电阻r<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">i<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">和输出电阻r<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">o<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">。&nbsp;</span></span></span></span></span></span></span></span></p><p style=\\\"text-align:center;text-indent:18.0pt;\\nmso-char-indent-count:2.0\\\"><span style=\\\"font-size:9.0pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">放大电路</span></p><p style=\\\"text-align:center;text-indent:21.0pt;\\nmso-char-indent-count:2.0\\\">\\n \\n\\n \\n \\n<span style=\\\"font-size:9.0pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;;background:yellow\\\"></span></p><p style=\\\"text-align:center;text-indent:18.0pt;\\nmso-char-indent-count:2.0\\\"><span style=\\\"font-size:9.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">图2-2&nbsp;&nbsp; 输入电阻与输出电阻的测量输入电阻r<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">i的测量：由图2-2看出：<span style=\\\"position:relative;\\ntop:30.0pt;mso-text-raise:-30.0pt\\\">\\n \\n\\n \\n \\n式中的R<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">S是已知的，因此，只要用电子毫伏表分别量出u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">i<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">与u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">s<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">即可求得r<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">i<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">。&nbsp;输出电阻r<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">o的测量：图2-2中，<span style=\\\"font-size:14.0pt;\\nmso-bidi-font-size:12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">o<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">是负载开路时的输出电压，u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">L<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">是接入负载R<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">L<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">后的输出电压。则：\\n \\n\\n \\n \\n&nbsp;&nbsp;&nbsp;&nbsp; 所以&nbsp; <span style=\\\"position:relative;\\ntop:15.0pt;mso-text-raise:-15.0pt\\\">\\n \\n\\n \\n \\n因此只要测量出u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">o<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">、u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">L<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">，即可求得r<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">o<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">。&nbsp;<span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">四、实验内容与步骤</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l3 level1 lfo2;tab-stops:36.75pt\\\">1、确认实验电路及各测试点的位置，测量电流放大系数β值对照模拟实验系统与图2-1，把稳压电源的输出调至12V，将放大电路的U<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">CC端和地端分别接+12V电源的正极和负极。VT<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">1<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">接在电路上，分别将电流表串入基极和集电极支路，连接读出Ib、Ic，计算β值，记入表2-1中。静态工作点的调试与测量</span></span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l8 level1 lfo3\\\">①　调整并测试给定的静态工作点<span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">为防止干扰，应先将<span style=\\\"font-size:12.0pt;\\nfont-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">i<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">短路。将万用表的直流5mA挡串接在集电极的C—C<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">1<span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">ˊ中，B1—B1ˊ短接，调节R<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">P1使I<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">C=1mA，分别测出此时的I<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">B、U<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">B、U<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">C U<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">E<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">、U<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">RC<span style=\\\"font-family:\\n宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">值，并将测试结果记入表2-1中。I<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">C1<span style=\\\"font-family:\\n宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">也可通过测R<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">C1<span style=\\\"font-family:\\n宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">两端电压再换算。</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p><p style=\\\"margin-left:21.0pt;mso-para-margin-left:\\n2.0gd;text-align:center\\\"><span style=\\\"position:relative;\\ntop:5.0pt;mso-text-raise:-5.0pt\\\">\\n \\n\\n \\n \\n<span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\"><span style=\\\"position:relative;top:15.0pt;mso-text-raise:\\n-15.0pt\\\">\\n \\n\\n \\n \\n</span></span></span></p><p style=\\\"margin-left:0cm;text-align:left;\\ntext-indent:21.0pt;mso-char-indent-count:2.0;mso-list:l8 level1 lfo3\\\">②　调整并测试最大动态范围的静态工作点。调节函数信号发生器使其产生1KHz<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">的正弦波信号，幅度调在最低位，将此信号作为us<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">信号接至放大电路的输入端，经电阻<span style=\\\"mso-bidi-font-size:\\n10.5pt\\\">Rs<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;\\nmso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">送到VT1<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">的基极，电路的净输入信号为<span style=\\\"mso-bidi-font-size:\\n10.5pt\\\">ui<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;\\nmso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">，这样可防止us<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">太高时损坏三极管<span style=\\\"mso-bidi-font-size:\\n10.5pt\\\">VT1<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;\\nmso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">。用示波器监视输出电压uo<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">波形。调节函数信号发生器使<span style=\\\"mso-bidi-font-size:\\n10.5pt\\\">us<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;\\nmso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">逐渐加大，如uo<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">出现半边失真则可调节<span style=\\\"mso-bidi-font-size:\\n10.5pt\\\">RP1<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;\\nmso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">，使失真消失；再继续加大us<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">，经过反复加大<span style=\\\"mso-bidi-font-size:\\n10.5pt\\\">us<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;\\nmso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">及调节RP1<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">，uo波形会出现上下峰均稍有相同程度的失真，这叫双向失真。此时电路所处的工作点为最大动态范围的静态工作点，即工作点Q<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">刚好在交流负载线的中间，也称最佳工作点。波形调好后将输入端的信号us<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">撤除，保持RP1不变，<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">将ui短路。<span style=\\\"mso-bidi-font-size:10.5pt;\\nfont-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">用万用表直流档分别测<span style=\\\"mso-bidi-font-size:\\n10.5pt\\\">IB1<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;\\nmso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">、IC1<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">、UB1、UC1<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">、UE1、URC<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">，记入表2-1中。</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l3 level1 lfo2;tab-stops:36.75pt\\\">2、<span style=\\\"font-family:\\n宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">测量放大电路的电压放大倍数A<span style=\\\"font-size:\\n7.5pt;mso-bidi-font-size:12.0pt\\\">u</span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l1 level1 lfo4\\\">①　将电路中<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">B<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">1<span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">—B<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">1<span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">ˊ连接，C—Cˊ连接，将F点与G点连接。</span></span></span></span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l1 level1 lfo4\\\">②　将函数信号发生器输出调为正弦波，频率调为1KHZ，幅度调至最小。并将函数信号发生器的输出作为放大器的输入信号us加至放大电路输入端，用示波器监视<span style=\\\"font-size:\\n14.0pt;mso-bidi-font-size:12.0pt\\\">u<span style=\\\"font-size:\\n7.5pt;mso-bidi-font-size:12.0pt\\\">o<span style=\\\"font-family:宋体;\\nmso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">波形不失真，调节信号发生器的幅度旋钮，适当增加us幅度，若输出波形失真可适当减小<span style=\\\"font-size:\\n12.0pt\\\">u<span style=\\\"font-size:7.5pt;mso-bidi-font-size:\\n12.0pt\\\">s幅值。</span></span></span></span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l1 level1 lfo4\\\">③　按表2-2<span style=\\\"font-family:\\n宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">所列测试条件用电子毫伏表测试相应的u<span style=\\\"font-size:\\n7.5pt;mso-bidi-font-size:12.0pt\\\">i 和uo的有效值Ui和Uo，并用示波器观察uo波形与RL的关系，将以上结果填入表2-2中。</span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l3 level1 lfo2;tab-stops:36.75pt\\\">3、<span style=\\\"font-family:\\n宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">测量输入和输出电阻①　输入交流1KHZ的正弦信号，并使输出波形不失真，用示波器分别测出u<span style=\\\"font-size:7.5pt;\\nmso-bidi-font-size:12.0pt\\\">s<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">和u<span style=\\\"font-size:\\n7.5pt;mso-bidi-font-size:12.0pt\\\">i<span style=\\\"mso-bidi-font-size:10.5pt;\\nfont-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">幅值有效值<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">记入表2-3中。②　先测输出开路时的输出电压uo值，再接入RL的负载电阻，测出输出电压uL，记入表2-3中。③　根据以测量值分别计算出<span style=\\\"font-size:12.0pt;\\nfont-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">r<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">i<span style=\\\"font-family:宋体;\\nmso-hansi-font-family:&quot;Times New Roman&quot;\\\">和\\nr<span style=\\\"font-size:9.0pt;mso-bidi-font-size:12.0pt;\\nfont-family:宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">o写入表2-3中。4、观察静态工作点对放大电路的输出电压波形的影响。</span></span></span></span></span></span></span></span></span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l0 level1 lfo6\\\">①　断开RL，输入1KHZ的交流信号，调节RP1，直到观察到uo负半周波形有被削顶的失真，将波形画入表2-4中。撤掉信号us，测量此时的UCE，并将结果记入表2-4中。</p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l0 level1 lfo6\\\">②　仍加入1KHZ交流信号，RL仍断开，反向调节RP1，直到观察到uo正半周波形有被削顶的失真，将波形画入表2-4中。撤掉输入信号us，测量此时的UCE并记入表2-4中。</p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l0 level1 lfo6\\\">③　加入us观察到不失真的输出电压波形。再加大us的幅值并调整RP1，直到波形正负半周都有削顶，观察波形失真情况并记入表2-4中，这种失真称为大信号失真或双向失真，撤掉us，测出此时的UCE，记入表2-4中。</p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l0 level1 lfo6\\\">④　双向失真时，将电路中的F点与G<span style=\\\"font-family:\\n宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">点连线断开，把F点与H点短接，再观察波形有何变化（应当使uo幅度下降并减轻失真）。<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">五、实验注意事项</span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l5 level1 lfo7;tab-stops:36.75pt\\\">1、<span style=\\\"font-family:\\n宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">要爱护实验设备，不得损坏各种零配件。不要用力拉扯连接线，不要随意插拔元件。</span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l5 level1 lfo7;tab-stops:36.75pt\\\">2、实验前应先将稳压电源空载调至所需电压值后，关掉电源再接至电路，实验时再打开电源。改变电路结构前也应将电源断开。应保证电源和信号源不能出现短路。</p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l5 level1 lfo7;tab-stops:36.75pt\\\">3、实验过程中保持实验电路与各仪器仪表“共地”。<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">六、<span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">预习要求</span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l6 level1 lfo8;tab-stops:36.75pt\\\">1、复习单管放大电路的分析方法。</p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l6 level1 lfo8;tab-stops:36.75pt\\\">2、<span style=\\\"font-family:\\n宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">熟悉实验原理及表2-1、2-2、2-3中的测试要求。</span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l6 level1 lfo8;tab-stops:36.75pt\\\">3、<span style=\\\"font-family:\\n宋体;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">估算表2-1、2-2中各数值的范围。<span style=\\\"font-family:宋体;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">七、实验报告内容</span></span></p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l7 level1 lfo9;tab-stops:36.75pt\\\">1、认真列表整理结果，将测量值与理论值相比较，分析误差原因。</p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l7 level1 lfo9;tab-stops:36.75pt\\\">2、分析静态工作点对放大器的影响。</p><p style=\\\"margin-left:0cm;text-indent:21.0pt;mso-char-indent-count:\\n2.0;mso-list:l7 level1 lfo9;tab-stops:36.75pt\\\">3、总结分析实验过程中出现的问题。&nbsp;表2-1&nbsp; 放大电路的静态工作点（Uces=1.2V）\\n\\n\\n \\n  \\n  测试条件\\n  \\n  \\n  测试值 Ic=1mA\\n  \\n  \\n  计算值\\n  \\n \\n \\n  \\n  IB\\n  \\n  \\n  IC\\n  \\n  \\n  UB\\n  \\n  \\n  UE\\n  \\n  \\n  UC\\n  \\n  \\n  RP1<span style=\\\"font-size:7.5pt;mso-bidi-font-size:12.0pt;mso-fareast-font-family:\\n  宋体;mso-fareast-theme-font:minor-fareast\\\">\\n  \\n  \\n  UBE\\n  \\n  \\n  UCE\\n  \\n  \\n  β\\n  \\n \\n \\n  \\n  理论值\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n \\n \\n  \\n  IC=2mA\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n \\n \\n  \\n  最大动态范围\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n \\n\\n\\n&nbsp;&nbsp;表2-2&nbsp; 放大电路的放大倍数（f=1kHz<span style=\\\"font-size:\\n9.0pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">）\\n\\n\\n \\n  \\n  测试条件\\n  \\n  \\n  测试值\\n  \\n  \\n  计算值\\n  \\n \\n \\n  \\n  Ui(mV)\\n  \\n  \\n  Uo(mV)\\n  \\n  \\n  uo<span style=\\\"mso-bidi-font-size:\\n  10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n  &quot;Times New Roman&quot;\\\">波形\\n  \\n  \\n  Au= Uo/ Ui\\n  \\n \\n \\n  \\n  RL=∞\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  \\n   \\n   \\n  \\n   \\n   \\n  \\n  \\n  \\n  &nbsp;\\n  \\n \\n \\n  \\n  RL=1KΩ\\n  \\n  \\n  &nbsp;\\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  \\n   \\n   \\n  \\n   \\n   \\n  \\n  \\n  \\n  &nbsp;\\n  \\n \\n\\n\\n&nbsp;&nbsp;表2-3&nbsp; 静态工作点对输出电压波形的影响\\n\\n\\n \\n  \\n  测试条件&nbsp; R<span style=\\\"font-size:\\n  7.5pt;mso-bidi-font-size:12.0pt\\\">L=∞\\n  \\n  \\n  波形失真类&nbsp;&nbsp;&nbsp; <span style=\\\"font-family:宋体;\\n  mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">型\\n  \\n \\n \\n  \\n  输出电压波形\\n  \\n  \\n  UCE\\n  \\n \\n \\n  \\n  正半周削顶 \\n  &nbsp;\\n  \\n  \\n  \\n   \\n   \\n  \\n   \\n   \\n  \\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n \\n \\n  \\n  负半周削顶 \\n  &nbsp;\\n  \\n  \\n  \\n   \\n   \\n  \\n   \\n   \\n  \\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n \\n \\n  \\n  <span style=\\\"font-family:\\n  宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">正负半周削顶\\n  \\n  \\n  \\n   \\n   \\n  \\n   \\n   \\n  \\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n \\n\\n\\n<span style=\\\"font-size:12.0pt;\\nfont-family:宋体;mso-bidi-font-family:宋体\\\">&nbsp;<span style=\\\"font-size:12.0pt;\\nfont-family:宋体;mso-bidi-font-family:宋体\\\">&nbsp;表2-4&nbsp; 输入与输出电阻的测量\\n\\n\\n \\n  \\n  u<span style=\\\"font-size:7.5pt;\\n  mso-bidi-font-size:12.0pt\\\">s\\n  \\n  \\n  u<span style=\\\"font-size:7.5pt;\\n  mso-bidi-font-size:12.0pt\\\">i\\n  \\n  \\n  r<span style=\\\"font-size:7.5pt;\\n  mso-bidi-font-size:12.0pt\\\">i<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n  &quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">（KΩ）\\n  \\n  \\n  u<span style=\\\"font-size:7.5pt;\\n  mso-bidi-font-size:12.0pt\\\">o\\n  \\n  \\n  u<span style=\\\"font-size:5.5pt;\\n  mso-bidi-font-size:12.0pt\\\">L\\n  \\n  \\n  r<span style=\\\"font-size:7.5pt;\\n  mso-bidi-font-size:12.0pt\\\">o<span style=\\\"font-family:宋体;mso-ascii-font-family:\\n  &quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;\\\">（KΩ）\\n  \\n \\n \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  测量值\\n  \\n  \\n  理论值\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  测量值\\n  \\n  \\n  理论值\\n  \\n \\n \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n  \\n  &nbsp;\\n  \\n \\n\\n\\n<span style=\\\"font-size:12.0pt;\\nfont-family:宋体;mso-bidi-font-family:宋体\\\">&nbsp;<span style=\\\"font-size:12.0pt;\\nfont-family:宋体;mso-bidi-font-family:宋体\\\">&nbsp;<span style=\\\"font-size:12.0pt;\\nfont-family:宋体;mso-bidi-font-family:宋体\\\">&nbsp;<span style=\\\"font-size:12.0pt;font-family:\\n宋体;mso-bidi-font-family:宋体\\\">八、实验思考题<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">1<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">、能否用直流电压表直接测量晶体管的<span style=\\\"position:relative;\\ntop:5.0pt;mso-text-raise:-5.0pt\\\">\\n \\n\\n \\n \\n？ 为什么实验中要采用测<span style=\\\"position:\\nrelative;top:5.0pt;mso-text-raise:-5.0pt\\\">\\n \\n\\n \\n \\n、<span style=\\\"position:relative;\\ntop:5.0pt;mso-text-raise:-5.0pt\\\">\\n \\n\\n \\n \\n，再间接算出<span style=\\\"position:relative;\\ntop:5.0pt;mso-text-raise:-5.0pt\\\">\\n \\n\\n \\n \\n的方法？<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">2<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">、当调节偏置电阻<span style=\\\"position:relative;top:5.0pt;\\nmso-text-raise:-5.0pt\\\">\\n \\n\\n \\n \\n，使放大器输出波形出现饱和或截止失真时，晶体管的管压降\\n \\n\\n \\n \\n怎样变化？<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">3<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">、改变静态工作点对放大器的输入电阻<span style=\\\"position:relative;\\ntop:6.0pt;mso-text-raise:-6.0pt\\\">\\n \\n\\n \\n \\n有否影响？改变外接电阻<span style=\\\"position:\\nrelative;top:5.0pt;mso-text-raise:-5.0pt\\\">\\n \\n\\n \\n \\n对输出电阻<span style=\\\"position:relative;\\ntop:6.0pt;mso-text-raise:-6.0pt\\\">\\n \\n\\n \\n \\n有否影响？<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">4<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">、在测试<span style=\\\"position:relative;top:6.0pt;mso-text-raise:\\n-6.0pt\\\">\\n \\n\\n \\n \\n，<span style=\\\"position:relative;\\ntop:6.0pt;mso-text-raise:-6.0pt\\\">\\n \\n\\n \\n \\n和<span style=\\\"position:relative;\\ntop:6.0pt;mso-text-raise:-6.0pt\\\">\\n \\n\\n \\n \\n时怎样选择输入信号的大小和频率？为什么信号频率一般选1KHz，而不选100KHz或更高？ <span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">5<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">、测试中，如果将函数信号发生器、交流毫伏表、示波器中任一仪器的二个测试端子接线换位（即各仪器的接地端不再连在一起），将会出现什么问题？<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">6<span style=\\\"mso-bidi-font-size:10.5pt;font-family:宋体;mso-bidi-font-family:\\n宋体\\\">、调试电路的静态工作点时，电阻Rb1为什么需要用一只固定电阻与可调电阻相串联？7<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">、负载大小对放大倍数有何影响？<span style=\\\"mso-bidi-font-size:\\n10.5pt\\\">8<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">、如果把F点悬空（电容Ce1<span style=\\\"mso-bidi-font-size:\\n10.5pt;font-family:宋体;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:\\n&quot;Times New Roman&quot;\\\">接入电路)，对放大倍数有何影响？\\n\\n\\n\\n\\nv\\\\:* {behavior:url(#default#VML);}\\no\\\\:* {behavior:url(#default#VML);}\\nw\\\\:* {behavior:url(#default#VML);}\\n.shape {behavior:url(#default#VML);}\\n\\n\\n\\n\\n\\n \\n  Normal\\n  0\\n  \\n  \\n  \\n  7.8 磅\\n  0\\n  2\\n  \\n  false\\n  false\\n  false\\n  \\n  EN-US\\n  ZH-CN\\n  X-NONE\\n  \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n  \\n  \\n  \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n   \\n  \\n\\n \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n  \\n \\n\\n\\n\\n\\n /* Style Definitions */\\n table.MsoNormalTable\\n\\t{mso-style-name:普通表格;\\n\\tmso-tstyle-rowband-size:0;\\n\\tmso-tstyle-colband-size:0;\\n\\tmso-style-noshow:yes;\\n\\tmso-style-priority:99;\\n\\tmso-style-parent:\\\"\\\";\\n\\tmso-padding-alt:0cm 5.4pt 0cm 5.4pt;\\n\\tmso-para-margin:0cm;\\n\\tmso-para-margin-bottom:.0001pt;\\n\\tmso-pagination:widow-orphan;\\n\\tfont-size:10.0pt;\\n\\tfont-family:\\\"Times New Roman\\\",serif;}\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n&nbsp;</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p></span>, projectSort=null, state=1, reportUrl=null, reportSize=null, operator=null, createTime=null, updateTime=2020-04-07T09:58:01.270)\"', '192.168.2.169', '2020-04-07 09:58:01', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1291, 'admin', '修改项目', 72, 'cc.mrbird.febs.system.controller.ExperimentProjectController.updateExperiment()', ' experimentProject: \"ExperimentProject(projectId=24, projectCode=null, projectName=范德萨发的, courseId=null, courseName=数字电子实验, projectContent=&lt;div class=\\\"layui-fluid layui-anim febs-anim\\\" id=\\\"febs-user\\\" lay-title=\\\"我的实验项目\\\"&gt;    &lt;table class=\\\"layui-hide\\\" id=\\\"demo\\\" lay-filter=\\\"test\\\"&gt;&lt;/table&gt;&lt;/div&gt;&lt;script &gt;=\\\"none\\\" type=\\\"text/javascript\\\"&gt;    layui.use([\'table\'],function () {        var table = layui.table        table.render({            elem: \'#demo\',            height: 420,            title: \'我的项目\',            page: true,//开启分页            toolbar: \'false\',//开启工具栏，此处显示默认图标，可以自定义模板，详见文档            totalRow: true, //开启合计行,            cols: [[ //表头                {type: \'checkbox\', fixed: \'left\'},                {field: \'id\', title: \'项目编号\', width:300, sort: true, fixed: \'left\', totalRowText: \'合计：\'},                {field: \'projectName\', title: \'项目实验名称\', width:300},                {field: \'projectContent\', title: \'项目实验描述\', width: 300, totalRow: true},                {field: \'projectReport\', title: \'项目实验报告\', width:300}            ]]        });    })&lt;/script&gt;, projectSort=null, state=1, reportUrl=null, reportSize=null, operator=null, createTime=null, updateTime=2020-04-07T09:58:15.042)\"', '192.168.2.169', '2020-04-07 09:58:15', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1292, 'admin', '修改项目', 81, 'cc.mrbird.febs.system.controller.ExperimentProjectController.updateExperiment()', ' experimentProject: \"ExperimentProject(projectId=28, projectCode=null, projectName=hgfhgfhgfj, courseId=null, courseName=模拟电子实验, projectContent=&nbsp; 请输入 实验描述&nbsp; 请输入 实验描述&nbsp; 请输入 实验描述&nbsp; 请输入 实验描述&nbsp; 请输入 实验描述&nbsp; 请输入 实验描述&nbsp; 请输入 实验描述&nbsp; 请输入 实验描述, projectSort=null, state=1, reportUrl=null, reportSize=null, operator=null, createTime=null, updateTime=2020-04-07T09:58:24.181)\"', '192.168.2.169', '2020-04-07 09:58:24', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1293, 'admin', '修改课程', 83, 'cc.mrbird.febs.system.controller.CourseController.updateCourses()', ' course: \"Course(courseId=23, courseName=电工技术实验课, courseContent=null, deptNameList=1522班,软件工程, state=1, createUser=null, createTime=null, createTimes=null, updateTime=2020-04-07T09:58:46.912)\"', '192.168.2.169', '2020-04-07 09:58:47', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1294, 'admin', '修改项目', 84, 'cc.mrbird.febs.system.controller.ExperimentProjectController.updateExperiment()', ' experimentProject: \"ExperimentProject(projectId=6, projectCode=null, projectName=实验四 运算放大器信号运算电路, courseId=null, courseName=电工技术实验课, projectContent=实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路实验四 运算放大器信号运算电路, projectSort=null, state=1, reportUrl=null, reportSize=null, operator=null, createTime=null, updateTime=2020-04-07T09:59:16.381)\"', '192.168.2.169', '2020-04-07 09:59:16', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1295, 'admin', '新增课程', 145, 'cc.mrbird.febs.system.controller.CourseController.addExperiment()', ' course: \"Course(courseId=29, courseName=传感器实验课222, courseContent=请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述请输入&nbsp;课程内容&nbsp;或描述, deptNameList=电子信息工程1301,计算机科学,1522班,软件工程,经济学院,外国语学院, state=1, createUser=null, createTime=2020-04-07T10:01:00.819, createTimes=null, updateTime=null)\" deptIds: \"[Ljava.lang.Integer;@1551c297\" deptNameLists: \"[Ljava.lang.String;@7ac15d75\"', '192.168.2.169', '2020-04-07 10:01:01', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1296, 'admin', '修改项目', 92, 'cc.mrbird.febs.system.controller.ExperimentProjectController.updateExperiment()', ' experimentProject: \"ExperimentProject(projectId=1, projectCode=null, projectName=实验一 单管放大器放大电路, courseId=null, courseName=传感器实验课222, projectContent=实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路实验一 单管放大器放大电路, projectSort=null, state=1, reportUrl=null, reportSize=null, operator=null, createTime=null, updateTime=2020-04-07T10:01:18.697)\"', '192.168.2.169', '2020-04-07 10:01:19', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1297, 'admin', '修改项目', 92, 'cc.mrbird.febs.system.controller.ExperimentProjectController.updateExperiment()', ' experimentProject: \"ExperimentProject(projectId=5, projectCode=null, projectName=实验三 负反馈放大器, courseId=null, courseName=传感器实验课222, projectContent=请输入 项目描述实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器实验三 负反馈放大器, projectSort=null, state=1, reportUrl=null, reportSize=null, operator=null, createTime=null, updateTime=2020-04-07T10:01:25.196)\"', '192.168.2.169', '2020-04-07 10:01:25', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1298, 'admin', '修改用户', 289, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=14, username=null, password=null, idNumber=null, deptId=null, courseId=null, courseName=传感器实验课222, email=105@qq.com, mobile=13512341234, status=1, createTime=null, modifyTime=Tue Apr 07 10:02:31 CST 2020, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=我是一名老师, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=81, roleName=null)\" dept: \"[Ljava.lang.Integer;@11cbe1de\"', '192.168.2.169', '2020-04-07 10:02:31', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1299, 'teacher001', '新增实验项目', 100, 'cc.mrbird.febs.system.controller.TeacherProjectController.teacherProjectAdd()', ' deptId: \"13\" projectId: \"24\"', '192.168.2.169', '2020-04-07 10:29:51', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1300, 'teacher001', '删除实验项目', 80, 'cc.mrbird.febs.system.controller.TeacherProjectController.deleteTeacherProject()', ' teacherProjectId: \"22\"', '192.168.2.169', '2020-04-07 10:29:59', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1301, 'teacher001', '新增实验项目', 116, 'cc.mrbird.febs.system.controller.TeacherProjectController.teacherProjectAdd()', ' deptId: \"13\" projectId: \"7,24\"', '192.168.2.169', '2020-04-07 10:31:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1302, 'admin', '新增项目', 233983, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=29, projectCode=null, projectName=sss, courseId=null, courseName=, projectContent=请输入 实验描述, projectSort=null, state=1, reportUrl=http://macwoss.http://oss-cn-beijing.aliyuncs.com/http://henaumcw.top//project/2020/04/07/989cf650-81b7-4bb1-aefd-6d46b6ef49dd/设计思路(1).docx, reportSize=15.86KB, operator=null, createTime=2020-04-07T17:02:22.861, updateTime=null)\" file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@22e239cd\"', '192.168.2.138', '2020-04-07 17:02:23', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1303, 'admin', '新增项目', 59107, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=30, projectCode=null, projectName=ssssddddd, courseId=null, courseName=, projectContent=请输入 实验描述, projectSort=null, state=1, reportUrl=project/2020/04/07/db326a98878e4ecfb23d3c73c0913e73/测试数据.docx, reportSize=18.79KB, operator=null, createTime=2020-04-07T17:07:46.733, updateTime=null)\" file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@3037d479\"', '192.168.2.138', '2020-04-07 17:07:47', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1304, 'admin', '新增项目', 7162, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=31, projectCode=null, projectName=dsffdfd, courseId=21, courseName=模拟电子实验课, projectContent=请输入 实验描述sdsadfsddc, projectSort=null, state=1, reportUrl=http://henaumcw.top/project/2020/04/07/5214d374e5ef4a728c8af1da35f93fe0/智慧电子实验室.docx, reportSize=448.96KB, operator=null, createTime=2020-04-07T17:56:08.798, updateTime=null)\" file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@b050f68\"', '192.168.2.138', '2020-04-07 17:56:09', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1305, 'teacher001', '查看提交情况', 181, 'cc.mrbird.febs.teacher.controller.TeacherCommitController.commitList()', ' deptId: \"13\" userCommit: \"UserCommit(commitId=null, stuId=null, deptId=13, projectId=null, courseId=null, teacherId=null, reportUrl=null, score=null, isCommit=null, createTime=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 15:09:33', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1306, 'teacher001', '查看提交情况', 75, 'cc.mrbird.febs.teacher.controller.TeacherCommitController.commitList()', ' deptId: \"13\" userCommit: \"UserCommit(commitId=null, stuId=null, deptId=13, projectId=null, courseId=null, teacherId=null, reportUrl=null, score=null, isCommit=null, createTime=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 15:15:53', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1307, 'teacher001', '查看提交情况', 4848, 'cc.mrbird.febs.teacher.controller.TeacherCommitController.commitList()', ' deptId: \"13\" userCommit: \"UserCommit(commitId=null, stuId=null, deptId=13, projectId=null, courseId=null, teacherId=null, reportUrl=null, score=null, isCommit=null, createTime=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 15:46:21', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1308, 'teacher001', '查看提交情况', 76, 'cc.mrbird.febs.teacher.controller.TeacherCommitController.commitList()', ' deptId: \"13\" userCommit: \"UserCommit(commitId=null, stuId=null, deptId=13, projectId=null, courseId=null, teacherId=null, reportUrl=null, score=null, isCommit=null, createTime=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 15:47:43', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1309, 'teacher001', '查看提交情况', 63, 'cc.mrbird.febs.teacher.controller.TeacherCommitController.commitList()', ' deptId: \"13\" userCommit: \"UserCommit(commitId=null, stuId=null, deptId=13, projectId=null, courseId=null, teacherId=null, reportUrl=null, score=null, isCommit=null, createTime=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 16:09:07', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1310, 'teacher001', '查看提交情况', 67, 'cc.mrbird.febs.teacher.controller.TeacherCommitController.commitList()', ' deptId: \"13\" userCommit: \"UserCommit(commitId=null, stuId=null, deptId=13, projectId=null, courseId=null, teacherId=null, reportUrl=null, score=null, isCommit=null, createTime=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 16:09:57', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1311, 'teacher001', '查看提交情况', 116, 'cc.mrbird.febs.teacher.controller.TeacherCommitController.commitList()', ' deptId: \"13\" userCommit: \"UserCommit(commitId=null, stuId=null, deptId=13, projectId=null, courseId=null, teacherId=null, reportUrl=null, score=null, isCommit=null, createTime=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 16:15:07', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1312, 'teacher001', '查看提交情况', 92, 'cc.mrbird.febs.teacher.controller.TeacherCommitController.commitList()', ' deptId: \"13\" userCommit: \"UserCommit(commitId=null, stuId=null, deptId=13, projectId=null, courseId=null, teacherId=null, reportUrl=null, score=null, isCommit=null, createTime=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 16:17:22', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1313, 'teacher001', '查看提交情况', 79, 'cc.mrbird.febs.teacher.controller.TeacherCommitController.commitList()', ' deptId: \"13\" userCommit: \"UserCommit(commitId=null, stuId=null, deptId=13, projectId=null, courseId=null, teacherId=null, reportUrl=null, score=null, isCommit=null, createTime=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 16:35:19', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1314, 'teacher001', '查看提交情况', 100, 'cc.mrbird.febs.teacher.controller.TeacherCommitController.commitList()', ' deptId: \"13\" userCommit: \"UserCommit(commitId=null, stuId=null, deptId=13, projectId=null, courseId=null, teacherId=null, reportUrl=null, score=null, isCommit=null, createTime=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 16:40:36', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1315, 'student001', '上传文件成功', 36, 'cc.mrbird.febs.student.controller.UserCommitController.upload()', '', '192.168.2.169', '2020-04-08 17:26:53', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1316, 'student001', '上传文件成功', 38, 'cc.mrbird.febs.student.controller.UserCommitController.upload()', '', '192.168.2.169', '2020-04-08 17:27:10', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1317, 'teacher001', '批阅报告', 70, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, deptId=null, projectId=null, courseId=null, teacherId=null, reportUrl=null, score=null, isCommit=null, createTime=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 17:35:59', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1318, 'student001', '上传文件成功', 517, 'cc.mrbird.febs.student.controller.UserCommitController.upload()', ' file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@4df21cd6\" teacherProject: \"TeacherProject(teacherProjectId=null, teacherId=14, projectId=24, courseId=29, projectName=范德萨发的, className=null, deptId=null)\"', '192.168.2.169', '2020-04-08 17:36:53', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1319, 'student001', '上传文件成功', 263, 'cc.mrbird.febs.student.controller.UserCommitController.upload()', ' file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@5b50b48c\" teacherProject: \"TeacherProject(teacherProjectId=null, teacherId=14, projectId=24, courseId=29, projectName=范德萨发的, className=null, deptId=null)\"', '192.168.2.169', '2020-04-08 17:42:06', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1320, 'teacher001', '批阅报告', 145, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 18:02:23', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1321, 'teacher001', '批阅报告', 85, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 18:02:52', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1322, 'teacher001', '批阅报告', 77, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 18:03:12', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1323, 'teacher001', '批阅报告', 74, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 18:05:46', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1324, 'teacher001', '批阅报告', 132, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 18:11:28', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1325, 'teacher001', '批阅报告', 127, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 18:11:30', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1326, 'teacher001', '批阅报告', 78, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 18:11:35', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1327, 'teacher001', '批阅报告', 78, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 18:17:30', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1328, 'teacher001', '批阅报告', 146, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 18:19:47', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1329, 'teacher001', '批阅报告', 53, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 18:20:08', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1330, 'student001', '上传文件成功', 424, 'cc.mrbird.febs.student.controller.UserCommitController.upload()', ' file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@512812\" teacherProject: \"TeacherProject(teacherProjectId=null, teacherId=14, projectId=24, courseId=29, projectName=范德萨发的, className=null, deptId=null)\"', '192.168.2.169', '2020-04-08 18:20:52', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1331, 'teacher001', '批阅报告', 68, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 18:24:28', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1332, 'student001', '上传文件成功', 419, 'cc.mrbird.febs.student.controller.UserCommitController.upload()', ' file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@2e021efd\" teacherProject: \"TeacherProject(teacherProjectId=null, teacherId=14, projectId=24, courseId=29, projectName=范德萨发的, className=null, deptId=null)\"', '192.168.2.169', '2020-04-08 18:25:37', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1333, 'teacher001', '批阅报告', 99, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 18:26:02', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1334, 'teacher001', '批阅报告', 76, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 18:26:09', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1335, 'student001', '上传文件成功', 102, 'cc.mrbird.febs.student.controller.UserCommitController.upload()', ' file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@44116ffd\" teacherProject: \"TeacherProject(teacherProjectId=null, teacherId=14, projectId=24, courseId=29, projectName=范德萨发的, className=null, deptId=null)\"', '192.168.2.169', '2020-04-08 18:27:26', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1336, 'teacher001', '批阅报告', 91, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-08 18:33:39', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1337, 'teacher001', '批阅报告', 121, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-09 08:48:33', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1338, 'teacher001', '查看提交情况', 63, 'cc.mrbird.febs.teacher.controller.TeacherCommitController.commitList()', ' deptId: \"13\" userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=13, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-09 08:50:30', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1339, 'teacher001', '新增实验项目', 67, 'cc.mrbird.febs.teacher.controller.TeacherProjectController.teacherProjectAdd()', ' deptId: \"20\" projectId: \"24\"', '192.168.2.138', '2020-04-09 08:50:51', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1340, 'teacher001', '查看提交情况', 49, 'cc.mrbird.febs.teacher.controller.TeacherCommitController.commitList()', ' deptId: \"20\" userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=20, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-09 08:50:55', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1341, 'teacher001', '查看提交情况', 40, 'cc.mrbird.febs.teacher.controller.TeacherCommitController.commitList()', ' deptId: \"20\" userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=20, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-09 08:51:07', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1342, 'student001', '上传文件成功', 98, 'cc.mrbird.febs.student.controller.UserCommitController.upload()', ' file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@6f9ce169\" teacherProject: \"TeacherProject(teacherProjectId=null, teacherId=14, projectId=24, courseId=29, projectName=范德萨发的, className=null, deptId=null)\"', '192.168.2.169', '2020-04-09 08:57:49', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1343, 'student001', '上传文件成功', 585, 'cc.mrbird.febs.student.controller.UserCommitController.upload()', ' file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@68a48533\" teacherProject: \"TeacherProject(teacherProjectId=null, teacherId=14, projectId=24, courseId=29, projectName=范德萨发的, className=null, deptId=null)\"', '192.168.2.169', '2020-04-09 09:00:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1344, 'student001', '上传文件成功', 233, 'cc.mrbird.febs.student.controller.UserCommitController.upload()', ' file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@17cd7bff\" teacherProject: \"TeacherProject(teacherProjectId=null, teacherId=14, projectId=24, courseId=29, projectName=范德萨发的, className=null, deptId=null)\"', '192.168.2.169', '2020-04-09 09:01:37', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1345, 'student001', '上传文件成功', 53, 'cc.mrbird.febs.student.controller.UserCommitController.upload()', ' file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@14f8f237\" teacherProject: \"TeacherProject(teacherProjectId=null, teacherId=14, projectId=24, courseId=29, projectName=范德萨发的, className=null, deptId=null)\"', '192.168.2.169', '2020-04-09 09:09:18', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1346, 'student001', '上传文件成功', 34, 'cc.mrbird.febs.student.controller.UserCommitController.upload()', ' file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@4df685c2\" teacherProject: \"TeacherProject(teacherProjectId=null, teacherId=14, projectId=24, courseId=29, projectName=范德萨发的, className=null, deptId=null)\"', '192.168.2.169', '2020-04-09 09:11:46', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1347, 'student001', '上传文件成功', 46, 'cc.mrbird.febs.student.controller.UserCommitController.upload()', ' file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@be1309\" teacherProject: \"TeacherProject(teacherProjectId=null, teacherId=14, projectId=24, courseId=29, projectName=范德萨发的, className=null, deptId=null)\"', '192.168.2.169', '2020-04-09 09:17:08', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1348, 'student001', '上传文件成功', 37, 'cc.mrbird.febs.student.controller.UserCommitController.upload()', ' file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@4c77c489\" teacherProject: \"TeacherProject(teacherProjectId=null, teacherId=14, projectId=24, courseId=29, projectName=范德萨发的, className=null, deptId=null)\"', '192.168.2.169', '2020-04-09 09:17:18', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1349, 'admin', '修改角色', 358, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=82, roleName=学生, remark=学校学生, createTime=null, modifyTime=Thu Apr 09 09:56:29 CST 2020, menuIds=213)\"', '192.168.2.169', '2020-04-09 09:56:29', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1350, 'student001', '上传文件成功', 52, 'cc.mrbird.febs.student.controller.UserCommitController.upload()', ' file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@31056a0f\" teacherProject: \"TeacherProject(teacherProjectId=null, teacherId=14, projectId=24, courseId=29, projectName=范德萨发的, className=null, deptId=null)\"', '192.168.2.169', '2020-04-09 10:20:52', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1351, 'student001', '上传文件成功', 306, 'cc.mrbird.febs.student.controller.UserCommitController.upload()', ' file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@608c88a2\" teacherProject: \"TeacherProject(teacherProjectId=null, teacherId=14, projectId=24, courseId=29, projectName=范德萨发的, className=null, deptId=null)\"', '192.168.2.169', '2020-04-09 10:22:12', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1352, 'student001', '上传文件成功', 232, 'cc.mrbird.febs.student.controller.UserCommitController.upload()', ' file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@10179f7b\" teacherProject: \"TeacherProject(teacherProjectId=null, teacherId=14, projectId=24, courseId=29, projectName=范德萨发的, className=null, deptId=null)\"', '192.168.2.169', '2020-04-09 11:17:46', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1353, 'student001', '上传文件成功', 193, 'cc.mrbird.febs.student.controller.UserCommitController.upload()', ' file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@419e7dd6\" teacherProject: \"TeacherProject(teacherProjectId=null, teacherId=14, projectId=24, courseId=29, projectName=范德萨发的, className=null, deptId=null)\"', '192.168.2.169', '2020-04-09 11:24:01', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1354, 'admin', '新增项目', 453, 'cc.mrbird.febs.system.controller.ExperimentProjectController.addExperiment()', ' experimentProject: \"ExperimentProject(projectId=32, projectCode=null, projectName=dsdsdsd, courseId=null, courseName=, projectContent=请输入 实验描述, projectSort=null, state=1, reportUrl=http://file.huistone.com/project/2020/04/09/160c7126f986432ab3d0bcefcaaeb73a/用Java语言向串口读写数据的方法.docx, reportSize=17.25KB, operator=null, createTime=2020-04-09T16:18:14.682, updateTime=null, teacherId=null)\" file: \"org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile@7566f14b\"', '192.168.2.138', '2020-04-09 16:18:15', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1355, 'teacher001', '批阅报告', 143, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.111', '2020-04-09 16:19:58', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1356, 'teacher001', '批阅报告', 162, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.169', '2020-04-10 10:25:04', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1357, 'teacher001', '批阅报告', 136, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.169', '2020-04-10 14:31:25', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1358, 'admin', '新增菜单/按钮', 186, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=218, parentId=0, menuName=打卡, url=, perms=, icon=layui-icon-verticalright, type=0, orderNum=null, createTime=Fri Apr 10 16:58:46 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-04-10 16:58:47', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1359, 'admin', '新增菜单/按钮', 119, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=219, parentId=0, menuName=统计, url=, perms=, icon=layui-icon-hourglass, type=0, orderNum=null, createTime=Fri Apr 10 16:59:33 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-04-10 16:59:34', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1360, 'admin', '修改菜单/按钮', 170, 'cc.mrbird.febs.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=218, parentId=0, menuName=打卡, url=, perms=, icon=layui-icon-verticalright, type=0, orderNum=50, createTime=null, modifyTime=Fri Apr 10 17:00:56 CST 2020)\"', '192.168.2.138', '2020-04-10 17:00:57', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1361, 'admin', '修改菜单/按钮', 152, 'cc.mrbird.febs.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=219, parentId=0, menuName=统计, url=, perms=, icon=layui-icon-hourglass, type=0, orderNum=51, createTime=null, modifyTime=Fri Apr 10 17:01:02 CST 2020)\"', '192.168.2.138', '2020-04-10 17:01:03', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1362, 'admin', '新增菜单/按钮', 128, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=220, parentId=0, menuName=请假, url=, perms=, icon=layui-icon-sever, type=0, orderNum=53, createTime=Fri Apr 10 17:03:00 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-04-10 17:03:00', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1363, 'admin', '修改角色', 304, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=82, roleName=学生, remark=学校学生, createTime=null, modifyTime=Fri Apr 10 17:04:05 CST 2020, menuIds=213,218,219,220)\"', '192.168.2.138', '2020-04-10 17:04:05', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1364, 'admin', '新增菜单/按钮', 112, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=221, parentId=0, menuName=查看/统计, url=, perms=, icon=layui-icon-unorderedlist, type=0, orderNum=60, createTime=Fri Apr 10 17:06:31 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-04-10 17:06:32', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1365, 'admin', '新增菜单/按钮', 118, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=222, parentId=0, menuName=审批, url=, perms=, icon=layui-icon-edit-square, type=0, orderNum=61, createTime=Fri Apr 10 17:06:51 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-04-10 17:06:52', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1366, 'admin', '修改菜单/按钮', 175, 'cc.mrbird.febs.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=181, parentId=0, menuName=考勤管理, url=, perms=, icon=layui-icon-project, type=0, orderNum=null, createTime=null, modifyTime=Fri Apr 10 17:14:57 CST 2020)\"', '192.168.2.138', '2020-04-10 17:14:58', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1367, 'teacher001', '批阅报告', 169, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '192.168.2.138', '2020-04-10 18:06:20', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1368, 'admin', '修改角色', 1397, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Tue Apr 14 11:37:44 CST 2020, menuIds=191,193,205,211,183,184,189,190,202,203,204,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.138', '2020-04-14 11:37:46', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1369, 'admin', '修改角色', 1300, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Tue Apr 14 11:37:46 CST 2020, menuIds=191,193,205,211,183,184,189,190,202,203,204,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.138', '2020-04-14 11:37:47', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1370, 'admin', '修改角色', 414, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=81, roleName=老师, remark=学校老师, createTime=null, modifyTime=Tue Apr 14 11:38:14 CST 2020, menuIds=191,193,205,211,183,199,206,215,216,217,207,200,208,201)\"', '192.168.2.138', '2020-04-14 11:38:15', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1371, 'admin', '修改角色', 426, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=81, roleName=老师, remark=学校老师, createTime=null, modifyTime=Tue Apr 14 11:38:32 CST 2020, menuIds=191,193,205,211,183,199,206,215,216,217,207,200,208,201,221,222)\"', '192.168.2.138', '2020-04-14 11:38:33', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1372, 'admin', '新增菜单/按钮', 114, 'cc.mrbird.febs.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=223, parentId=0, menuName=人像录入, url=, perms=, icon=layui-icon-deleteuser, type=0, orderNum=null, createTime=Tue Apr 14 11:40:11 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-04-14 11:40:12', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1373, 'admin', '修改角色', 1501, 'cc.mrbird.febs.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Tue Apr 14 11:42:18 CST 2020, menuIds=191,193,205,211,183,184,189,190,202,203,204,6,20,21,22,164,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,138,165,139,166,115,133,135,134,126,159,117,119,120,121,122,123,118,125,167,168,169)\"', '192.168.2.138', '2020-04-14 11:42:20', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1374, 'admin', '新增部门', 127, 'cc.mrbird.febs.system.controller.DeptController.addDept()', ' dept: \"Dept(deptId=26, parentId=0, deptName=注册用户, orderNum=null, createTime=Tue Apr 28 16:29:58 CST 2020, modifyTime=null)\"', '192.168.2.138', '2020-04-28 16:29:58', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1375, 'teacher001', '批阅报告', 206, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '39.149.14.194', '2020-05-01 14:58:14', '中国|华中|河南省|郑州市|移动');
INSERT INTO `t_log` VALUES (1376, 'teacher001', '批阅报告', 70, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '39.149.14.194', '2020-05-01 20:42:06', '中国|华中|河南省|郑州市|移动');
INSERT INTO `t_log` VALUES (1377, 'teacher001', '批阅报告', 76, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '39.149.14.194', '2020-05-01 21:07:12', '中国|华中|河南省|郑州市|移动');
INSERT INTO `t_log` VALUES (1378, 'admin', '新增用户', 172, 'cc.mrbird.febs.system.controller.UserController.addUser()', ' user: \"User(userId=143, username=student005, password=5f8fd2bbaf470093deec926b12b29fc2, idNumber=201850915301, deptId=null, courseId=null, courseName=, faceUrl=null, email=, mobile=13271732091, status=1, createTime=Fri May 01 22:08:23 CST 2020, modifyTime=null, lastLoginTime=null, sex=2, avatar=default.jpg, theme=black, isTab=1, description=, faceToken=null, faceId=null, groupId=null, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=82, roleName=null)\" dept: \"[Ljava.lang.Integer;@532991d8\"', '39.149.14.194', '2020-05-01 22:08:24', '中国|华中|河南省|郑州市|移动');
INSERT INTO `t_log` VALUES (1379, 'admin', '删除用户', 313, 'cc.mrbird.febs.system.controller.UserController.deleteUsers()', ' userIds: \"143\"', '39.149.14.194', '2020-05-01 22:10:10', '中国|华中|河南省|郑州市|移动');
INSERT INTO `t_log` VALUES (1380, 'teacher001', '新增用户', 264, 'cc.mrbird.febs.system.controller.UserController.addUser()', ' user: \"User(userId=144, username=student005, password=5f8fd2bbaf470093deec926b12b29fc2, idNumber=, deptId=null, courseId=null, courseName=, faceUrl=null, email=, mobile=13271732091, status=1, createTime=Fri May 01 22:13:05 CST 2020, modifyTime=null, lastLoginTime=null, sex=1, avatar=default.jpg, theme=black, isTab=1, description=我是一名农学院的学生。, faceToken=null, faceId=null, groupId=null, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=82, roleName=null)\" dept: \"[Ljava.lang.Integer;@8823e62\"', '39.149.14.194', '2020-05-01 22:13:06', '中国|华中|河南省|郑州市|移动');
INSERT INTO `t_log` VALUES (1381, 'teacher001', '修改用户', 164, 'cc.mrbird.febs.system.controller.UserController.updateUser()', ' user: \"User(userId=144, username=null, password=null, idNumber=null, deptId=null, courseId=null, courseName=, faceUrl=null, email=2121129926@qq.com, mobile=13271732091, status=1, createTime=null, modifyTime=Fri May 01 22:14:01 CST 2020, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=我是一名农学院的学生。, faceToken=null, faceId=null, groupId=null, deptName=null, deptIds=null, createTimeFrom=null, createTimeTo=null, roleId=82, roleName=null)\" dept: \"[Ljava.lang.Integer;@28562c03\"', '39.149.14.194', '2020-05-01 22:14:02', '中国|华中|河南省|郑州市|移动');
INSERT INTO `t_log` VALUES (1382, 'admin', '删除用户', 172, 'cc.mrbird.febs.system.controller.UserController.deleteUsers()', ' userIds: \"144\"', '39.149.14.194', '2020-05-01 22:17:42', '中国|华中|河南省|郑州市|移动');
INSERT INTO `t_log` VALUES (1383, 'teacher001', '查看提交情况', 90, 'cc.mrbird.febs.teacher.controller.TeacherCommitController.commitList()', ' deptId: \"13\" userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=13, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '171.13.131.139', '2020-05-05 01:30:25', '中国|华中|河南省|安阳市|电信');
INSERT INTO `t_log` VALUES (1384, 'teacher001', '批阅报告', 105, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=null, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '39.149.14.51', '2020-05-05 15:19:26', '中国|华中|河南省|郑州市|移动');
INSERT INTO `t_log` VALUES (1385, 'teacher001', '批阅报告', 42, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=20, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=)\"', '39.149.14.51', '2020-05-05 15:21:59', '中国|华中|河南省|郑州市|移动');
INSERT INTO `t_log` VALUES (1386, 'teacher001', '批阅报告', 107, 'cc.mrbird.febs.teacher.controller.TeacherScoreController.scoreList()', ' userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=13, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=)\"', '39.149.14.51', '2020-05-05 15:22:03', '中国|华中|河南省|郑州市|移动');
INSERT INTO `t_log` VALUES (1387, 'teacher001', '查看提交情况', 88, 'cc.mrbird.febs.teacher.controller.TeacherCommitController.commitList()', ' deptId: \"13\" userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=13, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '39.149.14.51', '2020-05-05 15:22:32', '中国|华中|河南省|郑州市|移动');
INSERT INTO `t_log` VALUES (1388, 'teacher001', '查看提交情况', 62, 'cc.mrbird.febs.teacher.controller.TeacherCommitController.commitList()', ' deptId: \"13\" userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=13, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '39.149.14.51', '2020-05-05 15:40:40', '中国|华中|河南省|郑州市|移动');
INSERT INTO `t_log` VALUES (1389, 'teacher001', '查看提交情况', 38, 'cc.mrbird.febs.teacher.controller.TeacherCommitController.commitList()', ' deptId: \"20\" userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=20, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '39.149.14.51', '2020-05-05 15:40:59', '中国|华中|河南省|郑州市|移动');
INSERT INTO `t_log` VALUES (1390, 'teacher001', '查看提交情况', 104, 'cc.mrbird.febs.teacher.controller.TeacherCommitController.commitList()', ' deptId: \"13\" userCommit: \"UserCommit(commitId=null, stuId=null, stuName=null, idNumber=null, deptId=13, deptName=null, projectId=null, projectName=null, courseId=null, courseName=null, teacherId=null, teacherName=null, reportUrl=null, score=null, isCommit=null, createTime=null, createTimes=null)\" request: \"QueryRequest(pageSize=10, pageNum=1, field=null, order=null)\"', '39.149.14.51', '2020-05-05 15:41:11', '中国|华中|河南省|郑州市|移动');

-- ----------------------------
-- Table structure for t_login_log
-- ----------------------------
DROP TABLE IF EXISTS `t_login_log`;
CREATE TABLE `t_login_log`  (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `USERNAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `LOGIN_TIME` datetime(0) NOT NULL COMMENT '登录时间',
  `LOCATION` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录地点',
  `IP` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'IP地址',
  `SYSTEM` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作系统',
  `BROWSER` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '浏览器',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 720 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '登录日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_login_log
-- ----------------------------
INSERT INTO `t_login_log` VALUES (70, 'mrbird', '2020-03-18 17:45:50', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (71, 'mrbird', '2020-03-18 18:20:02', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (72, 'mrbird', '2020-03-19 10:17:16', NULL, '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (73, 'mrbird', '2020-03-19 10:22:38', NULL, '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (74, 'mrbird', '2020-03-19 11:02:56', NULL, '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (75, 'mrbird', '2020-03-19 15:07:52', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (76, 'mrbird', '2020-03-19 15:15:07', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (77, 'mrbird', '2020-03-19 15:21:14', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (78, 'mrbird', '2020-03-19 15:23:00', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (79, 'mrbird', '2020-03-19 15:27:02', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (80, 'mrbird', '2020-03-19 15:31:11', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (81, 'mrbird', '2020-03-19 15:34:04', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (82, 'mrbird', '2020-03-19 16:26:31', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (83, 'mrbird', '2020-03-19 16:31:01', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (84, 'mrbird', '2020-03-19 17:05:20', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (85, 'mrbird', '2020-03-19 18:24:47', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (86, 'mrbird', '2020-03-20 08:55:41', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (87, 'mrbird', '2020-03-20 09:03:22', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (88, 'mrbird', '2020-03-20 09:05:33', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (89, 'admin', '2020-03-20 09:52:58', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (90, 'admin', '2020-03-20 14:29:54', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (91, 'admin', '2020-03-20 15:04:17', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (92, 'student001', '2020-03-20 15:21:53', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (93, '123456', '2020-03-20 15:23:24', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (94, 'admin', '2020-03-20 15:25:37', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (95, 'admin', '2020-03-23 09:09:31', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (96, 'admin', '2020-03-23 09:25:31', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (97, 'admin', '2020-03-23 09:42:41', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (98, 'admin', '2020-03-23 09:43:00', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (99, 'admin', '2020-03-23 09:50:06', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (100, 'admin', '2020-03-23 10:26:58', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (101, 'admin', '2020-03-23 11:59:46', NULL, '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (102, 'admin', '2020-03-23 14:09:41', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (103, 'admin', '2020-03-23 14:47:01', NULL, '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (104, 'admin', '2020-03-23 16:00:50', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (105, 'admin', '2020-03-23 16:02:00', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (106, 'admin', '2020-03-23 16:14:22', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (107, 'admin', '2020-03-23 16:17:55', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (108, 'admin', '2020-03-23 16:18:48', NULL, '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (109, 'admin', '2020-03-23 16:55:42', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (110, 'admin', '2020-03-23 16:57:23', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (111, 'admin', '2020-03-23 17:01:40', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (112, 'admin', '2020-03-23 17:05:42', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (113, 'admin', '2020-03-23 17:08:42', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (114, 'admin', '2020-03-23 17:21:36', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (115, 'admin', '2020-03-23 17:23:34', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (116, 'admin', '2020-03-23 17:33:46', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (117, 'admin', '2020-03-23 17:46:42', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (118, 'admin', '2020-03-23 17:57:49', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (119, 'student001', '2020-03-23 18:29:50', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (120, 'admin', '2020-03-23 18:37:16', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (121, 'admin', '2020-03-24 09:05:21', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (122, 'admin', '2020-03-24 11:49:25', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (123, 'admin', '2020-03-24 11:57:12', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (124, 'admin', '2020-03-24 14:11:56', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (125, 'admin', '2020-03-24 15:36:05', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (126, 'admin', '2020-03-24 17:06:17', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (127, 'admin', '2020-03-24 18:29:31', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (128, 'admin', '2020-03-25 08:39:27', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (129, 'admin', '2020-03-25 10:15:31', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (130, 'admin', '2020-03-25 10:41:08', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (131, 'admin', '2020-03-25 10:47:55', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (132, 'admin', '2020-03-25 10:57:49', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (133, 'admin', '2020-03-25 11:20:34', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (134, 'admin', '2020-03-25 14:15:09', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (135, 'admin', '2020-03-25 14:22:10', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (136, 'admin', '2020-03-25 14:27:27', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (137, 'admin', '2020-03-25 14:39:25', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (138, 'admin', '2020-03-25 15:09:42', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (139, 'admin', '2020-03-25 16:54:54', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (140, 'admin', '2020-03-25 17:25:25', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (141, 'admin', '2020-03-25 17:33:53', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (142, 'admin', '2020-03-25 17:35:32', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (143, 'admin', '2020-03-25 17:35:56', '内网IP|0|0|内网IP|内网IP', '192.168.2.175', 'Windows ', 'Chrome 53');
INSERT INTO `t_login_log` VALUES (144, 'admin', '2020-03-25 17:37:15', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (145, 'admin', '2020-03-25 17:38:02', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (146, 'admin', '2020-03-25 18:27:54', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (147, 'admin', '2020-03-26 08:35:29', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (148, 'admin', '2020-03-26 09:26:47', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (149, 'admin', '2020-03-26 10:16:51', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (150, 'admin', '2020-03-26 10:19:25', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (151, 'admin', '2020-03-26 10:31:24', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (152, 'admin', '2020-03-26 10:43:39', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (153, 'admin', '2020-03-26 11:31:31', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (154, 'admin', '2020-03-26 13:59:55', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (155, 'admin', '2020-03-26 14:03:20', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (156, 'admin', '2020-03-26 15:43:13', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (157, 'admin', '2020-03-26 15:56:30', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (158, 'admin', '2020-03-26 16:29:08', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (159, 'admin', '2020-03-26 16:42:18', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (160, 'admin', '2020-03-26 16:43:40', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (161, 'admin', '2020-03-26 16:51:57', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (162, 'admin', '2020-03-26 17:39:56', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (163, 'admin', '2020-03-26 18:11:27', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (164, 'admin', '2020-03-26 18:12:02', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (165, 'admin', '2020-03-27 08:34:42', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (166, 'admin', '2020-03-27 08:49:09', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (167, 'admin', '2020-03-27 08:56:01', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (168, 'admin', '2020-03-27 09:08:34', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (169, 'admin', '2020-03-27 09:10:47', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (170, 'admin', '2020-03-27 09:12:57', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (171, 'admin', '2020-03-27 09:29:33', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (172, 'admin', '2020-03-27 09:41:21', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (173, 'teacher001', '2020-03-27 11:11:36', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (174, 'admin', '2020-03-27 11:13:39', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (175, 'teacher001', '2020-03-27 11:19:08', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (176, 'admin', '2020-03-27 11:29:04', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (177, 'teacher001', '2020-03-27 11:32:30', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (178, 'teacher001', '2020-03-27 14:04:44', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (179, 'admin', '2020-03-27 14:18:47', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (180, 'teacher001', '2020-03-27 14:19:41', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (181, 'admin', '2020-03-27 14:38:53', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (182, 'teacher001', '2020-03-27 14:39:05', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (183, 'teacher001', '2020-03-27 14:44:37', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (184, 'teacher001', '2020-03-27 14:47:19', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (185, 'teacher001', '2020-03-27 14:52:43', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (186, 'teacher001', '2020-03-27 14:53:43', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (187, 'admin', '2020-03-27 15:08:13', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (188, 'admin', '2020-03-27 16:06:34', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (189, 'admin', '2020-03-27 16:17:27', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (190, 'admin', '2020-03-27 16:37:41', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (191, 'admin', '2020-03-27 17:08:04', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (192, 'admin', '2020-03-27 17:19:09', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (193, 'student001', '2020-03-27 17:52:17', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (194, 'admin', '2020-03-27 17:52:33', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (195, 'admin', '2020-03-27 17:54:25', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (196, 'admin', '2020-03-27 17:55:35', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (197, 'admin', '2020-03-27 17:58:22', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (198, 'admin', '2020-03-27 18:01:43', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (199, 'admin', '2020-03-27 18:16:48', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (200, 'admin', '2020-03-27 18:18:44', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (201, 'admin', '2020-03-28 08:36:33', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (202, 'admin', '2020-03-28 09:09:49', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (203, 'admin', '2020-03-28 09:41:19', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (204, 'admin', '2020-03-28 11:20:47', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (205, 'admin', '2020-03-28 11:45:37', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (206, 'admin', '2020-03-28 11:56:58', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (207, 'admin', '2020-03-28 14:15:02', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (208, 'admin', '2020-03-28 14:52:39', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (209, 'admin', '2020-03-28 14:53:09', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (210, 'admin', '2020-03-28 14:56:39', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (211, 'admin', '2020-03-28 14:58:04', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (212, 'admin', '2020-03-28 15:00:27', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (213, 'admin', '2020-03-28 15:07:45', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (214, 'admin', '2020-03-28 15:12:11', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (215, 'admin', '2020-03-28 15:19:19', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (216, 'admin', '2020-03-28 15:22:50', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (217, 'admin', '2020-03-28 15:32:03', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (218, 'admin', '2020-03-28 15:37:43', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (219, 'admin', '2020-03-28 15:42:46', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (220, 'admin', '2020-03-28 15:44:32', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (221, 'admin', '2020-03-28 15:45:40', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (222, 'teacher001', '2020-03-28 16:02:49', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (223, 'admin', '2020-03-28 16:37:00', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (224, 'admin', '2020-03-30 08:55:39', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (225, 'admin', '2020-03-30 09:09:20', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (226, 'teacher001', '2020-03-30 10:12:14', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (227, 'admin', '2020-03-30 10:54:18', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (228, 'admin', '2020-03-30 14:49:46', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (229, 'admin', '2020-03-30 15:07:16', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (230, 'admin', '2020-03-30 15:33:54', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (231, 'admin', '2020-03-30 15:57:06', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (232, 'admin', '2020-03-30 16:18:19', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (233, 'admin', '2020-03-30 17:36:36', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (234, 'admin', '2020-03-30 17:47:26', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (235, 'admin', '2020-03-30 17:56:15', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (236, 'admin', '2020-03-31 08:35:19', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (237, 'admin', '2020-03-31 08:46:20', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (238, 'admin', '2020-03-31 09:18:46', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (239, 'admin', '2020-03-31 10:27:17', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (240, 'admin', '2020-03-31 11:35:15', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (241, 'admin', '2020-03-31 11:43:00', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (242, 'admin', '2020-03-31 11:49:37', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (243, 'admin', '2020-03-31 11:57:39', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (244, 'admin', '2020-03-31 14:04:04', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (245, 'admin', '2020-03-31 14:05:33', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (246, 'admin', '2020-03-31 14:13:09', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (247, 'admin', '2020-03-31 14:35:39', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (248, 'admin', '2020-03-31 15:06:53', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (249, 'admin', '2020-03-31 15:13:12', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (250, 'admin', '2020-03-31 15:21:49', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (251, 'admin', '2020-03-31 15:26:00', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (252, 'admin', '2020-03-31 15:26:34', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (253, 'admin', '2020-03-31 15:30:38', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (254, 'admin', '2020-03-31 15:42:08', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (255, 'admin', '2020-03-31 15:46:09', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (256, 'admin', '2020-03-31 16:01:28', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (257, 'admin', '2020-03-31 16:09:56', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (258, 'admin', '2020-03-31 16:22:15', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (259, 'admin', '2020-03-31 16:53:40', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (260, 'admin', '2020-03-31 17:15:33', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (261, 'admin', '2020-03-31 17:17:45', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (262, 'admin', '2020-03-31 17:53:41', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (263, 'admin', '2020-03-31 18:07:10', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (264, 'admin', '2020-03-31 18:20:34', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (265, 'admin', '2020-03-31 18:27:19', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (266, 'admin', '2020-04-01 08:34:55', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (267, 'admin', '2020-04-01 08:35:15', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (268, 'admin', '2020-04-01 08:54:08', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (269, 'admin', '2020-04-01 09:15:37', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (270, 'admin', '2020-04-01 09:18:30', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (271, 'admin', '2020-04-01 09:23:19', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (272, 'admin', '2020-04-01 09:24:55', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (273, 'admin', '2020-04-01 09:36:32', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (274, 'admin', '2020-04-01 10:22:50', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (275, 'admin', '2020-04-01 11:44:09', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (276, 'admin', '2020-04-01 14:04:59', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (277, 'admin', '2020-04-01 14:09:35', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (278, 'admin', '2020-04-01 14:21:49', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (279, 'admin', '2020-04-01 14:22:34', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (280, 'admin', '2020-04-01 14:26:07', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (281, 'admin', '2020-04-01 14:26:33', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (282, 'teacher001', '2020-04-01 14:44:41', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (283, 'admin', '2020-04-01 14:47:56', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (284, 'admin', '2020-04-01 14:54:39', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (285, 'admin', '2020-04-01 15:04:13', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (286, 'teacher001', '2020-04-01 15:05:09', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (287, 'admin', '2020-04-01 15:35:38', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (288, 'admin', '2020-04-01 15:39:35', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (289, 'admin', '2020-04-01 15:41:43', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (290, 'admin', '2020-04-01 15:49:02', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (291, 'admin', '2020-04-01 15:56:04', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (292, 'admin', '2020-04-01 16:01:52', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (293, 'admin', '2020-04-01 16:05:24', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (294, 'teacher001', '2020-04-01 16:06:26', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (295, 'admin', '2020-04-01 16:12:25', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (296, 'admin', '2020-04-01 16:15:07', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 70');
INSERT INTO `t_login_log` VALUES (297, 'teacher001', '2020-04-01 16:15:24', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (298, 'admin', '2020-04-01 16:42:52', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (299, 'admin', '2020-04-01 16:45:49', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (300, 'admin', '2020-04-01 17:05:24', '内网IP|0|0|内网IP|内网IP', '192.168.2.146', 'Linux', 'Chrome 66');
INSERT INTO `t_login_log` VALUES (301, 'admin', '2020-04-01 17:07:26', '内网IP|0|0|内网IP|内网IP', '192.168.2.130', 'Linux', 'Chrome 67');
INSERT INTO `t_login_log` VALUES (302, 'admin', '2020-04-01 17:07:52', '内网IP|0|0|内网IP|内网IP', '192.168.2.113', 'Mac OS X) AppleW', '');
INSERT INTO `t_login_log` VALUES (303, 'teacher001', '2020-04-01 17:09:30', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (304, 'admin', '2020-04-01 17:19:14', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (305, 'admin', '2020-04-01 17:20:32', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (306, 'admin', '2020-04-01 17:34:55', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (307, 'admin', '2020-04-01 17:43:28', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (308, 'teacher001', '2020-04-01 17:50:18', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (309, 'admin', '2020-04-01 17:51:29', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 70');
INSERT INTO `t_login_log` VALUES (310, 'teacher001', '2020-04-01 18:05:26', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (311, 'admin', '2020-04-01 18:05:37', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (312, 'admin', '2020-04-01 18:37:04', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (313, 'admin', '2020-04-02 08:34:23', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (314, 'admin', '2020-04-02 08:42:43', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (315, 'admin', '2020-04-02 08:49:02', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (316, 'admin', '2020-04-02 08:50:59', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (317, 'admin', '2020-04-02 08:51:09', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (318, 'admin', '2020-04-02 08:52:32', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (319, 'admin', '2020-04-02 09:04:43', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (320, 'teacher001', '2020-04-02 09:07:27', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (321, 'admin', '2020-04-02 09:09:08', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (322, 'admin', '2020-04-02 09:13:31', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'IE 11');
INSERT INTO `t_login_log` VALUES (323, 'teacher001', '2020-04-02 09:28:44', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (324, 'teacher001', '2020-04-02 09:29:13', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (325, 'teacher001', '2020-04-02 09:30:14', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (326, 'teacher001', '2020-04-02 09:32:35', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (327, 'admin', '2020-04-02 09:34:36', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (328, 'teacher001', '2020-04-02 12:00:24', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (329, 'teacher001', '2020-04-02 14:11:33', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (330, 'admin', '2020-04-02 14:22:48', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (331, 'teacher001', '2020-04-02 15:46:20', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (332, 'teacher001', '2020-04-02 16:01:14', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (333, 'admin', '2020-04-02 16:03:51', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (334, 'teacher001', '2020-04-02 16:19:01', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (335, 'admin', '2020-04-02 16:54:03', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (336, 'teacher001', '2020-04-02 16:57:26', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (337, 'admin', '2020-04-02 17:03:34', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (338, 'admin', '2020-04-02 17:17:26', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (339, 'teacher001', '2020-04-02 17:27:12', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (340, 'teacher001', '2020-04-02 17:35:36', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (341, 'teacher001', '2020-04-02 17:37:41', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (342, 'teacher001', '2020-04-02 17:39:59', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (343, 'teacher001', '2020-04-02 17:42:57', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (344, 'teacher001', '2020-04-02 17:48:54', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (345, 'teacher001', '2020-04-02 18:03:55', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (346, 'teacher001', '2020-04-02 18:14:25', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (347, 'teacher001', '2020-04-02 18:16:34', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (348, 'teacher001', '2020-04-02 18:27:33', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (349, 'teacher001', '2020-04-02 18:33:28', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (350, 'admin', '2020-04-03 08:38:01', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (351, 'teacher001', '2020-04-03 08:40:07', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (352, 'teacher001', '2020-04-03 08:58:57', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (353, 'teacher001', '2020-04-03 09:17:57', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (354, 'teacher001', '2020-04-03 09:30:31', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (355, 'admin', '2020-04-03 09:31:11', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (356, 'teacher001', '2020-04-03 09:35:12', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (357, 'admin', '2020-04-03 09:37:28', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (358, 'teacher001', '2020-04-03 09:40:27', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (359, 'teacher001', '2020-04-03 09:43:23', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (360, 'teacher001', '2020-04-03 09:46:55', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (361, 'teacher001', '2020-04-03 09:53:33', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (362, 'teacher001', '2020-04-03 10:02:08', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (363, 'teacher001', '2020-04-03 10:09:07', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (364, 'teacher001', '2020-04-03 10:11:56', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (365, 'teacher001', '2020-04-03 10:15:01', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (366, 'teacher001', '2020-04-03 10:18:20', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (367, 'teacher001', '2020-04-03 10:48:38', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (368, 'teacher001', '2020-04-03 10:49:44', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (369, 'admin', '2020-04-03 10:54:32', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (370, 'teacher001', '2020-04-03 10:54:56', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (371, 'teacher001', '2020-04-03 11:23:40', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (372, 'teacher001', '2020-04-03 11:27:26', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (373, 'teacher001', '2020-04-03 11:29:46', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (374, 'teacher001', '2020-04-03 11:31:54', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (375, 'teacher001', '2020-04-03 11:33:13', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (376, 'teacher001', '2020-04-03 11:40:32', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (377, 'admin', '2020-04-03 11:46:35', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (378, 'teacher001', '2020-04-03 11:47:22', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (379, 'teacher001', '2020-04-03 11:51:22', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (380, 'teacher001', '2020-04-03 11:53:43', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (381, 'teacher001', '2020-04-03 11:54:54', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (382, 'admin', '2020-04-03 11:58:20', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (383, 'teacher001', '2020-04-03 12:01:44', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (384, 'teacher001', '2020-04-03 14:20:07', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (385, 'teacher001', '2020-04-03 14:57:47', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (386, 'teacher001', '2020-04-03 15:00:42', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (387, 'admin', '2020-04-03 15:12:01', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (388, 'teacher001', '2020-04-03 15:27:10', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (389, 'admin', '2020-04-03 15:28:25', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (390, 'admin', '2020-04-03 15:35:26', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (391, 'admin', '2020-04-03 15:38:15', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (392, 'admin', '2020-04-03 15:45:55', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (393, 'admin', '2020-04-03 16:03:53', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (394, 'admin', '2020-04-03 16:09:34', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (395, 'admin', '2020-04-03 16:20:22', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (396, 'admin', '2020-04-03 16:29:58', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (397, 'admin', '2020-04-03 16:39:49', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (398, 'admin', '2020-04-03 17:15:13', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (399, 'admin', '2020-04-03 17:31:43', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (400, 'admin', '2020-04-03 17:54:47', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (401, 'admin', '2020-04-03 17:57:08', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (402, 'admin', '2020-04-03 18:08:39', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (403, 'teacher001', '2020-04-06 08:42:00', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (404, 'admin', '2020-04-06 08:44:07', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (405, 'admin', '2020-04-06 09:03:46', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (406, 'admin', '2020-04-06 09:05:24', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (407, 'admin', '2020-04-06 09:27:17', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (408, 'admin', '2020-04-06 09:28:21', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (409, 'teacher001', '2020-04-06 09:36:14', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (410, 'admin', '2020-04-06 09:38:07', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (411, 'teacher001', '2020-04-06 09:38:59', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (412, 'admin', '2020-04-06 09:41:11', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (413, 'admin', '2020-04-06 09:44:29', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (414, 'admin', '2020-04-06 09:46:15', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (415, 'teacher001', '2020-04-06 09:50:55', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (416, 'teacher001', '2020-04-06 09:53:16', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (417, 'student001', '2020-04-06 10:00:51', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (418, 'admin', '2020-04-06 10:01:44', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (419, 'teacher001', '2020-04-06 10:06:35', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (420, 'teacher001', '2020-04-06 10:07:40', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (421, 'gzc', '2020-04-06 10:10:36', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (422, 'admin', '2020-04-06 10:10:51', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (423, 'admin', '2020-04-06 10:11:39', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (424, 'admin', '2020-04-06 10:14:06', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (425, 'gzca', '2020-04-06 10:14:20', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (426, 'teacher001', '2020-04-06 10:17:11', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (427, 'admin', '2020-04-06 10:20:18', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (428, 'admin', '2020-04-06 10:30:51', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (429, 'admin', '2020-04-06 11:28:57', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (430, 'admin', '2020-04-06 11:52:20', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (431, 'admin', '2020-04-06 11:58:37', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (432, 'gzca', '2020-04-06 14:28:56', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (433, 'admin', '2020-04-06 14:34:08', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (434, 'admin', '2020-04-06 14:39:34', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (435, 'admin', '2020-04-06 14:46:44', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (436, 'admin', '2020-04-06 14:50:06', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (437, 'admin', '2020-04-06 14:50:36', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (438, 'admin', '2020-04-06 15:04:47', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (439, 'gzca', '2020-04-06 15:05:28', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (440, 'teacher001', '2020-04-06 15:07:27', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (441, 'admin', '2020-04-06 15:08:05', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (442, 'gzca', '2020-04-06 15:08:31', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (443, 'admin', '2020-04-06 15:20:27', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (444, 'gzca', '2020-04-06 15:23:03', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (445, 'admin', '2020-04-06 16:09:32', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (446, 'student001', '2020-04-06 16:10:10', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (447, 'student001', '2020-04-06 16:12:37', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (448, 'student001', '2020-04-06 16:31:20', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (449, 'student001', '2020-04-06 16:33:38', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (450, 'admin', '2020-04-06 16:35:44', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (451, 'student001', '2020-04-06 16:37:15', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (452, 'teacher001', '2020-04-06 16:37:40', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (453, 'student001', '2020-04-06 16:44:08', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (454, 'gzca', '2020-04-06 16:51:15', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (455, 'gzca', '2020-04-06 16:54:54', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (456, 'student001', '2020-04-06 16:55:55', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (457, 'teacher001', '2020-04-06 17:05:52', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (458, 'admin', '2020-04-06 17:10:17', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (459, 'student001', '2020-04-06 17:10:40', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (460, 'admin', '2020-04-06 17:20:09', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (461, 'admin', '2020-04-06 17:20:32', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (462, 'admin', '2020-04-06 17:21:27', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (463, 'admin', '2020-04-06 17:25:30', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (464, 'admin', '2020-04-06 17:30:05', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (465, 'student001', '2020-04-06 17:35:50', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (466, '巴芝璧', '2020-04-06 17:43:09', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (467, 'teacher001', '2020-04-06 17:43:48', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (468, '巴芝璧', '2020-04-06 17:45:05', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (469, 'admin', '2020-04-06 17:56:21', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (470, '巴芝璧', '2020-04-06 18:07:43', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (471, 'student001', '2020-04-06 18:08:38', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (472, 'admin', '2020-04-06 18:12:53', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (473, 'teacher001', '2020-04-06 18:18:17', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (474, 'teacher001', '2020-04-06 18:19:28', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (475, 'teacher001', '2020-04-06 18:21:03', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (476, 'teacher001', '2020-04-06 18:23:55', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (477, 'student001', '2020-04-07 08:34:38', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (478, 'teacher001', '2020-04-07 08:41:39', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (479, 'teacher001', '2020-04-07 09:56:19', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (480, 'admin', '2020-04-07 09:57:26', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (481, 'teacher001', '2020-04-07 10:02:43', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (482, 'student001', '2020-04-07 10:20:04', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (483, 'teacher001', '2020-04-07 10:28:04', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (484, 'student001', '2020-04-07 10:32:25', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (485, 'student001', '2020-04-07 10:47:24', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (486, 'student001', '2020-04-07 10:50:41', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (487, 'student001', '2020-04-07 10:52:13', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (488, 'student001', '2020-04-07 10:54:17', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (489, 'student001', '2020-04-07 11:22:24', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (490, 'student001', '2020-04-07 11:24:05', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (491, 'student001', '2020-04-07 11:27:13', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (492, 'student001', '2020-04-07 11:35:53', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (493, 'student001', '2020-04-07 11:36:56', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (494, 'student001', '2020-04-07 11:42:31', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (495, 'student001', '2020-04-07 11:48:12', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (496, 'student001', '2020-04-07 12:06:29', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (497, 'student001', '2020-04-07 14:07:52', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (498, 'student001', '2020-04-07 14:44:03', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (499, 'student001', '2020-04-07 14:46:11', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (500, 'student001', '2020-04-07 14:51:10', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (501, 'student001', '2020-04-07 15:38:31', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (502, 'student001', '2020-04-07 15:57:20', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (503, 'student001', '2020-04-07 15:59:25', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (504, 'student001', '2020-04-07 16:11:34', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (505, 'admin', '2020-04-07 16:57:38', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (506, 'admin', '2020-04-07 17:11:08', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (507, '巴芝璧', '2020-04-07 17:12:23', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (508, 'admin', '2020-04-07 17:13:22', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (509, 'teacher001', '2020-04-07 17:13:25', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (510, 'admin', '2020-04-07 17:14:36', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (511, 'admin', '2020-04-07 17:22:03', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (512, 'admin', '2020-04-07 17:31:09', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (513, 'admin', '2020-04-07 17:41:29', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (514, 'admin', '2020-04-07 17:42:42', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (515, 'admin', '2020-04-07 17:44:14', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (516, 'admin', '2020-04-07 17:55:39', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (517, 'student001', '2020-04-08 08:31:58', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (518, 'admin', '2020-04-08 08:36:52', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (519, 'teacher001', '2020-04-08 08:37:33', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (520, 'admin', '2020-04-08 08:48:07', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (521, 'admin', '2020-04-08 08:58:02', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (522, 'student001', '2020-04-08 09:01:08', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (523, 'admin', '2020-04-08 09:01:09', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (524, '巴芝璧', '2020-04-08 09:04:12', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (525, 'student001', '2020-04-08 09:41:17', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (526, 'student001', '2020-04-08 09:58:33', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (527, 'admin', '2020-04-08 10:24:54', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (528, 'teacher001', '2020-04-08 10:31:08', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (529, 'student001', '2020-04-08 10:45:28', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (530, 'teacher001', '2020-04-08 10:47:50', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (531, 'student001', '2020-04-08 10:48:34', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (532, 'teacher001', '2020-04-08 10:57:42', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (533, 'teacher001', '2020-04-08 11:01:34', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (534, 'teacher001', '2020-04-08 11:02:59', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (535, 'teacher001', '2020-04-08 11:22:12', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (536, 'admin', '2020-04-08 11:32:16', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (537, 'student001', '2020-04-08 11:34:16', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (538, 'student001', '2020-04-08 14:17:19', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (539, 'teacher001', '2020-04-08 14:30:41', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (540, 'admin', '2020-04-08 14:39:06', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (541, 'student001', '2020-04-08 14:51:16', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (542, 'student001', '2020-04-08 15:02:37', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (543, 'teacher001', '2020-04-08 15:15:40', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (544, 'teacher001', '2020-04-08 15:39:57', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (545, 'student001', '2020-04-08 16:37:24', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (546, 'student001', '2020-04-08 17:10:53', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (547, 'student001', '2020-04-08 17:15:35', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (548, 'student001', '2020-04-08 17:24:57', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (549, 'teacher001', '2020-04-08 18:02:19', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (550, 'teacher001', '2020-04-08 18:11:23', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (551, 'teacher001', '2020-04-09 08:46:25', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (552, 'student001', '2020-04-09 08:54:02', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (553, 'student001', '2020-04-09 09:16:52', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (554, 'admin', '2020-04-09 09:55:53', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (555, 'student001', '2020-04-09 09:56:41', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (556, 'student001', '2020-04-09 10:06:57', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (557, 'teacher001', '2020-04-09 10:27:13', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (558, 'teacher001', '2020-04-09 12:03:16', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (559, 'student001', '2020-04-09 14:02:48', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (560, 'student001', '2020-04-09 14:28:34', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (561, 'teacher001', '2020-04-09 14:52:11', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (562, 'teacher001', '2020-04-09 16:13:10', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (563, 'admin', '2020-04-09 16:17:44', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (564, 'teacher001', '2020-04-09 16:19:48', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (565, 'student001', '2020-04-09 16:57:37', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (566, 'admin', '2020-04-09 17:20:07', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (567, 'admin', '2020-04-09 17:21:42', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (568, 'teacher001', '2020-04-09 17:28:29', '内网IP|0|0|内网IP|内网IP', '192.168.2.111', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (569, 'admin', '2020-04-09 17:29:14', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (570, 'admin', '2020-04-09 17:31:35', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (571, 'student001', '2020-04-09 17:47:13', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (572, 'admin', '2020-04-10 08:58:39', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (573, 'admin', '2020-04-10 09:13:20', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (574, 'admin', '2020-04-10 09:35:19', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (575, 'admin', '2020-04-10 09:36:05', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', '', '');
INSERT INTO `t_login_log` VALUES (576, 'admin', '2020-04-10 09:39:47', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', '', '');
INSERT INTO `t_login_log` VALUES (577, 'teacher001', '2020-04-10 10:23:20', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (578, 'teacher001', '2020-04-10 14:31:15', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (579, 'teacher001', '2020-04-10 14:37:58', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (580, 'teacher001', '2020-04-10 14:41:29', '内网IP|0|0|内网IP|内网IP', '192.168.2.169', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (581, 'admin', '2020-04-10 16:00:01', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (582, 'student001', '2020-04-10 18:03:14', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', '', '');
INSERT INTO `t_login_log` VALUES (583, 'teacher001', '2020-04-10 18:06:11', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (584, 'student001', '2020-04-13 18:00:56', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (585, 'admin', '2020-04-14 10:41:37', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (586, 'student001', '2020-04-14 10:42:20', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (587, 'admin', '2020-04-14 10:56:52', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (588, 'student001', '2020-04-14 10:57:12', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (589, 'student001', '2020-04-14 10:57:35', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', '', '');
INSERT INTO `t_login_log` VALUES (590, 'student001', '2020-04-14 11:16:40', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', '', '');
INSERT INTO `t_login_log` VALUES (591, 'admin', '2020-04-14 11:37:08', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (592, 'student001', '2020-04-14 11:42:49', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (593, 'admin', '2020-04-14 11:47:02', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (594, 'student001', '2020-04-14 11:47:24', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (595, 'student001', '2020-04-14 14:42:52', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (596, 'student001', '2020-04-15 09:33:09', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (597, 'student001', '2020-04-15 10:11:44', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (598, 'student001', '2020-04-15 15:14:30', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (599, 'student001', '2020-04-15 16:28:42', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (600, 'student001', '2020-04-15 16:44:31', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (601, 'student001', '2020-04-15 17:42:34', '内网IP|0|0|内网IP|内网IP', '192.168.2.146', 'Linux', 'Chrome 66');
INSERT INTO `t_login_log` VALUES (602, 'student001', '2020-04-15 18:13:16', '内网IP|0|0|内网IP|内网IP', '192.168.2.146', 'Linux', 'Chrome 57');
INSERT INTO `t_login_log` VALUES (603, 'student001', '2020-04-16 16:37:12', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (604, 'student001', '2020-04-16 16:55:21', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (605, 'student001', '2020-04-18 16:05:26', '内网IP|0|0|内网IP|内网IP', '192.168.20.1', 'Windows 10', 'Chrome 79');
INSERT INTO `t_login_log` VALUES (606, 'student001', '2020-04-18 21:10:21', '内网IP|0|0|内网IP|内网IP', '192.168.20.1', 'Windows 10', 'Chrome 79');
INSERT INTO `t_login_log` VALUES (607, 'student001', '2020-04-18 21:19:31', '内网IP|0|0|内网IP|内网IP', '192.168.20.1', 'Windows 10', 'Chrome 79');
INSERT INTO `t_login_log` VALUES (608, 'student001', '2020-04-21 16:52:45', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (609, 'admin', '2020-04-21 18:29:31', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (610, 'student001', '2020-04-21 18:29:41', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 80');
INSERT INTO `t_login_log` VALUES (611, 'student001', '2020-04-21 21:17:06', '内网IP|0|0|内网IP|内网IP', '192.168.20.1', 'Windows 10', 'Chrome 79');
INSERT INTO `t_login_log` VALUES (612, 'student001', '2020-04-21 22:08:40', '内网IP|0|0|内网IP|内网IP', '127.0.0.1', 'Linux', 'Chrome 62');
INSERT INTO `t_login_log` VALUES (613, 'student003', '2020-04-22 10:59:22', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (614, 'student001', '2020-04-22 12:02:06', '中国|华中|河南省|郑州市|联通', '221.15.185.162', 'Linux', 'Chrome 62');
INSERT INTO `t_login_log` VALUES (615, 'student001', '2020-04-22 13:08:26', '中国|华中|河南省|郑州市|联通', '221.15.185.162', 'Linux', 'Chrome 62');
INSERT INTO `t_login_log` VALUES (616, 'admin', '2020-04-22 13:12:21', '中国|华中|河南省|郑州市|移动', '39.149.14.42', 'Linux', 'Chrome 70');
INSERT INTO `t_login_log` VALUES (617, 'student001', '2020-04-22 13:17:16', '中国|华中|河南省|郑州市|移动', '39.149.14.42', 'Linux', 'Chrome 70');
INSERT INTO `t_login_log` VALUES (618, 'student001', '2020-04-22 13:22:55', '中国|华中|河南省|郑州市|移动', '39.149.14.42', 'Linux', 'Chrome 66');
INSERT INTO `t_login_log` VALUES (619, 'student001', '2020-04-22 14:37:40', '中国|华中|河南省|郑州市|移动', '39.149.14.42', 'Linux', 'Chrome 66');
INSERT INTO `t_login_log` VALUES (620, 'student001', '2020-04-22 15:37:32', '中国|华中|河南省|郑州市|移动', '39.149.14.42', 'Linux', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (621, 'admin', '2020-04-22 15:43:18', '中国|华中|河南省|郑州市|移动', '39.149.14.42', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (622, 'student001', '2020-04-22 16:09:42', '中国|华中|河南省|郑州市|移动', '39.149.14.42', 'Windows ', 'Firefox 75');
INSERT INTO `t_login_log` VALUES (623, 'admin', '2020-04-22 16:21:09', '中国|华中|河南省|郑州市|移动', '39.149.14.42', 'Windows ', 'Firefox 75');
INSERT INTO `t_login_log` VALUES (624, 'student001', '2020-04-22 16:23:56', '中国|华中|河南省|郑州市|联通', '221.15.185.162', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (625, 'student001', '2020-04-22 16:25:01', '中国|华中|河南省|郑州市|移动', '39.149.14.42', 'Windows ', 'Firefox 75');
INSERT INTO `t_login_log` VALUES (626, 'admin', '2020-04-22 16:31:21', '中国|华中|河南省|郑州市|移动', '39.149.14.42', 'Windows ', 'Firefox 75');
INSERT INTO `t_login_log` VALUES (627, 'student001', '2020-04-22 20:56:18', '内网IP|0|0|内网IP|内网IP', '192.168.20.1', 'Windows 10', 'Chrome 79');
INSERT INTO `t_login_log` VALUES (628, 'student001', '2020-04-22 21:27:59', '内网IP|0|0|内网IP|内网IP', '192.168.20.1', 'Windows 10', 'Chrome 79');
INSERT INTO `t_login_log` VALUES (629, 'student001', '2020-04-22 21:37:02', '内网IP|0|0|内网IP|内网IP', '192.168.20.1', 'Windows 10', 'Chrome 79');
INSERT INTO `t_login_log` VALUES (630, 'student001', '2020-04-22 22:01:59', '中国|0|0|0|移动', '223.104.111.53', 'Linux', 'Chrome 62');
INSERT INTO `t_login_log` VALUES (631, 'student001', '2020-04-22 22:03:43', '内网IP|0|0|内网IP|内网IP', '127.0.0.1', 'Linux', 'Chrome 62');
INSERT INTO `t_login_log` VALUES (632, 'student001', '2020-04-22 22:06:57', '中国|华中|河南省|郑州市|移动', '39.149.14.42', 'Linux', 'Chrome 70');
INSERT INTO `t_login_log` VALUES (633, 'admin', '2020-04-22 22:56:20', '中国|0|0|0|移动', '39.144.22.123', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (634, 'admin', '2020-04-24 18:01:25', '中国|华中|河南省|郑州市|联通', '61.52.41.232', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (635, 'GuiZicheng', '2020-04-25 17:15:56', '中国|华中|河南省|郑州市|联通', '61.52.41.25', 'Windows 10', 'Chrome 53');
INSERT INTO `t_login_log` VALUES (636, 'student001', '2020-04-26 11:08:02', '中国|华中|河南省|郑州市|移动', '39.149.14.42', 'Windows ', 'Firefox 75');
INSERT INTO `t_login_log` VALUES (637, 'student001', '2020-04-26 11:10:37', '中国|华中|河南省|郑州市|移动', '39.149.14.42', 'Linux', 'Chrome 66');
INSERT INTO `t_login_log` VALUES (638, 'admin', '2020-04-26 13:32:07', '中国|华中|河南省|郑州市|移动', '39.149.14.83', 'Windows 10', 'Chrome 79');
INSERT INTO `t_login_log` VALUES (639, 'admin', '2020-04-26 13:40:10', '中国|华中|河南省|郑州市|移动', '39.149.14.83', 'Windows 10', 'Chrome 79');
INSERT INTO `t_login_log` VALUES (640, '郑爽', '2020-04-26 16:18:32', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Linux', 'Chrome 66');
INSERT INTO `t_login_log` VALUES (641, '郑爽', '2020-04-26 16:20:56', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Firefox 75');
INSERT INTO `t_login_log` VALUES (642, '郑爽', '2020-04-26 18:01:11', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Firefox 75');
INSERT INTO `t_login_log` VALUES (643, '郑爽', '2020-04-26 20:10:39', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Firefox 75');
INSERT INTO `t_login_log` VALUES (644, 'admin', '2020-04-26 20:14:52', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (645, 'admin', '2020-04-26 23:34:41', '中国|华中|河南省|郑州市|移动', '39.149.14.83', 'Windows 10', 'Chrome 79');
INSERT INTO `t_login_log` VALUES (646, 'student001', '2020-04-27 12:33:03', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Firefox 75');
INSERT INTO `t_login_log` VALUES (647, 'admin', '2020-04-27 22:16:43', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (648, 'student001', '2020-04-27 22:20:06', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Firefox 75');
INSERT INTO `t_login_log` VALUES (649, 'student001', '2020-04-28 11:28:40', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Firefox 75');
INSERT INTO `t_login_log` VALUES (650, 'student001', '2020-04-28 11:30:03', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (651, 'admin', '2020-04-28 12:10:07', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 53');
INSERT INTO `t_login_log` VALUES (652, 'admin', '2020-04-28 12:10:47', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (653, 'student001', '2020-04-28 12:14:25', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 53');
INSERT INTO `t_login_log` VALUES (654, 'student001', '2020-04-28 14:42:37', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (655, 'student001', '2020-04-28 14:51:19', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (656, 'student004', '2020-04-28 15:03:07', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (657, 'student004', '2020-04-28 15:20:16', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (658, 'student004', '2020-04-28 15:23:13', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (659, 'student004', '2020-04-28 15:25:32', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (660, 'student004', '2020-04-28 16:18:40', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (661, 'admin', '2020-04-28 16:26:27', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (662, 'student004', '2020-04-28 16:37:03', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (663, 'student004', '2020-04-28 16:37:32', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (664, 'student004', '2020-04-28 16:38:58', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (665, 'student004', '2020-04-28 16:40:07', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (666, 'student004', '2020-04-28 17:17:40', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (667, 'student004', '2020-04-28 17:21:11', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (668, 'student004', '2020-04-28 17:23:20', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (669, 'student004', '2020-04-29 09:34:15', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (670, 'student001', '2020-04-29 11:06:49', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 53');
INSERT INTO `t_login_log` VALUES (671, 'student004', '2020-04-29 17:44:24', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (672, 'admin', '2020-04-29 17:46:04', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (673, 'student004', '2020-04-29 17:46:14', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (674, 'admin', '2020-04-29 17:51:19', '内网IP|0|0|内网IP|内网IP', '192.168.2.138', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (675, 'teacher001', '2020-05-01 14:57:02', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 53');
INSERT INTO `t_login_log` VALUES (676, 'admin', '2020-05-01 15:25:13', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 53');
INSERT INTO `t_login_log` VALUES (677, 'student001', '2020-05-01 17:35:42', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Linux', 'Chrome 70');
INSERT INTO `t_login_log` VALUES (678, 'teacher001', '2020-05-01 17:39:56', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Linux', 'Chrome 70');
INSERT INTO `t_login_log` VALUES (679, 'teacher001', '2020-05-01 20:19:31', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 53');
INSERT INTO `t_login_log` VALUES (680, 'student001', '2020-05-01 21:36:01', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (681, '郑爽', '2020-05-01 21:38:15', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (682, 'student001', '2020-05-01 21:49:54', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Linux', 'Chrome 70');
INSERT INTO `t_login_log` VALUES (683, 'admin', '2020-05-01 21:51:03', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (684, 'admin', '2020-05-01 22:05:30', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (685, 'teacher001', '2020-05-01 22:19:53', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (686, 'admin', '2020-05-01 22:20:57', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (687, 'teacher001', '2020-05-01 22:23:54', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (688, 'admin', '2020-05-01 22:29:28', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (689, 'student001', '2020-05-02 13:43:40', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Linux', 'Chrome 70');
INSERT INTO `t_login_log` VALUES (690, 'teacher001', '2020-05-02 13:57:04', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Linux', 'Chrome 70');
INSERT INTO `t_login_log` VALUES (691, 'admin', '2020-05-02 21:36:37', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Windows ', 'Chrome 53');
INSERT INTO `t_login_log` VALUES (692, 'student001', '2020-05-02 21:52:01', '中国|华中|河南省|郑州市|移动', '39.149.14.194', 'Linux', 'Chrome 70');
INSERT INTO `t_login_log` VALUES (693, 'admin', '2020-05-03 21:35:03', '中国|华北|北京市|北京市|电信', '106.39.149.72', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (694, 'student001', '2020-05-03 21:36:30', '中国|华北|北京市|北京市|电信', '106.39.149.72', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (695, 'teacher001', '2020-05-03 21:37:08', '中国|华北|北京市|北京市|电信', '106.39.149.72', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (696, 'student001', '2020-05-03 23:00:49', '中国|华中|河南省|郑州市|移动', '39.149.14.51', 'Linux', 'Chrome 70');
INSERT INTO `t_login_log` VALUES (697, 'student001', '2020-05-04 18:21:41', '中国|华中|河南省|郑州市|移动', '39.149.14.51', 'Linux', 'Chrome 70');
INSERT INTO `t_login_log` VALUES (698, 'student001', '2020-05-04 20:16:29', '中国|华中|河南省|郑州市|移动', '39.149.14.51', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (699, 'student006', '2020-05-05 00:04:11', '中国|华中|河南省|郑州市|移动', '39.149.14.51', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (700, 'student006', '2020-05-05 00:06:37', '中国|华中|河南省|郑州市|移动', '39.149.14.51', 'Linux', 'Chrome 70');
INSERT INTO `t_login_log` VALUES (701, 'student006', '2020-05-05 00:07:37', '中国|华中|河南省|郑州市|移动', '39.149.14.51', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (702, 'student001', '2020-05-05 00:10:14', '中国|华中|河南省|郑州市|移动', '39.149.14.51', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (703, 'student006', '2020-05-05 00:13:09', '中国|华中|河南省|郑州市|移动', '39.149.14.51', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (704, 'admin', '2020-05-05 00:17:06', '中国|华中|河南省|安阳市|电信', '171.13.131.139', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (705, 'admin', '2020-05-05 00:23:36', '中国|华中|河南省|郑州市|移动', '39.149.14.51', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (706, 'teacher001', '2020-05-05 00:25:29', '中国|华中|河南省|郑州市|移动', '39.149.14.51', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (707, 'student001', '2020-05-05 00:29:35', '中国|华中|河南省|郑州市|移动', '39.149.14.51', 'Windows ', 'Chrome 63');
INSERT INTO `t_login_log` VALUES (708, 'student001', '2020-05-05 01:27:37', '中国|华中|河南省|安阳市|电信', '171.13.131.139', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (709, 'teacher001', '2020-05-05 01:28:33', '中国|华中|河南省|安阳市|电信', '171.13.131.139', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (710, 'admin', '2020-05-05 01:29:44', '中国|华中|河南省|安阳市|电信', '171.13.131.139', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (711, 'teacher001', '2020-05-05 01:30:11', '中国|华中|河南省|安阳市|电信', '171.13.131.139', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (712, 'student007', '2020-05-05 09:57:51', '中国|华中|河南省|郑州市|联通', '221.15.186.144', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (713, 'student006', '2020-05-05 11:37:33', '中国|华中|河南省|0|移动', '223.104.105.246', 'Linux', 'Chrome 78');
INSERT INTO `t_login_log` VALUES (714, 'student006', '2020-05-05 11:40:46', '中国|华中|河南省|0|移动', '223.104.105.246', 'Linux', 'Chrome 78');
INSERT INTO `t_login_log` VALUES (715, 'student006', '2020-05-05 15:05:19', '中国|华中|河南省|郑州市|移动', '39.149.14.51', 'Windows ', 'Firefox 75');
INSERT INTO `t_login_log` VALUES (716, 'teacher001', '2020-05-05 15:19:10', '中国|华中|河南省|郑州市|移动', '39.149.14.51', 'Windows ', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (717, 'student001', '2020-05-05 22:45:37', '中国|华南|广东省|广州市|电信', '113.103.9.232', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (718, 'teacher001', '2020-05-05 22:46:38', '中国|华南|广东省|广州市|电信', '14.145.90.70', 'Windows 10', 'Chrome 81');
INSERT INTO `t_login_log` VALUES (719, 'admin', '2020-05-06 08:51:02', '中国|华中|河南省|安阳市|联通', '123.8.12.129', 'Windows 10', 'Chrome 81');

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu`  (
  `MENU_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单/按钮ID',
  `PARENT_ID` bigint(20) NOT NULL COMMENT '上级菜单ID',
  `MENU_NAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单/按钮名称',
  `URL` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单URL',
  `PERMS` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '权限标识',
  `ICON` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
  `TYPE` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型 0菜单 1按钮',
  `ORDER_NUM` bigint(20) NULL DEFAULT NULL COMMENT '排序',
  `CREATE_TIME` datetime(0) NOT NULL COMMENT '创建时间',
  `MODIFY_TIME` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`MENU_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 224 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_menu
-- ----------------------------
INSERT INTO `t_menu` VALUES (1, 0, '系统管理', NULL, NULL, 'layui-icon-setting', '0', 1, '2017-12-27 16:39:07', NULL);
INSERT INTO `t_menu` VALUES (2, 0, '系统监控', '', '', 'layui-icon-alert', '0', 2, '2017-12-27 16:45:51', '2019-06-13 11:20:40');
INSERT INTO `t_menu` VALUES (3, 1, '用户管理', '/system/user', 'user:view', '', '0', 1, '2017-12-27 16:47:13', '2020-03-26 16:38:59');
INSERT INTO `t_menu` VALUES (4, 1, '角色管理', '/system/role', 'role:view', '', '0', 2, '2017-12-27 16:48:09', '2019-06-13 08:57:19');
INSERT INTO `t_menu` VALUES (5, 1, '菜单管理', '/system/menu', 'menu:view', '', '0', 3, '2017-12-27 16:48:57', '2019-06-13 08:57:34');
INSERT INTO `t_menu` VALUES (6, 184, '班级管理', '/system/dept', 'dept:view', 'layui-icon-solution', '0', 4, '2017-12-27 16:57:33', '2020-03-24 10:38:42');
INSERT INTO `t_menu` VALUES (8, 2, '在线用户', '/monitor/online', 'online:view', '', '0', 1, '2017-12-27 16:59:33', '2019-06-13 14:30:31');
INSERT INTO `t_menu` VALUES (10, 2, '系统日志', '/monitor/log', 'log:view', '', '0', 2, '2017-12-27 17:00:50', '2019-06-13 14:30:37');
INSERT INTO `t_menu` VALUES (11, 3, '新增用户', NULL, 'user:add', NULL, '1', NULL, '2017-12-27 17:02:58', NULL);
INSERT INTO `t_menu` VALUES (12, 3, '修改用户', NULL, 'user:update', NULL, '1', NULL, '2017-12-27 17:04:07', NULL);
INSERT INTO `t_menu` VALUES (13, 3, '删除用户', NULL, 'user:delete', NULL, '1', NULL, '2017-12-27 17:04:58', NULL);
INSERT INTO `t_menu` VALUES (14, 4, '新增角色', NULL, 'role:add', NULL, '1', NULL, '2017-12-27 17:06:38', NULL);
INSERT INTO `t_menu` VALUES (15, 4, '修改角色', NULL, 'role:update', NULL, '1', NULL, '2017-12-27 17:06:38', NULL);
INSERT INTO `t_menu` VALUES (16, 4, '删除角色', NULL, 'role:delete', NULL, '1', NULL, '2017-12-27 17:06:38', NULL);
INSERT INTO `t_menu` VALUES (17, 5, '新增菜单', NULL, 'menu:add', NULL, '1', NULL, '2017-12-27 17:08:02', NULL);
INSERT INTO `t_menu` VALUES (18, 5, '修改菜单', NULL, 'menu:update', NULL, '1', NULL, '2017-12-27 17:08:02', NULL);
INSERT INTO `t_menu` VALUES (19, 5, '删除菜单', NULL, 'menu:delete', NULL, '1', NULL, '2017-12-27 17:08:02', NULL);
INSERT INTO `t_menu` VALUES (20, 6, '新增部门', NULL, 'dept:add', NULL, '1', NULL, '2017-12-27 17:09:24', NULL);
INSERT INTO `t_menu` VALUES (21, 6, '修改部门', NULL, 'dept:update', NULL, '1', NULL, '2017-12-27 17:09:24', NULL);
INSERT INTO `t_menu` VALUES (22, 6, '删除部门', NULL, 'dept:delete', NULL, '1', NULL, '2017-12-27 17:09:24', NULL);
INSERT INTO `t_menu` VALUES (23, 8, '踢出用户', NULL, 'user:kickout', NULL, '1', NULL, '2017-12-27 17:11:13', NULL);
INSERT INTO `t_menu` VALUES (24, 10, '删除日志', NULL, 'log:delete', NULL, '1', NULL, '2017-12-27 17:11:45', '2019-06-06 05:56:40');
INSERT INTO `t_menu` VALUES (101, 0, '任务调度', NULL, NULL, 'layui-icon-time-circle', '0', 3, '2018-02-24 15:52:57', NULL);
INSERT INTO `t_menu` VALUES (102, 101, '定时任务', '/job/job', 'job:view', '', '0', 1, '2018-02-24 15:53:53', '2018-04-25 09:05:12');
INSERT INTO `t_menu` VALUES (103, 102, '新增任务', NULL, 'job:add', NULL, '1', NULL, '2018-02-24 15:55:10', NULL);
INSERT INTO `t_menu` VALUES (104, 102, '修改任务', NULL, 'job:update', NULL, '1', NULL, '2018-02-24 15:55:53', NULL);
INSERT INTO `t_menu` VALUES (105, 102, '删除任务', NULL, 'job:delete', NULL, '1', NULL, '2018-02-24 15:56:18', NULL);
INSERT INTO `t_menu` VALUES (106, 102, '暂停任务', NULL, 'job:pause', NULL, '1', NULL, '2018-02-24 15:57:08', NULL);
INSERT INTO `t_menu` VALUES (107, 102, '恢复任务', NULL, 'job:resume', NULL, '1', NULL, '2018-02-24 15:58:21', NULL);
INSERT INTO `t_menu` VALUES (108, 102, '立即执行任务', NULL, 'job:run', NULL, '1', NULL, '2018-02-24 15:59:45', NULL);
INSERT INTO `t_menu` VALUES (109, 101, '调度日志', '/job/log', 'job:log:view', '', '0', 2, '2018-02-24 16:00:45', '2019-06-09 02:48:19');
INSERT INTO `t_menu` VALUES (110, 109, '删除日志', NULL, 'job:log:delete', NULL, '1', NULL, '2018-02-24 16:01:21', NULL);
INSERT INTO `t_menu` VALUES (115, 0, '其他模块', NULL, NULL, 'layui-icon-gift', '0', 5, '2019-05-27 10:18:07', NULL);
INSERT INTO `t_menu` VALUES (116, 115, 'Apex图表', '', '', NULL, '0', 2, '2019-05-27 10:21:35', NULL);
INSERT INTO `t_menu` VALUES (117, 116, '线性图表', '/others/apex/line', 'apex:line:view', NULL, '0', 1, '2019-05-27 10:24:49', NULL);
INSERT INTO `t_menu` VALUES (118, 115, '高德地图', '/others/map', 'map:view', '', '0', 3, '2019-05-27 17:13:12', '2019-06-12 15:33:00');
INSERT INTO `t_menu` VALUES (119, 116, '面积图表', '/others/apex/area', 'apex:area:view', NULL, '0', 2, '2019-05-27 18:49:22', NULL);
INSERT INTO `t_menu` VALUES (120, 116, '柱形图表', '/others/apex/column', 'apex:column:view', NULL, '0', 3, '2019-05-27 18:51:33', NULL);
INSERT INTO `t_menu` VALUES (121, 116, '雷达图表', '/others/apex/radar', 'apex:radar:view', NULL, '0', 4, '2019-05-27 18:56:05', NULL);
INSERT INTO `t_menu` VALUES (122, 116, '条形图表', '/others/apex/bar', 'apex:bar:view', NULL, '0', 5, '2019-05-27 18:57:02', NULL);
INSERT INTO `t_menu` VALUES (123, 116, '混合图表', '/others/apex/mix', 'apex:mix:view', '', '0', 6, '2019-05-27 18:58:04', '2019-06-06 02:55:23');
INSERT INTO `t_menu` VALUES (125, 115, '导入导出', '/others/eximport', 'others:eximport:view', '', '0', 4, '2019-05-27 19:01:57', '2019-06-13 01:20:14');
INSERT INTO `t_menu` VALUES (126, 132, '系统图标', '/others/febs/icon', 'febs:icons:view', '', '0', 4, '2019-05-27 19:03:18', '2019-06-06 03:05:26');
INSERT INTO `t_menu` VALUES (127, 2, '请求追踪', '/monitor/httptrace', 'httptrace:view', '', '0', 6, '2019-05-27 19:06:38', '2019-06-13 14:36:43');
INSERT INTO `t_menu` VALUES (128, 2, '系统信息', NULL, NULL, NULL, '0', 7, '2019-05-27 19:08:23', NULL);
INSERT INTO `t_menu` VALUES (129, 128, 'JVM信息', '/monitor/jvm', 'jvm:view', '', '0', 1, '2019-05-27 19:08:50', '2019-06-13 14:36:51');
INSERT INTO `t_menu` VALUES (131, 128, '服务器信息', '/monitor/server', 'server:view', '', '0', 3, '2019-05-27 19:10:07', '2019-06-13 14:37:04');
INSERT INTO `t_menu` VALUES (132, 115, 'FEBS组件', '', '', NULL, '0', 1, '2019-05-27 19:13:54', NULL);
INSERT INTO `t_menu` VALUES (133, 132, '表单组件', '/others/febs/form', 'febs:form:view', NULL, '0', 1, '2019-05-27 19:14:45', NULL);
INSERT INTO `t_menu` VALUES (134, 132, 'FEBS工具', '/others/febs/tools', 'febs:tools:view', '', '0', 3, '2019-05-29 10:11:22', '2019-06-12 13:21:27');
INSERT INTO `t_menu` VALUES (135, 132, '表单组合', '/others/febs/form/group', 'febs:formgroup:view', NULL, '0', 2, '2019-05-29 10:16:19', NULL);
INSERT INTO `t_menu` VALUES (136, 2, '登录日志', '/monitor/loginlog', 'loginlog:view', '', '0', 3, '2019-05-29 15:56:15', '2019-06-13 14:35:56');
INSERT INTO `t_menu` VALUES (137, 0, '代码生成', '', NULL, 'layui-icon-verticalright', '0', 4, '2019-06-03 15:35:58', NULL);
INSERT INTO `t_menu` VALUES (138, 137, '生成配置', '/generator/configure', 'generator:configure:view', NULL, '0', 1, '2019-06-03 15:38:36', NULL);
INSERT INTO `t_menu` VALUES (139, 137, '代码生成', '/generator/generator', 'generator:view', '', '0', 2, '2019-06-03 15:39:15', '2019-06-13 14:31:38');
INSERT INTO `t_menu` VALUES (159, 132, '其他组件', '/others/febs/others', 'others:febs:others', '', '0', 5, '2019-06-12 07:51:08', '2019-06-12 07:51:40');
INSERT INTO `t_menu` VALUES (160, 3, '密码重置', NULL, 'user:password:reset', NULL, '1', NULL, '2019-06-13 08:40:13', NULL);
INSERT INTO `t_menu` VALUES (161, 3, '导出Excel', NULL, 'user:export', NULL, '1', NULL, '2019-06-13 08:40:34', NULL);
INSERT INTO `t_menu` VALUES (162, 4, '导出Excel', NULL, 'role:export', NULL, '1', NULL, '2019-06-13 14:29:00', '2019-06-13 14:29:11');
INSERT INTO `t_menu` VALUES (163, 5, '导出Excel', NULL, 'menu:export', NULL, '1', NULL, '2019-06-13 14:29:32', NULL);
INSERT INTO `t_menu` VALUES (164, 6, '导出Excel', NULL, 'dept:export', NULL, '1', NULL, '2019-06-13 14:29:59', NULL);
INSERT INTO `t_menu` VALUES (165, 138, '修改配置', NULL, 'generator:configure:update', NULL, '1', NULL, '2019-06-13 14:32:09', '2019-06-13 14:32:20');
INSERT INTO `t_menu` VALUES (166, 139, '生成代码', NULL, 'generator:generate', NULL, '1', NULL, '2019-06-13 14:32:51', NULL);
INSERT INTO `t_menu` VALUES (167, 125, '模板下载', NULL, 'eximport:template', NULL, '1', NULL, '2019-06-13 14:33:37', NULL);
INSERT INTO `t_menu` VALUES (168, 125, '导出Excel', NULL, 'eximport:export', NULL, '1', NULL, '2019-06-13 14:33:57', NULL);
INSERT INTO `t_menu` VALUES (169, 125, '导入Excel', NULL, 'eximport:import', NULL, '1', NULL, '2019-06-13 14:34:19', NULL);
INSERT INTO `t_menu` VALUES (170, 10, '导出Excel', NULL, 'log:export', NULL, '1', NULL, '2019-06-13 14:34:55', NULL);
INSERT INTO `t_menu` VALUES (171, 136, '删除日志', NULL, 'loginlog:delete', NULL, '1', NULL, '2019-06-13 14:35:27', '2019-06-13 14:36:08');
INSERT INTO `t_menu` VALUES (172, 136, '导出Excel', NULL, 'loginlog:export', NULL, '1', NULL, '2019-06-13 14:36:26', NULL);
INSERT INTO `t_menu` VALUES (173, 102, '导出Excel', NULL, 'job:export', NULL, '1', NULL, '2019-06-13 14:37:25', NULL);
INSERT INTO `t_menu` VALUES (174, 109, '导出Excel', NULL, 'job:log:export', NULL, '1', NULL, '2019-06-13 14:37:46', '2019-06-13 14:38:02');
INSERT INTO `t_menu` VALUES (175, 2, 'Swagger文档', '/monitor/swagger', 'swagger:view', '', '0', 8, '2019-08-18 17:25:36', '2019-08-18 17:25:59');
INSERT INTO `t_menu` VALUES (181, 0, '考勤管理', '', '', 'layui-icon-project', '0', NULL, '2020-03-20 15:06:12', '2020-04-10 17:14:57');
INSERT INTO `t_menu` VALUES (183, 181, '实验报告', '', '', 'layui-icon-barchart', '0', 3, '2020-03-20 15:09:13', '2020-03-25 15:44:45');
INSERT INTO `t_menu` VALUES (184, 0, '教学管理', '', '', 'layui-icon-qrcode', '0', NULL, '2020-03-24 09:50:57', NULL);
INSERT INTO `t_menu` VALUES (189, 184, '课程管理', '/system/course', 'course:view', 'layui-icon-detail', '0', NULL, '2020-03-24 10:43:51', '2020-03-24 10:44:45');
INSERT INTO `t_menu` VALUES (190, 189, '新增课程', NULL, 'course:add', NULL, '1', NULL, '2020-03-25 10:49:06', NULL);
INSERT INTO `t_menu` VALUES (191, 181, '实验项目', '/system/experiment/project', 'experimentProject:view', 'layui-icon-detail', '0', 2, '2020-03-25 15:43:55', '2020-03-25 15:44:18');
INSERT INTO `t_menu` VALUES (193, 191, '添加项目', NULL, 'experimentProject:add', NULL, '1', NULL, '2020-03-26 10:32:06', '2020-03-26 10:32:30');
INSERT INTO `t_menu` VALUES (198, 0, '班级管理', '/system/teacher/dept', 'teacherDept:view', 'layui-icon-layout', '0', NULL, '2020-03-27 11:17:51', NULL);
INSERT INTO `t_menu` VALUES (199, 0, '课程管理', '', '', 'layui-icon-appstore', '0', NULL, '2020-03-27 11:18:03', NULL);
INSERT INTO `t_menu` VALUES (200, 0, '实验报告', '', '', 'layui-icon-check-square', '0', NULL, '2020-03-27 11:18:33', '2020-04-01 16:17:02');
INSERT INTO `t_menu` VALUES (201, 0, '学生信息', '/dept/select/student', 'deptStudent:view', 'layui-icon-user', '0', NULL, '2020-03-28 10:07:41', '2020-04-01 16:17:43');
INSERT INTO `t_menu` VALUES (202, 189, '更新课程', NULL, 'course:update', NULL, '1', NULL, '2020-03-30 15:46:40', NULL);
INSERT INTO `t_menu` VALUES (203, 189, '查询课程', NULL, 'course:select', NULL, '1', NULL, '2020-03-30 15:50:01', NULL);
INSERT INTO `t_menu` VALUES (204, 189, '删除课程', NULL, 'course:delete', NULL, '1', NULL, '2020-03-30 15:51:14', NULL);
INSERT INTO `t_menu` VALUES (205, 191, '修改项目', NULL, 'experimentProject:update', NULL, '1', NULL, '2020-04-01 14:59:04', '2020-04-01 15:01:54');
INSERT INTO `t_menu` VALUES (206, 199, '课程项目', '/teacher/select/project', 'teacherProject:view', '', '0', NULL, '2020-04-01 16:16:11', NULL);
INSERT INTO `t_menu` VALUES (207, 199, '提交情况', '/teacher/getCommit', 'teacher:commit', '', '0', NULL, '2020-04-01 16:16:24', NULL);
INSERT INTO `t_menu` VALUES (208, 200, '批阅报告', '/teacher/getScore', 'teacher:score', '', '0', NULL, '2020-04-01 16:17:20', NULL);
INSERT INTO `t_menu` VALUES (209, 200, '常用评语', '', '', '', '0', NULL, '2020-04-01 16:17:34', NULL);
INSERT INTO `t_menu` VALUES (210, 0, '删除项目', NULL, 'experimentProject:delect', NULL, '1', NULL, '2020-04-01 17:27:59', NULL);
INSERT INTO `t_menu` VALUES (211, 191, '删除项目', NULL, 'experimentProject:delete', NULL, '1', NULL, '2020-04-01 17:30:13', NULL);
INSERT INTO `t_menu` VALUES (213, 0, '我的课程', '/student/project', '', 'layui-icon-eye', '0', NULL, '2020-04-02 17:43:19', NULL);
INSERT INTO `t_menu` VALUES (214, 213, '我的项目', '/student/myProject', '', '', '0', NULL, '2020-04-02 17:43:43', NULL);
INSERT INTO `t_menu` VALUES (215, 206, '新增实验项目', NULL, 'teacherProject:add', NULL, '1', NULL, '2020-04-03 09:32:34', NULL);
INSERT INTO `t_menu` VALUES (216, 206, '修改实验项目', NULL, 'teacherProject:update', NULL, '1', NULL, '2020-04-03 11:48:17', NULL);
INSERT INTO `t_menu` VALUES (217, 206, '删除实验项目', NULL, 'teacherProject:delete', NULL, '1', NULL, '2020-04-03 11:49:17', NULL);
INSERT INTO `t_menu` VALUES (218, 0, '打卡', '/student/faceMatch\"', '', 'layui-icon-verticalright', '0', 50, '2020-04-10 16:58:47', '2020-04-10 17:00:57');
INSERT INTO `t_menu` VALUES (219, 0, '统计', '/student/count', 'student:count', 'layui-icon-hourglass', '0', 51, '2020-04-10 16:59:34', '2020-04-10 17:01:03');
INSERT INTO `t_menu` VALUES (220, 0, '请假', '', '', 'layui-icon-sever', '0', 53, '2020-04-10 17:03:00', NULL);
INSERT INTO `t_menu` VALUES (221, 0, '查看/统计', '/student/count', '', 'layui-icon-unorderedlist', '0', 60, '2020-04-10 17:06:32', NULL);
INSERT INTO `t_menu` VALUES (222, 0, '审批', '', '', 'layui-icon-edit-square', '0', 61, '2020-04-10 17:06:52', NULL);
INSERT INTO `t_menu` VALUES (223, 0, '人像录入', '/student/faceIn', '', 'layui-icon-deleteuser', '0', NULL, '2020-04-14 11:40:12', NULL);

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role`  (
  `ROLE_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `ROLE_NAME` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `REMARK` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `CREATE_TIME` datetime(0) NOT NULL COMMENT '创建时间',
  `MODIFY_TIME` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`ROLE_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 83 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES (1, '系统管理员', '系统管理员，拥有所有操作权限 ^_^', '2019-06-14 16:23:11', '2020-04-14 11:42:18');
INSERT INTO `t_role` VALUES (81, '老师', '学校老师', '2020-03-20 08:58:03', '2020-04-14 11:38:33');
INSERT INTO `t_role` VALUES (82, '学生', '学校学生', '2020-03-20 08:59:29', '2020-04-14 11:47:15');

-- ----------------------------
-- Table structure for t_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_role_menu`;
CREATE TABLE `t_role_menu`  (
  `ROLE_ID` bigint(20) NOT NULL COMMENT '角色ID',
  `MENU_ID` bigint(20) NOT NULL COMMENT '菜单/按钮ID'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role_menu
-- ----------------------------
INSERT INTO `t_role_menu` VALUES (81, 191);
INSERT INTO `t_role_menu` VALUES (81, 193);
INSERT INTO `t_role_menu` VALUES (81, 205);
INSERT INTO `t_role_menu` VALUES (81, 211);
INSERT INTO `t_role_menu` VALUES (81, 183);
INSERT INTO `t_role_menu` VALUES (81, 199);
INSERT INTO `t_role_menu` VALUES (81, 206);
INSERT INTO `t_role_menu` VALUES (81, 215);
INSERT INTO `t_role_menu` VALUES (81, 216);
INSERT INTO `t_role_menu` VALUES (81, 217);
INSERT INTO `t_role_menu` VALUES (81, 207);
INSERT INTO `t_role_menu` VALUES (81, 200);
INSERT INTO `t_role_menu` VALUES (81, 208);
INSERT INTO `t_role_menu` VALUES (81, 201);
INSERT INTO `t_role_menu` VALUES (81, 221);
INSERT INTO `t_role_menu` VALUES (81, 222);
INSERT INTO `t_role_menu` VALUES (1, 191);
INSERT INTO `t_role_menu` VALUES (1, 193);
INSERT INTO `t_role_menu` VALUES (1, 205);
INSERT INTO `t_role_menu` VALUES (1, 211);
INSERT INTO `t_role_menu` VALUES (1, 183);
INSERT INTO `t_role_menu` VALUES (1, 184);
INSERT INTO `t_role_menu` VALUES (1, 189);
INSERT INTO `t_role_menu` VALUES (1, 190);
INSERT INTO `t_role_menu` VALUES (1, 202);
INSERT INTO `t_role_menu` VALUES (1, 203);
INSERT INTO `t_role_menu` VALUES (1, 204);
INSERT INTO `t_role_menu` VALUES (1, 6);
INSERT INTO `t_role_menu` VALUES (1, 20);
INSERT INTO `t_role_menu` VALUES (1, 21);
INSERT INTO `t_role_menu` VALUES (1, 22);
INSERT INTO `t_role_menu` VALUES (1, 164);
INSERT INTO `t_role_menu` VALUES (1, 1);
INSERT INTO `t_role_menu` VALUES (1, 3);
INSERT INTO `t_role_menu` VALUES (1, 11);
INSERT INTO `t_role_menu` VALUES (1, 12);
INSERT INTO `t_role_menu` VALUES (1, 13);
INSERT INTO `t_role_menu` VALUES (1, 160);
INSERT INTO `t_role_menu` VALUES (1, 161);
INSERT INTO `t_role_menu` VALUES (1, 4);
INSERT INTO `t_role_menu` VALUES (1, 14);
INSERT INTO `t_role_menu` VALUES (1, 15);
INSERT INTO `t_role_menu` VALUES (1, 16);
INSERT INTO `t_role_menu` VALUES (1, 162);
INSERT INTO `t_role_menu` VALUES (1, 5);
INSERT INTO `t_role_menu` VALUES (1, 17);
INSERT INTO `t_role_menu` VALUES (1, 18);
INSERT INTO `t_role_menu` VALUES (1, 19);
INSERT INTO `t_role_menu` VALUES (1, 163);
INSERT INTO `t_role_menu` VALUES (1, 2);
INSERT INTO `t_role_menu` VALUES (1, 8);
INSERT INTO `t_role_menu` VALUES (1, 23);
INSERT INTO `t_role_menu` VALUES (1, 10);
INSERT INTO `t_role_menu` VALUES (1, 24);
INSERT INTO `t_role_menu` VALUES (1, 170);
INSERT INTO `t_role_menu` VALUES (1, 136);
INSERT INTO `t_role_menu` VALUES (1, 171);
INSERT INTO `t_role_menu` VALUES (1, 172);
INSERT INTO `t_role_menu` VALUES (1, 127);
INSERT INTO `t_role_menu` VALUES (1, 128);
INSERT INTO `t_role_menu` VALUES (1, 129);
INSERT INTO `t_role_menu` VALUES (1, 131);
INSERT INTO `t_role_menu` VALUES (1, 175);
INSERT INTO `t_role_menu` VALUES (1, 101);
INSERT INTO `t_role_menu` VALUES (1, 102);
INSERT INTO `t_role_menu` VALUES (1, 103);
INSERT INTO `t_role_menu` VALUES (1, 104);
INSERT INTO `t_role_menu` VALUES (1, 105);
INSERT INTO `t_role_menu` VALUES (1, 106);
INSERT INTO `t_role_menu` VALUES (1, 107);
INSERT INTO `t_role_menu` VALUES (1, 108);
INSERT INTO `t_role_menu` VALUES (1, 173);
INSERT INTO `t_role_menu` VALUES (1, 109);
INSERT INTO `t_role_menu` VALUES (1, 110);
INSERT INTO `t_role_menu` VALUES (1, 174);
INSERT INTO `t_role_menu` VALUES (1, 138);
INSERT INTO `t_role_menu` VALUES (1, 165);
INSERT INTO `t_role_menu` VALUES (1, 139);
INSERT INTO `t_role_menu` VALUES (1, 166);
INSERT INTO `t_role_menu` VALUES (1, 115);
INSERT INTO `t_role_menu` VALUES (1, 133);
INSERT INTO `t_role_menu` VALUES (1, 135);
INSERT INTO `t_role_menu` VALUES (1, 134);
INSERT INTO `t_role_menu` VALUES (1, 126);
INSERT INTO `t_role_menu` VALUES (1, 159);
INSERT INTO `t_role_menu` VALUES (1, 117);
INSERT INTO `t_role_menu` VALUES (1, 119);
INSERT INTO `t_role_menu` VALUES (1, 120);
INSERT INTO `t_role_menu` VALUES (1, 121);
INSERT INTO `t_role_menu` VALUES (1, 122);
INSERT INTO `t_role_menu` VALUES (1, 123);
INSERT INTO `t_role_menu` VALUES (1, 118);
INSERT INTO `t_role_menu` VALUES (1, 125);
INSERT INTO `t_role_menu` VALUES (1, 167);
INSERT INTO `t_role_menu` VALUES (1, 168);
INSERT INTO `t_role_menu` VALUES (1, 169);
INSERT INTO `t_role_menu` VALUES (82, 213);
INSERT INTO `t_role_menu` VALUES (82, 223);
INSERT INTO `t_role_menu` VALUES (82, 218);
INSERT INTO `t_role_menu` VALUES (82, 219);
INSERT INTO `t_role_menu` VALUES (82, 220);

-- ----------------------------
-- Table structure for t_sys_config
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_config`;
CREATE TABLE `t_sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_config
-- ----------------------------
INSERT INTO `t_sys_config` VALUES (18, '识别成功比', 'score', '80', 'N', '', NULL, '', NULL, NULL);

-- ----------------------------
-- Table structure for t_teacher_project
-- ----------------------------
DROP TABLE IF EXISTS `t_teacher_project`;
CREATE TABLE `t_teacher_project`  (
  `teacher_project_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '老师和项目表主键ID',
  `teacher_id` int(11) NULL DEFAULT NULL COMMENT '老师用户表主键ID',
  `project_id` int(11) NULL DEFAULT NULL COMMENT '项目主键ID',
  `dept_id` int(11) NULL DEFAULT NULL COMMENT '班级表id',
  `course_id` int(11) NULL DEFAULT NULL COMMENT '项目所属课程id',
  PRIMARY KEY (`teacher_project_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_teacher_project
-- ----------------------------
INSERT INTO `t_teacher_project` VALUES (23, 14, 7, 13, 29);
INSERT INTO `t_teacher_project` VALUES (24, 14, 24, 13, 29);
INSERT INTO `t_teacher_project` VALUES (25, 14, 24, 13, 29);

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `USER_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `ID_NUMBER` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '学号/工号',
  `USERNAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `PASSWORD` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `DEPT_ID` bigint(20) NULL DEFAULT NULL COMMENT '部门ID（学生班级ID）',
  `COURSE_ID` bigint(20) NULL DEFAULT NULL COMMENT '课程ID',
  `COURSE_NAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '课程名称',
  `EMAIL` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `MOBILE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `STATUS` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态 0锁定 1有效',
  `CREATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `MODIFY_TIME` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `LAST_LOGIN_TIME` datetime(0) NULL DEFAULT NULL COMMENT '最近访问时间',
  `SSEX` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '性别 0男 1女 2保密',
  `IS_TAB` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否开启tab，0关闭 1开启',
  `THEME` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主题',
  `AVATAR` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `DESCRIPTION` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `FACE_URL` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '人脸图片地址',
  `FACE_TOKEN` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户人脸注册成功后的唯一标识',
  `FACE_ID` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Face用户ID',
  `GROUP_ID` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Face用户组',
  PRIMARY KEY (`USER_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 148 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES (8, NULL, 'admin', '470263a5622b6153be9e681545233f55', 15, NULL, '', '105@qq.com', '13011112222', '1', '2020-03-20 09:23:15', '2020-04-02 17:04:04', '2020-05-06 08:51:05', '0', '1', 'black', 'default.jpg', '系统管理员', NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (12, NULL, 'teacher002', '8dc7a55f5a4fc7a1b90c6615b85e941a', NULL, 22, '', '135@163.com', '13500002222', '1', '2020-03-27 15:58:11', '2020-03-31 16:23:03', NULL, '0', '1', 'black', 'default.jpg', '我是一名老师', NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (14, '201311112232', 'teacher001', '31bccac7c4f4ad7c19d6b60e8bca9653', NULL, 29, '传感器实验课222', '105@qq.com', '13512341234', '1', '2020-04-01 14:37:37', '2020-04-07 10:02:31', '2020-05-05 22:46:39', '1', '1', 'black', 'default.jpg', '我是一名老师', NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (15, '2121221323', 'student001', '2e50094ee60d4e9d1b5d3fff7853ef00', 13, NULL, '', '120@163.com', '13526325632', '1', '2020-04-01 14:42:32', '2020-04-28 14:51:31', '2020-05-05 22:45:41', '0', '1', 'black', 'default.jpg', '我是一名学生', 'E:\\git\\ww_Face/src/main/resources/file/userFilesPath/20200428145129632453346.jpg', '39d152257a676c9a31f4e67a27051321', '7f24f4e1c1e64ac59b56c234f69287af', 'sx_user_v1');
INSERT INTO `t_user` VALUES (17, '', 'gzca', '63caeb98447f5d554789321adfed7eba', NULL, NULL, '', '872231868@qq.com', '17637389874', '1', '2020-04-06 10:13:41', NULL, '2020-04-06 16:54:55', '0', '1', 'black', 'default.jpg', '', NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (18, NULL, 'student002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-04-06 14:50:49', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (79, '182574473', '百荔', '1b16ea8a2fae66be7c669485d227c221', 13, NULL, NULL, 'tbuw9@gmail.com', '15907232611', '1', '2019-05-23 01:23:15', NULL, NULL, '1', NULL, NULL, 'http://henaumcw.top/avator/729419fa11ee4d2da1b0dc23feb6517e182574473.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (80, '141078291', '巴芝璧', 'c69a90eb1ae4718715edde841f5abff9', 13, NULL, NULL, '3snvylq40oxby@0355.net', '15806006877', '1', '2019-03-17 11:43:59', '2020-04-06 17:45:40', '2020-04-08 09:04:14', '1', '1', 'black', 'http://henaumcw.top/avator/afac82e293c8425f86f8fdb010ad7e83141078291.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (81, '156470428', '巫树', 'a1c12805ae9eefb3c73ae72371c56b71', 13, NULL, NULL, 'cz47f2pwlw4ze8m@aol.com', '13101787055', '1', '2019-08-31 03:32:24', NULL, NULL, '1', NULL, NULL, 'http://henaumcw.top/avator/c5ca54f9ecd0439785c4a3ded1942650156470428.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (82, '164609335', '霍宁有', '408743bf1f795e8e83df4fc7aaafdbf0', 13, NULL, NULL, 'me3iepvu7b@3721.net', '13802138505', '1', '2019-08-29 10:54:01', NULL, NULL, '1', NULL, NULL, 'http://henaumcw.top/avator/080a2b224394450b9fad791aed2524a7164609335.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (83, '135395248', '红君', '76f00097a583d50f3440f085dc666e36', 13, NULL, NULL, 'yj0wab60etpq@yeah.net', '15903832042', '1', '2019-08-14 20:15:13', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (84, '164584572', '逯艳', '971c151f97be0e7e7d3f12d34c8a8e00', 13, NULL, NULL, '4q72we2l2@live.com', '13006355842', '1', '2019-04-10 09:17:36', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (85, '179173935', '滑广启', '438004fd042fe5e69850e17a05dcc6f5', 13, NULL, NULL, 'z70qsd8pilxfxa@qq.com', '13302691464', '1', '2019-05-26 16:52:19', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (86, '159073484', '桓功', 'ebb4a102a104492e1c6eecf8fe390e1c', 13, NULL, NULL, 'u5jbm750gonv@msn.com', '15302265509', '1', '2019-04-29 10:20:03', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (87, '158361167', '益进', 'f5f70ff27e50980ecf7ab0bc8b75de61', 13, NULL, NULL, 'm9chjcnbf@aol.com', '13801803073', '1', '2019-03-27 03:59:01', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (88, '175732531', '郦眉丽', '2d2f4f35f1cc6f16782d295bf9dd9626', 13, NULL, NULL, 'ajnat78outf66@gmail.com', '15606630465', '1', '2019-12-07 08:51:17', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (89, '148803002', '夏成士', '13b7465a353977dd8acae79c183f48a7', 13, NULL, NULL, 'bzxe3gjlwl@263.net', '13904257447', '1', '2020-01-11 13:50:51', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (90, '131624188', '钟勤', '53d44c0a7505e17fedd25ad6cc08fc9f', 13, NULL, NULL, '3as2u@msn.com', '13106112431', '1', '2020-03-06 23:22:30', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (91, '182394135', '鲁寒玉', 'a3b1ffd0fc90343b0a7074929c586b07', 13, NULL, NULL, 'i51gxcn1zb@3721.net', '15107952660', '1', '2019-11-29 01:38:16', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (92, '134909428', '邓进', '92c44b57e5ed628299b14e2ee843cdc4', 13, NULL, NULL, 'hh0hg@qq.com', '15201414850', '1', '2019-05-20 19:34:14', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (93, '156882207', '樊菁昭', '35776f5ac818482149a8506b92f0be5d', 13, NULL, NULL, 'su8xdh9e0m1zs@263.net', '15007745289', '1', '2019-05-28 19:33:53', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (94, '137587447', '戴子敬', '873c741ea1349f7b220a30ccf78e6659', 13, NULL, NULL, '4c351v8oetm4ec@msn.com', '15105438667', '1', '2019-10-08 20:07:31', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (95, '149040165', '却永毅', '0f03bc502aa11fbaf45e98358ee7fc6a', 13, NULL, NULL, 'gew65n@aol.com', '13200736230', '1', '2019-09-01 04:23:14', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (96, '180870347', '公固', '72962bc3f1edfa04c27755ea746339a7', 13, NULL, NULL, 'a7gbi3@hotmail.com', '15602902245', '1', '2020-02-14 17:06:43', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (97, '183796883', '司岩若', '731b0a0995234d2ab4f5c0abd05b7c25', 13, NULL, NULL, '0f13lv2qzx@163.net', '15608288110', '1', '2020-04-05 22:14:49', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (98, '153807326', '闾秋', '7b093cf24b308afd5236af3dde5487ed', 13, NULL, NULL, '3yyxxjezvsq@yahoo.com', '13101077077', '1', '2019-06-08 00:58:50', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (99, '135185047', '巫家', 'd4a89bc80d74baae0cbc446c90e35501', 13, NULL, NULL, 'sbucok13fx33q7n@126.com', '15307022921', '1', '2019-03-22 12:35:46', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (100, '142209889', '于晶', 'e04b1d500d498e6825708c22eada2ab2', 13, NULL, NULL, 'e6ydr8h@qq.com', '15300651000', '1', '2019-12-16 20:56:34', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (101, '184929851', '辛士健', '1028e9b7c08b3c36f9a1fdd9bb76e7d0', 13, NULL, NULL, 'inxwg4n@sina.com', '13505917545', '1', '2020-02-12 15:13:04', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (102, '157119889', '薛承', 'aec1883400b025698271431ae37685f0', 13, NULL, NULL, 'ibse5@aol.com', '13303235451', '1', '2019-11-29 14:15:18', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (103, '160513862', '於丽锦', 'cb6843475ae5c6e41e55d38fa5f59b4e', 13, NULL, NULL, 'eaphgvs@yeah.net', '15108133704', '1', '2019-03-04 22:21:04', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (104, '145207590', '雍育', '8c43aad5e6fcec8c6c0dca3a9c12065c', 13, NULL, NULL, 'iix5af0@msn.com', '13106350320', '1', '2020-03-19 07:54:27', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (105, '144056423', '门红', '54f1971065ad86a25eba8c93a2608578', 13, NULL, NULL, 'dtc3haiap0u@gmail.com', '15701180063', '1', '2019-03-21 10:24:42', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (106, '170477594', '芮霄丽', 'a2b6be130692bccbd40179c33a1f4f4b', 13, NULL, NULL, 'vvav0jgf@263.net', '13308856490', '1', '2019-06-26 02:26:16', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (107, '176182663', '乐颖', '4909af0b51d99bdc97a032c9ef9f470c', 13, NULL, NULL, 'dzgnzuc5z@aol.com', '13000610173', '1', '2019-09-23 19:46:41', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (108, '141570225', '冷茜', 'f04e48dbfad4101c51753e8f3fc89f9d', 13, NULL, NULL, 'f1g83tvl@163.net', '15606853263', '1', '2019-03-12 23:54:00', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (109, NULL, 'student003', 'd76b2fd179a36220acd5ec8eba251155', NULL, NULL, NULL, NULL, NULL, '1', '2020-04-22 10:59:13', NULL, '2020-04-22 10:59:23', '2', '1', 'black', 'default.jpg', '注册用户', NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (110, '163103259', '边政风', 'fc14405131b650ce81850a48bea728eb', 13, NULL, NULL, 'ivxfk@163.net', '13608503231', '1', '2019-10-06 01:10:38', NULL, NULL, '女', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (111, '158969674', '阎秋', 'ab17feca1eea67381594ab186f40c20a', 13, NULL, NULL, 'p9rmjft6@qq.com', '15308803466', '1', '2019-04-09 08:07:43', NULL, NULL, '女', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (112, '169900955', '堵真', '1c5413e21e60b426e4c9a14ffdd29bb8', 13, NULL, NULL, 'ja6cui5c4b@yahoo.com.cn', '13204073984', '1', '2019-09-05 04:57:34', NULL, NULL, '男', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (113, '165046509', '于荣璧', 'b7a15b3c607871cbfd040f34c17d74bf', 13, NULL, NULL, '4obui4hwpa52@sohu.com', '13507604789', '1', '2019-09-19 23:21:57', NULL, NULL, '男', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (114, '154423259', '和娅岚', '1670b75bb503722ad29e45adba554a19', 13, NULL, NULL, '6pc2eljlsmn4ry@ask.com', '13601027652', '1', '2019-06-01 01:59:24', NULL, NULL, '女', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (115, '155875325', '谈民榕', '8ebb7aafbae72cedd2b9e05513c00722', 13, NULL, NULL, 'q21k1vsk@yeah.net', '15805537118', '1', '2019-11-13 05:05:31', NULL, NULL, '男', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (116, '165870371', '阴艺融', '1f547890345a1a587f714b939c126310', 13, NULL, NULL, 'g7gcu@163.com', '13706937477', '1', '2019-05-06 20:08:33', NULL, NULL, '男', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (117, '170110355', '毕之', 'bc84290099d61717924e50e2d927f36a', 13, NULL, NULL, 'faxg2h3v@aol.com', '15502356550', '1', '2019-04-29 00:32:15', NULL, NULL, '女', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (118, '144279599', '晏鸣旭', 'aa3076e52345fcbb3298eef7883b678a', 13, NULL, NULL, 'z336a5bf1@163.com', '13506435364', '1', '2019-04-08 16:10:42', NULL, NULL, '男', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (119, '183677402', '边璐玉', '2e40cc0f4105b7375dbeec2309f0dfab', 13, NULL, NULL, 'dahstf0n9hb@0355.net', '15003723784', '1', '2019-09-25 06:17:17', NULL, NULL, '男', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (120, '159923421', '屠凝筠', '242a7ee150c1a013592b302488e99f8d', 13, NULL, NULL, 'm7e8hg@sohu.com', '13006078961', '1', '2020-01-16 09:33:11', NULL, NULL, '男', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (121, '171466908', '容冠', '28e82a6d6353f4f583f864ab53966b5a', 13, NULL, NULL, 'e9xjy@msn.com', '13003101800', '1', '2019-11-07 07:22:23', NULL, NULL, '男', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (122, '148114421', '马姣凤', '6df69c511d21ce3a98391e7e0b23d60e', 13, NULL, NULL, 'nq95bve5unfw@263.net', '15102631964', '1', '2019-05-04 12:30:13', NULL, NULL, '女', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (123, '160373326', '燕香', '6f48c3ceeca1f080c620714855553178', 13, NULL, NULL, 'dm0scm74h6@126.com', '15308361887', '1', '2020-01-18 05:56:38', NULL, NULL, '女', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (124, '165188945', '宋育莺', '9382dd41573194de0e0ce0ded9d7285d', 13, NULL, NULL, 'b3v9p3rc@163.com', '15602640369', '1', '2020-02-07 11:14:42', NULL, NULL, '男', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (125, '146232231', '任信', 'd5d3b5c78c9008cba0d474371e7df8f7', 13, NULL, NULL, 'g964wnbd@ask.com', '15502758930', '1', '2019-10-11 16:02:17', NULL, NULL, '男', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (126, '170013464', '刁筠', '088a21a616608eafb15ab9b6e460298b', 13, NULL, NULL, 'zmra70i7qegtt6@yeah.net', '13302003341', '1', '2019-04-20 11:10:06', NULL, NULL, '女', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (127, '188861434', '干彩欣', '1fbae45c7392c0ff1d7a31dc75b60c87', 13, NULL, NULL, 'k336zmnw@sina.com', '13305687110', '1', '2019-07-26 07:33:43', NULL, NULL, '女', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (128, '181560026', '吴媛妹', '93ebbaff08b5d5c718b110440544922f', 13, NULL, NULL, 'gi5uljnl0xzxeq@ask.com', '13202177400', '1', '2019-04-15 01:47:39', NULL, NULL, '男', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (129, '170966670', '池学国', '15ed6b782871c330d6ae3ebb273f51f3', 13, NULL, NULL, 'jowflg@126.com', '13002774406', '1', '2019-11-14 10:45:50', NULL, NULL, '男', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (130, '133415405', '庄华', '5fdf7351c97871aff2eb1e3cde8fd9f3', 13, NULL, NULL, '1uv30mn38@163.com', '15807878781', '1', '2019-07-18 17:49:49', NULL, NULL, '女', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (131, '177078810', '奚丽澜', 'bef3f56cf79f40f78aea76e0130043b0', 13, NULL, NULL, 'ry0moxoamj@googlemail.com', '15107525666', '1', '2019-11-27 22:39:14', NULL, NULL, '男', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (132, '133251758', '端谦固', '275f629475a916694fd69175b68be84c', 13, NULL, NULL, '3kvj6xgrd@126.com', '15607454393', '1', '2020-02-16 04:46:24', NULL, NULL, '女', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (133, '180832416', '漆家', '9282bc853a787b9c432b5deee39dd6db', 13, NULL, NULL, '8yjen47mhtxsok@live.com', '13905318492', '1', '2020-01-16 17:52:56', NULL, NULL, '女', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (134, '145848472', '杨贞娜', '0104e970047dc0f8adaa953a59efa834', 13, NULL, NULL, '9lvvsklv@googlemail.com', '15502650364', '1', '2020-01-13 04:45:44', NULL, NULL, '男', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (135, '149025156', '禄 ', 'd2fbde4019d1efa3b4162603b7743aa0', 13, NULL, NULL, '31xg1cxgsvg3@yahoo.com', '13300880732', '1', '2019-10-13 15:33:23', NULL, NULL, '男', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (136, '162276499', '广博', '788d3a3840fce68161a9d1dd617cf1fc', 13, NULL, NULL, 'z037l160fmawuxd@live.com', '13405935044', '1', '2019-03-04 13:31:03', NULL, NULL, '男', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (137, '149576763', '万超', '33d66748bbdd97c8f20f708eb4fb0766', 13, NULL, NULL, 'pe77q@sohu.com', '15508314504', '1', '2020-01-07 03:03:33', NULL, NULL, '男', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (138, '152993252', '党伊璐', '905c0dbcd38df450ae00ab2c5a34e801', 13, NULL, NULL, 'b8s4sdu@263.net', '15608006189', '1', '2019-10-29 21:44:59', NULL, NULL, '女', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (139, '156835280', '咸行', '9b90962040c3192916bd0189292258f4', 13, NULL, NULL, 'tp527lf5xkf3vr@yeah.net', '13002002670', '1', '2019-09-24 23:09:48', NULL, NULL, '男', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (140, NULL, 'GuiZicheng', 'd77d2a0e71673108e8fdbcd9ce8d553d', NULL, NULL, NULL, NULL, NULL, '1', '2020-04-25 17:15:40', NULL, '2020-04-25 17:15:59', '2', '1', 'black', 'default.jpg', '注册用户', NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (142, NULL, 'student004', 'f659f44222144e30156c61420746a225', NULL, NULL, NULL, NULL, NULL, '1', '2020-04-28 15:02:56', '2020-04-28 15:03:16', '2020-04-29 17:48:49', '2', '1', 'black', 'default.jpg', '注册用户', 'E:\\git\\ww_Face/src/main/resources/file/userFilesPath/20200428150315011428508.jpg', '39d152257a676c9a31f4e67a27051321', '3c2b349f5f4a4a159fde250f9f64292a', 'sx_user_v1');
INSERT INTO `t_user` VALUES (146, NULL, 'student006', 'da6001720b270ff148ae04a79be004f6', 26, NULL, NULL, NULL, NULL, '1', '2020-05-05 00:03:45', '2020-05-05 15:06:22', '2020-05-05 11:40:56', '2', '1', 'black', 'default.jpg', '注册用户', '/home/face/src/main/resources/file/userFilesPath/20200505150605140416962.jpg', 'd8807e099f3eed9c94e401d9653e7f45', 'ff7cdb492e7642dbaa298fecf50b934b', 'sx_user_v1');
INSERT INTO `t_user` VALUES (147, NULL, 'student007', 'b5dc677fc6674b12f08d02e5fff533e7', 26, NULL, NULL, NULL, NULL, '1', '2020-05-05 09:57:35', '2020-05-05 09:58:06', '2020-05-05 09:57:54', '2', '1', 'black', 'default.jpg', '注册用户', '/home/face/src/main/resources/file/userFilesPath/20200505095804199069619.jpg', '1813438fb6c0fd3392e91718cff5ba7e', '29db4074d0c346a9b99ed685880e35d1', 'sx_user_v1');

-- ----------------------------
-- Table structure for t_user_commit
-- ----------------------------
DROP TABLE IF EXISTS `t_user_commit`;
CREATE TABLE `t_user_commit`  (
  `commit_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '学生报告提交表',
  `stu_id` int(11) NULL DEFAULT NULL COMMENT '学生ID',
  `dept_id` int(11) NULL DEFAULT NULL COMMENT '学生班级ID',
  `project_id` int(11) NULL DEFAULT NULL COMMENT '学生做的项目ID',
  `course_id` int(11) NULL DEFAULT NULL COMMENT '课程ID',
  `teacher_id` int(11) NULL DEFAULT NULL COMMENT '项目老师ID',
  `report_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '学生提交的报告URL',
  `score` double(10, 2) NULL DEFAULT NULL COMMENT '学生成绩',
  `is_commit` int(2) NULL DEFAULT 0 COMMENT '1提交，0，未提交',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '提交时间',
  PRIMARY KEY (`commit_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学生报告提交记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user_commit
-- ----------------------------
INSERT INTO `t_user_commit` VALUES (9, 15, 13, 24, 29, 14, 'http://henaumcw.top/studentProject/2020/04/09/8feb48584c2240c992132cf2e0b73f97/信号源_u271.png', 10.00, 1, '2020-04-09 11:24:01');

-- ----------------------------
-- Table structure for t_user_dept
-- ----------------------------
DROP TABLE IF EXISTS `t_user_dept`;
CREATE TABLE `t_user_dept`  (
  `user_dept_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户班级表',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户ID',
  `dept_id` int(11) NULL DEFAULT NULL COMMENT '部门ID',
  PRIMARY KEY (`user_dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user_dept
-- ----------------------------
INSERT INTO `t_user_dept` VALUES (15, 10, 18);
INSERT INTO `t_user_dept` VALUES (16, 10, 19);
INSERT INTO `t_user_dept` VALUES (17, 10, 16);
INSERT INTO `t_user_dept` VALUES (18, 10, 17);
INSERT INTO `t_user_dept` VALUES (19, 9, 23);
INSERT INTO `t_user_dept` VALUES (32, 13, 15);
INSERT INTO `t_user_dept` VALUES (33, 13, 16);
INSERT INTO `t_user_dept` VALUES (36, 12, 15);
INSERT INTO `t_user_dept` VALUES (37, 12, 16);
INSERT INTO `t_user_dept` VALUES (40, 15, 13);
INSERT INTO `t_user_dept` VALUES (43, 8, 15);
INSERT INTO `t_user_dept` VALUES (44, 8, 16);
INSERT INTO `t_user_dept` VALUES (45, 17, 16);
INSERT INTO `t_user_dept` VALUES (46, 14, 13);
INSERT INTO `t_user_dept` VALUES (47, 14, 20);
INSERT INTO `t_user_dept` VALUES (48, 143, 17);
INSERT INTO `t_user_dept` VALUES (50, 144, 17);

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role`  (
  `USER_ID` bigint(20) NOT NULL COMMENT '用户ID',
  `ROLE_ID` bigint(20) NOT NULL COMMENT '角色ID'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
INSERT INTO `t_user_role` VALUES (12, 81);
INSERT INTO `t_user_role` VALUES (15, 82);
INSERT INTO `t_user_role` VALUES (8, 1);
INSERT INTO `t_user_role` VALUES (17, 82);
INSERT INTO `t_user_role` VALUES (14, 81);
INSERT INTO `t_user_role` VALUES (109, 82);
INSERT INTO `t_user_role` VALUES (140, 82);
INSERT INTO `t_user_role` VALUES (142, 82);
INSERT INTO `t_user_role` VALUES (146, 82);
INSERT INTO `t_user_role` VALUES (147, 82);

SET FOREIGN_KEY_CHECKS = 1;
