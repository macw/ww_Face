package cc.mrbird.febs.common.baidu.Constant;

import com.baidu.aip.face.AipFace;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 常量类，读取配置文件application.yml中的配置
 */
@Component
public class BaiduConstant implements InitializingBean {

    @Value("${scanconf.appId}")
    private String appId;

    @Value("${scanconf.apiKey}")
    private String apiKey;

    @Value("${scanconf.secretkey}")
    private String secretkey;

    private static AipFace client;

    public static String APP_ID;
    public static String API_KEY;
    public static String SECRET_KEY;

    @Override
    public void afterPropertiesSet() throws Exception {
        APP_ID = appId;
        API_KEY = apiKey;
        SECRET_KEY = secretkey;
    }

}
