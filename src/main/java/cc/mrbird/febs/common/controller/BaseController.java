package cc.mrbird.febs.common.controller;

import cc.mrbird.febs.common.entity.FebsConstant;
import cc.mrbird.febs.common.oss.OSSConstant;
import cc.mrbird.febs.common.oss.OssConfig;
import cc.mrbird.febs.common.utils.FileUploadUtil;
import cc.mrbird.febs.system.entity.User;
import com.aliyun.oss.OSS;
import com.aliyun.oss.model.OSSObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cc.mrbird.febs.common.oss.OssConfig.*;

/**
 * @author MrBird
 */
public class BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    private OSS ossClient = getOssClient();

    public static final int USE_INFO = 1;

    public static final int FAIL = 0;

    private Subject getSubject() {
        return SecurityUtils.getSubject();
    }

    /**
     * 获取当前登录用户
     *
     * @return
     */
    protected User getCurrentUser() {
        return (User) getSubject().getPrincipal();
    }

    protected Session getSession() {
        return getSubject().getSession();
    }

    protected Session getSession(Boolean flag) {
        return getSubject().getSession(flag);
    }

    protected void login(AuthenticationToken token) {
        getSubject().login(token);
    }

    protected Map<String, Object> getDataTable(IPage<?> pageInfo) {
        return getDataTable(pageInfo, FebsConstant.DATA_MAP_INITIAL_CAPACITY);
    }


    public HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
    }

    public HttpServletResponse getResponse() {
        return ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getResponse();
    }

    /**
     * 转换初始大小为常见单位
     *
     * @param size
     * @return
     */
    public static String getSize(long size) {
        final long unit = 1024;
        java.text.DecimalFormat df = new java.text.DecimalFormat("#.##");

        String[] units = {"B", "KB", "MB", "GB", "TB"};
        for (int i = 0; i < units.length; i++) {
            double d = getNumSize(size, i);
            if (d < unit) {
                return df.format(d) + units[i];
            }
        }
        return null;
    }

    //单位换算
    private static double getNumSize(long size, int n) {
        double d = size / Math.pow(1024, n);
        return d;
    }

    protected Map<String, Object> getDataTable(IPage<?> pageInfo, int dataMapInitialCapacity) {
        Map<String, Object> data = new HashMap<>(dataMapInitialCapacity);
        data.put("rows", pageInfo.getRecords());
        data.put("total", pageInfo.getTotal());
        return data;
    }

    protected Map<String, Object> getLayuiDataTable(IPage<?> pageInfo) {
        return getLayuiDataTable(pageInfo, FebsConstant.DATA_MAP_INITIAL_CAPACITY);
    }

    /**
     * 返回Layui格式要求的table数据格式
     * @return
     */
    protected Map<String, Object> getLayuiDataTable(IPage<?> pageInfo, int dataMapInitialCapacity) {
        Map<String, Object> data = new HashMap<>(dataMapInitialCapacity);
        data.put("data", pageInfo.getRecords());
        data.put("count", pageInfo.getTotal());
        data.put("code", 0);
        return data;
    }

    /**
     * 返回Layui格式要求的table数据格式
     *
     * @param list
     * @return
     */
    protected Map<String, Object> getLayuiTable(List<?> list) {
        Map<String, Object> data = new HashMap<>(16);
        data.put("data", list);
        data.put("count", list.size());
        data.put("code", 0);
        return data;
    }







}
