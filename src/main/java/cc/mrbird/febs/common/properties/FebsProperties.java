package cc.mrbird.febs.common.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

/**
 * @author MrBird
 */
@Data
@SpringBootConfiguration
@PropertySource(value = {"classpath:febs.properties"})
@ConfigurationProperties(prefix = "febs")
public class FebsProperties {

    private ShiroProperties shiro = new ShiroProperties();
    private boolean autoOpenBrowser = true;
    private SwaggerProperties swagger = new SwaggerProperties();

    private int maxBatchInsertNum = 1000;

    private ValidateCodeProperties code = new ValidateCodeProperties();

    private OssProperties oss = new OssProperties();

  /*  @Value("${febs.oss.bucketName}")
    public String bucketName;

    @Value("${febs.oss.endpoint}")
    private String endpoint;

    @Value("${febs.oss.accessKeyId}")
    private String accessKeyId;

    @Value("${febs.oss.accessKeySecret}")
    private String accessKeySecret;*/



}
