package cc.mrbird.febs.common.service;

import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.oss.OSSConstant;
import com.aliyun.oss.OSS;
import com.aliyun.oss.model.OSSObject;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;

import static cc.mrbird.febs.common.oss.OssConfig.*;

@Service
public class OssService extends BaseController {


    public String aliOSSUpload(MultipartFile file, String basePath) throws IOException {
        String upload = upload(file,basePath);
        System.out.println(upload);
        return upload;
    }

    /**
     * 从阿里云下载单个文件
     *
     * @param objectName
     * @return
     */
    public void downloadOss(String objectName) {
        BufferedInputStream input = null;
        OutputStream outputStream = null;
        OSSObject ossObject = null;
        HttpServletResponse response = getResponse();
        try {
            response.reset();
            response.setCharacterEncoding("utf-8");
            response.setContentType("application/x-msdownload");
            System.out.println(objectName);
            String substring = objectName.substring(objectName.lastIndexOf("/") + 1);
            response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(substring, "UTF-8"));
            OSS ossClient = getOssClient();
            // ossObject包含文件所在的存储空间名称、文件名称、文件元信息以及一个输入流。
            ossObject = ossClient.getObject(OSSConstant.BUCKET_NAME, objectName);
            input = new BufferedInputStream(ossObject.getObjectContent());
            byte[] buffBytes = new byte[1024];
            outputStream = response.getOutputStream();
            int read = 0;
            while ((read = input.read(buffBytes)) != -1) {
                outputStream.write(buffBytes, 0, read);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ossObject != null) {
                    ossObject.close();
                }
                if (outputStream != null) {
                    outputStream.flush();
                    outputStream.close();
                }
                if (input != null) {
                    input.close();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    /**
     * 从阿里云OSS批量下载文件，并压缩成zip
     *
     * @param objectNames
     * @param zipName
     */
    public void aliOssDownloadToZip(List<String> objectNames, String zipName) {
        try {
            fileToZip(objectNames, zipName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*
    public String aliOSSUpload(MultipartFile[] file, String basePath) throws IOException {
        String fileName = FileUploadUtil.getFileName(file);
        basePath = FileUploadUtil.getFiledir(basePath);
        File files = FileUploadUtil.MultipartFileConverFile(file);
        new OssConfig().uploadByFile(ossClient, files, OSSConstant.BUCKET_NAME, basePath + fileName);
        return OSSConstant.FILE_HOST + basePath + fileName;
    }
*/


}
