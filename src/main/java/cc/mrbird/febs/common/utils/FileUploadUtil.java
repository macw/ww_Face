package cc.mrbird.febs.common.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class FileUploadUtil {

    /**
     * MultipartFile file转File
     * @param file
     * @return
     * @throws IOException
     */
    public static File MultipartFileConverFile(MultipartFile file) throws IOException {
        String fileName = file.getOriginalFilename();
        String prefix = fileName.substring(fileName.lastIndexOf("."));
        // 用uuid作为文件名，防止生成的临时文件重复
        File excelFile = File.createTempFile(UUIDUtil.getUUID(), prefix);
        // MultipartFile to File
        file.transferTo(excelFile);
        return excelFile;
    }


}
