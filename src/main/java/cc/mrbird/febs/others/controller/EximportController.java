package cc.mrbird.febs.others.controller;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.exception.FebsException;
import cc.mrbird.febs.common.utils.MD5Util;
import cc.mrbird.febs.others.entity.Eximport;
import cc.mrbird.febs.others.entity.UserImport;
import cc.mrbird.febs.others.service.IEximportService;
import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.wuwenze.poi.ExcelKit;
import com.wuwenze.poi.handler.ExcelReadHandler;
import com.wuwenze.poi.pojo.ExcelErrorField;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.keyvalue.core.event.KeyValueEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

/**
 * @author MrBird
 */
@Slf4j
@RestController
@RequestMapping("eximport")
public class EximportController extends BaseController {

    @Autowired
    private IEximportService eximportService;

    @Resource
    private IUserService userService;

    @Resource
    private IDeptService deptService;

    @GetMapping
    @RequiresPermissions("others:eximport:view")
    public FebsResponse findEximports(QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(eximportService.findEximports(request, null));
        return new FebsResponse().success().data(dataTable);
    }

    /**
     * 生成 Excel导入模板
     */
    @GetMapping("template")
    @RequiresPermissions("eximport:template")
    public void generateImportTemplate(HttpServletResponse response) {
        List<UserImport> userList = new ArrayList<>();
        UserImport userImport = new UserImport();
        userImport.setUsername("student001");
        userImport.setCollege("机电学院");
        userImport.setYears("2015");
        userImport.setCourse("电子信息工程1301");
        userImport.setEmail("105111222@qq.com");
        userImport.setIdNumber("20152222456");
        userImport.setMobile("13512341234");
        userImport.setSex("男");
        userImport.setDescription("导入学生测试，正式导入请删除这一行");
        userList.add(userImport);
        // 构建模板
        ExcelKit.$Export(UserImport.class, response).downXlsx(userList, true);
    }

    /**
     * 导入Excel数据，并批量插入 T_EXIMPORT表
     */
    @PostMapping("import")
    @RequiresPermissions("eximport:import")
    @ControllerEndpoint(exceptionMessage = "导入Excel数据失败")
    public FebsResponse importExcels(MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            throw new FebsException("导入数据为空");
        }
        String filename = file.getOriginalFilename();
        if (!StringUtils.endsWith(filename, ".xlsx")) {
            throw new FebsException("只支持.xlsx类型文件导入");
        }
        // 开始导入操作
        Stopwatch stopwatch = Stopwatch.createStarted();
        List<User> data = new ArrayList();
        final List<Map<String, Object>> error = Lists.newArrayList();
        ExcelKit.$Import(UserImport.class).readXlsx(file.getInputStream(), new ExcelReadHandler<UserImport>() {
            @Override
            public void onSuccess(int sheet, int row, UserImport userImport) {
                // 数据校验成功时，加入集合
                User user = new User();
                user.setCreateTime(new Date());
                user.setUsername(userImport.getUsername());
                user.setPassword(MD5Util.encrypt(userImport.getUsername(), User.DEFAULT_PASSWORD));
                user.setSex(userImport.getSex());
                user.setStatus(User.STATUS_VALID);
                user.setEmail(userImport.getEmail());
                user.setMobile(userImport.getMobile());
                user.setIdNumber(userImport.getIdNumber());
                user.setDescription(userImport.getDescription());
                user.setDeptName(userImport.getCourse());
                System.out.println(userImport);
                data.add(user);
            }

            @Override
            public void onError(int sheet, int row, List<ExcelErrorField> errorFields) {
                // 数据校验失败时，记录到 error集合
                error.add(ImmutableMap.of("row", row, "errorFields", errorFields));
            }
        });
        if (CollectionUtils.isNotEmpty(data)) {
            // 将合法的记录批量入库
//            this.eximportService.batchInsert();
            System.out.println(data);
            for (User user : data) {
                Dept dept = deptService.getOne(new QueryWrapper<Dept>().lambda().eq(Dept::getDeptName, user.getDeptName()));
                if (dept!=null){
                    user.setDeptId(dept.getDeptId());
                }
            }
            boolean b = userService.saveBatch(data);
            System.out.println(b);
        }
        ImmutableMap<String, Object> result = ImmutableMap.of(
                "time", stopwatch.stop().toString(),
                "data", data,
                "error", error
        );
        return new FebsResponse().success().data(result);
    }

    @GetMapping("excel")
    @RequiresPermissions("eximport:export")
    @ControllerEndpoint(exceptionMessage = "导出Excel失败")
    public void export(QueryRequest queryRequest, Eximport eximport, HttpServletResponse response) {
        List<Eximport> eximports = this.eximportService.findEximports(queryRequest, eximport).getRecords();
        ExcelKit.$Export(Eximport.class, response).downXlsx(eximports, false);
    }
}
