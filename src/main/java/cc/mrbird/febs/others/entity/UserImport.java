package cc.mrbird.febs.others.entity;

import cc.mrbird.febs.common.annotation.IsMobile;
import cc.mrbird.febs.common.converter.TimeConverter;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.wuwenze.poi.annotation.Excel;
import com.wuwenze.poi.annotation.ExcelField;
import com.wuwenze.poi.config.Options;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Data
@TableName("t_user")
@Excel("导入用户数据")
public class UserImport   {

    /**
     * 用户名
     */
    @TableField("USERNAME")
    @Size(min = 4, max = 10, message = "{range}")
    @ExcelField(value = "用户名")
    private String username;

    /**
     * 学号/工号
     */
    @ExcelField(value = "学号/工号")
    private String idNumber;

    @ExcelField(
            value = "学院",//
            required = true,//
            comment = "请填写学院名称"
    )
    private String college;

    @ExcelField(//
            value = "年级",//
            required = true,//
            comment = "请填写学院名称"
    )
    private String years;

    @ExcelField(//
            value = "专业班级",//
            required = true,//
            comment = "请填写专业班级"
    )
    private String course;

    /**
     * 邮箱
     */
    @Size(max = 50, message = "{noMoreThan}")
    @Email(message = "{email}")
    @ExcelField(value = "邮箱")
    private String email;

    /**
     * 联系电话
     */
    @TableField("MOBILE")
    @IsMobile(message = "{mobile}")
    @ExcelField(value = "联系电话")
    private String mobile;


    /**
     * 性别 0男 1女 2 保密
     */
    @ExcelField(value = "性别",  readConverterExp = "未知=2,男=0,女=1", writeConverterExp = "0=男,1=女,2=保密")
    private String sex;

    /**
     * 描述
     */
    @TableField("DESCRIPTION")
    @Size(max = 100, message = "{noMoreThan}")
    @ExcelField(value = "个人描述")
    private String description;

}
