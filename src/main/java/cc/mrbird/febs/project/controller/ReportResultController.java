package cc.mrbird.febs.project.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

/**
 * <p>
 * 报告结果表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
@RestController
@RequestMapping("/report-result")
public class ReportResultController extends BaseController {

}
