package cc.mrbird.febs.project.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

/**
 * <p>
 * 表2-2  放大电路的放大倍数（f=1kHz） 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
@RestController
@RequestMapping("/test2-enlarge")
public class Test2EnlargeController extends BaseController {

}
