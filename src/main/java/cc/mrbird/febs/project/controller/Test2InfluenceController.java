package cc.mrbird.febs.project.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

/**
 * <p>
 * 表2-3  静态工作点对输出电压波形的影响 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
@RestController
@RequestMapping("/test2-influence")
public class Test2InfluenceController extends BaseController {

}
