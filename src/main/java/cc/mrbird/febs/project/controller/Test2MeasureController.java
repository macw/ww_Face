package cc.mrbird.febs.project.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

/**
 * <p>
 * 表2-4  输入与输出电阻的测量 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
@RestController
@RequestMapping("/test2-measure")
public class Test2MeasureController extends BaseController {

}
