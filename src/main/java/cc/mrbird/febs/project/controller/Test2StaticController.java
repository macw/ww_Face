package cc.mrbird.febs.project.controller;


import cc.mrbird.febs.common.entity.FebsConstant;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.FebsUtil;
import cc.mrbird.febs.project.entity.Test2Static;
import cc.mrbird.febs.project.service.ITest2StaticService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 表2-1  放大电路的静态工作点 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
@RestController
@RequestMapping("/test2Static")
public class Test2StaticController extends BaseController {

    @Resource
    private ITest2StaticService test2StaticService;

    @RequestMapping("listPage")
    public FebsResponse listPage(){
        List<Test2Static> list = test2StaticService.list();
        FebsResponse data = new FebsResponse();
        data.put("data", list);
        data.put("count", list.size());
        data.put("code", 0);
        return data;
    }


}
