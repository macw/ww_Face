package cc.mrbird.febs.project.controller;


import cc.mrbird.febs.common.utils.FebsUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("febs/views/")
public class ViewController {

    @GetMapping("project/test2")
    public String projectTest2() {
        return FebsUtil.view("project/testTwo/view");
    }


}
