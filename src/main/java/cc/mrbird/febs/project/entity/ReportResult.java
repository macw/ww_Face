package cc.mrbird.febs.project.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 报告结果表
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("p_report_result")
public class ReportResult implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 报告结果表ID
     */
    private Integer reportResutlId;

    /**
     * 测试数据
     */
    private String testData;

    /**
     * 理论分析
     */
    private String analysis;

    /**
     * 电路影响
     */
    private String influence;

    /**
     * 问题反馈
     */
    private String feedback;

    /**
     * 问题解答
     */
    private String answer;

    /**
     * 教师指导
     */
    private String guide;

    /**
     * 系统评分
     */
    private Double systemScore;

    /**
     * 最终成绩
     */
    private Double finalScore;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 所属实验报告ID
     */
    private Integer testId;


}
