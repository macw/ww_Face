package cc.mrbird.febs.project.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 表2-2  放大电路的放大倍数（f=1kHz）
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("p_test2_enlarge")
public class Test2Enlarge implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表2-2  放大电路的放大倍数（f=1kHz）
     */
    @TableId(value = "enlarge_id", type = IdType.AUTO)
    private Integer enlargeId;

    /**
     * 测试条件
     */
    private String testCondition;

    /**
     * 测量值Ui(mV)
     */
    private Double ui;

    /**
     * 测量值Uo(mV)
     */
    private Double uo;

    /**
     * 测量值Uo波形
     */
    private String uoUrl;

    /**
     * 计算值Au= uo/ Ui
     */
    private Double au;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 所属实验报告ID
     */
    private Integer reportId;


}
