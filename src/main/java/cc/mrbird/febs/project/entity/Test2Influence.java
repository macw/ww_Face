package cc.mrbird.febs.project.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 表2-3  静态工作点对输出电压波形的影响
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("p_test2_influence")
public class Test2Influence implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 静态工作点对输出电压波形的影响
     */
    @TableId(value = "influence_id", type = IdType.AUTO)
    private Integer influenceId;

    /**
     * 测试条件
     */
    private String testCondition;

    /**
     * 测量值Uce
     */
    private Double uce;

    /**
     * 输出电压波形
     */
    private String uceUrl;

    /**
     * 波形失真类型
     */
    private String uceType;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 所属实验报告ID
     */
    private Integer reportId;


}
