package cc.mrbird.febs.project.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 表2-4  输入与输出电阻的测量
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("p_test2_measure")
public class Test2Measure implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 输入与输出电阻的测量
     */
    @TableId(value = "measure_id", type = IdType.AUTO)
    private Integer measureId;

    /**
     * 测量值
     */
    private Double us;

    /**
     * 测量值
     */
    private Double ui;

    /**
     * 测量值ri（KΩ）
     */
    private Double ricl;

    /**
     * 理论值ri（KΩ）
     */
    private Double rill;

    /**
     * 测量值
     */
    private Double uo;

    /**
     * 测量值
     */
    private Double ul;

    /**
     * 测量值ro（KΩ）
     */
    private Double rocl;

    /**
     * 理论值ro（KΩ）
     */
    private Double roll;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 所属实验报告ID
     */
    private Integer reportId;


}
