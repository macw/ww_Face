package cc.mrbird.febs.project.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 表2-1  放大电路的静态工作点
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("p_test2_static")
public class Test2Static implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表2-1  放大电路的静态工作点
     */
    @TableId(value = "static_id", type = IdType.AUTO)
    private Integer staticId;

    /**
     * 测试条件
     */
    private String testCondition;

    /**
     * 测量值
     */
    private Double ib;

    /**
     * 测量值
     */
    private Double ic;

    /**
     * 测量值
     */
    private Double ub;

    /**
     * 测量值
     */
    private Double ue;

    /**
     * 测量值
     */
    private Double uc;

    /**
     * 测量值
     */
    private Double bb;

    /**
     * 计算值
     */
    private Double ube;

    /**
     * 计算值
     */
    private Double uce;

    /**
     * 计算值
     */
    private Double bb2;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 所属实验报告ID
     */
    private Integer reportId;


}
