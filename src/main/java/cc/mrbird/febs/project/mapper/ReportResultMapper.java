package cc.mrbird.febs.project.mapper;

import cc.mrbird.febs.project.entity.ReportResult;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 报告结果表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
public interface ReportResultMapper extends BaseMapper<ReportResult> {

}
