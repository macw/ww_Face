package cc.mrbird.febs.project.mapper;

import cc.mrbird.febs.project.entity.Test2Enlarge;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 表2-2  放大电路的放大倍数（f=1kHz） Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
public interface Test2EnlargeMapper extends BaseMapper<Test2Enlarge> {

}
