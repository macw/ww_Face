package cc.mrbird.febs.project.mapper;

import cc.mrbird.febs.project.entity.Test2Influence;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 表2-3  静态工作点对输出电压波形的影响 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
public interface Test2InfluenceMapper extends BaseMapper<Test2Influence> {

}
