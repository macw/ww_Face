package cc.mrbird.febs.project.mapper;

import cc.mrbird.febs.project.entity.Test2Measure;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 表2-4  输入与输出电阻的测量 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
public interface Test2MeasureMapper extends BaseMapper<Test2Measure> {

}
