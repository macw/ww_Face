package cc.mrbird.febs.project.mapper;

import cc.mrbird.febs.project.entity.Test2Static;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 表2-1  放大电路的静态工作点 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
public interface Test2StaticMapper extends BaseMapper<Test2Static> {

}
