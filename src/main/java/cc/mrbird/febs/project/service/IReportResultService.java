package cc.mrbird.febs.project.service;

import cc.mrbird.febs.project.entity.ReportResult;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 报告结果表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
public interface IReportResultService extends IService<ReportResult> {

}
