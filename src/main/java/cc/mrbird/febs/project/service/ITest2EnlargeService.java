package cc.mrbird.febs.project.service;

import cc.mrbird.febs.project.entity.Test2Enlarge;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 表2-2  放大电路的放大倍数（f=1kHz） 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
public interface ITest2EnlargeService extends IService<Test2Enlarge> {

}
