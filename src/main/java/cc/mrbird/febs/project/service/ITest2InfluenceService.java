package cc.mrbird.febs.project.service;

import cc.mrbird.febs.project.entity.Test2Influence;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 表2-3  静态工作点对输出电压波形的影响 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
public interface ITest2InfluenceService extends IService<Test2Influence> {

}
