package cc.mrbird.febs.project.service;

import cc.mrbird.febs.project.entity.Test2Measure;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 表2-4  输入与输出电阻的测量 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
public interface ITest2MeasureService extends IService<Test2Measure> {

}
