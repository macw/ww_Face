package cc.mrbird.febs.project.service;

import cc.mrbird.febs.project.entity.Test2Static;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 表2-1  放大电路的静态工作点 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
public interface ITest2StaticService extends IService<Test2Static> {

}
