package cc.mrbird.febs.project.service.impl;

import cc.mrbird.febs.project.entity.ReportResult;
import cc.mrbird.febs.project.mapper.ReportResultMapper;
import cc.mrbird.febs.project.service.IReportResultService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 报告结果表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
@Service
public class ReportResultServiceImpl extends ServiceImpl<ReportResultMapper, ReportResult> implements IReportResultService {

}
