package cc.mrbird.febs.project.service.impl;

import cc.mrbird.febs.project.entity.Test2Enlarge;
import cc.mrbird.febs.project.mapper.Test2EnlargeMapper;
import cc.mrbird.febs.project.service.ITest2EnlargeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 表2-2  放大电路的放大倍数（f=1kHz） 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
@Service
public class Test2EnlargeServiceImpl extends ServiceImpl<Test2EnlargeMapper, Test2Enlarge> implements ITest2EnlargeService {

}
