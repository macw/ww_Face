package cc.mrbird.febs.project.service.impl;

import cc.mrbird.febs.project.entity.Test2Influence;
import cc.mrbird.febs.project.mapper.Test2InfluenceMapper;
import cc.mrbird.febs.project.service.ITest2InfluenceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 表2-3  静态工作点对输出电压波形的影响 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
@Service
public class Test2InfluenceServiceImpl extends ServiceImpl<Test2InfluenceMapper, Test2Influence> implements ITest2InfluenceService {

}
