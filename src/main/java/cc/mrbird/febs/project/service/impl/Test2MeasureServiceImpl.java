package cc.mrbird.febs.project.service.impl;

import cc.mrbird.febs.project.entity.Test2Measure;
import cc.mrbird.febs.project.mapper.Test2MeasureMapper;
import cc.mrbird.febs.project.service.ITest2MeasureService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 表2-4  输入与输出电阻的测量 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
@Service
public class Test2MeasureServiceImpl extends ServiceImpl<Test2MeasureMapper, Test2Measure> implements ITest2MeasureService {

}
