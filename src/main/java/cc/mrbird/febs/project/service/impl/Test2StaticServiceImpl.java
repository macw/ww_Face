package cc.mrbird.febs.project.service.impl;

import cc.mrbird.febs.project.entity.Test2Static;
import cc.mrbird.febs.project.mapper.Test2StaticMapper;
import cc.mrbird.febs.project.service.ITest2StaticService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 表2-1  放大电路的静态工作点 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-03-31
 */
@Service
public class Test2StaticServiceImpl extends ServiceImpl<Test2StaticMapper, Test2Static> implements ITest2StaticService {

}
