package cc.mrbird.febs.student.controller;


import cc.mrbird.febs.common.entity.FebsConstant;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.DateUtil;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.student.entity.Attendance;
import cc.mrbird.febs.student.service.IAttendanceService;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IUserScanService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

/**
 * <p>
 * 考勤出席表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-04-18
 */
@RestController
@RequestMapping("attendance")
public class AttendanceController extends BaseController {

    @Resource
    private IAttendanceService attendanceService;

    @Resource
    private IDeptService deptService;

    @Resource
    private IUserScanService userScanService;

    /**
     * 首先进行人脸匹配验证，匹配后再添加入库
     *
     * @param attendance
     * @param file
     * @return
     */
    @RequestMapping("/add")
    public FebsResponse addAttendance(Attendance attendance, @RequestParam(name = "file", required = true) MultipartFile file) {
        User currentUser = getCurrentUser();
        //进行人脸验证是否匹配
        FebsResponse febsResponse = userScanService.compareFace(file, currentUser.getUserId().toString()).success();
        Integer code = (Integer) febsResponse.get("code");
        if (code == 200) {
            attendance.setCreateTime(LocalDateTime.now());
            if (IntegerUtils.isLongNotBlank(currentUser.getDeptId())) {
                attendance.setDeptId(currentUser.getDeptId().intValue());
                attendance.setDeptName(deptService.getById(currentUser.getDeptId()).getDeptName());
            }
            attendance.setUserId(currentUser.getUserId().intValue());
            attendance.setUsername(currentUser.getUsername());
            boolean save = attendanceService.save(attendance);
            if (save) {
                return febsResponse;
            }
            return febsResponse.fail().message("系统异常！打卡失败~");
        }
        return febsResponse;
    }


    @GetMapping("list")
    @RequiresPermissions("student:count")
    public FebsResponse attendanceList(Attendance attendance, QueryRequest request) {
        User currentUser = getCurrentUser();
        Page<Attendance> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "create_time", FebsConstant.ORDER_ASC, false);
        IPage<Attendance> iPage = attendanceService.page(page, new QueryWrapper<Attendance>().lambda()
                .eq(attendance.getStatus()!=null, Attendance::getStatus, attendance.getStatus())
                .eq(attendance.getType()!=null, Attendance::getType, attendance.getType())
                .eq(Attendance::getUserId, currentUser.getUserId()));
        Map<String, Object> dataTable = getDataTable(iPage);
        return new FebsResponse().success().data(dataTable);
    }

    /**
     * 请假
     * @return
     */
    @RequestMapping("/suppose")
    public FebsResponse suppose(Attendance attendance,String createTimes){
        User currentUser = getCurrentUser();
        if (StringUtils.isNotBlank(createTimes)){
            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(createTimes, df);
            LocalDateTime localDateTime = localDate.atStartOfDay();
            attendance.setCreateTime(localDateTime);
        }
        attendance.setUserId(currentUser.getUserId().intValue());
        attendance.setStatus(Attendance.STATUS_SUPPOSE);
        attendance.setUsername(currentUser.getUsername());
        attendanceService.save(attendance);
        return new FebsResponse().success().message("请假成功！");
    }



}
