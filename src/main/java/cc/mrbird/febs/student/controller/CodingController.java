package cc.mrbird.febs.student.controller;

import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.service.FaceService;
import cc.mrbird.febs.common.service.OssService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;

/**
 * dk
 * @author Administrator
 */
@RestController
@RequestMapping("/face")
public class  CodingController extends BaseController {

    @Resource
    private FaceService faceService;

    @Resource
    private OssService ossService;

    @RequestMapping("/faceMatch")
    public void faceMatch(String filePath){
        File file = new File(filePath);
        faceService.faceMatch(filePath);
    }

    @RequestMapping("/getFile")
    public String getFile(MultipartFile file){
        if (file != null && file.getSize() > 0) {
            //获取上传文件的路径
            String basePath = "project";
            String url ="";
            try {
                //调用阿里云OSS的上传方法
                url = ossService.aliOSSUpload(file, basePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("-------"+url);
            return url;
        }
        return null;
    }

}
