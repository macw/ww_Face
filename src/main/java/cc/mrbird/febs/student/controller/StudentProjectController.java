package cc.mrbird.febs.student.controller;

import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsConstant;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.service.OssService;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.ICourseDeptService;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IUserService;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author admin
 */
@RestController
@RequestMapping("studentProject")
public class StudentProjectController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private ICourseDeptService courseDeptService;

    @Resource
    private IUserService userService;

    @Resource
    private IExperimentProjectService experimentProjectService;

    @Resource
    private ITeacherProjectService teacherProjectService;

    @Resource
    private OssService ossService;

    @RequestMapping("/list")
    public FebsResponse studentProjectList(Integer courseId,ExperimentProject experimentProject, QueryRequest request){
        //获取当前登录用户
        User currentUser = getCurrentUser();
        //获取当前登录的老师用户信息
        User one = userService.getOne(new QueryWrapper<User>().lambda().eq(User::getCourseId, courseId));
        //查询当前登录用户老师开启的全部项目
        List<TeacherProject> teacherProjectList = teacherProjectService.list(new QueryWrapper<TeacherProject>().lambda()
                .eq(TeacherProject::getDeptId, currentUser.getDeptId())
                .eq(TeacherProject::getCourseId, courseId)
                .eq(TeacherProject::getTeacherId, one.getUserId()));
        //创建分页对象
        Page<ExperimentProject> page = new Page<>(request.getPageNum(), request.getPageSize());
        //创建排序处理器
        SortUtil.handlePageSort(request, page, "project_id", FebsConstant.ORDER_ASC, false);
        //创建分页条件
        IPage<ExperimentProject> iPage = this.experimentProjectService.page(page, new QueryWrapper<ExperimentProject>().lambda()
                //课程号
                .eq(ExperimentProject::getCourseId,courseId)
                //开启的项目列表
                .in(ExperimentProject::getProjectId,teacherProjectList.stream().map(TeacherProject::getProjectId).collect(Collectors.toList()))
                //根据项目名的查询条件
                .like(StringUtils.isNotBlank(experimentProject.getProjectName()), ExperimentProject::getProjectName, experimentProject.getProjectName())
                //根据项目的状态条件查询
                .like(experimentProject.getState() != null,ExperimentProject::getState,experimentProject.getState()));
        //封装返回结果集
        Map<String, Object> dataTable = getDataTable(iPage);
        return new FebsResponse().success().data(dataTable);
    }

    @RequestMapping("/downloadReport")
    public FebsResponse downloadReport(String url){
        ossService.downloadOss(url);
        return new FebsResponse().success();
    }


}
