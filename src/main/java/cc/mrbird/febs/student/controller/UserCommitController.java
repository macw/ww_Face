package cc.mrbird.febs.student.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.service.OssService;
import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.service.IUserCommitService;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.time.LocalDateTime;

/**
 * <p>
 * 学生报告提交记录表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-04-02
 */
@RestController
@RequestMapping("/userCommit")
public class UserCommitController extends BaseController {

    @Autowired
    private IUserCommitService userCommitService;

    @Resource
    private OssService ossService;

    @RequestMapping("/upload")
    @ControllerEndpoint(operation = "上传文件成功", exceptionMessage = "上传文件失败")
    public FebsResponse upload(@RequestParam("file") MultipartFile file, TeacherProject teacherProject) {
        User currentUser = getCurrentUser();
        UserCommit userCommit = new UserCommit();
        UserCommit bool = userCommitService.getOne(new QueryWrapper<UserCommit>().lambda().eq(UserCommit::getStuId, currentUser.getUserId()).eq(UserCommit::getProjectId, teacherProject.getProjectId()));
        String url = "";
        userCommit.setCreateTime(LocalDateTime.now());
        userCommit.setDeptId(currentUser.getDeptId().intValue());
        userCommit.setIsCommit(BaseController.USE_INFO);
        userCommit.setStuId(currentUser.getUserId().intValue());
        userCommit.setTeacherId(teacherProject.getTeacherId().intValue());
        userCommit.setProjectId(teacherProject.getProjectId());
        userCommit.setCourseId(teacherProject.getCourseId());
        String basePath = "studentProject";
        if (bool == null && file != null && file.getSize() > 0) {
            try {
                //调用阿里云OSS的上传方法
                url = ossService.aliOSSUpload(file, basePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //将文件大小，和文件上传后返回的路径，入库
            userCommit.setReportUrl(url);
            if (userCommitService.save(userCommit)) {
                return new FebsResponse().success().message("上传成功！");
            } else {
                return new FebsResponse().fail();
            }
        }/*else {
                try{
                    url = aliOSSUpload(file, basePath);
                    userCommit.setReportUrl(url);
                    userCommit.setCommitId(bool.getCommitId());
                    userCommitService.updateById(userCommit);
                    return new FebsResponse().success().message("上传成功！");
                }catch (Exception e){
                    e.printStackTrace();
                    return new FebsResponse().fail().message("上传失败！");
                }
            }*/
        return new FebsResponse().fail().message("每人只能上传一次实验附件");
    }



}



