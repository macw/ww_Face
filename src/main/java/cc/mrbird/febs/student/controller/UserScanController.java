package cc.mrbird.febs.student.controller;


import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IUserScanService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 扫脸库用户信息表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2019-12-18
 */
@RestController
@RequestMapping("/scan/face")
@Api(value = "UserScanController",tags = "扫脸用户录入")
public class UserScanController extends BaseController {

    @Autowired
    private IUserScanService userScanService;

    /**
     * 人脸注册
     * @param file 文件
     * @return RestResponse
     * @author Macw
     * @since 2019-12-18
     */
    @PostMapping(value = "/register", produces = "application/json; charset=utf-8")
    @ApiOperation(value = "人脸注册", notes = "人脸注册", code = 200, produces = "application/json")
    public FebsResponse resisterFace(@RequestParam(name = "file", required = true) MultipartFile file) {
        User currentUser = getCurrentUser();
        String url = userScanService.resisterFace(file, currentUser);
        return new FebsResponse().success().message("人像注册成功！").data(url);
    }

    /**
     * 人脸对比
     * @param file 文件
     * @return RestResponse
     * @author 马超伟
     * @since 2020-03-18
     */
    @PostMapping(value = "/compare", produces = "application/json; charset=utf-8")
    @ApiOperation(value = "人脸对比", notes = "人脸对比", code = 200, produces = "application/json")
    public FebsResponse compareFace(@RequestParam(name = "file", required = true) MultipartFile file) {
        User currentUser = getCurrentUser();
        FebsResponse febsResponse = userScanService.compareFace(file, currentUser.getUserId().toString());
        return febsResponse;
    }

}
