package cc.mrbird.febs.student.controller;

import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsConstant;
import cc.mrbird.febs.common.utils.DateUtil;
import cc.mrbird.febs.common.utils.FebsUtil;
import cc.mrbird.febs.student.entity.Attendance;
import cc.mrbird.febs.student.service.IAttendanceService;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IUserService;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author admin
 */
@RequestMapping(FebsConstant.VIEW_PREFIX)
@Controller("studentView")
public class ViewController extends BaseController {

    @Autowired
    private IExperimentProjectService experimentProjectService;

    @Autowired
    private ITeacherProjectService teacherProjectService;

    @Resource
    private IUserService userService;

    @Resource
    private IAttendanceService attendanceService;

    @GetMapping("/student/getOneExper/{courseId}")
    public String getOneProgect(@PathVariable Integer courseId, Model model) {
        model.addAttribute("courseId", courseId);
        return FebsUtil.view("student/myProject");
    }

    @GetMapping("/myProjectView/{projectId}")
    public String getViewHtml(@PathVariable Integer projectId, Model model) {
        System.out.println("-----------------" + projectId);
        ExperimentProject experimentProject = experimentProjectService.getOne(new QueryWrapper<ExperimentProject>().lambda().eq(ExperimentProject::getProjectId, projectId));
        TeacherProject teacherProject = teacherProjectService.getOne(new QueryWrapper<TeacherProject>().lambda().eq(TeacherProject::getProjectId, projectId));
        experimentProject.setTeacherId(teacherProject.getTeacherId().intValue());
        model.addAttribute("data", experimentProject);
        return FebsUtil.view("student/myProjectView");
    }

    @GetMapping("/student/faceIn")
    public String faceIn(Model model) {
        User currentUser = getCurrentUser();
        currentUser = userService.getById(currentUser.getUserId());
        if (StringUtils.isNotBlank(currentUser.getFaceToken())){
            model.addAttribute("face","1");
        }else {
            model.addAttribute("face", "0");
        }
        return FebsUtil.view("student/face/faceIn");
    }

    @GetMapping("/student/faceMatch")
    public String faceMatch(Model model) {
        User currentUser = getCurrentUser();
        Attendance attendance = attendanceService.getOne(new QueryWrapper<Attendance>()
                .lambda()
                .eq(Attendance::getUserId, currentUser.getUserId())
                .orderByDesc(Attendance::getCreateTime)
                .last("limit 0 , 1"));
        if (attendance!=null){
            String time = DateUtil.formatFullTime(attendance.getCreateTime(), DateUtil.FULL_TIME_SPLIT_PATTERN);
            model.addAttribute("time",time);
        }else {
            model.addAttribute("time"," ");
        }
        return FebsUtil.view("student/face/faceMatch");
    }

    @GetMapping("/student/count")
    public String count(Model model) {
        User currentUser = getCurrentUser();
        return FebsUtil.view("student/count/count");
    }

    /**
     * 去请假
     * @return
     */
    @GetMapping("/student/suppose")
    public String suppose() {
        return FebsUtil.view("student/suppose/suppose");
    }
}
