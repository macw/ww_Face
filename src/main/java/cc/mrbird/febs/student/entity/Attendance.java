package cc.mrbird.febs.student.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 考勤出席表
 * </p>
 *
 * @author Macw
 * @since 2020-04-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_attendance")
public class Attendance implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * //正常
     */
    public static final int STATUS_NORMAL = 1;
    /**
     * //请假
     */
    public static final int STATUS_SUPPOSE = 3;
    /**
     * //4补卡
     */
    public static final int STATUS_BUKA = 4;
    /**
     *  //缺卡
     */
    public static final int STATUS_LESS = 0;

    /**
     * 考勤、出席ID
     */
    @TableId(value = "attendance_id",type = IdType.AUTO)
    private Integer attendanceId;

    /**
     * 出席人用户名
     */
    private String username;

    /**
     * 出席人ID
     */
    private Integer userId;

    /**
     * 出席时间
     */
    private LocalDateTime createTime;

    /**
     * 所属部门ID
     */
    private Integer deptId;

    /**
     * 所属部门name
     */
    private String deptName;

    /**
     * 1：正常，0缺卡，3请假，4补卡。
     */
    private Integer status;

    /**
     * 1上班，0下班
     */
    private Integer type;

    /**
     * 维度
     */
    private String lat;

    /**
     * 经度
     */
    private String lon;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 描述
     */
    private String description;


}
