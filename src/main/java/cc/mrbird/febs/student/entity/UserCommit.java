package cc.mrbird.febs.student.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 学生报告提交记录表
 * </p>
 *
 * @author Macw
 * @since 2020-04-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_user_commit")
public class UserCommit implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 学生报告提交表
     */
    @TableId(value = "commit_id",type = IdType.AUTO)
    private Integer commitId;

    /**
     * 学生ID
     */
    private Integer stuId;

    /**
     * 学生姓名
     */
    @TableField(exist = false)
    private String stuName;

    /**
     * 学号
     */
    @TableField(exist = false)
    private String idNumber;

    /**
     * 学生班级ID
     */
    private Integer deptId;

    /**
     * 班级名称
     */
    @TableField(exist = false)
    private String deptName;

    /**
     * 学生做的项目ID
     */
    private Integer projectId;

    /**
     * 项目名称
     */
    @TableField(exist = false)
    private String projectName;

    /**
     * 学生做的课程ID
     */
    private Integer courseId;

    /**
     * 课程名称
     */
    @TableField(exist = false)
    private String courseName;

    /**
     * 项目负责人老师ID
     */
    private Integer teacherId;

    /**
     * 老师姓名
     */
    @TableField(exist = false)
    private String teacherName;

    /**
     * 学生提交的报告URL
     */
    private String reportUrl;

    /**
     * 学生成绩
     */
    private Double score;

    /**
     * 1提交，0，未提交
     */
    private Integer isCommit;

    /**
     * 提交时间
     */
    private LocalDateTime createTime;

    /**
     * 提交时间
     */
    @TableField(exist = false)
    private String createTimes;


}
