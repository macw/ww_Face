package cc.mrbird.febs.student.mapper;

import cc.mrbird.febs.student.entity.Attendance;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 考勤出席表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-04-18
 */
public interface AttendanceMapper extends BaseMapper<Attendance> {

}
