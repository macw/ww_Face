package cc.mrbird.febs.student.mapper;

import cc.mrbird.febs.student.entity.UserCommit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 学生报告提交记录表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-04-02
 */
public interface UserCommitMapper extends BaseMapper<UserCommit> {

}
