package cc.mrbird.febs.student.service;

import cc.mrbird.febs.student.entity.Attendance;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 考勤出席表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-04-18
 */
public interface IAttendanceService extends IService<Attendance> {

}
