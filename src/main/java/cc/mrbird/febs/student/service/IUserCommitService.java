package cc.mrbird.febs.student.service;

import cc.mrbird.febs.student.entity.UserCommit;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 学生报告提交记录表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-04-02
 */
public interface IUserCommitService extends IService<UserCommit> {

}
