package cc.mrbird.febs.student.service.impl;

import cc.mrbird.febs.student.entity.Attendance;
import cc.mrbird.febs.student.mapper.AttendanceMapper;
import cc.mrbird.febs.student.service.IAttendanceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 考勤出席表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-04-18
 */
@Service
public class AttendanceServiceImpl extends ServiceImpl<AttendanceMapper, Attendance> implements IAttendanceService {

}
