package cc.mrbird.febs.student.service.impl;

import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.mapper.UserCommitMapper;
import cc.mrbird.febs.student.service.IUserCommitService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 学生报告提交记录表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-04-02
 */
@Service
public class UserCommitServiceImpl extends ServiceImpl<UserCommitMapper, UserCommit> implements IUserCommitService {

}
