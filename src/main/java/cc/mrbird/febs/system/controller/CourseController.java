package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.entity.FebsConstant;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.exception.FebsException;
import cc.mrbird.febs.common.utils.FebsUtil;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.system.entity.Course;
import cc.mrbird.febs.system.entity.CourseDept;
import cc.mrbird.febs.system.entity.Experiment;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.ICourseDeptService;
import cc.mrbird.febs.system.service.ICourseService;
import cc.mrbird.febs.system.service.IDeptService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import cc.mrbird.febs.common.controller.BaseController;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.*;

/**
 * <p>
 * 课程表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-03-24
 */
@Slf4j
@Validated
@RestController
@RequestMapping("/course")
public class CourseController extends BaseController {

    @Resource
    private ICourseService courseService;

    @Resource
    private ICourseDeptService courseDeptService;

    @GetMapping("list")
    @RequiresPermissions("course:view")
    public FebsResponse courseList(Course course, QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(this.courseService.findUserDetailList(course, request));
        return new FebsResponse().success().data(dataTable);
    }

    @PostMapping("/add")
    @RequiresPermissions("course:add")
    @ControllerEndpoint(operation = "新增课程", exceptionMessage = "新增课程失败")
    public FebsResponse addExperiment(@Valid Course course, @RequestParam("deptIds[]")Integer deptIds[],@RequestParam("deptNameLists[]")String deptNameLists[]) {
        if (deptNameLists!=null && deptNameLists.length>0){
            course.setDeptNameList(StringUtils.join(deptNameLists,","));
        }
        course.setCreateTime(LocalDateTime.now());
        courseService.save(course);
        List<CourseDept> courseDeptList = new ArrayList<>();
        for (Integer deptId : deptIds) {
            CourseDept courseDept = new CourseDept();
            courseDept.setDeptId(deptId);
            courseDept.setCourseId(course.getCourseId().intValue());
            courseDeptList.add(courseDept);
        }
        courseDeptService.saveBatch(courseDeptList);
        return new FebsResponse().success();
    }

    @GetMapping("delete/{courseIds}")
    @RequiresPermissions("course:delete")
    @ControllerEndpoint(operation = "删除课程", exceptionMessage = "删除课程失败")
    public FebsResponse deleteCourses(@NotBlank(message = "{required}") @PathVariable String courseIds) {
        String[] ids = courseIds.split(StringPool.COMMA);
        List<String> list = Arrays.asList(ids);
        courseService.removeByIds(list);
        return new FebsResponse().success();
    }

    /**
     * @Auther:贵永康
     * @Date: Create in 9:21 2020/3/31
     * @Description: 修改课程信息
     */
    @PostMapping("update")
    @RequiresPermissions("course:update")
    @ControllerEndpoint(operation = "修改课程", exceptionMessage = "修改课程失败")
    public FebsResponse updateCourses(@Valid Course course) {
        if (course.getCourseId() == null) {
            throw new FebsException("用户ID为空");
        }
        course.setUpdateTime(LocalDateTime.now());
        this.courseService.updateById(course);
        return new FebsResponse().success();
    }

    /*
     *搜索框查询
     */
    @GetMapping("select/course")
    @ControllerEndpoint(exceptionMessage = "获取课程列表失败")
    public FebsResponse getTeachers(Course course, QueryRequest request) throws FebsException {
        Page<Course> page = new Page<Course>(request.getPageNum(), request.getPageSize());
        IPage<Course> iPage = this.courseService.page(page, new QueryWrapper<Course>().lambda().eq(Course::getState, USE_INFO));
        FebsResponse febsResponse = new FebsResponse();
        febsResponse.put("code",0);
        febsResponse.put("count",iPage.getTotal());
        febsResponse.put("data",iPage.getRecords());
        return febsResponse;
    }


}
