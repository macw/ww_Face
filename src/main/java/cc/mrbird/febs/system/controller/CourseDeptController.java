package cc.mrbird.febs.system.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

/**
 * <p>
 * 课程班级表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-03-26
 */
@RestController
@RequestMapping("/courseDept")
public class CourseDeptController extends BaseController {



}
