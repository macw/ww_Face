package cc.mrbird.febs.system.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.exception.FebsException;
import cc.mrbird.febs.system.entity.*;
import cc.mrbird.febs.system.service.IExperimentService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Map;

/**
 * <p>
 * 实验表 前端控制器
 * </p>
 *  暂时不用（已合并为课程表）
 * @author Macw
 * @since 2020-03-24
 */
@RestController
@RequestMapping("/experiment")
public class ExperimentController extends BaseController {

    @Resource
    private IExperimentService experimentService;

    @GetMapping("/getAll")
    public FebsResponse getAllExperiment(Experiment experiment) {
        return new FebsResponse().success().data(experimentService.list(new QueryWrapper<Experiment>().lambda().eq(Experiment::getState,USE_INFO)));
    }

    @GetMapping("list")
    @RequiresPermissions("experiment:view")
    public FebsResponse courseList(Experiment experiment, QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(this.experimentService.findList(experiment, request));
        return new FebsResponse().success().data(dataTable);
    }

    @PostMapping("/add")
    @RequiresPermissions("experiment:add")
    @ControllerEndpoint(operation = "新增实验", exceptionMessage = "新增实验失败")
    public FebsResponse addExperiment(@Valid Experiment experiment) {
        experiment.setCreateTime(LocalDateTime.now());
        this.experimentService.save(experiment);
        return new FebsResponse().success();
    }

    @GetMapping("select/experiment")
    @ControllerEndpoint(exceptionMessage = "获取实验列表失败")
    public FebsResponse getTeachers(Experiment experiment, QueryRequest request) throws FebsException {
        Page<Experiment> page = new Page<>(request.getPageNum(), request.getPageSize());
        IPage<Experiment> iPage = this.experimentService.page(page, new QueryWrapper<Experiment>().lambda().eq(Experiment::getState, USE_INFO));
        FebsResponse febsResponse = new FebsResponse();
        febsResponse.put("code",0);
        febsResponse.put("count",iPage.getTotal());
        febsResponse.put("data",iPage.getRecords());
        return febsResponse;
    }

}
