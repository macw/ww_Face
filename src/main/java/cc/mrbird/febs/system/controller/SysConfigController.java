package cc.mrbird.febs.system.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.mrbird.febs.common.controller.BaseController;

/**
 * <p>
 * 参数配置表 前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-04-15
 */
@RestController
@RequestMapping("/sys-config")
public class SysConfigController extends BaseController {

}
