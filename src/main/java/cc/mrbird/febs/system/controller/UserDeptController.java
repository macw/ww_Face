package cc.mrbird.febs.system.controller;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.utils.FebsUtil;
import cc.mrbird.febs.common.entity.FebsConstant;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.UserDept;
import cc.mrbird.febs.system.service.IUserDeptService;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 *  Controller
 *
 * @author Macw
 * @date 2020-03-23 16:04:49
 */
@Slf4j
@Validated
@Controller
public class UserDeptController extends BaseController {

    @Autowired
    private IUserDeptService userDeptService;

    @GetMapping(FebsConstant.VIEW_PREFIX + "userDept")
    public String userDeptIndex(){
        return FebsUtil.view("userDept/userDept");
    }

    @GetMapping("userDept")
    @ResponseBody
    @RequiresPermissions("userDept:list")
    public FebsResponse getAllUserDepts(UserDept userDept) {
        return new FebsResponse().success().data(userDeptService.findUserDepts(userDept));
    }

    @GetMapping("userDept/list")
    @ResponseBody
    @RequiresPermissions("userDept:list")
    public FebsResponse userDeptList(QueryRequest request, UserDept userDept) {
        Map<String, Object> dataTable = getDataTable(this.userDeptService.findUserDepts(request, userDept));
        return new FebsResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "新增UserDept", exceptionMessage = "新增UserDept失败")
    @PostMapping("userDept")
    @ResponseBody
    @RequiresPermissions("userDept:add")
    public FebsResponse addUserDept(@Valid UserDept userDept) {
        this.userDeptService.createUserDept(userDept);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "删除UserDept", exceptionMessage = "删除UserDept失败")
    @GetMapping("userDept/delete")
    @ResponseBody
    @RequiresPermissions("userDept:delete")
    public FebsResponse deleteUserDept(UserDept userDept) {
        this.userDeptService.deleteUserDept(userDept);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改UserDept", exceptionMessage = "修改UserDept失败")
    @PostMapping("userDept/update")
    @ResponseBody
    @RequiresPermissions("userDept:update")
    public FebsResponse updateUserDept(UserDept userDept) {
        this.userDeptService.updateUserDept(userDept);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改UserDept", exceptionMessage = "导出Excel失败")
    @PostMapping("userDept/excel")
    @ResponseBody
    @RequiresPermissions("userDept:export")
    public void export(QueryRequest queryRequest, UserDept userDept, HttpServletResponse response) {
        List<UserDept> userDepts = this.userDeptService.findUserDepts(queryRequest, userDept).getRecords();
        ExcelKit.$Export(UserDept.class, response).downXlsx(userDepts, false);
    }
}
