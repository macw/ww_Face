package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 课程学生表
 * </p>
 *
 * @author Macw
 * @since 2020-03-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_course_student")
public class CourseStudent implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 课程选择学生id
     */
    @TableId(value = "course_student_id", type = IdType.AUTO)
    private Long courseStudentId;

    /**
     * 课程id
     */
    private Long courseId;

    /**
     * 课程名称
     */
    private String courseName;

    /**
     * 学生id
     */
    private Long studentId;

    /**
     * 学生名称
     */
    private String studentName;

    /**
     * 已做实验报告内容
     */
    private String exampleContent;

    /**
     * 实验报告学生已做[0:未做, 1:已做]
     */
    private Integer exampleFlag;

    /**
     * 实验报告是否批改[0:未批改, 1:已批改]
     */
    private Integer exampleChecked;

    /**
     * 实验报告评分
     */
    private Integer exampleScore;

    /**
     * 实验报告评语
     */
    private String exampleComment;

    /**
     * 学生实验实验报告
     */
    private String experimentReport;

    /**
     * 实验报告学生已做[0:未做, 1:已做]
     */
    private Integer experimentFlag;

    /**
     * 实验报告是否批改[0:未批改, 1:已批改]
     */
    private Integer experimentChecked;

    /**
     * 实验报告评分
     */
    private Integer experimentScore;

    /**
     * 实验报告评语
     */
    private String experimentComment;

    /**
     * 开始答题时间
     */
    private LocalDateTime beginTime;

    /**
     * 结束答题时间
     */
    private LocalDateTime endTime;

    /**
     * 作业状态[0:停用, 1:未开始 20:执行中 30:已完成]
     */
    private Integer exampleState;

    /**
     * 实验报告状态[0:停用, 1:未开始 20:执行中 30:已完成]
     */
    private Integer experimentState;

    /**
     * 操作人
     */
    private String operator;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;


}
