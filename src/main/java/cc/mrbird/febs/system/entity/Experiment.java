package cc.mrbird.febs.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 实验表
 * </p>
 *
 * @author Macw
 * @since 2020-03-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_experiment")
public class Experiment implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 实验id
     */
    @TableId(value = "experiment_id", type = IdType.AUTO)
    private Long experimentId;

    /**
     * 实验编号
     */
    private String experimentCode;

    /**
     * 实验名称
     */
    private String experimentName;

    /**
     * 计划学时
     */
    private Integer studyTime;

    /**
     * 实验教程
     */
    private String experimentBook;

    /**
     * 考核方式
     */
    private String checkType;

    /**
     * 实验设备及数量
     */
    private String experimentCount;

    /**
     * 实验描述
     */
    private String remark;

    /**
     * 目的与要求
     */
    private String requireContent;

    /**
     * 实验附件名称
     */
    private String fileName;

    /**
     * 实验附件URL
     */
    private String fileUrl;

    /**
     * 实验附件大小
     */
    private Integer fileSize;

    /**
     * 删除状态[0:停用, 1:启用 ]
     */
    private Integer state;

    /**
     * 操作人
     */
    private String operator;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;


}
