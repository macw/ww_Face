package cc.mrbird.febs.system.entity;


import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 *  Entity
 *
 * @author Macw
 * @date 2020-03-23 16:04:49
 */
@Data
@TableName("t_user_dept")
public class UserDept {

    /**
     * 用户班级表
     */
    @TableId(value = "user_dept_id", type = IdType.AUTO)
    private Integer userDeptId;

    /**
     * 用户ID
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 部门ID
     */
    @TableField("dept_id")
    private Integer deptId;

}
