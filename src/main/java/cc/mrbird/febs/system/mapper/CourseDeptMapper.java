package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.CourseDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程班级表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-03-26
 */
public interface CourseDeptMapper extends BaseMapper<CourseDept> {

}
