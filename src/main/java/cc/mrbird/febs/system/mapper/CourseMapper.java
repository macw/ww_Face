package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.Course;
import cc.mrbird.febs.system.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 课程表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-03-24
 */
public interface CourseMapper extends BaseMapper<Course> {

}
