package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.Experiment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 实验表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-03-24
 */
public interface ExperimentMapper extends BaseMapper<Experiment> {

}
