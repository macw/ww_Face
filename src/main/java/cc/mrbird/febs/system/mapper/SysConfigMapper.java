package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.SysConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 参数配置表 Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-04-15
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
