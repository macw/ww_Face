package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.CourseDept;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程班级表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-03-26
 */
public interface ICourseDeptService extends IService<CourseDept> {

}
