package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.ExperimentProject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 实验项目表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-03-25
 */
public interface IExperimentProjectService extends IService<ExperimentProject> {

}
