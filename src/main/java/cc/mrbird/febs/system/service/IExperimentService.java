package cc.mrbird.febs.system.service;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.Experiment;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 实验表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-03-24
 */
public interface IExperimentService extends IService<Experiment> {

    /**
     * 查询所有的实验列表
     * @param experiment
     * @param request
     * @return
     */
    IPage<Map<String, Object>> findList(Experiment experiment, QueryRequest request);

}
