package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.SysConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 参数配置表 服务类
 * </p>
 *
 * @author Macw
 * @since 2020-04-15
 */
public interface ISysConfigService extends IService<SysConfig> {

}
