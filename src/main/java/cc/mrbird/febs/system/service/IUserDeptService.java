package cc.mrbird.febs.system.service;

import cc.mrbird.febs.system.entity.UserDept;

import cc.mrbird.febs.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *  Service接口
 *
 * @author Macw
 * @date 2020-03-23 16:04:49
 */
public interface IUserDeptService extends IService<UserDept> {
    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param userDept userDept
     * @return IPage<UserDept>
     */
    IPage<UserDept> findUserDepts(QueryRequest request, UserDept userDept);

    /**
     * 查询（所有）
     *
     * @param userDept userDept
     * @return List<UserDept>
     */
    List<UserDept> findUserDepts(UserDept userDept);

    /**
     * 新增
     *
     * @param userDept userDept
     */
    void createUserDept(UserDept userDept);

    /**
     * 修改
     *
     * @param userDept userDept
     */
    void updateUserDept(UserDept userDept);

    /**
     * 删除
     *
     * @param userDept userDept
     */
    void deleteUserDept(UserDept userDept);
}
