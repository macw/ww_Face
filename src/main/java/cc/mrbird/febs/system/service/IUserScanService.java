package cc.mrbird.febs.system.service;

import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.system.entity.User;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 扫脸库用户信息表 服务类
 * </p>
 * @author Macw
 * @since 2019-12-18
 */
public interface IUserScanService {

    /**
     * 人脸注册
     * @param file 文件
     * @return String
     * @author Macw
     * @since 2019-12-18
     */
    String resisterFace(MultipartFile file, User user);

    /**
     * 人脸对比
     * @param file 文件
     * @param userId 用户ID
     * @return String
     * @author Macw
     * @since 2019-12-18
     */
    FebsResponse compareFace(MultipartFile file, String userId);
}
