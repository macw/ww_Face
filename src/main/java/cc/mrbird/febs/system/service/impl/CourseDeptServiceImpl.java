package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.system.entity.CourseDept;
import cc.mrbird.febs.system.mapper.CourseDeptMapper;
import cc.mrbird.febs.system.service.ICourseDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程班级表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-03-26
 */
@Service
public class CourseDeptServiceImpl extends ServiceImpl<CourseDeptMapper, CourseDept> implements ICourseDeptService {

}
