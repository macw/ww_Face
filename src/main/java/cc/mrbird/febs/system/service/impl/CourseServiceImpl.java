package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.common.entity.FebsConstant;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.system.entity.Course;
import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.UserDept;
import cc.mrbird.febs.system.mapper.CourseMapper;
import cc.mrbird.febs.system.service.ICourseService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-03-24
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements ICourseService {

    @Override
    public IPage<Map<String, Object>> findUserDetailList(Course course, QueryRequest request) {
        log.debug("--------------------"+course);
        Page<Course> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "course_id", FebsConstant.ORDER_ASC, false);
        IPage<Map<String, Object>> courseIPage = this.baseMapper.selectMapsPage(page, new QueryWrapper<Course>().lambda()
            .like(StringUtils.isNotBlank(course.getCourseName()),Course::getCourseName,course.getCourseName())
                .eq(course.getState()!=null,Course::getState,course.getState())
                .like(StringUtils.isNotBlank(course.getDeptNameList()),Course::getDeptNameList,course.getDeptNameList())
        );

        return courseIPage;
    }
}
