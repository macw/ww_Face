package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.mapper.ExperimentProjectMapper;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 实验项目表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-03-25
 */
@Service
public class ExperimentProjectServiceImpl extends ServiceImpl<ExperimentProjectMapper, ExperimentProject> implements IExperimentProjectService {

}
