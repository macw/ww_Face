package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.common.entity.FebsConstant;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.system.entity.Course;
import cc.mrbird.febs.system.entity.Experiment;
import cc.mrbird.febs.system.mapper.ExperimentMapper;
import cc.mrbird.febs.system.service.IExperimentService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 实验表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-03-24
 */
@Service
public class ExperimentServiceImpl extends ServiceImpl<ExperimentMapper, Experiment> implements IExperimentService {

    @Override
    public IPage<Map<String, Object>> findList(Experiment experiment, QueryRequest request) {
        Page<Experiment> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "experiment_id", FebsConstant.ORDER_ASC, false);
        IPage<Map<String, Object>> iPage = this.baseMapper.selectMapsPage(page, new QueryWrapper<Experiment>().lambda()
                .eq(StringUtils.isNotBlank(experiment.getExperimentName()),Experiment::getExperimentName,experiment.getExperimentName()));
        return iPage;
    }
}
