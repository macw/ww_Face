package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.system.entity.SysConfig;
import cc.mrbird.febs.system.mapper.SysConfigMapper;
import cc.mrbird.febs.system.service.ISysConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 参数配置表 服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-04-15
 */
@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements ISysConfigService {

}
