package cc.mrbird.febs.system.service.impl;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.system.entity.UserDept;
import cc.mrbird.febs.system.mapper.UserDeptMapper;
import cc.mrbird.febs.system.service.IUserDeptService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 *  Service实现
 *
 * @author Macw
 * @date 2020-03-23 16:04:49
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class UserDeptServiceImpl extends ServiceImpl<UserDeptMapper, UserDept> implements IUserDeptService {


    @Override
    public IPage<UserDept> findUserDepts(QueryRequest request, UserDept userDept) {
        LambdaQueryWrapper<UserDept> queryWrapper = new LambdaQueryWrapper<>();
        // TODO 设置查询条件
        Page<UserDept> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.page(page, queryWrapper);
    }

    @Override
    public List<UserDept> findUserDepts(UserDept userDept) {
	    LambdaQueryWrapper<UserDept> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
		return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional
    public void createUserDept(UserDept userDept) {
        this.save(userDept);
    }

    @Override
    @Transactional
    public void updateUserDept(UserDept userDept) {
        this.saveOrUpdate(userDept);
    }

    @Override
    @Transactional
    public void deleteUserDept(UserDept userDept) {
        LambdaQueryWrapper<UserDept> wrapper = new LambdaQueryWrapper<>();
	    // TODO 设置删除条件
	    this.remove(wrapper);
	}
}
