package cc.mrbird.febs.teacher.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsConstant;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.service.IUserCommitService;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IUserService;
import cc.mrbird.febs.teacher.entity.TableCols;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController("teacherCommit")
@RequestMapping("/teacherCommit")
public class TeacherCommitController extends BaseController {

    @Resource
    private IUserCommitService userCommitService;

    @Resource
    private ITeacherProjectService teacherProjectService;

    @Resource
    private IExperimentProjectService projectService;

    @Resource
    private IUserService userService;

    @GetMapping("/commitList")
    @RequiresPermissions("teacher:commit")
    @ControllerEndpoint(operation = "查看提交情况", exceptionMessage = "学生提交情况查询失败")
    public FebsResponse commitList(Integer deptId, UserCommit userCommit, QueryRequest request) {
        User currentUser = getCurrentUser();
        Page<UserCommit> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "commit_id", FebsConstant.ORDER_ASC, false);
        IPage<Map<String, Object>> mapIPage = this.userCommitService.pageMaps(page, new QueryWrapper<UserCommit>().lambda()
                .eq(IntegerUtils.isNotBlank(deptId), UserCommit::getDeptId, deptId)
        );
        List<Map<String, Object>> mapList = mapIPage.getRecords();
        for (Map<String, Object> map : mapList) {
            map.put("idNumber", "211111");
            Collection<ExperimentProject> experimentProjects = getProjectsID(deptId, currentUser);
            for (ExperimentProject experimentProject : experimentProjects) {
                UserCommit commit = userCommitService.getOne(new QueryWrapper<UserCommit>().lambda().eq(UserCommit::getProjectId, experimentProject.getProjectId()));
                String status = "<div style=\"color: red;font-style:oblique\">未提交</div>";
                if (commit != null) {
                    if(IntegerUtils.isNotBlank(commit.getIsCommit())){
                        status = "<div style=\"color: orange;\">未批阅</div>";
                    }
                    if (StringUtils.isNotBlank(commit.getScore().toString())) {
                        status = commit.getScore().toString();
                    }
                }

                User stu = userService.getById((Serializable) map.get("stu_id"));

                map.put(experimentProject.getProjectId().toString(), status);
                map.put("idNumber", stu.getIdNumber());
                map.put("name", stu.getUsername());
            }
        }
        Map<String, Object> dataTable = getDataTable(mapIPage);
        return new FebsResponse().success().data(dataTable);
    }


    /**
     * 创建所有的表头
     *
     * @param deptId
     * @return
     */
    @RequestMapping("/getCols")
    public FebsResponse getCols(Integer deptId) {
        User currentUser = getCurrentUser();
        Collection<ExperimentProject> experimentProjects = getProjectsID(deptId, currentUser);
        List<TableCols> colsList = new ArrayList<>();
        colsList.add(new TableCols("idNumber", "学号", "center", true));
        colsList.add(new TableCols("name", "姓名", "center", false));
        for (ExperimentProject experimentProject : experimentProjects) {
            TableCols tableCols = new TableCols();
            tableCols.setTitle(experimentProject.getProjectName());
            tableCols.setField(experimentProject.getProjectId().toString());
            tableCols.setAlign("center");
            tableCols.setSort(false);
            colsList.add(tableCols);
        }

        return new FebsResponse().success().data(colsList);

    }

    /**
     * 根据部门ID和当前登录老师，查询已开通的所有项目列表
     *
     * @param deptId
     * @param user
     * @return
     */
    private Collection<ExperimentProject> getProjectsID(Integer deptId, User user) {
        List<TeacherProject> teacherProjects = teacherProjectService.list(new QueryWrapper<TeacherProject>().lambda()
                .eq(TeacherProject::getDeptId, deptId)
                .eq(TeacherProject::getTeacherId, user.getUserId()));
        List<Integer> collect = teacherProjects.stream().map(TeacherProject::getProjectId).collect(Collectors.toList());
        Collection<ExperimentProject> experimentProjects = projectService.listByIds(collect);
        return experimentProjects;
    }

}
