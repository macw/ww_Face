package cc.mrbird.febs.teacher.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsConstant;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.entity.ExperimentProject;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.constraints.NotBlank;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Macw
 * @since 2020-04-02
 */
@RestController
@RequestMapping("/teacherProject")
public class TeacherProjectController extends BaseController {

    @Autowired
    private ITeacherProjectService teacherProjectService;

    @Resource
    private IExperimentProjectService projectService;

    @Autowired
    private IDeptService deptService;

    @Autowired
    private IExperimentProjectService experimentProjectService;

    @GetMapping("/getAllTeacherProject")
    public FebsResponse getAllTeacherProjects(TeacherProject teacherProject) {
        User currentUser = getCurrentUser();

        List<ExperimentProject> projectList = projectService.list(new QueryWrapper<ExperimentProject>().lambda().eq(ExperimentProject::getCourseId,currentUser.getCourseId()));
        System.out.println("----------------projectList"+projectList);

        /*for (TeacherProject project : projectList) {
            project.setProjectName(projectService.getById(project.getProjectId()).getProjectName());
            System.out.println("projectName----------"+project.getProjectName());
        }*/
        return new FebsResponse().success().data(projectList);
    }

    @RequestMapping("/list")
    @RequiresPermissions("teacherProject:view")
    public FebsResponse getTeacherProjectList(QueryRequest request,TeacherProject teacherProject){
        User currentUser = getCurrentUser();
        //获取当前登录用户信息
        Page<TeacherProject> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "project_id", FebsConstant.ORDER_ASC, false);
        IPage<TeacherProject> iPage = this.teacherProjectService.page(page,new QueryWrapper<TeacherProject>().lambda()
                .like(StringUtils.isNotBlank(teacherProject.getClassName()),TeacherProject::getClassName,teacherProject.getClassName())
                .like(StringUtils.isNotBlank(teacherProject.getProjectName()),TeacherProject::getProjectName,teacherProject.getProjectName())
                .like(TeacherProject::getTeacherId,currentUser.getUserId())
                .eq(IntegerUtils.isNotBlank(teacherProject.getDeptId()),TeacherProject::getDeptId,teacherProject.getDeptId())
        );
        List<TeacherProject> records = iPage.getRecords();

        for (TeacherProject r:records ) {
            Dept dept = deptService.getById(r.getDeptId());
            r.setClassName(dept.getDeptName());
            ExperimentProject eProject = experimentProjectService.getById(r.getProjectId());
            r.setProjectName(eProject.getProjectName());
        }
        iPage.setRecords(records);
        Map<String, Object> dataTable = getDataTable(iPage);
        return new FebsResponse().success().data(dataTable);
    }

    @RequestMapping("/add")
    @RequiresPermissions("teacherProject:add")
    @ControllerEndpoint(operation = "新增实验项目", exceptionMessage = "新增实验项目失败")
    public FebsResponse teacherProjectAdd(String deptId,String projectId){
        String projectIds [] = projectId.split(",");
        User currentUser = getCurrentUser();
        for (String id : projectIds) {
            TeacherProject teacherProject = new TeacherProject();
            teacherProject.setTeacherId(currentUser.getUserId());
            teacherProject.setProjectId(Integer.parseInt(id));
            teacherProject.setDeptId(Integer.parseInt(deptId));
            teacherProject.setCourseId(projectService.getById(Integer.parseInt(id)).getCourseId().intValue());
            teacherProjectService.save(teacherProject);
        }
        System.out.println(currentUser);
        return new FebsResponse().success();
    }

    @RequestMapping("/delete/{teacherProjectId}")
    @RequiresPermissions("teacherProject:delete")
    @ControllerEndpoint(operation = "删除实验项目", exceptionMessage = "删除实验项目失败")
    public FebsResponse deleteTeacherProject(@NotBlank(message = "{required}") @PathVariable String teacherProjectId){
        System.out.println("teacherProjectId--------------------"+teacherProjectId);
        String[] ids = teacherProjectId.split(StringPool.COMMA);

        List<String> list = Arrays.asList(ids);
        teacherProjectService.removeByIds(list);
        return new FebsResponse().success();
    }




    



}
