package cc.mrbird.febs.teacher.controller;


import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsConstant;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.common.utils.DateUtil;
import cc.mrbird.febs.common.utils.IntegerUtils;
import cc.mrbird.febs.common.utils.SortUtil;
import cc.mrbird.febs.student.entity.UserCommit;
import cc.mrbird.febs.student.service.IUserCommitService;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.service.ICourseService;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IExperimentProjectService;
import cc.mrbird.febs.system.service.IUserService;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController("teacherScore")
@RequestMapping("/teacherScore")
public class TeacherScoreController extends BaseController {

    @Resource
    private IUserCommitService userCommitService;

    @Resource
    private ITeacherProjectService teacherProjectService;

    @Resource
    private IExperimentProjectService projectService;

    @Resource
    private IUserService userService;

    @Resource
    private ICourseService courseService;

    @Resource
    private IDeptService deptService;

    @GetMapping("/scoreList")
    @RequiresPermissions("teacher:score")
    @ControllerEndpoint(operation = "批阅报告", exceptionMessage = "批阅报告失败")
    public FebsResponse scoreList(UserCommit userCommit, QueryRequest request) {
        User currentUser = getCurrentUser();
        Page<UserCommit> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "commit_id", FebsConstant.ORDER_ASC, false);
        IPage<UserCommit> iPage = this.userCommitService.page(page, new QueryWrapper<UserCommit>().lambda()
                .eq(UserCommit::getTeacherId,currentUser.getUserId())
                .eq(IntegerUtils.isNotBlank(userCommit.getDeptId()), UserCommit::getDeptId, userCommit.getDeptId())
        );
        List<UserCommit> userCommitList = iPage.getRecords();
        for (UserCommit commit : userCommitList) {
            commit.setCourseName(courseService.getById(commit.getCourseId()).getCourseName());
            commit.setDeptName(deptService.getById(commit.getDeptId()).getDeptName());
            commit.setProjectName(projectService.getById(commit.getProjectId()).getProjectName());
            commit.setStuName(userService.getById(commit.getStuId()).getUsername());
            commit.setIdNumber(userService.getById(commit.getStuId()).getIdNumber());
            commit.setTeacherName(currentUser.getUsername());
            commit.setCreateTimes(DateUtil.formatFullTime(commit.getCreateTime(),DateUtil.FULL_TIME_SPLIT_PATTERN));
        }
        Map<String, Object> dataTable = getDataTable(iPage);
        return new FebsResponse().success().data(dataTable);
    }

    @RequestMapping("/updateScore")
    public FebsResponse update(UserCommit userCommit){
        boolean b = userCommitService.updateById(userCommit);
        if (b){
            return new FebsResponse().success();
        }else {
            return new FebsResponse().fail();
        }
    }



}
