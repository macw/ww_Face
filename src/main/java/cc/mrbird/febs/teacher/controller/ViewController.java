package cc.mrbird.febs.teacher.controller;

import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsConstant;
import cc.mrbird.febs.common.utils.FebsUtil;
import cc.mrbird.febs.system.entity.Dept;
import cc.mrbird.febs.system.entity.User;
import cc.mrbird.febs.system.entity.UserDept;
import cc.mrbird.febs.system.service.IDeptService;
import cc.mrbird.febs.system.service.IUserDeptService;
import cc.mrbird.febs.teacher.entity.TableCols;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping(FebsConstant.VIEW_PREFIX)
@Controller("teacherView")
public class ViewController extends BaseController {

    @Resource
    private IUserDeptService userDeptService;

    @Resource
    private IDeptService deptService;

    /**
     * Teacher，老师管理
     */
    @GetMapping( "system/teacher/dept")
    @RequiresPermissions("teacherDept:view")
    public String systemTeacherDept() {
        return FebsUtil.view("teacher/dept/dept");
    }

    @GetMapping("dept/select/student")
    @RequiresPermissions("deptStudent:view")
    public String selectStudentDept(){
        return FebsUtil.view("teacher/dept/deptStudent");
    }

    /**
     *GuiYongkang
     * 2020/4/02 10:36am
     * 控制老师账号的查询课程项目
     */
    @GetMapping("teacher/select/project")
    @RequiresPermissions("teacherProject:view")
    public String selectTeacherProject(Model model){
        Collection<Dept> depts = getDepts();
        model.addAttribute("depts",depts);
        return FebsUtil.view("teacher/teacherProject/teacherProject");
    }

    /**
     * GuiYongkang
     * 2020/4/02 2:35pm
     * 控制老师账号课程项目的增加页面跳转
     */
    @GetMapping("system/teacher/teacherProject/add")
    @RequiresPermissions("experimentProject:add")
    public String systemTeacherProjectAdd(Model model) {
        Collection<Dept> depts = getDepts();
        model.addAttribute("depts",depts);
        return FebsUtil.view("teacher/teacherProject/teacherProjectAdd");
    }

    @GetMapping("teacher/getCommit")
    @RequiresPermissions("teacher:commit")
    public String getCommit(Model model){
        Collection<Dept> depts = getDepts();
        model.addAttribute("depts",depts);
        return FebsUtil.view("teacher/commit/commit");
    }



    @GetMapping("teacher/getScore")
    @RequiresPermissions("teacher:score")
    public String getScore(Model model){
        Collection<Dept> depts = getDepts();
        model.addAttribute("depts",depts);
        return FebsUtil.view("teacher/score/score");
    }


    /**
     * 查询当前登录老师所拥有的班级列表
     * @return
     */
    private  Collection<Dept> getDepts(){
        User currentUser = getCurrentUser();
        List<UserDept> userDeptList = userDeptService.list(new QueryWrapper<UserDept>().lambda().eq(UserDept::getUserId, currentUser.getUserId()));
        List<Integer> collect = userDeptList.stream().map(UserDept::getDeptId).collect(Collectors.toList());
        Collection<Dept> depts = deptService.listByIds(collect);
        return depts;
    }


}
