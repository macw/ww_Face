package cc.mrbird.febs.teacher.entity;

import lombok.Data;

@Data
public class TableCols {

    private String field;
    private String title;
    private String align;
    private Boolean sort;

    public TableCols() {
    }

    public TableCols(String field, String title, String align, Boolean sort) {
        this.field = field;
        this.title = title;
        this.align = align;
        this.sort = sort;
    }
}
