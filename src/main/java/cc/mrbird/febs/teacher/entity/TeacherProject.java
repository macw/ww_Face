package cc.mrbird.febs.teacher.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Macw
 * @since 2020-04-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_teacher_project")
public class TeacherProject implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 老师和项目表主键ID
     */
    @TableId(value = "teacher_project_id", type = IdType.AUTO)
    private Integer teacherProjectId;

    /**
     * 老师用户表主键ID
     */
    private Long teacherId;

    /**
     * 项目主键ID
     */
    private Integer projectId;

    /**
     * 项目所属课程id
     */
    private Integer courseId;

    /**
     * 项目名称
     */
    @TableField(exist = false)
    private String projectName;

    /**
     * 班级名称
     */
    @TableField(exist = false)
    private String className;


    /**
     * 班级表ID
     */
    private Integer deptId;
}
