package cc.mrbird.febs.teacher.mapper;

import cc.mrbird.febs.teacher.entity.TeacherProject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Macw
 * @since 2020-04-08
 */
public interface TeacherProjectMapper extends BaseMapper<TeacherProject> {

}
