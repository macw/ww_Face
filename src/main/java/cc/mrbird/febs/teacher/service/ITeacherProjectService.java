package cc.mrbird.febs.teacher.service;

import cc.mrbird.febs.teacher.entity.TeacherProject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Macw
 * @since 2020-04-08
 */
public interface ITeacherProjectService extends IService<TeacherProject> {

}
