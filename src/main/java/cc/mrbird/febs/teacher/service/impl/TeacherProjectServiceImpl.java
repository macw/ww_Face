package cc.mrbird.febs.teacher.service.impl;

import cc.mrbird.febs.teacher.entity.TeacherProject;
import cc.mrbird.febs.teacher.mapper.TeacherProjectMapper;
import cc.mrbird.febs.teacher.service.ITeacherProjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Macw
 * @since 2020-04-08
 */
@Service
public class TeacherProjectServiceImpl extends ServiceImpl<TeacherProjectMapper, TeacherProject> implements ITeacherProjectService {


}
